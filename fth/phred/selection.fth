\
\ phred editor
\
\ programmed by Mike Schwartz
\
\ Selection handling
\

variable selection-start# -1 selection-start# !
variable selection-end# -1 selection-end# !

variable selection-buffer  s" SELECTION_BUFFER" Buffer.new selection-buffer !


: Selection.reset  { | -- , free selection }
    selection-buffer @ Buffer.clear
;

: Selection.head ( -- head , return head of selection buffer )
    selection-buffer @ Buffer.keystone
;

: Selection.first-line ( -- lin , return first line in selection )
    selection-buffer @ Buffer.first-line
;

: Selection.empty? ( -- flg , is selection empty? )
    Selection.first-line Selection.head = 
;

: Selection.iterate { 'handler -- , call 'handler for each line in selection-list }
    selection-buffer @ 'handler Buffer.iterate
;

: Selection.append { zstr -- , append zstr to selection }
    zstr selection-buffer @ Buffer.append-text drop
;

: Selection.dump 
    selection-buffer @ Buffer.dump
;

