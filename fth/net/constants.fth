anew task-net/constants.fth

: getconstant ( caddr u -- constant , get unix/linux constant from string name )
    net::getconstant
;

\ DOMAIN
s" AF_UNSPEC" getconstant constant AF_UNSPEC                \ unspecified
s" AF_UNIX" getconstant constant AF_UNIX                  \ local to host (pipes)
s" AF_UNIX" getconstant  constant AF_LOCAL           \ backward compatibility
s" AF_INET" getconstant constant AF_INET                  \ IPv4
\ s" AF_IMPLINK" getconstant constant AF_IMPLINK               \ arpanet imp addresses
\ s" AF_PUP" getconstant constant AF_PUP                   \ pup protocols: e.g. BSP;
\ 0 constant AF_SYSTEM                \ System domain
\ 0 constant AF_NDRV                  \ Raw access to network device
\ 0 constant AF_VSOCK                 \ VM Sockets protocol

\ SOCKET TYPE
s" SOCK_STREAM" getconstant constant SOCK_STREAM
s" SOCK_DGRAM" getconstant constant SOCK_DGRAM
s" SOCK_RAW" getconstant constant SOCK_RAW

\ INTERNET ADDRESS
s" INADDR_ANY"  getconstant  constant INADDR_ANY
s" INADDR_BROADCAST" getconstant constant INADDR_BROADCAST
s" INADDR_LOOPBACK" getconstant constant INADDR_LOOPBACK
s" INADDR_NONE" getconstant constant INADDR_NONE

\ PROTOCOL  (see /etc/protocols)
s" IPPROTO_IP" getconstant constant IPPROTO_IP              \ internet protocol
\ 1 constant IPPROTO_HOPQPT        \ hop-by-hop options for ipv6
s" IPPROTO_ICMP" getconstant constant IPPROTO_ICMP            \ internet control message protocol 
s" IPPROTO_IGMP" getconstant constant IPPROTO_IGMP            \ internet group management protocol
s" IPPROTO_GGP" getconstant constant IPPROTO_GGP             \ gateway-gateway protocol
\ s" IPPROTO_ENCAP" getconstant constant IPPROTO_IPENCAP         \ IP encapsulated in IP (officially "IP")
s" IPPROTO_ST" getconstant constant IPPROTO_ST2             \ ST2 datagram mode (RFC 1819, officially "ST")
s" IPPROTO_TCP" getconstant constant IPPROTO_TCP             \ transmission control protocol
\ s" IPPROTO_CBT" getconstant constant IPPROTO_CBT             \ CBT (Tony Ballardie <A.Ballaardie@cs.ucl.ac.uk>)
s" IPPROTO_EGP" getconstant constant IPPROTO_EGP             \ Exterior Gateway Protocol
s" IPPROTO_PIGP" getconstant constant IPPROTO_PIGP             \ any private interior gateway (Cisco for IGRP)
s" IPPROTO_RCCMON"  getconstant constant PROTOCOL_RCCMON        \ BBN RCC Monitoring
s" IPPROTO_NVPII" getconstant constant IPPROTO_NVPII       \ Network Voice Protocol
s" IPPROTO_PUP" getconstant constant IPPROTO_PUP            \ PARC universal packet protocol
\ ...
s" IPPROTO_IPV6" getconstant constant IPPROTO_IPV6           \ IPV6
\ s" IPPROTO_SDRP" getconstant constant IPPROTO_SDRP           \ Source Demand Routing Protocol
\ s" IPPROTO_IPV6_ROUTE" getconstant constant IPPROTO_IPV6_ROUTE     \ routing header for IPV6
\ s" IPPROTO_IPV6_FRAG" getconstant constant IPPROTO_IPV6_FRAG      \ fragment header for IPV6
\ ...
\ not sure if any of the missing are actually needed

\ SOCKET OPTIONS
$ 0001 constant SO_DEBUG                \ enables recording of debugging information
$ 0002 constant SO_ACCEPTCONN           \ socket has listen()
$ 0004 constant SO_REUSEADDR            \ enables local address reuse
$ 0008 constant SO_REUSEPORT            \ enables duplicate address and port bindings
$ 0010 constant SO_KEEPALIVE            \ enables keep connections alive
$ 0020 constant SO_DONTROUTE            \ enables routing bypass for outgoing messages
$ 0040 constant SO_LINGER               \ linger on close if data present
$ 0080 constant SO_BROADCAST            \ enables permission to transmit broadcast messages
$ 0080 constant SO_OOBINLINE            \ enables reception of out-of-band data in band
$ 1080 constant SO_SNDBUF               \ set buffer size for output
$ 0100 constant SO_RCVBUF               \ set buffer size for input
$ 0000 constant SO_SNDLOWAT             \ set minimum count for output
$ 0000 constant SO_RCVLOWAT             \ set minimum count for input
$ 0000 constant SO_SNDTIMEO             \ set timeout value for output
$ 0000 constant SO_RCVTIMEO             \ set timeout value for input
$ 0000 constant SO_TYPE                 \ get the type of the socket (get only)
$ 0000 constant SO_ERROR                \ get and clear error on the socket (get only)
$ 0000 constant SO_NOSIGPIPE            \ do not generate SIGPIPE, instead return EPIPE
$ 0000 constant SO_NREAD                \ number of bytes to be read (get only)
$ 0000 constant SO_NWRITE               \ number of bytes written not yet sent by the protocol (get only)
$ 0000 constant SO_LINGER_SEC           \ linger on close if data present with timeout in seconds

s" SOL_SOCKET" getconstant constant SOL_SOCKET

\ IOCTL ( these call a C routine to lookup the string and get the value so it should be portable )

s" SIOCGHIWAT" getconstant constant SIOCGHIWAT 
s" SIOCSLOWWAT" getconstant constant SIOCSLOWAT 
s" SIOCGLOWWAT" getconstant constant SIOCGLOWAT 
s" SIOCATMARK" getconstant constant SIOCATMARK 
s" SIOCSPGRP" getconstant constant SIOCSPGRP 
s" SIOCGPGRP" getconstant constant SIOCGPGRP 
s" SIOCSIFADDR" getconstant constant SIOCSIFADDR 
s" SIOCSIFDSTADDR" getconstant constant SIOCSIFDSTADDR 
s" SIOCSIFFLAGS" getconstant constant SIOCSIFFLAGS 
s" SIOCGIFFLAGS" getconstant constant SIOCGIFFLAGS 
s" SIOCSIFBRDADDR" getconstant constant SIOCSIFBRDADDR 
s" SIOCSIFNETMASK" getconstant constant SIOCSIFNETMASK 
s" SIOCGIFMETRIC" getconstant constant SIOCGIFMETRIC 
s" SIOCSIFMETRIC" getconstant constant SIOCSIFMETRIC 
s" SIOCDIFADDR" getconstant constant SIOCDIFADDR 
s" SIOCAIFADDR" getconstant constant SIOCAIFADDR 
s" SIOCGIFADDR" getconstant constant SIOCGIFADDR 
s" SIOCGIFDSTADDR" getconstant constant SIOCGIFDSTADDR 
s" SIOCGIFBRDADDR" getconstant constant SIOCGIFBRDADDR 
s" SIOCGIFCONF" getconstant constant SIOCGIFCONF 
s" SIOCGIFNETMASK" getconstant constant SIOCGIFNETMASK 
s" SIOCAUTOADDR" getconstant constant SIOCAUTOADDR 
s" SIOCAUTONETMASK" getconstant constant SIOCAUTONETMASK 
s" SIOCARPIPLL" getconstant constant SIOCARPIPLL 

s" SIOCADDMULTI" getconstant constant SIOCADDMULTI 
s" SIOCDELMULTI" getconstant constant SIOCDELMULTI 
s" SIOCGIFMTU" getconstant constant SIOCGIFMTU 
s" SIOCSIFMTU" getconstant constant SIOCSIFMTU 
s" SIOCGIFPHYS" getconstant constant SIOCGIFPHYS 
s" SIOCSIFPHYS" getconstant constant SIOCSIFPHYS 
s" SIOCSIFMEDIA" getconstant constant SIOCSIFMEDIA 

s" SIOCGIFMEDIA" getconstant constant SIOCGIFMEDIA 

s" SIOCSIFGENERIC" getconstant constant SIOCSIFGENERIC 
s" SIOCRSLVMULTI" getconstant constant SIOCRSLVMULTI
s" SIOCRSLVMULTI" getconstant constant SIOCRGLVMULTI

s" SIOCSIFLLADDR" getconstant constant SIOCSIFLLADDR 
s" SIOCGIFSTATUS" getconstant constant SIOCGIFSTATUS 
s" SIOCSIFPHYADDR" getconstant constant SIOCSIFPHYADDR 
s" SIOCGIFPSRCADDR" getconstant constant SIOCGIFPSRCADDR 
s" SIOCGIFPDSTADDR" getconstant constant SIOCGIFPDSTADDR 
s" SIOCDIFPHYADDR" getconstant constant SIOCDIFPHYADDR 

s" SIOCGIFDEVMTU" getconstant constant SIOCGIFDEVMTU 
s" SIOCSIFALTMTU" getconstant constant SIOCSIFALTMTU 
s" SIOCGIFALTMTU" getconstant constant SIOCGIFALTMTU 
s" SIOCSIFBOND" getconstant constant SIOCSIFBOND 
s" SIOCGIFBOND" getconstant constant SIOCGIFBOND 

s" SIOCGIFXMEDIA" getconstant constant SIOCGIFXMEDIA 

s" SIOCSIFCAP" getconstant constant SIOCSIFCAP 
s" SIOCGIFCAP" getconstant constant SIOCGIFCAP 

s" SIOCSIFMANAGEMENT" getconstant constant SIOCSIFMANAGEMENT 

s" SIOCIFCREATE" getconstant constant SIOCIFCREATE 
s" SIOCIFDESTROY" getconstant constant SIOCIFDESTROY 
s" SIOCIFCREATE2" getconstant constant SIOCIFCREATE2 

s" SIOCSDRVSPEC" getconstant constant SIOCSDRVSPEC 
s" SIOCGDRVSPEC" getconstant constant SIOCGDRVSPEC 
s" SIOCSIFVLAN" getconstant constant SIOCSIFVLAN 
s" SIOCGIFVLAN" getconstant constant SIOCGIFVLAN 
s" SIOCSETVLAN" getconstant constant SIOCSETVLAN 
s" SIOCGETVLAN" getconstant constant SIOCGETVLAN 

s" SIOCIFGCLONERS" getconstant constant SIOCIFGCLONERS 

s" SIOCGIFASYNCMAP" getconstant constant SIOCGIFASYNCMAP 
s" SIOCSIFASYNCMAP" getconstant constant SIOCSIFASYNCMAP 

s" SIOCGIFMAC" getconstant constant SIOCGIFMAC 
s" SIOCSIFMAC" getconstant constant SIOCSIFMAC 
s" SIOCSIFKPI" getconstant constant SIOCSIFKPI 
s" SIOCGIFKPI" getconstant constant SIOCGIFKPI 

s" SIOCGIFWAKEFLAGS" getconstant constant SIOCGIFWAKEFLAGS 

s" SIOCGIFFUNCTIONALTYPE" getconstant constant SIOCGIFFUNCTIONALTYPE 

s" SIOCSIF6LOWPAN" getconstant constant SIOCSIF6LOWPAN 
s" SIOCGIF6LOWPAN" getconstant constant SIOCGIF6LOWPAN 

linux? [if]
require net/linux.fth
[then]
