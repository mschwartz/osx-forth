require lib/vocabulary.fth

: handler { n -- f , handler for vocabulary.iterate }
    cr n $type
    true
;

: demo 
    ['] handler dictionary.iterate
    cr
    c" EXIT" find drop >NAME cr ." >name " count 31 and type
    c" EXIT" find drop N>LINK REL->USE cr 32 dump
cr .s cr
;

demo

