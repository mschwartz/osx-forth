: keymap.leader.bufed  { w -- f , handle leader commands }
    keymap.key case
        ascii d of
            keymap.key case
                ascii e of
                    w Dired.begin
                    w Window.dired-mode
                endof
                ncurses.bell
            endcase
        endof
        ncurses.bell
    endcase
;

: Keymap.bufed { w | n ch l b line# -- , run editor keymap for window }
    Keymap.input# -> n
    begin
        w Window.paint
        w s@ Window.line# -> line#
        w Window.refresh
        Keymap.key -> ch
        Window.end-search

        begin
            ch ncurses.key_mouse = 
        while
            Keymap.mouse if exit then \ because a different window may have been clicked on (activated)
            Keymap.key -> ch
        repeat

        MessageLine.clear
        w Window.address-cursor

        ch $ 0a = if
            \ switch to selected buffer
            w s@ Window.current-line  -> l
            l s@ Line.data -> b
            w b Window.set-buffer
            w Window.paint
            exit
        then

        w ch n keymap.common if
            exit
        then

        ch case
            ansi_escape of \ escape key
                ncurses.beep
                drop
                exit
            endof
            ncurses.key_enter of
                \ switch to selected buffer
                w s@ Window.current-line  -> l
                l s@ Line.data -> b
                w b Window.set-buffer
                exit
            endof
            ascii o of
                \ switch to selected buffer
                w s@ Window.current-line  -> l
                l s@ Line.data -> b
                w b Window.set-buffer
                w Window.paint
                exit
            endof
            ascii d of
                \ delete buffer
                w s@ Window.current-line  -> l
                l s@ Line.data -> b
                b API.delete-buffer if
                    w Bufed.begin
                then
            endof
            ascii S of
                w s@ Window.current-line  -> l
                BufferList.saveall
                w Bufed.begin
            endof
            ascii s of
                w s@ Window.current-line  -> l
                l s@ Line.data -> b
                b Buffer.save if
                    messageline{
                        ." >>> Saved "
                        ascii " emit
                        b s@ Buffer.name zcount type
                        ascii " emit
                        ascii . emit
                    }messageline
                    w Bufed.begin
                then
            endof
            ascii , of
                w Keymap.leader.bufed
                exit
            endof
            \ default
            MessageLine.move-to w hex . decimal
        endcase
        w line# Window.goto-line
        w Window.paint
        w Window.refresh
    again
;
