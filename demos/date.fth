#!/usr/local/bin/pforth_standalone -q
\
\ Date class demo
\

require lib/date.fth

: demo { | d -- , date demo }
    Date.new -> d
    d Date.dump
    cr
    cr ."    mm/dd/yy " d Date.mm/dd/yy type
    cr ."    hh:mm:ss " d Date.hh:mm:ss type
    cr ." hh:mm:ss.ms " d Date.hh:mm:ss.ms type
    cr ."    Date.now " Date.now .
    cr

    cr ." stack on exit " .s
;

demo
cr
