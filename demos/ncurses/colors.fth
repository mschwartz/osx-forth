require lib/ncurses/ncurses.fth
require lib/ncurses/ncurses-colors.fth

: demo1 { | win -- , colors demo }
    8 0 do
        8 0 do
            i . j . ." = " i j color-num  .hex cr
        loop
        cr
    loop
    0 sys::exit
;

: demo { | win -- , colors demo }
    ncurses.initscr -> win
    ncurses.stdscr  true ncurses.keypad
    ncurses.cbreak
    ncurses.noecho

    ncurses.start_color
    init-color-pairs
\     bye

\     ncurses.COLOR_RED ncurses.COLOR_BLUE cr .s color-num cr .hex bye
    0 35 z" COLOR DEMO" 1+ ncurses.mvaddstr
    2 0  z" low intensity text colors 0-7 FG BG" 1+ ncurses.mvaddstr
    12 0  z" hight intensity text colors 8-15 FG BG" 1+ ncurses.mvaddstr
    hex
    8 0 do  \ j = BG
        8 0 do \ i = FG
            i j set-color
            i 3 + j 10 * z" .test." 1+ ncurses.mvaddstr
            i ncurses.# j ncurses.#
            i j unset-color
        loop 
        16 8  do \ i = FG
            i j set-color
            i 5 + j 10 * z" .test." 1+ ncurses.mvaddstr
            i ncurses.# j ncurses.#
            i j unset-color
        loop
    loop
    ncurses.refresh
    ncurses.getch
    ncurses.endwin
    decimal
    ." colors = " ncurses::colors . cr
    ." color_pairs = " ncurses::COLOR_PAIRS . cr
    ." A_REVERSE = " ncurses::A_REVERSE .hex cr
;
demo
