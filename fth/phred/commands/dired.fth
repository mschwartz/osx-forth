\
\ phred editor
\
\ programmed by Mike Schwartz
\
\ Command Line handling
\   :dired command
\   :de command
\

anew task-phred/commands/dired.fth

: CommandLine.dired { w str tok toku -- flg , return true if executed }
    tok toku s" de" compare 0= if
        w Dired.begin
        w Window.dired-mode
        true exit
    then
    tok toku s" dired" compare 0= if
        w Dired.begin
        w Window.dired-mode
        true exit
    then
    false
;
