\
\ phred editor
\
\ programmed by Mike Schwartz
\
\ Selection keymap
\
private{

: end-selection { w -- , end selecton mode }
    -1 selection-start# !
    -1 selection-end# !
    w Window.visual-mode
;
}private

: Keymap.selection { w | b ch n s1 s2 done -- , run selection keymap for window }
    w s@ Window.buffer -> b
    Keymap.input# -> n
    messageline.clear
    begin
        w s@ Window.line# dup s1 > if 
            selection-end# !
        else
            selection-start# !
        then
        selection-start# @ -> s1 
        selection-end# @ -> s2
        s1 s2 > if
            s2 selection-start# !
            s1 selection-end# !
        then
        w Window.paint
        Keymap.key -> ch
        w Window.refresh
        w Window.address-cursor

        ch ncurses.key_mouse = if
            Keymap.mouse
            if exit then \ because a different window may have been clicked on (activated)
        then

        w ch n keymap.common if
            exit
        then
        ch case
            3 of 
                die{ cr ." control-c " }die 
            endof
            ansi_escape of
                w end-selection exit 
            endof
            ascii V of 
                w end-selection 
                exit 
            endof
            ascii c of
                w API.delete-selection
                -1 selection-start# !
                -1 selection-end# !
                w Window.insert-mode
                exit
            endof
            ascii d of
                \ cut range
                w API.delete-selection
                w end-selection exit
            endof
            ascii x of
                \ cut range
                w API.delete-selection
                w end-selection exit
            endof
            ascii y of 
                \ yank
                w API.yank
                w end-selection exit
            endof
            \ motions
            ncurses.ctrl_d of
                n 0 do w Window.page-down loop
            endof
            key.ctrl_u of
                n 0 do w Window.page-up loop
            endof
            ncurses.key_left of
                n 0 do w Window.cursor-left loop
            endof
            ascii h of
                n 0 do w Window.cursor-left loop
            endof
            ncurses.key_down of
                n 0 do w Window.cursor-down loop
            endof
            ascii j of
                n 0 do w Window.cursor-down loop
            endof
            ncurses.key_up of
                n 0 do w Window.cursor-up loop
            endof
            ascii k of
                n 0 do w Window.cursor-up loop
            endof
            ncurses.key_right of
                n 0 do w Window.cursor-right loop
            endof
            ascii l of
                n 0 do w Window.cursor-right loop
            endof
            ascii 0 of
                w Window.beginning-of-line
            endof
            ascii $ of 
                w Window.end-of-line
            endof
            ascii b of
                n 0 do w Window.word-backward loop
            endof
            ascii w of
                n 0 do w Window.word-forward loop
            endof
            ascii e of
                n 0 do w Window.endword-forward loop
            endof
            ascii / of
                \ search forward
                w ascii / Window.search
            endof
        endcase
    again
;

privatize
