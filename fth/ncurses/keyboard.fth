octal
0177 constant ncurses.key_delete
0400 constant ncurses.key_code_yes 
0401 constant ncurses.key_min
0401 constant ncurses.key_break
0530 constant ncurses.key_sreset    \ soft (partial) reset (unreliable)
0531 constant ncurses.key_reset     \ reset or hard reset (unreliable)
0402 constant ncurses.key_down
0403 constant ncurses.key_up
0404 constant ncurses.key_left
0405 constant ncurses.key_right
0406 constant ncurses.key_home
0407 constant ncurses.key_backspace
0410 constant ncurses.key_f0
: ncurses.key_f ( n -- code )
    ncurses.key_f0 +
;

decimal

1 ncurses.key_f constant ncurses.key_f1
2 ncurses.key_f constant ncurses.key_f2
3 ncurses.key_f constant ncurses.key_f3
4 ncurses.key_f constant ncurses.key_f4
5 ncurses.key_f constant ncurses.key_f5
6 ncurses.key_f constant ncurses.key_f6
7 ncurses.key_f constant ncurses.key_f7
8 ncurses.key_f constant ncurses.key_f8
9 ncurses.key_f constant ncurses.key_f9
10 ncurses.key_f constant ncurses.key_f10
11 ncurses.key_f constant ncurses.key_f11
12 ncurses.key_f constant ncurses.key_f12

octal
0510 constant ncurses.key_dl            \ delete line key
0511 constant ncurses.key_il            \ insert line key
0512 constant ncurses.key_dc            \ delete character key
0513 constant ncurses.key_ic            \ insert character key
0514 constant ncurses.key_eic           \ sent by rmir or smir in insert mode
0515 constant ncurses.key_clear         \ clear screen of erase key
0516 constant ncurses.key_eos           \ clear to end of screen key
0517 constant ncurses.key_eol           \ clear to end of line key
0520 constant ncurses.key_sf            \ scroll forward key
0521 constant ncurses.key_sr            \ scroll backward key
0522 constant ncurses.key_npage         \ next page key
0523 constant ncurses.key_ppage         \ prev page key
0524 constant ncurses.key_stab          \ set tab key
0525 constant ncurses.key_ctab          \ clear tab key
0526 constant ncurses.key_catab         \ clear all tabs key
0527 constant ncurses.key_enter         \ enter key
0532 constant ncurses.key_print         \ print key
0533 constant ncurses.key_ll            \ lower left key
0534 constant ncurses.key_a1            \ upper left of keypad
0535 constant ncurses.key_a3            \ upper right of keypad
0536 constant ncurses.key_b2            \ center of keypad
0537 constant ncurses.key_c1            \ lower left of keypad
0540 constant ncurses.key_c3            \ lower right of keypad
0541 constant ncurses.key_btab          \ back tab key
0542 constant ncurses.key_beg           \ begin key
0543 constant ncurses.key_cancel        \ cancel key
0544 constant ncurses.key_close         \ close key
0545 constant ncurses.key_command       \ command key
0546 constant ncurses.key_copy          \ copy key
0547 constant ncurses.key_create        \ create key
0550 constant ncurses.key_end           \ end key
0551 constant ncurses.key_exit          \ exit key
0552 constant ncurses.key_find          \ find key
0553 constant ncurses.key_help          \ help key
0554 constant ncurses.key_mark          \ mark key
0555 constant ncurses.key_message       \ message ky
0556 constant ncurses.key_move          \ move key
0557 constant ncurses.key_next          \ next key
0560 constant ncurses.key_open          \ open key
0561 constant ncurses.key_options       \ options key
0562 constant ncurses.key_previous      \ previous key
0563 constant ncurses.key_redo          \ redo key
0564 constant ncurses.key_reference     \ reference key
0565 constant ncurses.key_refresh       \ refresh key
0566 constant ncurses.key_replace       \ replace key
0567 constant ncurses.key_restart       \ restart key
0570 constant ncurses.key_resume        \ resume key
0571 constant ncurses.key_save          \ save key
0572 constant ncurses.key_sbeg          \ shifted begin key
0573 constant ncurses.key_scancel       \ shifted cancel key
0574 constant ncurses.key_scommand      \ shifted command key
0575 constant ncurses.key_scopy         \ shifted copy key
0576 constant ncurses.key_screate       \ shifted create key
0577 constant ncurses.key_sdc           \ shifted delete character key
0600 constant ncurses.key_sdl           \ shifted delete line key
0601 constant ncurses.key_select        \ select key
0602 constant ncurses.key_send          \ shifted end key
0603 constant ncurses.key_seol          \ shifted clear to end of line key
0604 constant ncurses.key_sexit         \ shifted exit key
0605 constant ncurses.key_sfind         \ shifted find key
0606 constant ncurses.key_shelp         \ shifted help key
0607 constant ncurses.key_shome         \ shifted home key
0610 constant ncurses.key_sic           \ shifted insert character key
0611 constant ncurses.key_sleft         \ shifted left arrow key
0612 constant ncurses.key_smessage      \ shifted message key
0613 constant ncurses.key_smove         \ shifted move key
0614 constant ncurses.key_snext         \ shifted next key
0615 constant ncurses.key_soptions      \ shifted options key
0616 constant ncurses.key_sprevious     \ shifted previous key
0617 constant ncurses.key_sprint        \ shifted print key
0620 constant ncurses.key_sredo         \ shifted redo key
0621 constant ncurses.key_sreplace      \ shifted replace key
0622 constant ncurses.key_sright        \ shfited right arrow key
0623 constant ncurses.key_sresume       \ shifted resume key
0624 constant ncurses.key_ssave         \ shifted save key
0625 constant ncurses.key_ssuspend      \ shifted suspend key
0626 constant ncurses.key_sundo         \ shifted undo key
0627 constant ncurses.key_suspend       \ suspend key
0630 constant ncurses.key_undo          \ undo key
0631 constant ncurses.key_mouse         \ mouse event has occurred
0632 constant ncurses.key_resize        \ terminal resize event
0633 constant ncurses.key_event         \ we were interrupted by an event

0777 constant ncurses.key_max           \ maximum key value
decimal

1001 constant ncurses.key_focus         \ window has gained focus
1002 constant ncurses.key_blur          \ window has lost focus


\ control keys
$ 01 constant ncurses.ctrl_a
$ 02 constant ncurses.ctrl_b
$ 03 constant ncurses.ctrl_c
$ 04 constant ncurses.ctrl_d
$ 05 constant ncurses.ctrl_e
$ 06 constant ncurses.ctrl_f
$ 07 constant ncurses.ctrl_g
$ 08 constant ncurses.ctrl_h
$ 09 constant ncurses.ctrl_i
$ 0a constant ncurses.ctrl_j
$ 0b constant ncurses.ctrl_k
$ 0c constant ncurses.ctrl_l
$ 0d constant ncurses.ctrl_m
$ 0e constant ncurses.ctrl_n
$ 0f constant ncurses.ctrl_o
$ 10 constant ncurses.ctrl_p
$ 11 constant ncurses.ctrl_q
$ 12 constant ncurses.ctrl_r
$ 13 constant ncurses.ctrl_s
$ 14 constant ncurses.ctrl_t
$ 15 constant ncurses.ctrl_u
$ 16 constant ncurses.ctrl_v
$ 17 constant ncurses.ctrl_w
$ 18 constant ncurses.ctrl_x
$ 19 constant ncurses.ctrl_y
$ 1a constant ncurses.ctrl_z

: ncurses.nl ( -- , newline on )
    ncurses::nl
    drop
;

: ncurses.nonl ( -- , newline off )
    ncurses::nonl
    drop
;

: ncurses.raw ( -- )
    ncurses::raw
    drop
;
: ncurses.echo ( -- )
    ncurses::echo
    drop
;
: ncurses.noecho ( -- )
    ncurses::noecho
    drop
;

: ncurses.cbreak ( -- , turn on cbreak mode )
    ncurses::cbreak
    drop
;

: ncurses.nodelay ( w f -- , set no delay mode )
    ncurses::nodelay
    drop
;

: ncurses.timeout ( msec -- , set timeout )
    ncurses::timeout
;

: ncurses.keypad ( win bf -- , enable keypad mode to get F1, F2, etc. )
    ncurses::keypad
    drop
;

: ncurses.getstr ( str -- , read string from terminal )
    ncurses::getstr
;

: ncurses.getch ( -- c )
    ncurses::getch
;

: ncurses.wgetch ( w -- c )
    ncurses::wgetch
;

: ncurses.ungetch ( ch --  )
    ncurses::ungetch
;
