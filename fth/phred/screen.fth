\
\ phred editor
\
\ programmed by Mike Schwartz
\
\ Screen logic
\   The screen is the full size terminal window.  It contains a display area
\   where one or more windows can be rendered.  The last line is for editor
\   to print messages.  The line above is for editing and entering command lines.
\

:STRUCT Screen
    LONG Screen.rows
    LONG Screen.cols
    LONG Screen.client-rows
;STRUCT

private{
    24 constant NERDTREE-COLS
    create screen-pad 4096 allot
}private

Screen theScreen
variable stdscr

: Screen.init { | w b -- , initialize screen variables }
    true sys::raw
    ncurses.initscr stdscr !
    ncurses.start_color
    1 ncurses.COLOR_CYAN ncurses.COLOR_BLACK ncurses.init_pair
\     ncurses.raw
    ncurses.noecho
    ncurses.cbreak
    ncurses::stdscr true ncurses.keypad
    100 ncurses::set_escdelay 
    true ncurses::focus_mode

    Screen.resize
    ncurses.refresh
\     true sys::raw
\     true sys::raw-io !
    ncurses-install
    stdscr @ true ncurses.keypad
    true ncurses.mouse-on
    Theme.init

    s" **scratch**" Buffer.new -> b
    z"  " 1+ b Buffer.append-text drop
    true b s! Buffer.permanent
    Buffer.filetype.text b s! Buffer.filetype
    
    b BufferList.add
\     s\" \z" drop b Buffer.append-text drop  \ note use of \z for null terminated string
    
    \ : Window.new { typ top left nlines ncols | w -- w }
        0 0 
        theScreen s@ Screen.client-rows 
        theScreen s@ Screen.cols 
    EditorWindow.new  -> w
    w WindowList.add
    w BufferList.first Window.set-buffer

    0 0 theScreen s@ Screen.client-rows NERDTREE-COLS EditorWindow.new nerdtree-window !
    nerdtree-window @ true Nerded.begin
    WindowType.nerdtree nerdtree-window @  s! Window.type

;

: Screen.destroy ( -- , end screen )
    ncurses-uninstall
    ncurses.endwin
;

: (Screen.paint)  { | w -- , paint all windows }
    Window.list -> w
    begin
        w s@ ListNode.next -> w
        w Window.list <>
    while
        w Window.paint
    repeat
    nerdtree-active @ if 
        nerdtree-window @ Window.paint
    then
    ncurses.refresh
;
' (Screen.paint) is Screen.paint

: (Screen.toggle-nerdtree) { | flg w -- , toggle nerdtree open/closed based upon state }
    Window.list -> w
    nerdtree-active @ -> flg
    flg not nerdtree-active !
    begin
        w s@ ListNode.next -> w
        w Window.list <>
    while
        w s@ Window.left flg if NERDTREE-COLS - else NERDTREE-COLS + then w s! Window.left
        w s@ Window.width flg if NERDTREE-COLS + else NERDTREE-COLS - then w s! Window.width
    repeat
    flg if
        nerdtree-window @ Nerdtree.deactivate
    else
        nerdtree-window @ true nerded.begin
        Nerdtree.activate
    then
    Screen.paint
    ncurses.refresh
;
' (Screen.toggle-nerdtree) is Screen.toggle-nerdtree

: (Screen.resize) { | w top newheight newwidth cheight oheight owidth -- , get screen dimensions }
    terminal-rows @ -> newheight
    terminal-cols @ -> newwidth
    newheight newwidth ncurses::resize_term drop

    theScreen s@ Screen.client-rows -> oheight
    theScreen s@ Screen.cols -> owidth

    newheight 2- -> cheight
    cheight theScreen s! Screen.client-rows   \ reserve 2 lines for command and message lines

    \ resize nerdtree height
    nerdtree-window @ ?dup if
        cheight swap s! Window.height
    then

    0 -> top
    Window.list -> w
    begin
        w s@ ListNode.next -> w
        w Window.list <>
    while
\ newwidth = currentwidth / oldwidth * newwidth
            newwidth w s@ Window.width * owidth / 
        w s! Window.width
            cheight w s@ Window.height 10 * 5 + * oheight /  10 /
        w s! Window.height
        top w s! Window.top
        w s@ Window.height top + -> top
        w Window.fix-cursor
    repeat
    newwidth  theScreen s! Screen.cols
    newheight theScreen s! Screen.rows
;

' (Screen.resize) is Screen.resize

: sigwinch-handler 
\     terminal-rows @ 1+ terminal-cols @ 1+ ncurses::resize_term drop
\     Screen.resize
\     screen.repaint
    true screen-resized !
;
' sigwinch-handler is handle-SIGWINCH

: (CommandLine.move-to)
    ncurses-window-reset
    theScreen s@ Screen.rows 2- 0 ncurses.move
;
' (CommandLine.move-to) is CommandLine.move-to

: Screen.run ( -- , main program once in ncurses mode )
    ncurses.refresh

    begin
        WindowList.active-window Window.paint
        WindowList.active-window Window.run
    again
;

: (Screen.cursor-within) { win row col -- flg }
    row win s@ Window.top < if false exit then
    row win s@ Window.top win s@ Window.height + 1- > if false exit then
    col win s@ Window.left < if false exit then
    col win s@ Window.left win s@ Window.width + 1- > if false exit then
    true 
;
' (Screen.cursor-within) is Screen.cursor-within

: (Screen.window-at) { row col | win -- win , get window at row col on the screen }
    nerdtree-active @ if
        nerdtree-window @ row col Screen.cursor-within if
            nerdtree-window @ exit
        then
    then
    Window.list s@ List.next -> win
    begin
        win Window.list <>
    while
        win row col Screen.cursor-within if 
            win exit 
        then
        win s@ ListNode.next -> win
    repeat
    Window.list s@ List.prev 
;
' (Screen.window-at) is Screen.window-at

: (Screen.split-vertically) { | newwin height newh -- , split active window }
    WindowList.active-window s@ Window.height -> height 
    height 2/ -> newh       \ total height for old and new window combined
    height newh -           \ height of new window
    3 < if
        \ new window doesn't fit
        ncurses.bell
        exit
    then

    WindowList.active-window Window.clone -> newwin

    newh WindowList.active-window s! Window.height
    height newh -           \ height of new window
    newwin s! Window.height
    WindowList.active-window s@ Window.top newh + newwin s! Window.top
    newwin WindowList.active-window List.InsertAfter
    Screen.paint
;
' (Screen.split-vertically) is Screen.split-vertically

: (Screen.remove-active) { | w nxt prev rm -- , remove active window }
    WindowList.active-window -> w
    w s@ ListNode.next -> nxt
    w s@ ListNode.prev -> prev
    nxt Window.List = prev Window.List = and if exit then
    nxt Window.List = if
        prev s@ Window.height w s@ Window.height + prev s@ Window.height
        w List.remove
        w Window.destroy
        prev WindowList.activate-window
        Screen.paint
        exit
    then
    prev Window.List = if
        w s@ Window.top nxt s! Window.top
        nxt s@ Window.height w s@ Window.height + nxt s! Window.height
        w List.remove
        w Window.destroy
        nxt WindowList.activate-window
        exit
    then
    prev s@ Window.height nxt s@ Window.height < if
        prev s@ Window.height w s@ Window.height + prev s@ Window.height
        w List.remove
        w Window.destroy
        prev WindowList.activate-window
        Screen.paint
    else
        w s@ Window.top nxt s! Window.top
        nxt s@ Window.height w s@ Window.height + nxt s@ Window.height
        w List.remove
        w Window.destroy
        nxt WindowList.activate-window
        Screen.paint
        exit
    then
;
' (Screen.remove-active) is Screen.remove-active

: (Screen.left-window) ( -- , activate  window to left )
    nerdtree-active @ if
        nerdtree.activate
        Screen.paint
    then
;
' (Screen.left-window) is Screen.left-window

: (Screen.right-window) { | nxt -- , activate next window }
    nerdtree-active @ if
        nerdtree-window @ WindowList.active-window = if
            nerdtree-window @ nerdtree.next-window
\             Screen.paint
        then
    then
;
' (Screen.right-window) is Screen.right-window

: (Screen.next-window) { | nxt -- , activatee next window }
    WindowList.active-window s@ ListNode.next -> nxt
    nxt Window.list <> if
        nxt WindowList.activate-window 
        Screen.paint
    then
;
' (Screen.next-window) is Screen.next-window

: (Screen.prev-window) { | prev -- , activatee previous window }
    WindowList.active-window s@ ListNode.prev -> prev
    prev Window.list <> if
        prev WindowList.activate-window
        Screen.paint
    then
;
' (Screen.prev-window) is Screen.prev-window

: (Screen.dump) ( -- , dump useful stuff )  
    cr ." MAP "
    map
    cr ." Window List " cr
    WindowList.dump
    cr ." Active Window $" WindowList.active-window .hex cr
    Dired.dump
    cr ." Current selection"
    Selection.dump
    cr ." Current recording "
    dot-recording @ Recording.dump
    cr ." screen dimensions "
    cr theScreen s@ Screen.rows . ." x " theScreen s@ Screen.cols . 
    cr terminal-rows ." x " terminal-cols .
    cr ." Project "
    cr theProject @ Project.dump
;
' (Screen.dump) is Screen.dump

: (Screen.press-any-key) ( -- , print message and wait for key )
    s" press any key " type-save @ execute
    sys::key
;
' (Screen.press-any-key) is Screen.press-any-key

: (Screen.execute) { caddr u flg -- , execute command using system call }
    caddr u screen-pad place-cstr
    ncurses.endwin
    ncurses.endwin
    0 0 TIO.move-to TIO.cls

    screen-pad sys::system
    flg if
        Screen.press-any-key
    then
    ncurses::flushinp drop
    Screen.paint
    ncurses.refresh
;
' (Screen.execute) is Screen.execute

privatize
