\
\ phred editor
\
\ programmed by Mike Schwartz
\
\ Command Line handling
\   Command Buffer
\
private{
    8192 constant PADSIZE
    create cmded-pad PADSIZE allot
}private

: Cmded.append-command { buf caddr u | tmpnam fp -- , append output of command to buf] }
    caddr u  cmded-pad place-cstr
    cmded-pad z"  >" 1+ string::strcat drop
    z" /tmp/temp.phred.cmd" 1+ -> tmpnam
    cmded-pad tmpnam string::strcat drop
    cmded-pad sys::system  drop
    z" r" 1+ tmpnam sys::fopen -> fp
\     z" rm /tmp/temp.phred.cmd"  1+ sys::system drop
    fp 0= if
        sys::strerror zcount cmded-pad place

        TAG_END
            cmded-pad TAG_STRING
            c" *** Error: " TAG_$STRING
        buf Buffer.format
    else
            TAG_END
            caddr u TAG_STRING
            s"  Output of " TAG_STRING
            buf Buffer.format
        TAG_END c" " TAG_$STRING 
        buf Buffer.format
        begin
            fp sys::feof not
        while
            fp sys::ferror ?dup if
                die{ ." ferror returned " . .s }die
            then
            cmded-pad PADSIZE fp sys::fgets 0= if
                fp sys::fclose drop
                exit
            then
            cmded-pad buf Buffer.append-text drop
        repeat
        fp sys::fclose drop
    then
;

: (cmded.begin) { w caddr u | fp buf -- , initialize and attach command output editor }
    caddr u Buffer.new -> buf
    buf caddr u Cmded.append-command
    true buf s! Buffer.read-only
    Buffer.filetype.cmd buf s! Buffer.filetype

    buf BufferList.add
    w buf Window.set-buffer
    w Window.paint
;

' (cmded.begin) is Cmded.begin

privatize
