\
\ PHRed editor
\
\ programmed by Mike Schwartz
\
\ Forth Syntax Highlighting
\

private{
    create word-buf 8192 allot
    variable word-source
}private

: highlight-color.core { len -- color , return color if word-buf is a core word, or -1 }
    word-buf len  forth-core-word? 
    if
        OPCODE.keyword
    else
        -1
    then
;

\ src must not be preceded by blanks
: highlight-word.core { src dst | done ch ptr len color -- src dst , highlight a word }
    word-buf -> ptr
    0 -> len
    false -> done
    begin
        src c@ -> ch 
        ch 0= if true -> done then
        ch ctype::isblank  if true -> done then
        done false =
    while
        ch ptr c! ptr 1+ -> ptr
        src 1+ -> src
        len 1+ -> len
    repeat
    0 ptr c!

    \ word-buf now has a word in it
    len highlight-color.core -> color
    color 0 >= if
        OPCODE.save dst c! dst 1+ -> dst
        color dst c! dst 1+ -> dst
    then
    len 0 <= if
        die{ ." bad length " len . }die
    then
    word-buf dst len move
    dst len + -> dst
    color 0 >= if
        OPCODE.default dst c!   dst 1+ -> dst
\         OPCODE.restore dst c! 
\         dst 1+ -> dst
    then
    src dst
;

: copy-comment { delim src dst | ch -- src dst , copy from open paren to close paren }
    begin
        src c@ -> ch src 1+ -> src
        ch dst c! dst 1+ -> dst
        ch 0= ch delim = OR if
            src dst exit
        then
    again
;

\ caller must MEM.free new-text returned
: Highlight.forth { text | new-text lastbl ch src dst -- highlighted , syntax highlight line based upon fancy strings }
    \ allocate new-line with plenty of room for fancy string op codes
    text string::strlen 2* MEM.malloc -> new-text
    new-text word-source !
    text -> src
    new-text -> dst
    true -> lastbl
    begin
        src c@ -> ch 
        ch 0<>
    while
        ch case 
            OPCODE.BKGDSET of
                ch dst c! dst 1+ -> dst
                src 1+ -> src
                src c@ src 1+ -> src dst c! dst 1+ -> dst
                src c@ src 1+ -> src dst c! dst 1+ -> dst
            endof
            OPCODE.ATTRON of
                ch dst c! dst 1+ -> dst
                src 1+ -> src
                src c@ src 1+ -> src dst c! dst 1+ -> dst
            endof
            OPCODE.ATTROFF of
                ch dst c! dst 1+ -> dst
                src 1+ -> src
                src c@ src 1+ -> src dst c! dst 1+ -> dst
            endof
            OPCODE.ICON of
                ch dst c! dst 1+ -> dst
                src 1+ -> src
                src c@ src 1+ -> src dst c! dst 1+ -> dst
            endof
            bl of
                true -> lastbl 
                src 1+ -> src
                bl dst c! dst 1+ -> dst
            endof
            ascii \ of
                OPCODE.save dst c!      dst 1+ -> dst
                OPCODE.comment dst c!   dst 1+ -> dst
                begin
                    src c@ ?dup
                    src 1+ -> src
                while
                    dst c!              dst 1+ -> dst
                repeat
                OPCODE.default dst c!   dst 1+ -> dst
            endof
            ascii ( of
                OPCODE.save dst c! dst 1+ -> dst
                OPCODE.signature dst c!   dst 1+ -> dst
                ascii ) src dst copy-comment -> dst -> src
                OPCODE.default dst c!   dst 1+ -> dst
\                 OPCODE.restore dst c!   dst 1+ -> dst
            endof
            ascii { of
                OPCODE.save dst c! dst 1+ -> dst
                OPCODE.locals dst c!   dst 1+ -> dst
                ascii } src dst copy-comment -> dst -> src
\                 OPCODE.restore dst c!   dst 1+ -> dst
                OPCODE.default dst c!   dst 1+ -> dst
            endof
            \ default
            lastbl if
                src dst highlight-word.core -> dst -> src
                false -> lastbl 
            else
                dup dst c! 
                dst 1+ -> dst
                src 1+ -> src
                false -> lastbl 
            then
        endcase
    repeat
    0 dst c!
    new-text
;

privatize
\ ncurses-uninstall
\ see highlight.forth
\ 0 sys::exit