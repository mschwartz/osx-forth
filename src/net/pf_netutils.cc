#ifdef PF_SUPPORT_NET

#include "src/net/pf_netutils.h"
#include "src/pf_all.h"
#include "sys/ioctl.h"
#ifdef __APPLE__
#include "sys/sockio.h"
#endif
#include "net/if.h"
#include "netinet/in.h"

#ifdef __LINUX__
#include <linux/kd.h>
#endif

typedef struct SIO {
  const char *name;
  cell_t value;
} SIO;

SIO sios[] = {
#ifdef __APPLE__
  { .name = "SIOCGHIWAT", .value = SIOCGHIWAT },
  { .name = "SIOCSLOWWAT", .value = SIOCSLOWAT },
  { .name = "SIOCGLOWWAT", .value = SIOCGLOWAT },
#endif
  { .name = "SIOCATMARK", .value = SIOCATMARK },
  { .name = "SIOCSPGRP", .value = SIOCSPGRP },
  { .name = "SIOCGPGRP", .value = SIOCGPGRP },
  { .name = "SIOCSIFADDR", .value = SIOCSIFADDR },
  { .name = "SIOCSIFDSTADDR", .value = SIOCSIFDSTADDR },
  { .name = "SIOCSIFFLAGS", .value = SIOCSIFFLAGS },
  { .name = "SIOCGIFFLAGS", .value = SIOCGIFFLAGS },
  { .name = "SIOCSIFBRDADDR", .value = SIOCSIFBRDADDR },
  { .name = "SIOCSIFNETMASK", .value = SIOCSIFNETMASK },
#ifdef __APPLE__
  { .name = "SIOCGIFMETRIC", .value = SIOCGIFMETRIC },
  { .name = "SIOCSIFMETRIC", .value = SIOCSIFMETRIC },
  { .name = "SIOCDIFADDR", .value = SIOCDIFADDR },
  { .name = "SIOCAIFADDR", .value = SIOCAIFADDR },
#endif
  { .name = "SIOCGIFADDR", .value = SIOCGIFADDR },
  { .name = "SIOCGIFDSTADDR", .value = SIOCGIFDSTADDR },
  { .name = "SIOCGIFBRDADDR", .value = SIOCGIFBRDADDR },
  { .name = "SIOCGIFCONF", .value = SIOCGIFCONF },
  { .name = "SIOCGIFNETMASK", .value = SIOCGIFNETMASK },
#ifdef __APPLE__
  { .name = "SIOCAUTOADDR", .value = SIOCAUTOADDR },
  { .name = "SIOCAUTONETMASK", .value = SIOCAUTONETMASK },
  { .name = "SIOCARPIPLL", .value = SIOCARPIPLL },
#endif

  { .name = "SIOCADDMULTI", .value = SIOCADDMULTI },
  { .name = "SIOCDELMULTI", .value = SIOCDELMULTI },
  { .name = "SIOCGIFMTU", .value = SIOCGIFMTU },
  { .name = "SIOCSIFMTU", .value = SIOCSIFMTU },
#ifdef __APPLE__
  { .name = "SIOCGIFPHYS", .value = SIOCGIFPHYS },
  { .name = "SIOCSIFPHYS", .value = SIOCSIFPHYS },
  { .name = "SIOCSIFMEDIA", .value = SIOCSIFMEDIA },

  { .name = "SIOCGIFMEDIA", .value = SIOCGIFMEDIA },

  { .name = "SIOCSIFGENERIC", .value = SIOCSIFGENERIC },
  { .name = "SIOCSIFGENERIC", .value = SIOCSIFGENERIC },
  { .name = "SIOCGIFGENERIC", .value = SIOCGIFGENERIC },
  { .name = "SIOCRSLVMULTI", .value = SIOCRSLVMULTI },

  { .name = "SIOCSIFLLADDR", .value = SIOCSIFLLADDR },
  { .name = "SIOCGIFSTATUS", .value = SIOCGIFSTATUS },
  { .name = "SIOCSIFPHYADDR", .value = SIOCSIFPHYADDR },
  { .name = "SIOCGIFPDSTADDR", .value = SIOCGIFPDSTADDR },
  { .name = "SIOCDIFPHYADDR", .value = SIOCDIFPHYADDR },
  { .name = "SIOCGIFPSRCADDR", .value = SIOCGIFPSRCADDR },

  { .name = "SIOCGIFDEVMTU", .value = SIOCGIFDEVMTU },
  { .name = "SIOCSIFALTMTU", .value = SIOCSIFALTMTU },
  { .name = "SIOCGIFALTMTU", .value = SIOCGIFALTMTU },
  { .name = "SIOCSIFBOND", .value = SIOCSIFBOND },
  { .name = "SIOCGIFBOND", .value = SIOCGIFBOND },

  { .name = "SIOCGIFXMEDIA", .value = SIOCGIFXMEDIA },

  { .name = "SIOCSIFCAP", .value = SIOCSIFCAP },
  { .name = "SIOCGIFCAP", .value = SIOCGIFCAP },

  { .name = "SIOCSIFMANAGEMENT", .value = SIOCSIFMANAGEMENT },

  { .name = "SIOCIFCREATE", .value = SIOCIFCREATE },
  { .name = "SIOCIFDESTROY", .value = SIOCIFDESTROY },
  { .name = "SIOCIFCREATE2", .value = SIOCIFCREATE2 },

  { .name = "SIOCSDRVSPEC", .value = SIOCSDRVSPEC },
  { .name = "SIOCGDRVSPEC", .value = SIOCGDRVSPEC },

  { .name = "SIOCSIFVLAN", .value = SIOCSIFVLAN },
  { .name = "SIOCGIFVLAN", .value = SIOCGIFVLAN },
  { .name = "SIOCSETVLAN", .value = SIOCSETVLAN },
  { .name = "SIOCGETVLAN", .value = SIOCGETVLAN },

  { .name = "SIOCIFGCLONERS", .value = SIOCIFGCLONERS },

  { .name = "SIOCGIFASYNCMAP", .value = SIOCGIFASYNCMAP },
  { .name = "SIOCSIFASYNCMAP", .value = SIOCSIFASYNCMAP },

  { .name = "SIOCGIFMAC", .value = SIOCGIFMAC },
  { .name = "SIOCSIFMAC", .value = SIOCSIFMAC },
  { .name = "SIOCSIFKPI", .value = SIOCSIFKPI },
  { .name = "SIOCGIFKPI", .value = SIOCGIFKPI },

  { .name = "SIOCGIFWAKEFLAGS", .value = SIOCGIFWAKEFLAGS },

  { .name = "SIOCGIFFUNCTIONALTYPE", .value = SIOCGIFFUNCTIONALTYPE },

  { .name = "SIOCSIF6LOWPAN", .value = SIOCSIF6LOWPAN },
  { .name = "SIOCGIF6LOWPAN", .value = SIOCGIF6LOWPAN },
#endif

  // address families
  { .name = "AF_UNSPEC", .value = AF_UNSPEC },
  { .name = "AF_UNIX", .value = AF_UNIX },
  { .name = "AF_INET", .value = AF_INET },
  { .name = "AF_LOCAL", .value = AF_LOCAL },
  { .name = "AF_INET6", .value = AF_INET6 },

  // socket types
  { .name = "SOCK_STREAM", .value = SOCK_STREAM },
  { .name = "SOCK_DGRAM", .value = SOCK_DGRAM },
  { .name = "SOCK_RAW", .value = SOCK_RAW },

  // protocol types
  { .name = "IPPROTO_IP", .value = IPPROTO_IP },
  { .name = "IPPROTO_ICMP", .value = IPPROTO_ICMP },
  { .name = "IPPROTO_IGMP", .value = IPPROTO_IGMP },
#ifdef __APPLE__
  { .name = "IPPROTO_GGP", .value = IPPROTO_GGP },
  { .name = "IPPROTO_IPV4", .value = IPPROTO_IPV4 },
  { .name = "IPPROTO_IPIP", .value = IPPROTO_IPIP },
  { .name = "IPPROTO_ST", .value = IPPROTO_ST },
  { .name = "IPPROTO_TRUNK1", .value = IPPROTO_TRUNK1 },
  { .name = "IPPROTO_TRUNK2", .value = IPPROTO_TRUNK2 },
  { .name = "IPPROTO_LEAF1", .value = IPPROTO_LEAF1 },
  { .name = "IPPROTO_LEAF2", .value = IPPROTO_LEAF2 },
  { .name = "IPPROTO_RDP", .value = IPPROTO_RDP },
  { .name = "IPPROTO_PIGP", .value = IPPROTO_PIGP },
  { .name = "IPPROTO_RCCMON", .value = IPPROTO_RCCMON },
  { .name = "IPPROTO_NVPII", .value = IPPROTO_NVPII },
  { .name = "IPPROTO_ARGUS", .value = IPPROTO_ARGUS },
  { .name = "IPPROTO_EMCON", .value = IPPROTO_EMCON },
  { .name = "IPPROTO_XNET", .value = IPPROTO_XNET },
  { .name = "IPPROTO_CHAOS", .value = IPPROTO_CHAOS },
  { .name = "IPPROTO_MUX", .value = IPPROTO_MUX },
  { .name = "IPPROTO_MEAS", .value = IPPROTO_MEAS },
  { .name = "IPPROTO_HMP", .value = IPPROTO_HMP },
  { .name = "IPPROTO_PRM", .value = IPPROTO_PRM },
#endif
  { .name = "IPPROTO_TCP", .value = IPPROTO_TCP },
  { .name = "IPPROTO_EGP", .value = IPPROTO_EGP },
  { .name = "IPPROTO_PUP", .value = IPPROTO_PUP },
  { .name = "IPPROTO_UDP", .value = IPPROTO_UDP },
  { .name = "IPPROTO_IDP", .value = IPPROTO_IDP },
  { .name = "IPPROTO_IPV6", .value = IPPROTO_IPV6 },
  { .name = "IPPROTO_ICMPV6", .value = IPPROTO_ICMPV6 },
  { .name = "IPPROTO_SCTP", .value = IPPROTO_SCTP },
  // { .name = "IPPROTO_UDPLITE", .value = IPPROTO_UDPLITE },
  { .name = "IPPROTO_RAW", .value = IPPROTO_RAW },

  // protocol options
  { .name = "IP_OPTIONS", .value = IP_OPTIONS },
  { .name = "IP_HDRINCL", .value = IP_HDRINCL },
  { .name = "IP_TOS", .value = IP_TOS },
  { .name = "IP_TTL", .value = IP_TTL },
  { .name = "IP_MULTICAST_TTL", .value = IP_MULTICAST_TTL },
  { .name = "IP_MULTICAST_LOOP", .value = IP_MULTICAST_LOOP },
  { .name = "IP_ADD_MEMBERSHIP", .value = IP_ADD_MEMBERSHIP },
  { .name = "IP_DROP_MEMBERSHIP", .value = IP_DROP_MEMBERSHIP },
  { .name = "IP_MULTICAST_IF", .value = IP_MULTICAST_IF },
  { .name = "IP_ADD_SOURCE_MEMBERSHIP", .value = IP_ADD_SOURCE_MEMBERSHIP },
  { .name = "IP_DROP_SOURCE_MEMBERSHIP", .value = IP_DROP_SOURCE_MEMBERSHIP },
  { .name = "IP_BLOCK_SOURCE", .value = IP_BLOCK_SOURCE },
  { .name = "IP_UNBLOCK_SOURCE", .value = IP_UNBLOCK_SOURCE },
  { .name = "IP_PKTINFO", .value = IP_PKTINFO },
  { .name = "IP_RECVOPTS", .value = IP_RECVOPTS },
  { .name = "IP_RETOPTS", .value = IP_RETOPTS },
#if 0
  { .name = "IP_PASSSEC", .value = IP_PASSSEC },
  { .name = "IP_TRANSPARENT", .value = IP_TRANSPARENT },
  { .name = "IP_MULTICAST_ALL", .value = IP_MULTICAST_ALL },
  { .name = "IP_UNICAST_IF", .value = IP_UNICAST_IF },
  { .name = "IP_RECVTTL", .value = IP_RECVTTL },
  { .name = "IP_RECVTOS", .value = IP_RECVTOS },
  { .name = "IP_SENDSRCADDR", .value = IP_SENDSRCADDR },
  { .name = "IP_RECVORIGDSTADDR", .value = IP_RECVORIGDSTADDR },
  { .name = "IP_MINTTL", .value = IP_MINTTL },
  { .name = "IP_DONTFRAG", .value = IP_DONTFRAG },
  { .name = "IP_DF", .value = IP_DF },
  { .name = "IP_MF", .value = IP_MF },
  { .name = "IP_ROUTER_ALERT", .value = IP_ROUTER_ALERT },
  { .name = "IP_PKTINFO", .value = IP_PKTINFO },
  { .name = "IP_PKTOPTIONS", .value = IP_PKTOPTIONS },
  { .name = "IP_PMTUDISC", .value = IP_PMTUDISC },
  { .name = "IP_PMTUDISC_DONT", .value = IP_PMTUDISC_DONT },
  { .name = "IP_PMTUDISC_DO", .value = IP_PMTUDISC_DO },
  { .name = "IP_PMTUDISC_PROBE", .value = IP_PMTUDISC_PROBE },
  { .name = "IP_PMTUDISC_WANT", .value = IP_PMTUDISC_WANT },
  { .name = "IP_RECVERR", .value = IP_RECVERR },
#endif

  // socket level
  { .name = "SOL_SOCKET", .value = SOL_SOCKET },
#if 0
  { .name = "SOL_IP", .value = SOL_IP },
  { .name = "SOL_IPV6", .value = SOL_IPV6 },
  { .name = "SOL_ICMPV6", .value = SOL_ICMPV6 },
  { .name = "SOL_RAW", .value = SOL_RAW },
  { .name = "SOL_TCP", .value = SOL_TCP },
  { .name = "SOL_UDP", .value = SOL_UDP },
  { .name = "SOL_ICMP", .value = SOL_ICMP },
  { .name = "SOL_PACKET", .value = SOL_PACKET },
#endif

  // socket options
  { .name = "SO_DEBUG", .value = SO_DEBUG },
  { .name = "SO_ACCEPTCONN", .value = SO_ACCEPTCONN },
  { .name = "SO_REUSEADDR", .value = SO_REUSEADDR },
  { .name = "SO_KEEPALIVE", .value = SO_KEEPALIVE },
  { .name = "SO_DONTROUTE", .value = SO_DONTROUTE },

  // INTERNET ADDRESS
  { .name = "INADDR_ANY", .value = INADDR_ANY },
  { .name = "INADDR_BROADCAST", .value = INADDR_BROADCAST },
  { .name = "INADDR_LOOPBACK", .value = INADDR_LOOPBACK },
  { .name = "INADDR_NONE", .value = INADDR_NONE },

};

cell_t pfConstant(char *name) {
  for (unsigned int i = 0; i < sizeof(sios) / sizeof(struct SIO); i++) {
    // printf("pfConstant(%s) name: %s = %lx\n", name, sios[i].name, sios[i].value);
    if (strcmp(sios[i].name, name) == 0) {
      return sios[i].value;
    }
  }
  printf("pfConstant(%s) not found\n", name);
  return -1;
}

#endif
