\
\ cursor
\

: ncurses.getyx ( win -- y x , get cursor position )
    ncurses::getyx
;

0 constant cursormode-hidden
1 constant cursormode-visible
2 constant cursormode-highly-visible

: ncurses.curs_set { mode | old -- old , set cursor mode, return old-mode or err }
    mode ncurses::curs_set -> old
    old ncurses.ERR = if
        mode cursormode-visible >= if
            cursormode-visible ncurses::curs_set -> old
        then
    then
    old
;

\ : ncurses.cursor-color { $color -- , set cursor color to xterm color }
    \ xterm color can be blue, white, green, brown, etc.
\     $color count pad place-cstr
\     ncurses::cursor-color
\ ;

: ncurses.cursor-hide ( -- , hide cursor )
    cursormode-hidden ncurses.curs_set drop
;
: ncurses.cursor-show ( -- , show cursor )
    cursormode-hidden ncurses.curs_set drop
\     cursormode-visible ncurses.curs_set
;
: ncurses.cursor-insert ( -- , show insert mode cursor )
\     cursormode-visible ncurses.curs_set
    cursormode-hidden ncurses.curs_set
;
