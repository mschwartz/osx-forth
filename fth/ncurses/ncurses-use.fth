\
\ Import ncurses words so you don't need to prefix with ncurses. and 
\ install words like . and ." and type that use ncurses 
\
\ Installs versions of output words that we can use with ncurses
\   implement using ncurses: cr . .s ."  type and so on
\

: flush-emit 
    ncurses-window ncurses.wrefresh
;
variable emit-save ' emit emit-save !
defer emit 

: [emit] { ch | y x -- , output ch to screen }
    ncurses-window ncurses.getyx -> x -> y
    x 0< if exit then
    x COLS < if
        ncurses-window ch ncurses.waddch
    then
;
' [emit] is emit

variable cr-save ' cr cr-save !
defer cr 
: (cr) ( -- , output newline )
    ncurses-window 10 ncurses.waddch
;
' (cr) is cr

variable space-save ' space space-save !
defer space 
: (space) ( -- , output a space )
    bl emit
;
' (space) is space

variable spaces-save ' spaces spaces-save !
defer spaces
: (spaces) ( n -- , emit n spaces )
    0 do 
        space 
    loop
;
' (spaces) is spaces

variable type-save ' type type-save !
defer type
: (type) { caddr u -- print string to screen }
    u if
        u 0 do
            caddr c@ emit
            caddr 1+ -> caddr
        loop
    then
\     ncurses-window caddr u ncurses.wprintw
;
' (type) is type

variable .-save ' . .-save !
defer .
: [.] ( n -- , print number )
    (.) type space
;
' [.] is .

variable .r-save ' .r .r-save !
defer .r
: (.r) { n wid | str len -- , print n with w width }
    n (.) 
    -> len -> str
    wid len - spaces
    str len type
;

' (.r) is .r

variable .hex-save ' .hex .hex-save !
defer .hex
: (.hex) ( n -- , output n in hex )
    base @ hex swap . base !
;
' (.hex) is .hex

: (.")  ( -- , type following string )
        r> count 2dup + aligned >r type
;
variable ."-save ' ." ."-save !
defer ." immediate
: [."]   ( <string> -- , type string )
    state @
    IF      compile (.")  ,"
    ELSE [char] " parse type
    THEN
; immediate
' [."] is ."

variable cls-save ' cls cls-save !
defer cls
: (cls) ( -- clear screen )
    ncurses-window ncurses.werase
;
' (cls) is cls

variable key?-save ' key? key?-save !
defer key?
: (key?)  { | ch -- f , is keypress ready }
    true sys::raw
    -1 ncurses.timeout
    ncurses::stdscr true ncurses.nodelay
    ncurses.getch -> ch
\     ch .hex cr
    ch ncurses.ERR = if
        false
    else
        ch ncurses.ungetch
        true
    then
;
' (key?) is key?

variable key-save ' key key-save !
defer key
: (key) { | ch -- ch , wait for and read key from keyboard }
    -1 ncurses.timeout
    ncurses::stdscr false ncurses.nodelay
    ncurses.getch -> ch
    ch ncurses.ERR = if
        ncurses-uninstall cr ." key returned -1? " .s bye
    then
    ch
;

' (key) is key

: ncurses.print# { n w -- , print number with width w adding leading spaces }
    n s>d <# #s #> 
    dup w swap - 
    spaces type
;

: .# number>string type ;

variable ncurses-uninstalled  false ncurses-uninstalled !

require lib/ncurses/ncurses-mouse.fth

: (ncurses-uninstall)
\     ncurses-uninstalled if exit then
\     true ncurses-uninstalled !
    false ncurses.mouse-on
    emit-save @ is emit
    cr-save @ is cr
    space-save @ is space
    spaces-save @ is spaces
    type-save @ is type
    .r-save @ is .r
    .-save @ is .
    .hex-save @ is .hex
    ."-save @ is ."
    cls-save @ is cls
    key-save is key
    key?-save is key?
    ncurses.endwin
    0 0 TIO.move-to TIO.cls
;

: ncurses-install 
;

' (ncurses-uninstall) is ncurses-uninstall
decimal
