require phred/keymaps/visual-leader.fth

: Keymap.yes/no { | ch -- flg , wait for Y or N and set flg }
    begin
        ncurses.getch -> ch
        ch ncurses.key_mouse <> if 
            ch ascii Y = if true exit then
            ch ascii y = if true exit then
            ch ascii N = if false exit then
            ch ascii n = if false exit then
        then
    again
;

: Keymap.visual { w | n ch b caddr u s lin fn -- , run editor keymap for window }
    begin
        w s@ Window.buffer -> b
        b Buffer.file-modified if \ modified on disk
            messageline{ 
                ." file has changed on disk! reload (y/n)? " 
            }messageline
            Keymap.yes/no if
                b Buffer.revert
                w b Window.set-buffer
            then
            b Buffer.touch
        then

        w Window.paint

        1 -> n
        Keymap.key -> ch
        ch ascii 0 > ch ascii 9 <= and if
            \ get any numeric value pre-command (like the 10 in "10dd")
            ch ascii 0 -  -> n
            begin
                Keymap.key -> ch
                ch ctype::isdigit
            while
                n 10 * ch ascii 0 - + -> n
            repeat
        then
\         w Window.refresh

        w Window.address-cursor
        Window.end-search

        begin
            ch ncurses.key_mouse = 
        while
            Keymap.mouse if exit then \ because a different window may have been clicked on (activated)
            Keymap.key -> ch
        repeat

\         messageline.clear
\         messageline{ ch emit }messageline 

        w Window.address-cursor

        w ch n keymap.common if
            exit
        then
        ch case
            ansi_escape of \ escape key
                ncurses.beep
                drop exit
            endof
            ascii . of
                Keymap.start-playback
                exit
            endof
            ascii : of
                w CommandLine.run 
                exit
            endof

            \ macros  

            ascii q of
                \ start/end record macro
                m-recording @ if
                    Keymap.end-macro
                else
                    Keymap.key dup >r tolower -> ch
                    ch ascii a >= ch ascii z <= and if
                        ch Keymap.record-macro
                        r> drop
                    else
                        r> Keymap.ungetch
                    then
                then
            endof

            ascii @ of
                \ playback macro
                m-playing @ if 
                    ncurses.bell
                else
                    Keymap.key tolower -> ch
                    n ch Keymap.play-macro
                then
            endof

            \ indentation

            ascii > of
                w API.>>
                exit
            endof

            ascii < of
                w API.<<
                exit
            endof

            \ misc
            ascii % of  \ matching paren, brace, or bracket or :;
                w API.%
                exit
            endof

            ascii ? of \ get signature of word under cursor
                w Window.word-under-cursor -> u -> caddr 
                u not if 
                    messageline{ 
                        ."  *** " 
                        caddr u type 
                        ." is not defined in any file." 
                    }messageline
                    exit 
                then
                caddr u Documentation.word -> s
                s if
                    messageline{ caddr u type ."  '" s zcount type ." '" }messageline
                    s mem::free
                    exit
                else
                    messageline{ 
                        ."  *** " 
                        caddr u type 
                        ." is not defined in any file." 
                    }messageline
                then
                exit
            endof
            
            ascii , of
                w Keymap.leader.visual
                exit
            endof

            ascii c of
                keymap.key case
                    ascii w of
                        w Window.read-only? if 
                            ncurses.bell drop 
                            Keymap.reset-recording 
                            exit 
                        then
                        Keymap.playing? not if
                            Keymap.reset-recording
                            n 1 > if n (.) Keymap.append-string then
                            s" cw" Keymap.append-string
                        then
                        w API.cw
                    endof
                endcase
            endof

            ascii d of  \ d prefix
                keymap.key case
                    ascii e of
                        w Dired.begin
                        w Window.dired-mode
                        exit
                    endof
                    ascii a of              \ da introducer
                        keymap.key ascii w <> if true exit then
                        w Window.read-only? if 
                            ncurses.bell drop exit 
                        then

                        \ delete (cut) word under the cursor
                        Keymap.playing? not if
                            Keymap.reset-recording
                            n 1 > if n (.) Keymap.append-string then
                            s" daw" Keymap.append-string
                            Keymap.end-recording
                        then

                        n 0 do w API.daw loop
                    endof
                    ascii d of              
                        \ dd = delete line
                        w Window.read-only? if 
                            ncurses.bell drop exit 
                        then

                        Keymap.playing? not if
                            Keymap.reset-recording
                            n 1 > if n (.) Keymap.append-string then
                            s" dd" Keymap.append-string
                            Keymap.end-recording
                        then
                        n 0 do w API.dd loop
                    endof
                    ascii w of              
                        \ dw = delete word forward
                        w Window.read-only? if 
                            ncurses.bell drop exit 
                        then
                        Keymap.playing? not if
                            Keymap.reset-recording
                            n 1 > if n (.) Keymap.append-string then
                            s" dw" Keymap.append-string
                            Keymap.end-recording
                        then
                        n 0 do w API.dw loop
                    endof
                    ascii i of              \ di introducer
                        keymap.key ascii w <> if true exit then
                        w Window.read-only? if 
                            ncurses.bell drop exit 
                        then
                        \ delete (cut) word under the cursor
                        Keymap.playing? not if
                            Keymap.reset-recording
                            n 1 > if n (.) Keymap.append-string then
                            s" diw" Keymap.append-string
                            Keymap.end-recording
                        then
                        n 0 do w API.diw loop
                    endof
                    ncurses.bell
                endcase
            endof

            \ increment/decrement ( does only current line at or right of cursor )
            ncurses.ctrl_a of
                n 1 > if n (.) Keymap.append-string then
                s\" \x01" Keymap.append-string
                w n API.ctrl_ax
            endof
            ncurses.ctrl_x of
                n 1 > if n (.) Keymap.append-string then
                s\" \x18" Keymap.append-string
                w n -1 * API.ctrl_ax
            endof

            \ O open file for word under cursor 
            ascii O of
                w Window.word-under-cursor -> u -> caddr
                caddr u find-file -> fn
                fn if 
                    w fn zcount WindowList.find-or-create drop
                else
                    messageline{ ."  *** " caddr u type ."  Not found. " }messageline
                then
            endof

            \ selection and paste

            ascii V of  \ toggle selection on/off
                w s@ Window.line# selection-start# !
                w s@ Window.line# selection-end# !
                w Window.selection-mode
                exit
            endof
            ascii p of \ paste selection
                w Window.read-only? if 
                    ncurses.bell drop exit 
                then
                Selection.empty? if
                    ncurses.bell
                else
                    w n API.paste
                    w Window.line-down
                    exit
                then
            endof

            ascii x of
                w Window.read-only? if 
                    ncurses.bell drop exit 
                then
                Keymap.playing? not if
                    Keymap.reset-recording
                    n 1 > if n (.) Keymap.append-string then
                    s" x" Keymap.append-string
                    Keymap.end-recording
                then
                n 0 do w API.x
            endof
            ascii i of
                w Window.read-only? if 
                    ncurses.bell drop exit 
                then
                Keymap.playing? not if
                    Keymap.reset-recording
                    n 1 > if n (.) Keymap.append-string then
                    s" i" Keymap.append-string
                then
                w API.i
                exit
            endof
            ascii I of
                w Window.read-only? if 
                    ncurses.bell drop exit 
                then
                Keymap.playing? not if
                    Keymap.reset-recording
                    n 1 > if n (.) Keymap.append-string then
                    s" I" Keymap.append-string
                then
                w Window.beginning-of-line
                w API.i
                exit
            endof
            ascii a of
                w Window.read-only? if 
                    ncurses.bell drop exit 
                then
                Keymap.playing? not if
                    Keymap.reset-recording
                    n 1 > if n (.) Keymap.append-string then
                    s" a" Keymap.append-string
                then
                w API.a
                exit
            endof
            ascii A of
                w Window.read-only? if 
                    ncurses.bell drop exit 
                then
                Keymap.playing? not if
                    Keymap.reset-recording
                    n 1 > if n (.) Keymap.append-string then
                    s" A" Keymap.append-string
                then
                w Window.end-of-line
                w API.a
                exit
            endof
            \ undo/redo

            ascii u of          \ undo
                w s@ Window.buffer Buffer.undo
                w Window.fix-cursor
                exit
            endof

            ncurses.ctrl_r of   \ redo
                w s@ Window.buffer Buffer.redo
                exit
            endof

            \ default
            MessageLine.move-to ch hex . decimal
        endcase
    again
;
