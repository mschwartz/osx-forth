anew task-lib/debug.fth

\ variable _debug 0 _debug !
\ : debug _debug @ ;
: debug@ debug @ ;
: debug! debug ! ;
: debug-on true debug! ;
: debug-off false debug! ;
: debug{ debug-on ;
: }debug debug-off ;

: die 0 sys::exit ;
[defined] ncurses-uninstall [if] 
: die{ ncurses-uninstall ; 
: panic ( caddr u --  print message and exit program )
    ncurses-uninstall type .s 5000 msec 0 sys::exit
;
[then]
[undefined] ncurses-uninstall [if] 
: die{ ; 
: panic ( caddr u --  print message and exit program )
    type .s 5000 msec 0 sys::exit
;
[then]
: }die 0 sys::exit ;

