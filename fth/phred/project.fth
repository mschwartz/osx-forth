 \
\ PHRed editor
\
\ programmed by Mike Schwartz
\
\ Project words
\

\ Projects are driven by a .project file in the current directory - or the directory where
\ the git repo root is.

\ The project file keeps track of current window layout, buffers loaded, which buffers are
\ shown in windows, current search/replace pattern.

\ The project file maintains a list of @path style aliases, where any @<path> can be 
\ aliased to a subdiretory of the project.  For example, @lib mapped to fth/lib and
\ then require/include/etc. can use @lib:
\           require @lib/whatever.fth

\ The project file is read in at startup and is maintained by Phred.  It shouldn't be 
\ edited by the user, though it will be human readable.

\ Project file format:
\  \ comment
\  # comment
\  // comment
\ filename <name>
\ include-path <pathspec>
\ search-pad <pattern>
\ nerdtree-active TRUE|FALSE
\ buffer <filename>
\ window <line#> <col#> <filename>
\ pathalias <key> <value>

:STRUCT ProjectIncludePath
    struct ListNode ProjectIncludePath.node
    APTR ProjectIncludePath.path
;STRUCT

: ProjectIncludePath.destroy { pi -- , release resources }
    pi List.remove
    pi s@ ProjectIncludePath.path ?dup if mem::free then
    pi ?dup if mem::free then
;
: ProjectIncludePath.dump { pi -- , dump project include }
    cr ." ProjectIncludePath $ " pi .hex cr
    pi s@ ProjectIncludePath.path ztype ."  "
;

:STRUCT ProjectBuffer
    struct ListNode ProjectBuffer.node
    APTR ProjectBuffer.filename
;STRUCT

: ProjectBuffer.dump { pb -- , dump project buffer }
    cr ." ProjectBuffer $ " pb .hex cr
    ."       filename: " pb s@ ProjectBuffer.filename ztype cr
;

: ProjectBuffer.destroy { pb -- , release resources }
    pb List.remove
    pb s@ ProjectBuffer.filename ?dup if mem::free then
    pb ?dup if mem::free then
;

private{
    2048 constant project-padsize
    create project-pad project-padsize allot
    create project-pad2 project-padsize allot
}private

: ProjectIncludePath.new { caddr u | inc-path -- inc-path , new ProjectIncludePath }
    sizeof() ProjectIncludePath mem::malloc -> inc-path
    caddr u project-pad place-cstr 
    project-pad string::strdup inc-path s! ProjectIncludePath.path
    inc-path
;

: ProjectBuffer.new { filename | pbuf -- pbuf , allocate and initialize ProjectBuffer }
    sizeof() ProjectBuffer mem::malloc -> pbuf
    filename string::strdup pbuf s! ProjectBuffer.filename
;

:STRUCT ProjectWindow
    struct ListNode ProjectWindow.node
    APTR ProjectWindow.filename
    ULONG ProjectWindow.line#
    ULONG ProjectWindow.col#
;STRUCT

: ProjectWindow.dump { pw -- , dump project window }
    cr ." ProjectWindow $ " pw .hex cr
    ."       filename: " pw s@ ProjectWindow.filename ztype cr
    ."          line#: " pw s@ ProjectWindow.line# . cr
    ."           col#: " pw s@ ProjectWindow.col# . cr
;

: ProjectWindow.new { filename | pw -- pw , new ProjectWindow with given filename }
    sizeof() ProjectWindow mem::malloc -> pw
    filename string::strdup pw s! ProjectWindow.filename
    0 s! ProjectWindow.line#
    0 s! ProjectWindow.col#
    pw
;
: ProjectWindow.destroy { pw -- , release resources }
    pw List.remove
    pw s@ ProjectWindow.filename ?dup if mem::free then
    pw ?dup if mem::free then
;

:STRUCT ProjectAlias
    struct ListNode ProjectAlias.node
    APTR ProjectAlias.key       \ key as in z" @lib"
    APTR ProjectAlias.val       \ value as in z" fth/lib"
;STRUCT

: ProjectAlias.dump { pa -- , dump project alias }
    cr ." Project Alias $ " pa .hex cr
    pa s@ ProjectAlias.key ztype ."  "
    pa s@ ProjectAlias.val ztype cr
;

: ProjectAlias.destroy { pa -- , release resources }
    pa List.remove
    pa s@ ProjectAlias.key ?dup if mem::free then
    pa s@ ProjectAlias.val ?dup if mem::free then
    pa ?dup if mem::free then
;

: ProjectAlias.new { k v | pa -- pa , create new ProjectAlias from key and value }
    sizeof() ProjectAlias mem::malloc -> pa
    k string::strdup pa s! ProjectAlias.key
    v string::strdup pa s! ProjectAlias.val
    pa
;

:STRUCT Project
    APTR Project.filename       \ filename of project file
    struct List Project.alias-list     \ list of aliases
    struct List Project.buffer-list
    struct List Project.window-list
    struct List Project.include-path-list
    APTR Project.search-pad
    BOOL Project.nerdtree-active
;STRUCT

: ProjectAlias.print { pa -- , print project alias }
    ." : " pa s@ ProjectAlias.key zcount type ."  = " pa s@ ProjectAlias.val zcount type ."  "
;
: Project.print-aliases { proj -- , }
    proj Project.alias-list + ['] ProjectAlias.print List.iterate
;

: ProjectAlias.remove { proj zkey | keystone current -- , delete project alias }
    proj Project.alias-list +  -> keystone
    keystone s@ List.next -> current
    begin
        current keystone <>
    while
        current s@ ProjectAlias.key zkey string::strcmp 0= if
            current ProjectAlias.destroy
            messageline{ ." Removed pathalias " zkey zcount type }messageline
            exit
        then
        current s@ ListNode.next -> current
    repeat
    errorline{ ." *** PATHALIAS " zkey zcount type ."  NOT FOUND "}errorline
;

: (Project.dump) { proj | nod -- , dump project in human readable form }
    cr ." Project at $ " proj .hex cr
    proj 0= if
        cr ."  null! " cr
        exit
    then
    cr ."           filename: " proj s@ Project.filename ztype cr
    cr ."         search-pad: " proj s@ Project.search-pad ?dup if ztype then cr
    cr ."    nerdtree-active: " proj s@ Project.nerdtree-active if ." TRUE " else ." FALSE " then cr 

    ."          include-path:" cr
    proj Project.include-path-list + ['] ProjectIncludePath.dump List.iterate

    ."            aliases:" cr
    proj Project.alias-list + ['] ProjectAlias.dump List.iterate

    ."            buffers:"  cr
    proj Project.buffer-list + ['] ProjectBuffer.dump List.iterate

    ."            windows:"  cr
    proj Project.window-list + ['] ProjectWindow.dump List.iterate
;
' (Project.dump) is Project.dump

: Project.destroy { proj | nod -- , destroy Project }
    proj  Project.buffer-list + ['] ProjectBuffer.destroy  List.iterate
    proj  Project.window-list + ['] ProjectWindow.destroy List.iterate
    proj  Project.alias-list + ['] ProjectAlias.destroy List.iterate
    proj  Project.include-path-list + ['] ProjectIncludePath.destroy List.iterate
    proj mem::free
;

: Project.clear { proj | nod -- , clear window and buffer list }
    proj Project.buffer-list + ['] ProjectBuffer.destroy List.iterate
    proj Project.window-list + ['] ProjectWindow.destroy List.iterate
;

: ProjectIncludePath.clear { proj -- , clear project include path }
    proj  Project.include-path-list + ['] ProjectIncludePath.destroy List.iterate
;
: ProjectIncludePath.print { pi -- , print project alias }
    ." :" pi s@ ProjectIncludePath.path zcount type cr
;
: Project.print-include-path { proj -- , }
    proj Project.include-path-list + ['] ProjectIncludePath.print List.iterate
;

: Project.add-include-path { proj caddr -- , set include-path to caddr }
        caddr zcount ProjectIncludePath.new
        proj Project.include-path-list +
    List.addtail
;

: ProjectIncludePath.reset { proj -- , clear project include path }
    proj ProjectIncludePath.clear
    s" fth" project-pad place-cstr
    proj project-pad Project.add-include-path
    s" fth/lib" project-pad place-cstr
    proj project-pad Project.add-include-path
;

: Project.add-buffer { proj caddr | pb -- , new project Buffer }
    sizeof() ProjectBuffer mem::malloc -> pb
    pb ProjectBuffer.node + List.init
    caddr string::strdup pb s! ProjectBuffer.filename
    pb proj Project.buffer-list + List.addtail
;

: Project.add-window { proj caddr line# col# | pw -- , new project Buffer }
    sizeof() ProjectWindow mem::malloc -> pw
    caddr string::strdup pw s! ProjectWindow.filename
    line# pw s! ProjectWindow.line#
    col# pw s! ProjectWindow.col#
    pw proj Project.window-list + List.addtail
;

: Project.add-alias { proj k val | pa -- , add key/value to proj alias list }
    sizeof() ProjectAlias mem::malloc -> pa
    k string::strdup  pa s! ProjectAlias.key
    val string::strdup  pa s! ProjectAlias.val
    pa proj Project.alias-list + List.addtail
;

: (Project.load) { proj | frst fp flg str tok toku tok2 toku2 line# col# -- , load in a project }
    true -> frst
    proj nullptr = if exit then
    z"  " 1+ CString.new -> str

    z" r" 1+ proj s@ Project.filename sys::fopen -> fp
    fp 0= if
        exit
        die{ ." Can't load project file " proj s@ Project.filename ztype cr }die
    then

    false -> flg
    begin
        fp sys::feof if 
            fp sys::fclose drop  exit 
        then
        str fp CString.fgets ?dup 0= if
            fp sys::fclose drop  exit 
        then
    while
        str CString.parse-token -> toku -> tok
        str CString.skip-blanks
        tok toku s" FILENAME" compare 0= if
            proj s@ Project.filename ?dup if mem::free then
            str CString.zrest 
            string::strdup proj s! Project.filename
            true -> flg
        then
        tok toku s" INCLUDE-PATH" compare 0= if
            frst if proj ProjectIncludePath.clear then
            false -> frst
            proj str CString.zrest string::strdup Project.add-include-path
            true -> flg
        then
        tok toku s" SEARCH-PAD" compare 0= if
            proj s@ Project.search-pad ?dup if mem::free then
            str CString.zrest string::strdup proj s! Project.search-pad
            true -> flg
        then
        tok toku s" NERDTREE-ACTIVE" compare 0= if
            str CString.parse-token -> toku2 -> tok2
            tok2 toku2 s" true" compare 0= if true else false then
            proj s! Project.nerdtree-active
            true -> flg
        then
        tok toku s" BUFFER" compare 0= if
            proj str CString.zrest Project.add-buffer
            true -> flg
        then
        tok toku s" WINDOW" compare 0= if
            true -> flg
            str CString.parse-number if
                -> line#
            else
                false -> flg
                errorline{ ." *** Project Window missing line# "}errorline
            then
            str CString.skip-blanks
            str CString.parse-number  if
                -> col#
            else
                false -> flg
                errorline{ ." *** Project Window missing col# "}errorline
            then
            str CString.skip-blanks
            proj str CString.zrest line# col# Project.add-window
        then
        tok toku s" PATHALIAS" compare 0= if
            str CString.parse-token -> toku2 -> tok2
            tok2 toku2 project-pad place-cstr

            str CString.parse-token -> toku2 -> tok2
            tok2 toku2 project-pad2 place-cstr

            proj project-pad project-pad2 Project.add-alias
            true -> flg
        then
        tok toku s" #" compare 0= if
            \ ignore # comments
            true -> flg
        then
        tok toku s" \" compare 0= if
            \ ignore \ comments
            true -> flg
        then
        tok toku s" //" compare 0= if
            \ ignore // comments
            true -> flg
        then
        toku  0= if
            \ ignore // comments
            true -> flg
        then
        
        flg false = if
            errorline{ ." *** Project Window syntax error " str CString.zstring ztype }errorline
        then
    repeat

    fp sys::fclose drop
;
' (Project.load) is Project.load

: Project.construct { proj caddr -- , construct project }
    caddr Path.realpathz string::strdup proj s! Project.filename
    proj Project.alias-list + List.init
    proj Project.buffer-list + List.init
    proj Project.window-list + List.init
    proj Project.include-path-list + List.init
    proj ProjectIncludePath.reset
\     s" fth" project-pad place-cstr
\     proj project-pad Project.add-include-path
\     s" fth/lib" project-pad place-cstr
\     proj project-pad Project.add-include-path
\     die{ ." here " proj .hex .s proj project.dump }die
    s" " project-pad place-cstr project-pad string::strdup proj s! Project.search-pad
\     s" fth:fth/lib" project-pad place-cstr project-pad string::strdup proj s! Project.include-path
    false proj s! Project.nerdtree-active
;

: Project.new { caddr | proj -- proj , create new Project instance }
    sizeof() Project mem::malloc -> proj
    proj caddr Project.construct 
    proj
;

: Project.find { | proj -- proj , search for .project file in current directory and up }
    project-pad project-padsize Path.getcwd project-pad string::strcpy drop
    begin
        z" .project" 1+ Path.file-exists? if
            project-pad Path.chdir drop
            z" .project" 1+ Path.realpathz Project.new -> proj
            proj Project.load 
            proj exit
        then
        
        z" .git" 1+ Path.file-exists? if
            z" .project" 1+ Path.realpathz Project.new -> proj
            proj Project.load 
            proj exit
        then
        z" .." 1+ Path.chdir drop
    again
;

: (Project.save) { proj | fp lst nod -- , save project file }
    z" w" 1+ proj s@ Project.filename sys::fopen -> fp
    fp 0= if
        die{ ." *** Can't open for write project file " proj s@ Project.filename ztype cr }die
        exit
    then
    s" FILENAME        " project-pad place-cstr
    project-pad proj s@ Project.filename  string::strcat drop

    project-pad fp String.fputs 0= if
        die{ ." *** Error write project filename " proj s@ Project.filename ztype }die
        fp sys::fclose drop exit
    then

    s" SEARCH-PAD      " project-pad place-cstr
    project-pad proj s@ Project.search-pad  string::strcat drop
    project-pad fp String.fputs 0= if
        die{ ." *** Error write project search-pad " proj s@ Project.search-pad ztype }die
        fp sys::fclose drop exit
    then
    proj s@ Project.nerdtree-active if z" NERDTREE-ACTIVE TRUE" else z" NERDTREE-ACTIVE FALSE" then
    1+ fp String.fputs 0= if
        die{ ." *** Error write project nerdtree-active " }die
        fp sys::fclose drop exit
    then



\     s" INCLUDE-PATH    " project-pad place-cstr
\     project-pad proj s@ Project.include-path  string::strcat drop
\     project-pad fp String.fputs 0= if
\         die{ ." *** Error write project include-path " proj s@ Project.include-path ztype }die
\         fp sys::fclose drop exit
\     then


    \ write include-path-list
    proj Project.include-path-list + -> lst
    lst s@ ListNode.next -> nod
    begin
        lst nod <>
    while
        s" INCLUDE-PATH    " project-pad place-cstr
        project-pad nod s@ ProjectIncludePath.path string::strcat drop
        project-pad fp String.fputs 0= if
            die{ ." *** Error write project include-path" }die
            fp sys::fclose drop exit
        then
        nod s@ ListNode.next -> nod
    repeat


    \ write alias-list
    proj Project.alias-list + -> lst
    lst s@ ListNode.next -> nod
    begin
        lst nod <>
    while
        s" PATHALIAS " project-pad place-cstr
        project-pad nod s@ ProjectAlias.key string::strcat drop
        project-pad z"  " 1+ string::strcat drop
        project-pad nod s@ ProjectAlias.val string::strcat drop
        project-pad fp String.fputs 0= if
            die{ ." *** Error write project alias" }die
            fp sys::fclose drop exit
        then
        nod s@ ListNode.next -> nod
    repeat

    \ write buffer-list
    proj Project.buffer-list + -> lst
    lst s@ ListNode.next -> nod
    begin
        lst nod <>
    while
        s" BUFFER " project-pad place-cstr 
        project-pad nod s@ ProjectBuffer.filename string::strcat drop
        project-pad fp String.fputs 0= if
            die{ ." *** Error write project buffer" }die
            fp sys::fclose drop exit
        then
        nod s@ ListNode.next -> nod
    repeat

    \ write window-list
    proj Project.window-list + -> lst
    lst s@ ListNode.next -> nod
    begin
        lst nod <>
    while
        s" WINDOW " project-pad place-cstr 
        nod s@ ProjectWindow.line# (u.) project-pad2 place-cstr
        project-pad project-pad2 string::strcat drop
        project-pad bl String.append.char
        nod s@ ProjectWindow.col# (u.) project-pad2 place-cstr
        project-pad project-pad2 string::strcat drop
        project-pad bl String.append.char
        project-pad nod s@ ProjectWindow.filename string::strcat drop
        project-pad fp String.fputs 0= if
            die{ ." *** Error write project buffer" }die
            fp sys::fclose drop exit
        then
        nod s@ ListNode.next -> nod
    repeat
    fp sys::fclose drop
;
' (Project.save) is Project.save

: (Project.update) { proj | nod lst nam -- , update project from current Phred state }
    proj Project.clear
    \ iterate buffers and make new buffer-list
    Bufferlist.list -> lst
    lst -> nod
    begin
        nod s@ ListNode.next -> nod
        nod lst <>
    while
        nod s@ Buffer.permanent 0= if
                proj 
                nod s@ Buffer.fullpath  
            Project.add-buffer
        else
        then
    repeat

    Windowlist.list -> lst
    lst s@ List.next -> nod
    begin
        nod lst <>
    while
        nod s@ Window.buffer s@ Buffer.fullPath ?dup if
            -> nam
        else
            nod s@ Window.buffer s@ Buffer.name -> nam
        then
        nod s@ Window.type WindowType.editor = if
                proj 
                nam
                nod s@ Window.line# nod Window.current-line-index
            Project.add-window
        then
        nod s@ ListNode.next -> nod
    repeat

    \ copy search-pad
    proj s@ Project.search-pad ?dup if mem::free then
    search-pad string::strdup proj s! Project.search-pad

    \ update nerdtree-active
    nerdtree-active @ proj s! Project.nerdtree-active
;

' (Project.update) is Project.update

privatize