\
\ Arrays functions
\
\ Programmed by Mike Schwartz
\
\ Using Array.push and Array.pop, an array can be used as a stack
\ Using Array.unshift and Array.shift an array can be used as a queue
\

anew task-lib/arrays.fth

require sys/memory.fth

:STRUCT Array
    APTR Array.data     \ allocated array
    ULONG Array.size    \ size of allocated array (in cells)
    ULONG Array.next    \ next unused index into array
;STRUCT

: Array.dump { ary -- , print array }
    cr ." Array at $" ary .hex
    cr ."    .data $" ary s@ Array.data .hex
    cr ."    .size " ary s@ Array.size .
    cr ."    .next " ary s@ Array.next .
    ary s@ Array.data ary s@ Array.size cells dump
;

: Array.construct { ary num-elements -- , construct array }
    num-elements cell * mem::malloc ary s! Array.data
    num-elements ary s! Array.size
    0 ary s! Array.next
;
: Array.new { num-elements | ary -- new-array , create new array }
    sizeof() Array MEM::malloc -> ary
    ary num-elements Array.construct
    ary
;

: Array.destroy { destructor a | ptr -- , destroy array }
    a s@ Array.data  -> ptr
    a s@ Array.next ?dup if 
        0 do
            ptr @ destructor
            ptr cell+ -> ptr
        loop
    then
;

: Array.length { ary -- len , get length of array }
    ary s@ Array.next
;

: Array.empty? { ary -- flg , is array empty? }
    ary Array.length 0=
;

: Array.sizeof { ary -- size , get size of array }
    ary s@ Array.size
;

: Array.resize { ary new-size -- , resize array to new-ssize cells }
    ary s@ Array.data new-size cells MEM.realloc
    ary s! Array.data
    new-size ary s! Array.size
;

: Array.need { ary needed -- , resize array to fit needed elements }
    needed 0< if exit then
    ary Array.size ary Array.length - needed cells < if
        ary ary Array.size needed + Array.resize 
    then
;

: Array.get { ary ndx -- value , get value from array at index }
    ndx ary s@ Array.next > if
        ERR_RESULT_OUT_OF_RANGE THROW 
    then
    ary s@ Array.data
    ndx cells +
    @
;

\ setting an index greater than array.next will set array.next to that index
\  which leaves sevearl unused or uninitialized indecies between.
\ if index is greater than the array size, the array is resized accordingly
: Array.set { ary ndx val -- , set value in  array at index }
    ary Array.size ndx - Array.need
    ndx ary s@ Array.next > if
        ndx 1+ ary s! Array.next
    then
\     ndx ary s@ Array.size >= if
\         ary ary s@ Array.size 2*  Array.resize
\     then

    ary s@ Array.data
    ndx cells +
    val swap !
;

: Array.delete-at { arr ndx | src cnt d -- , remove element at ndx }
    ndx 0< if exit then                     \ invalid ndx
    arr s@ Array.data -> d
    d ndx cells + -> src                    \ address of cell to remove
    arr s@ Array.size ndx - 1- -> cnt       \ number of cells to move
    cnt 0> if
        cnt 0 do
            src cell+ @ src !
            src cell+ -> src
        loop
        arr s@ Array.next 1- arr s! Array.next
    then
;

: Array.insert-at { arr ndx val | src dst -- , insert val at ndx in arr }
    arr 1 Array.need
    arr s@ Array.data arr Array.length cells + -> src
    src 1 cells - -> dst
    arr Array.size ndx do
        src @ dst !
        src cell +  -> src
        dst cell +  -> dst
    loop
    arr ndx val Array.set
;

: Array.push { ary val -- , push value onto array, grow it if necessary }
    ary s@ Array.next ary s@ Array.size >= if
        ary ary s@ Array.size 2*  Array.resize
    then
    ary s@ Array.data ary s@ Array.next cells + val swap !
    ary s@ Array.next 1+ ary s! Array.next
;

: Array.pop { ary -- val , pop value from last element in array }
    ary s@ Array.next ?dup if
        1- ary s! Array.next
    else
        ERR_RESULT_OUT_OF_RANGE THROW 
    then
    ary s@ Array.data
    ary s@ Array.next cells +
    @
;

: Array.shift { ary | val -- val , get first element and shift rest down }
    ary 0 Array.get -> val
    ary 0 Array.delete-at
    val
;

: Array.unshift { ary val -- , add val to beginning of array and shift up one }

;

: Array.peek { ary | nxt -- val , peek at value from last element in array }
    ary s@ Array.next ?dup if
        1- -> nxt
    else
        ERR_RESULT_OUT_OF_RANGE THROW 
    then
    ary s@ Array.data
    nxt cells +
    @
;

: Array.index-of { ary val | d -- ndx or -1 }
    ary s@ Array.data -> d
    ary s@ Array.next 0 do
        d i cells + @ val = if
            i exit
        then
    loop
    -1
;

: Array.iterate { arr 'callback | d -- , iterate over array }
    arr s@ Array.data -> d
    arr s@ Array.next 0 do
        d @ 'callback execute false if exit then
        d cell+ -> d
    loop
;

: Array.iterate< { arr 'callback | d -- , iterate over array backwards }
    arr s@ Array.data arr Array.length 1- cells * + -> d
    arr s@ Array.next 0 do
        d @ 'callback execute false if exit then
        d cell- -> d
    loop
;