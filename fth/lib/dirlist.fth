anew task-lib/dirlist.fth

require sys/path.fth
require lib/lists.fth
require lib/c-strings.fth

cr ." compilling dirlsit.fth " cr

Defer DirList.dump

private{
statbuf stbuf 
create dname 512 allot
create dirlist-pad 4096 allot
}private

:STRUCT DirNode
    struct ListNode DirNode.node
    APTR DirNode.name
    APTR DirNode.fullpath
    UINT64 DirNode.type
    UINT64 DirNode.flags
    APTR DirNode.dirent                   \ file/directory dirent
    APTR DirNode.statbuf                  \ file/directory statbuf
    APTR DirNode.parent
    struct List DirNode.children
;STRUCT

: DirNode.new { de sb | p  n -- n , create new DiredNode from dirent and statbuf }
    sizeof() DirNode MEM.malloc -> n
    sizeof() Dirent MEM.malloc -> p
    p n s! DirNode.dirent 
    0 n s! DirNode.parent
    0 n s! DirNode.flags

    de p sizeof() Dirent cmove
    p n s! DirNode.dirent

    sizeof() statbuf MEM.malloc -> p
    sb p sizeof() statbuf cmove
    p n s! DirNode.statbuf
    n DirNode.children + List.init
    n
;

defer DirNode.destroy
: (DirNode.destroy) { n -- , destroy node }
    n List.remove
    n s@ DirNode.statbuf MEM.free
    n s@ DirNode.dirent MEM.free
    n DirNode.children + ['] DirNode.destroy List.iterate
    n MEM.free
;
' (DirNode.destroy) is DirNode.destroy

: DirList.destroy-list { lst -- , destroy all nodes in DirWindow.List }
    lst  ['] DirNode.destroy List.iterate
;

: sort-insert { n sorted-list | nam p -- , find place to insert node and insert it }
    n s@ DirNode.name -> nam
    sorted-list s@ List.next -> p
    begin
        p sorted-list <>
    while
        nam p s@ DirNode.name string::strcmp 0> if
            \ nam < DirNode
            n p List.InsertBefore \ insert n before p
            exit
        then
        p s@ ListNode.next sorted-list = if
            n p List.InsertAfter
            exit
        then
        p s@ ListNode.next -> p
    repeat
;

: DirList.sort { dir-list | n nxt -- , sort dir-list by filename }
    dir-list s@ List.next -> n
    dir-list List.init 
    begin
        n dir-list <>
    while
        n s@ ListNode.next -> nxt
        n List.remove
        n dir-list sort-insert
        nxt -> n
    repeat
;

defer DirList.read
: (DirList.read) { lst caddr u recurse? | dir e n -- f , read directory for path into lst after freeing list }
    caddr u dirlist-pad place-cstr
    dirlist-pad Path.opendirz -> dir
    dir 0= if
        cr caddr u type c" opendir " sys::perror
        false exit
    then
    lst DirList.destroy-list

    begin
        dir Path.readdir -> e
        e
    while
        caddr u dname zplace 
        dname z" /" 1+ string::strcat drop
        dname e sys::d_name 
        string::strcat drop

        dname stbuf Path.statz
        0< if
            cr cr cr dname zcount type cr c" *** stat " sys::perror
            dir Path.closedir drop
            false exit
        then
        e stbuf DirNode.new -> n
        e sys::d_name string::strdup n s! DirNode.name
        dname string::strdup n s! DirNode.fullpath
        e sys::d_type n s! DirNode.type
        n lst List.addTail
        recurse? if
            n s@ DirNode.type DT_DIR = if
                n s@ DirNode.name z" ." 1+ string::strcmp 0<> if
                    n s@ DirNode.name z" .." 1+ string::strcmp 0<> if
                        caddr u dirlist-pad place-cstr
                        dirlist-pad z" /" 1+  string::strcat drop
                        dirlist-pad n s@ DirNode.name  
                        string::strcat drop
                            n DirNode.children +    \ lst
                            dirlist-pad zcount       \ caddr u
                            recurse?
                        DirList.read drop
                    then
                then
            then
        then
    repeat
    dir Path.closedir drop
    lst DirList.sort
    true
;
' (DirList.read) is DirList.read

defer DirNode.dump
: (DirNode.dump) { n -- , print node }
    cr ." DirNode $" n .hex
    cr ."       name: " n s@ DirNode.name ztype
    cr ."       type: " n s@ DirNode.type .
    cr ."   children: " 
    n DirNode.children + ['] DirNode.dump List.iterate
;

' (DirNode.dump) is DirNode.dump


: (DirList.dump) { lst -- , dump directory list }
    lst ['] DirNode.dump List.iterate
;
' (DirList.dump) is DirList.dump

defer DirNode.iterate
: (DirNode.iterate) { n 'handler -- , iterate all dnode and children }
    n 'handler execute
    n DirNode.children + 'handler List.iterate
;
' (DirNode.iterate) is DirNode.iterate

privatize

cr ." end dirlsit.fth " cr
