\
\ phred editor
\
\ programmed by Mike Schwartz
\
\ Command Line handling
\   :e
\   :e!
\   :e filename
\

anew task-phred/commands/edit.fth

: CommandLine.edit { w str tok toku | b -- flg , return true if executed }
    tok toku s" e" compare 0= if
        str CString.parse-token -> toku -> tok
        toku 0= if
            c" *** Filename expected" MessageLine.$type
            true exit
        then
        w tok toku WindowList.find-or-create drop
        true exit
    then

    tok toku s" e!" compare 0= if
        \ todo - reload existing file
        str CString.parse-token -> toku -> tok
        toku 0= if
            c" *** Filename expected" MessageLine.$type
            true exit
        then
        w tok toku WindowList.find-or-create drop
        true exit
    then

    tok toku s" edit" compare 0= if
        str CString.parse-token -> toku -> tok
        toku 0= if
            c" *** Filename expected" MessageLine.$type
            true exit
        then
        w tok toku WindowList.find-or-create 
        drop
        true exit
    then

    tok toku s" revert" compare 0= if
        w s@ Window.buffer -> b
        b Buffer.revert
        w b Window.set-buffer
        true exit
    then

    false
;
