#!/usr/local/bin/pforth_standalone -q

\
\ Demonstrate use of lower level network API.
\
\ Programmed by Mike Schwartz
\

false trace-include !

require net.fth
require net/netdb.fth
require lib/hashmap.fth

\ HTTP response headers are read into a HASHMAP
variable response-headers ' mem::free HASHMAP.new response-headers !

\ allocate a buffer into which we read lines 
1024 1024 * 4 * constant max-size       \ maximum line length
create headers-line max-size allot

\ we use this routine to look for the : in a line with a 'key: value' header in it
\ note that the first line is the status line and does not have a 'key: value'
: find-char { c strz | ptr  -- index ,  return index in string of char }
    strz -> ptr
    begin
        ptr c@
        dup 0= if   \ end of string, not found
            drop    \ drop character
            -1 exit
        then
        c = if
            ptr strz -
            exit             
        then
        ptr 1+ -> ptr
    again
;

\ Take a pointer to string and skip past any blanks and return pointer
\ to the string past the blanks.
: skip-blanks ( ptr -- ptr2 , skip blanks  )
    begin
        dup c@
        dup 0= if
            drop
            0 exit
        then
        bl <> if
            exit
        then
        1+
    again
;

\ Parse one line of text that is a key: value.
\ two counted strings are created within the line buffer itself: one 
\ is the key, the other is the value
: parse-header { | u1 caddr2 u2 -- , parse header from headers-line and add to response  headers }
    ascii : headers-line 1+ find-char -> u1
    u1 -1 = if 
        exit \ not a header!
    then

    u1 headers-line c!                          \ store length of the key
    headers-line u1 + 2+ skip-blanks -> caddr2
    caddr2 0= if
        exit  \ not a header
    then
    caddr2 sys::strlen caddr2 1- c!                  \ store length of the value
\     response-headers @ headers-line caddr2 HASHMAP.set 
\     drop
;

\ read headers from the response
: read-headers { fp -- , read headers from stream }
    begin
        fp sys::feof 0 <> if 
            exit
        then
        headers-line 1+ max-size 1- fp sys::fgets 
        0<> if 
            headers-line 1+ sys::strlen headers-line c!
            headers-line c@ 0= if
                exit
            then
            cr ." header line " headers-line count type
            parse-header
        then
    again
;

: demo { caddr u port | fd fp ip sockaddr -- , main program }
    0 SOCK_STREAM AF_INET NET.socket  -> fd
    cr cr cr ." lookup host " caddr u type ." ..."
    caddr 1- net::lookuphostname -> ip
    ." ip = " ip .ip-address ip ."  hex " .hex

    AF_INET ip port             \ family address port
    net::allocsockaddr           \  -- address sizeof
    swap -> sockaddr drop

    cr ." connecting..."
    sockaddr fd net::connect 
    0 < if
        drop
        sockaddr mem::free
        cr c" *** Error in connect() "  sys::perror
        0
        exit
    then
    ." TCPSocket connected, fd = " fd .  cr cr

    fd sys::TCP_NODELAY
    0< if
        c" no_delay " sys::perror
    then

    c" r+b" fd sys::fdopen -> fp

    cr ." sending request headers ..."
    c" GET / HTTP/1.0" fp sys::fputs drop 
    13 fp sys::fputc drop 
    10 fp sys::fputc drop
    13 fp sys::fputc drop 
    10 fp sys::fputc drop
    ." sent" .s

    cr ." getting response headers..."
    fp read-headers
    ." read headers "

    response-headers @ c" Content-Length" HASHMAP.get
    dup 0= if
        drop
        headers-line 1 max-size fp sys::fread
    else
        0 swap
        count  ( -- 0 number len )
        >number
        2drop
        headers-line swap 1 swap fp sys::read
    then
    ." read " dup . ." bytes"
    cr cr headers-line swap 500 min type
;

s" www.google.com" 80 demo
cr cr
