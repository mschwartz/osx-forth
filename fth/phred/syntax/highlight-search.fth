\ add highlight bytes to txt for search match
private{
    create pattern-pad 4096 allot
    create replace-pad 4096 allot
}private

: Highlight.search { txt search-pad | str -- new-text , highlight search matches in string }
    search-pad c@ 0= if 
        txt 
        exit 
    then
    search-pad zcount pattern-pad place

    txt CString.new -> str

    OPCODE.search replace-pad c!
    0 replace-pad 1+ c!
    replace-pad search-pad string::strcat drop
    replace-pad OPCODE.default String.append.char

    regex.default \ regex.ignore-case
    str pattern-pad  replace-pad  zcount CString.$regex_replace drop
    str CString.zstring string::strdup
    txt MEM.free
    str CString.destroy
;

privatize
