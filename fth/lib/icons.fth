\ fa-folder constant icon-folder
\ fa-folder-open constant icon-folder-open
fa-angle-double-right constant icon-folder
fa-angle-double-down constant icon-folder-open
fa-alternate-file constant icon-file
fa-link constant icon-link
fa-code-branch constant icon-branch 

: emit-icon ( icon-whatever -- , print icon to screen )
    print-wchar
;
