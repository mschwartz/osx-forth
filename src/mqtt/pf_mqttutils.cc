#include "pf_mqttutils.h"
#include <stdio.h>
#include <pthread.h>

static pthread_mutex_t mutex;
static MQTTMessage *head = nullptr;
static MQTTMessage *tail = nullptr;

MQTTMessage::MQTTMessage(uint64_t type) {
  this->next = nullptr;
  this->type = type;
  this->topic = nullptr;
  this->payload = nullptr;
}
MQTTMessage::~MQTTMessage() {
  delete this->topic;
  delete this->payload;
}

void onConnect(struct mosquitto *, void *opts, int rc);
void onConnect(struct mosquitto *, void *opts, int rc) {
  MQTTMessage *m = new MQTTMessage(MQTTMessageTypeConnect);
  addMessage(m);
}

void onDisconnect(struct mosquitto *mosq, void *opts, int rc) {
  MQTTMessage *m = new MQTTMessage(MQTTMessageTypeDisconnect);
  addMessage(m);
}

void onMessage(struct mosquitto *mosq, void *opts, const struct mosquitto_message *msg) {
  MQTTMessage *m = new MQTTMessage(MQTTMessageTypeMessage);
  m->topic = strdup(msg->topic);
  m->payload = strdup((const char *)msg->payload);
  addMessage(m);
  //  printf("onMessage topic=%s payload=%s\n", m->topic, m->payload);
}

// (*on_publish)(struct mosquitto *, void *, int)
void onPublish(struct mosquitto *mosq, void *opts, int mid) {
  MQTTMessage *m = new MQTTMessage(MQTTMessageTypePublish);
  addMessage(m);
  //  printf("onMessage topic=%s payload=%s\n", m->topic, m->payload);
}

void addMessage(MQTTMessage *m) {
  pthread_mutex_lock(&mutex);
  if (head == nullptr || tail == nullptr) {
    head = m;
    tail = m;
  }
  else {
    tail->next = m;
    tail = m;
  }
  pthread_mutex_unlock(&mutex);
}

MQTTMessage *mqttRemoveMessage() {
  for (;;) {
    pthread_mutex_lock(&mutex);
    MQTTMessage *m = head;
    if (m) {
      head = head->next;
      pthread_mutex_unlock(&mutex);
      return m;
    }
    pthread_mutex_unlock(&mutex);
    usleep(100);
  }
}

bool mqttHasMessage() {
  pthread_mutex_lock(&mutex);
  MQTTMessage *_head = head;
  pthread_mutex_unlock(&mutex);
  return _head != nullptr;
}

struct mosquitto *mosq = nullptr;
struct mosquitto *mqttInit() {
  head = nullptr;
  tail = nullptr;
  mosquitto_lib_init();
  mosq = mosquitto_new(nullptr, true, nullptr);
  mosquitto_connect_callback_set(mosq, onConnect);
  mosquitto_disconnect_callback_set(mosq, onDisconnect);
  mosquitto_message_callback_set(mosq, onMessage);
  mosquitto_publish_callback_set(mosq, onPublish);
  mosquitto_loop_start(mosq);
  return mosq;
}

void mqttCleanup() {
  mosquitto_loop_stop(mosq, true);
  mosquitto_lib_cleanup();
  while (head) {
    MQTTMessage *m = head;
    head = head->next;
    delete m;
  }
}
