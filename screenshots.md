# Phred (programming editor) Screenshots

Phred is an editor inspired by vim/nvim, but with special orientation towards forth and C/C++ editing.

Like vim, Phred has visual and insert mode, : command line, / search, Windows and Buffers.  There are several custom Window/Buffer types, as described below.

Phred also features undo/redo for edits made, and keyboard macros.  Visual commands can be prefixed by a number and commands are repeated that many times (typically).  For example, 10@a will run the @a macro 10 times.

## Starting Phred

This is the first screen you see when you start up Phred.

![image info](screenshots/first-screen.png)

## NERDtree sidebar

Phred features a toggle-able (^n) left sidebar that operates somewhat like NERDTree for vim.

![image info](screenshots/nerdtree.png)

## Window splits
Phred features horizontal window splits.  ^ws to split, ^wc to close current window, etc.  Each
split operates independently.

![image info](screenshots/bufed.png)

## Buffer editor

The Buffer Editor (custom buffer) allows you to see, select, and delete buffers in the editor.

![image info](screenshots/bufed.png)

## Directory editor

The Directory Editor (custom buffer) allows you to see, select/load files, and navigate in the file system.

![image info](screenshots/dired.png)

## Syntax highlighting

Phred features basic syntax highlighting for Forth, C & C++, Makefiles, markdown.

![image info](screenshots/forth-syntax.png)
![image info](screenshots/c++-syntax.png)

## WORDS and WORDS.LIKE

Special WORDS buffer that displays full set of words or words.like a substring

![image info](screenshots/worded.png)

## HELP

:help <word> at the command line opens a help window with all the words defined in the file that contains <word>.


![image info](screenshots/helped.png)

## GIT

:git at the command line opens a git window with git status and keybindings to perform common git operations.

You can also :commit and :push from the command line.

![image info](screenshots/gited.png)

## GREP

:grep [args...] at the command line opens a grep buffer with the files and line numbers where the pattern is matched.  You can select a line in the buffer to jump to the line in the file.

![image info](screenshots/greped.png)

## SELECT 

Phred features line-only selections and yank/paste.

![image info](screenshots/selections.png)
