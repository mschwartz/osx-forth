cr ." compiling demos/sdl"

include condcomp.fth
include sdl.fth
include sdl/surface.fth
include sdl/pixels.fth
include utils/dump_struct.fth

variable win 
variable surf 
variable format 

: fill-surface { surface color | row pixels w h pitch }
    surface s@ SDL_Surface.pixels -> pixels
    surface s@ SDL_Surface.w -> w
    surface s@ SDL_Surface.h -> h
    surface s@ SDL_Surface.pitch -> pitch
    cr ." fill-surface $" surface .hex ." pixels $" pixels .hex ." w,h " w . ." , " h . ." color $" color .hex
    h 0 do
        pixels -> row
        w 0 do
            color row l!
            row 4 + -> row
        loop
        pixels pitch + -> pixels
    loop
;

: demo
    c" testing sdl" 800 800 800 600 SDL_WINDOW_SHOWN  window.new
    win !

    win @ window.show

    win @ Window.surface 
    surf !

    surf @ 0= if
        cr ." surface is null " SDL_GetError ztype
    else
        surf @ dump-surface
    then

    surf @ s@ SDL_Surface.format format !
    cr cr format @ dump-pixelformat

    surf @ surface.lock 
    0 <> if
        cr ." failed to lock surface " SDL_GetError ztype
        exit
    then
    surf @ $ 00ff00 fill-surface
    surf @ surface.unlock

    win @ Window.UpdateWindowSurface
    0 <> if
        cr ." update returned " SDL_GetError ztype
    then
    begin
        sdl_pollevent
        dup 0 <> if
            CommonEvent.type + l@ 
            dup case
                SDL_EVENT_MOUSEMOTION of endof
                SDL_EVENT_MOUSEBUTTONDOWN of
                    drop
                    cr ." got mouse down"
                    surf @ surface.lock 
                    0 <> if
                        cr ." failed to lock surface " SDL_GetError ztype
                        exit
                    then
                    surf @ $ ff0000 fill-surface
                    surf @ surface.unlock
                    win @ Window.UpdateWindowSurface
                endof
                SDL_EVENT_MOUSEBUTTONUP of
                    drop
                    cr ." got mouse up"
                    surf @ surface.lock 
                    0 <> if
                        cr ." failed to lock surface " SDL_GetError ztype
                        exit
                    then
                    surf @ $ 0000ff fill-surface
                    surf @ surface.unlock
                    win @ Window.UpdateWindowSurface
                endof
                SDL_EVENT_QUIT of endof
                cr dup ." got event " .hex 
            endcase
        then
        SDL_EVENT_QUIT = 
    until

    win @ 0 <> if
        win @ window.destroy
        0 win !
    then
    sdl_quit
    decimal
;