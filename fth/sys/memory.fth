anew task-sys/memory.fth

: MEM.malloc ( size -- mem , allocate memory using malloc )
    mem::malloc
;
: MEM.realloc ( mem size -- data , reallocate memory using realloc )
    mem::realloc
;
: MEM.free ( data -- , free memory )
    mem::free
;
