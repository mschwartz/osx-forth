require lib/c-strings.fth
require lib/json.fth


: demo { | jo str }
    CString.new-empty -> str
cr ." JSON DEMO " cr cr
    JSON.new -> jo
    c" number" 1033 JSON# 
    c" string" s" a STRING"  JSON"
    c" obj" JSON{ 
        c" obj-number" 4033 JSON# 
    }JSON
\     jo JSON.dump
cr
    str jo JSON.stringify 
    cr ." RESULT " cr
    str CString.string type
    cr hex .s 0 sys::exit
;

demo
