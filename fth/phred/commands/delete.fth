\
\ phred editor
\
\ programmed by Mike Schwartz
\
\ Command Line handling
\   :d
\

anew task-phred/commands/delete.fth

: CommandLine.delete { w str tok toku | lin -- flg , return true if executed }
    tok toku s" d" compare 0= if
        CommandLine-start @ CommandLine-end @ <> if
            w CommandLine-start @ Window.goto-line
            CommandLine-end @ CommandLine-start @ - 1+ 0 do
                w window.delete-line
            loop
        else
            w window.delete-line
        then
        true exit
    then
    false
;
