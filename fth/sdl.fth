anew task-sdl.fth

\ trace-include @
\ 0 trace-include !

require lib/lists.fth

require sdl/blendmode.fth
require sdl/video.fth
require sdl/rect.fth
require sdl/render.fth

require sdl/keyboard.fth
require sdl/keycodes.fth
require sdl/scancodes.fth
require sdl/pixels.fth
require sdl/surface.fth
require sdl/events.fth

\ trace-include !

:STRUCT Window
    STRUCT ListNode Window.Node
    LONG Window.Handle
    LONG Window.WindowID
    BOOL Window.Shown
;STRUCT

List sdl-window-list
sdl-window-list List.init 

: window.Flash { sdl-window flash-type -- , flash window }
    sdl-window s@ Window.handle SDL_FlashWindow
;

: window.GetData { sdl-window name -- data , get arbitrary window data at name }
    sdl-window s@ Window.handle name SDL_SetWindowData
;

: window.SetData { sdl-window data name -- , set window data at name to data }
    sdl-window s@ Window.handle data name SDL_SetWindowData
;

: window.Show { sdl-window -- , show window }
    sdl-window s@ Window.shown if
        sdl-window s@ Window.handle
\        cr ." show " dup .hex
        SDL_ShowWindow
    else
        sdl-window s@ Window.handle
\        cr ." raise " dup .hex
        SDL_RaiseWindow
    then
    true sdl-window s! Window.shown
;

: window.Hide { sdl-window -- , hide window}
    sdl-window s@ Window.shown if
        false sdl-window s! Window.shown
        sdl-window s@ Window.handle SDL_HideWindow
    then
;

: window.Raise { sdl-window -- , raise window }
    sdl-window s@ Window.shown if
        false sdl-window s! Window.shown
        sdl-window s@ Window.handle SDL_RaiseWindow
    then
;

: window.Close { sdl-window -- }
    sdl-window List.Removed? not if
        sdl-window List.Remove
    then
    sdl-window s@ Window.shown if
        false sdl-window s! Window.Shown
        sdl-window s@ Window.Handle SDL_DestroyWindow
    then
;

\ initialize a window and return it
: window.Init { sdl-window title x y width height flags -- window }
\    cr ." start window.Init " .s
    SDL_Init
    0 <> if 
        cr ." Error in SDL_Init " SDL_GetError ztype
        exit
    then
\    cr ." window.Init 1 " .s
    title x y width height flags SDL_CreateWindow
\    cr ." window.Init 2 " .s
    sdl-window s! Window.handle
\    cr ." window.Init 3 " .s
    sdl-window s@ Window.handle SDL_GetWindowID
    sdl-window s! Window.WindowID
    flags SDL_WINDOW_SHOWN AND 0 <> sdl-window s! Window.shown
\    cr ." window.Init 5 " .s
    sdl-window sdl-window-list List.AddTail
\    cr ." window.Init 6 " .s
    sdl-window
\    cr ." end window.Init " .s
;

: window.HasSurface ( window -- f , has window surface )
    s@ Window.handle SDL_HasWindowSurface
;

: window.Surface ( window -- surface , get window surface)
    s@ Window.handle SDL_GetWindowSurface
;
: window.UpdateWindowSurface ( window -- , update window surface)
    s@ Window.handle 
    SDL_UpdateWindowSurface
;

\ allocate a window, initialize it, and return it
: window.New { title x y width height flags | win -- window , allocate window }
\    cr ." start new " .s
    sizeof() window mem::malloc -> win
\    cr ." new 1 " .s
    win List.Init 
\    cr ." new 2 " .s
    win title x y width height flags window.Init
\    cr ." end new " .s
;

: window.Destroy { win -- , destroy allocated window }
    win window.close
    win mem::free
;

: surface.lock ( surf -- , lock window surface )
    SDL_LockSurface
;
: surface.unlock ( surf -- , lock window surface )
    SDL_UnlockSurface
;
