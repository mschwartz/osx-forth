\
\ ncurses examples
\
\ Example 5: A simple attributes example
\   Translated by Mike Schwartz
\ 

false trace-include !

require lib/ncurses.fth
require sys/stdio.fth

: demo  { | fp ch win prev row col x y -- , pager functionality }
    EOF -> prev
    argc @ 1 <> if
        cr ." Usage: demo5 <filename>"
        1 sys::exit
    then
    c" r" next-arg stdio.fopen -> fp
    fp 0= if
        c" *** fopen " sys::perror
    then

    ncurses.initscr -> win 
    win ncurses.getmaxyx -> col -> row
    begin
        fp stdio.fgetc  -> ch
        ch EOF <>
    while
\         s" here" ncurses.printw 13 ncurses.emit 10 ncurses.emit
        win ncurses.getyx  -> x -> y
        y row 1- = if
            s" <- Press any key ->" ncurses.printw
            ncurses.getch
            ncurses.clear
            0 0 ncurses.move
        then
        prev ascii / = if
            ch ascii * = if
                A_DIM ncurses.attron
                win ncurses.getyx -> x -> y
                y x 1- ncurses.move
                s" /*" ncurses.printw
            else
                ch ncurses.emit
            then
        else
            ch ncurses.emit
        then
        prev ascii * = ch ascii / = and if
            A_DIM ncurses.attroff
        then
        ch -> prev
\         ncurses.refresh
    repeat

    s" Press any key to exit " ncurses.printw
    ncurses.getch
    ncurses.endwin
    fp stdio.fclose
;

demo

