# makefile for pForth
# Portable Forth written in 'C'
# by Phil Burk
# For more info visit http://www.softsynth.com/pforth/
#
# See "help" target below.

.POSIX:

CC=g++
LD=ld

# ############ FEATURES / OPTIONS ############
# Options include: PF_SUPPORT_FP PF_NO_MALLOC PF_NO_INIT PF_DEBUG PF_SUPPOR_SDL2
# See "docs/pf_ref.htm" file for more info.

# to use JSON support, json-c library must be installed
# to use MQTT support, mosquitto library must be installed
# to use SDL support, SDL2 library must be installed
# to use ncurses support, ncurses library must be installed

PF_SUPPORT_PTHREADS=1   # set to 1 to enable PTHREADS support
PF_SUPPORT_FP=1 	# set to 1 to enable floating point support
PF_SUPPORT_STDIO=1 	# set to 1 to enable stdio support
PF_SUPPORT_NCURSES=1 	# set to 1 to enable ncurses support
PF_SUPPORT_NET=1 	# set to 1 to enable network support
PF_SUPPORT_SDL2=1 	# set to 1 to enable SDL2 support
PF_SUPPORT_JSON=1 	# set to 1 to enable JSON support
PF_SUPPORT_MQTT=1 	# set to 1 to enable MQTT support


# Set this parameter to -m32 if you want to compile a 32-bit binary.
WIDTHOPT=

# ############ /OPTIONS  #####################

SRCDIR       = .
PFORTHDIR    = $(SRCDIR)
PFORTHDIR    = $(SRCDIR)
CSRCDIR      = $(PFORTHDIR)/src
OBJDIR       = $(PFORTHDIR)/obj
FTHDIR       = $(PFORTHDIR)/fth
UNIXDIR      = .

PFDICAPP     = pforth
PFORTHDIC    = pforth.dic
PFDICDAT     = pfdicdat.h
PFORTHAPP    = pforth_standalone

FULL_WARNINGS =  \
	-Werror \
	-fsigned-char \
	-fno-builtin \
	-fno-unroll-loops \
	-pedantic \
	-Wcast-qual \
	-Wall \
	-Wwrite-strings \
	-Winline  \
	-Wmissing-declarations

DEBUGOPTS = 
#DEBUGOPTS =  -g
CCOPTS = $(WIDTHOPT) -O3 -std=c++20 $(FULL_WARNINGS) $(EXTRA_CCOPTS) $(DEBUGOPTS)

IO_SOURCE = pf_io_posix.c pf_fileio_stdio.c


EMBCCOPTS = -DPF_STATIC_DIC #-DPF_NO_FILEIO

#######################################

PFINCLUDES = pf_all.h pf_cglue.h pf_clib.h pf_core.h pf_float.h \
	pf_guts.h pf_host.h pf_inc1.h pf_io.h pf_mem.h pf_save.h \
	pf_text.h pf_types.h pf_words.h pfcompfp.h sdl/pf_compsdl2.h \
	pfcompil.h sdl/pf_innersdl2.h pfinnrfp.h pforth.h

FTH_FILES=$(shell find fth -name '*.fth')
C_FILES= $(shell find src -name '*.cc')
OBJECTS=$(C_FILES:src/%.cc=$(OBJDIR)/%_cc.o)
EOBJECTS=$(C_FILES:src/%.cc=$(OBJDIR)/%_cc.eo)
DEP_FILES = $(OBJECTS:%.o=%.d)

VPATH = .:$(CSRCDIR):$(CSRCDIR)/posix:$(CSRCDIR)/stdio:$(CSRCDIR)/sdl:$(CSRCDIR)/ncurses:$(CSRCDIR)/json:$(CSRCDIR)/mqtt

uname_S := $(shell sh -c 'uname -s 2>/dev/null || echo not')
LD_LIBRARIES=
ifeq ($(uname_S),Linux)
LD_LIBRARIES=-lbsd
endif
#ifne ($(UNAME), Linux)
#LD_LIBRARIES=-lbsd
#else
#endif
PFORTH_OPTS=
ifdef PF_SUPPORT_PTHREADS
LD_LIBRARIES+=-lpthread
PFORTH_OPTS+=-pthread -DSUPPORT_PTHREADS
endif

ifdef PF_SUPPORT_FP
LD_LIBRARIES+=-lm
PFORTH_OPTS+=-DPF_SUPPORT_FP
endif

ifdef PF_SUPPORT_STDIO
PFORTH_OPTS+=-DPF_SUPPORT_STDIO
endif

ifdef PF_SUPPORT_NCURSES
LD_LIBRARIES+=-L/opt/homebrew/opt/ncurses/lib -lncurses
#LD_LIBRARIES+=-lncurses
#PFORTH_OPTS+=-DPF_SUPPORT_NCURSES
PFORTH_OPTS+=-I/opt/homebrew/opt/ncurses/include -DPF_SUPPORT_NCURSES
endif

ifdef PF_SUPPORT_NET
PFORTH_OPTS+=-DPF_SUPPORT_NET
endif

ifdef PF_SUPPORT_SDL2
LD_LIBRARIES+=-L/opt/homebrew/lib -lSDL2
PFORTH_OPTS+=-DPF_SUPPORT_SDL2
endif

ifdef PF_SUPPORT_JSON
#LD_LIBRARIES+=`pkg-config nlohmann_json --libs` 
#PFORTH_OPTS+=-DPF_SUPPORT_JSON
PFORTH_OPTS += -DPF_SUPPORT_JSON $(shell pkg-config --cflags json-c)
LD_LIBRARIES += $(shell pkg-config --libs json-c)
endif

ifdef PF_SUPPORT_MQTT
LD_LIBRARIES+=-L/opt/homebrew/opt/mosquitto/lib -lmosquitto
PFORTH_OPTS+=-I/opt/homebrew/opt/mosquitto/include -DPF_SUPPORT_MQTT
endif

XCFLAGS = $(PFORTH_OPTS) -I/opt/homebrew/include $(CCOPTS)
XCPPFLAGS = $(PFORTH_OPTS) -D_DEFAULT_SOURCE -D_GNU_SOURCE $(PFORTH_OPTS)
XLDFLAGS = $(WIDTHOPT) 
		
CPPFLAGS = -I. $(XCPPFLAGS) $(PFORTH_OPTS)
CFLAGS = $(XCFLAGS) $(PFORTH_OPTS)
LDFLAGS = $(XLDFLAGS)

COMPILE = $(CC) -MMD $(CFLAGS) $(CPPFLAGS) $(CCOPTS) $(PFORTH_OPTS)
LINK = $(CC) $(LDFLAGS) 

.SUFFIXES: .c .o .eo

PFOBJS	   = $(OBJECTS)
PFEMBOBJS  = $(EOBJECTS)

$(OBJDIR)/%_cc.o: $(CSRCDIR)/%.cc $(PFINCLUDES) $(FTH_FILES) Makefile
	@echo "CC" $<
	@mkdir -p $(@D)
	$(COMPILE) -c -o $@ $<

# $(OBJDIR)/%.o: $(CSRCDIR)/%.c $(PFINCLUDES)
# $(COMPILE) -c -o $@ $<

# %.eo: %.c $(PFINCLUDES) pfdicdat.h
$(OBJDIR)/%_cc.eo: $(CSRCDIR)/%.cc $(PFINCLUDES) pfdicdat.h $(FTH_FILES)
	@echo "CC" $<
	@mkdir -p $(@D)
	@$(COMPILE) $(EMBCCOPTS) -c -o $@ $<

.PHONY: all clean cleanup test tools
.PHONY: help pffiles pfdicapp pfdicdat pforthapp

all: $(PFORTHAPP) tools

tools: 
	cd tools && make

pffiles:
	@echo "FTH FILES -----------------"
	@echo ${FTH_FILES}
	@echo "C FILES -----------------"
	@echo ${C_FILES}
	@echo "OBJECT FILES -----------------"
	@echo ${OBJECTS}
	@echo "INCLUDE FILES -----------------"
	@echo ${PFINCLUDES}
	@echo "OBJECT FILES ------------------"
	@echo ${PFOBJS}
	@echo "EMBEDDED OBJECT FILES ------------------"
	@echo ${PFEMBOBJS}
	@echo "CCOPTS ------------------"
	@echo ${CCOPTS}
	@echo "PFORTH_OPTS ------------------"
	@echo ${PFORTH_OPTS}
ifdef PF_SUPPORT_FP
	@echo "FP"
endif
ifdef $(PF_SUPPORT_FP)
	@echo "FP"
endif

# Build pforth by compiling 'C' source.
$(PFDICAPP): $(PFINCLUDES) $(PFOBJS)
	$(LINK) -o $@ $(PFOBJS) $(LDADD) $(LD_LIBRARIES)

# Build basic dictionary image by running newly built pforth and including "system.fth".
$(PFORTHDIC): $(PFDICAPP)
	wd=$$(pwd); (cd $(FTHDIR); "$${wd}/$(PFDICAPP)" -i core/system.fth)
	(cd $(FTHDIR); cat pforth.dic; rm -f pforth.dic) > $@

$(PFDICDAT): $(PFORTHDIC) $(PFDICAPP)
	@test -f $(CSRCDIR)/$(PFDICDAT) && echo WARNING old $(CSRCDIR)/$(PFDICDAT) would interfere || true
	# Remove stray src/pfdicdat.h because it may accidentally get included.
	rm -f $(CSRCDIR)/$(PFDICDAT)
	echo 'include $(FTHDIR)/core/savedicd.fth SDAD BYE' | ./$(PFDICAPP) -d $(PFORTHDIC)

$(PFORTHAPP): $(PFDICDAT) $(PFEMBOBJS)
	$(LINK) -o $@ $(PFEMBOBJS) $(LDADD) $(LD_LIBRARIES)
	@echo ""
	@echo "Standalone pForth executable written to $(PFORTHAPP)"

# target aliases
pfdicapp: $(PFDICAPP)

pforthdic: $(PFORTHDIC)

pfdicdat: $(PFDICDAT)

pforthapp: $(PFORTHAPP)

help:
	@echo "Use 'make all' to build standalone pForth executable."
	@echo "PForth can be built in several stages using these targets:"
	@echo "   pfdicapp = executable pForth with minimal dictionary. All from 'C'."
	@echo "   pforthdic = executable pforth plus pforth.dic file"
	@echo "   pfdicdat = header image of full dictionary build by compiling Forth code."
	@echo "   pforthapp = executable with embedded dictionary image. DEFAULT 'all' target."
	@echo ""
	@echo "   The file 'fth/pfdicdat.h' is generated by pForth. It contains a binary image of the Forth dictionary."
	@echo "   It allows pForth to work as a standalone image that does not need to load a dictionary file."

test: $(PFORTHAPP)
	cd $(FTHDIR) && ../$(UNIXDIR)/$(PFORTHAPP) -q t_corex.fth
	cd $(FTHDIR) && ../$(UNIXDIR)/$(PFORTHAPP) -q t_strings.fth
	cd $(FTHDIR) && ../$(UNIXDIR)/$(PFORTHAPP) -q t_locals.fth
	cd $(FTHDIR) && ../$(UNIXDIR)/$(PFORTHAPP) -q t_alloc.fth
	cd $(FTHDIR) && ../$(UNIXDIR)/$(PFORTHAPP) -q t_floats.fth
	cd $(FTHDIR) && ../$(UNIXDIR)/$(PFORTHAPP) -q t_file.fth
	@echo "PForth Tests PASSED"

install: $(PFORTHAPP)
	install pforth pforth_standalone /usr/local/bin

cleanup:
	rm -f index.html*

clean:
	rm -rf index.html*
	rm -rf $(OBJDIR)/*
	rm -f $(PFOBJS) $(PFEMBOBJS)
	rm -f $(PFORTHAPP)
	rm -f $(PFDICDAT) $(FTHDIR)/$(PFDICDAT) $(CSRCDIR)/$(PFDICDAT)
	rm -f $(PFORTHDIC) $(FTHDIR)/$(PFORTHDIC)
	rm -f $(PFDICAPP)

-include $(DEP_FILES)
