anew task-signals-macos.fth

\ MACOS specific signal handling

trace-include @ [if]
cr ." COMPILING signals.fth"
[then]

1 constant SIGHUP           \ hangup
2 constant SIGINT           \ interrupt
3 constant SIGQUIT          \ quit
4 constant SIGILL           \ illegal instruction
5 constant SIGTRAP          \ trace trap 
6 constant SIGABRT          \ abort()
7 constant SIGPOLL          \ pollable event generated
SIGABRT constant SIGIOT     \ compatibility
7 constant SIGEMT           \ EMT instruction
8 constant SIGFPE           \ floating point exception
9 constant SIGKILL          \ KILL cannot be caught or ignored
10 constant SIGBUS          \ bus error
11 constant SIGSEGV         \ segment violation
12 constant SIGSYS          \ bad argument to system call
13 constant SIGPIPE         \ write on a pipe with no one to read it
14 constant SIGALRM         \ alarm clock
15 constant SIGTERM         \ software termination signal from kill
16 constant SIGURG          \ urgent ocndition on IO channel
17 constant SIGSTOP         \ sendable stop signal not from tty
18 constant SIGSTP          \ stop signal from tty
19 constant SIGCONT         \ continue a stopped process
20 constant SIGCHLD         \ to parent on child stop or exit
21 constant SIGTTIN         \ to readers prgrp upon background tty read
22 constant SIGTTOU         \ like TTIN for output 
23 constant SIGIO           \ input/output possible signal
24 constant SIGXCPU         \ exceeded CPU time limit
25 constant SIGXFSZ         \ exceeded file size limit
26 constant SIGVTALRM       \ virutal time alarm
27 constant SIGPROF         \ profiling time alarm
28 constant SIGWINCH        \ window size changes
29 constant SIGINFO         \ information request
30 constant SIGUSR1         \ user defined signal 1
31 constant SIGUSR2         \ user defined signal 2

1 SIGHUP LSHIFT constant SIGHUP-BIT
1 SIGINT LSHIFT constant SIGINT-BIT
1 SIGQUIT LSHIFT constant SIGQUIT-BIT
1 SIGILL LSHIFT constant SIGILL-BIT
1 SIGTRAP LSHIFT constant SIGTRAP-BIT
1 SIGABRT LSHIFT constant SIGABRT-BIT
1 SIGPOLL LSHIFT constant SIGPOLL-BIT
1 SIGIOT LSHIFT constant SIGIOT-BIT
1 SIGEMT LSHIFT constant SIGEMT-BIT
1 SIGFPE LSHIFT constant SIGFPE-BIT
1 SIGKILL LSHIFT constant SIGKILL-BIT
1 SIGBUS LSHIFT constant SIGBUS-BIT
1 SIGSEGV LSHIFT constant SIGSEGV-BIT
1 SIGSYS LSHIFT constant SIGsys::BIT
1 SIGPIPE LSHIFT constant SIGPIPE-BIT
1 SIGALRM LSHIFT constant SIGALRM-BIT
1 SIGTERM LSHIFT constant SIGTERM-BIT
1 SIGURG LSHIFT constant SIGURG-BIT
1 SIGSTOP LSHIFT constant SIGSTOP-BIT
1 SIGSTP LSHIFT constant SIGSTP-BIT
1 SIGCONT LSHIFT constant SIGCONT-BIT
1 SIGCHLD LSHIFT constant SIGCHLD-BIT
1 SIGTTIN LSHIFT constant SIGTTIN-BIT
1 SIGTTOU LSHIFT constant SIGTTOU-BIT
1 SIGIO LSHIFT constant SIGIO-BIT
1 SIGXCPU LSHIFT constant SIGXCPU-BIT
1 SIGXFSZ LSHIFT constant SIGXFSZ-BIT
1 SIGVTALRM LSHIFT constant SIGVTALRM-BIT
1 SIGPROF LSHIFT constant SIGPROF-BIT
1 SIGWINCH LSHIFT constant SIGWINCH-BIT
1 SIGINFO LSHIFT constant SIGINFO-BIT
1 SIGUSR1 LSHIFT constant SIGUSR1-BIT
1 SIGUSR2 LSHIFT constant SIGUSR2-BIT

: default-signal-handler ( -- )
;

0 [if]
-1 constant THROW_ABORT
-2 constant THROW_ABORT_QUOTE
-3 constant THROW_STACK_OVERFLOW
-4 constant THROW_STACK_UNDERFLOW
-13 constant THROW_UNDEFINED_WORD
-14 constant THROW_EXECUTING
-22 constant THROW_PAIRS
-45 constant THROW_FLOAT_STACK_UNDERFLOW
-56 constant THROW_QUIT
[then]

: default-sigint-handler ( -- )
cr ." default sigint "
    SIGINT-bit NOT signals @ and signals !
    err_abort throw 
\     0 sys::exit
;

defer handle-SIGHUP     ' default-signal-handler is handle-SIGHUP
defer handle-SIGINT     ' default-sigint-handler is handle-SIGINT
\ defer handle-SIGINT     ' default-signal-handler is handle-SIGINT
defer handle-SIGQUIT    ' default-signal-handler is handle-SIGQUIT
defer handle-SIGILL     ' default-signal-handler is handle-SIGILL
defer handle-SIGTRAP    ' default-signal-handler is handle-SIGTRAP
defer handle-SIGABRT    ' default-signal-handler is handle-SIGABRT
defer handle-SIGPOLL    ' default-signal-handler is handle-SIGPOLL
defer handle-SIGIOT     ' default-signal-handler is handle-SIGIOT
defer handle-SIGEMT     ' default-signal-handler is handle-SIGEMT
defer handle-SIGFPE     ' default-signal-handler is handle-SIGFPE
defer handle-SIGKILL    ' default-signal-handler is handle-SIGKILL
defer handle-SIGBUS     ' default-signal-handler is handle-SIGBUS
defer handle-SIGSEGV    ' default-signal-handler is handle-SIGSEGV
defer handle-SIGSYS     ' default-signal-handler is handle-SIGSYS
defer handle-SIGPIPE    ' default-signal-handler is handle-SIGPIPE
defer handle-SIGALRM    ' default-signal-handler is handle-SIGALRM
defer handle-SIGTERM    ' default-signal-handler is handle-SIGTERM
defer handle-SIGURG     ' default-signal-handler is handle-SIGURG
defer handle-SIGSTOP    ' default-signal-handler is handle-SIGSTOP
defer handle-SIGSTP     ' default-signal-handler is handle-SIGSTP
defer handle-SIGCONT    ' default-signal-handler is handle-SIGCONT
defer handle-SIGCHLD    ' default-signal-handler is handle-SIGCHLD
defer handle-SIGTTIN    ' default-signal-handler is handle-SIGTTIN
defer handle-SIGTTOU    ' default-signal-handler is handle-SIGTTOU
defer handle-SIGIO      ' default-signal-handler is handle-SIGIO
defer handle-SIGXCPU    ' default-signal-handler is handle-SIGXCPU
defer handle-SIGXFSZ    ' default-signal-handler is handle-SIGXFSZ
defer handle-SIGVTALRM  ' default-signal-handler is handle-SIGVTALRM
defer handle-SIGPROF    ' default-signal-handler is handle-SIGPROF
defer handle-SIGWINCH   ' default-signal-handler is handle-SIGWINCH
defer handle-SIGINFO    ' default-signal-handler is handle-SIGINFO
defer handle-SIGUSR1    ' default-signal-handler is handle-SIGUSR1
defer handle-SIGUSR2    ' default-signal-handler is handle-SIGUSR2

defer handle-signal

: (handle-signal) ( -- )
\     cr ." handle-signal received signals " signals @ .hex .s
    signals @ SIGHUP-bit and 0<> if
        handle-SIGHUP
    then
    signals @ SIGINT-bit and 0<> if
\         cr ." handle-signal SIGINT " 
        handle-SIGINT
\         ERR_ABORTQ throw
    then
    signals @ SIGQUIT-bit and 0<> if
        cr ." handle-signal SIGQUIT " 
        handle-SIGQUIT
    then
    signals @ SIGILL-bit and 0<> if
        cr ." handle-signal SIGILL " 
        handle-SIGILL
    then
    signals @ SIGTRAP-bit and 0<> if
        cr ." handle-signal SIGTRAP " 
        handle-SIGTRAP
    then
    signals @ SIGABRT-bit and 0<> if
        cr ." handle-signal SIGABRT " 
        handle-SIGABRT
    then
    signals @ SIGPOLL-bit and 0<> if
        cr ." handle-signal SIGPOLL " 
        handle-SIGPOLL
    then
    signals @ SIGIOT-bit and 0<> if
        cr ." handle-signal SIGIOT " 
        handle-SIGIOT
    then
    signals @ SIGEMT-bit and 0<> if
        cr ." handle-signal SIGEMT " 
        handle-SIGEMT
    then
    signals @ SIGFPE-bit and 0<> if
        cr ." handle-signal SIGFPE " 
        handle-SIGFPE
    then
    signals @ SIGKILL-bit and 0<> if
        cr ." handle-signal SIGKILL " 
        handle-SIGKILL
    then
    signals @ SIGBUS-bit and 0<> if
        cr ." handle-signal SIGBUS " 
        handle-SIGBUS
    then
    signals @ SIGSEGV-bit and 0<> if
        cr ." handle-signal SIGSEGV " 
        handle-SIGSEGV
    then
    signals @ SIGsys::bit and 0<> if
        cr ." handle-signal SIGSYS " 
        handle-SIGSYS
    then
    signals @ SIGPIPE-bit and 0<> if
        cr ." handle-signal SIGPIPE " 
        handle-SIGPIPE
    then
    signals @ SIGALRM-bit and 0<> if
        cr ." handle-signal SIGALRM " 
        handle-SIGALRM
    then
    signals @ SIGTERM-bit and 0<> if
        cr ." handle-signal SIGTERM " 
        handle-SIGTERM
    then
    signals @ SIGURG-bit and 0<> if
        cr ." handle-signal SIGURG " 
        handle-SIGURG
    then
    signals @ SIGSTOP-bit and 0<> if
        cr ." handle-signal SIGSTOP " 
        handle-SIGSTOP
    then
    signals @ SIGSTP-bit and 0<> if
        cr ." handle-signal SIGSTP " 
        handle-SIGSTP
    then
    signals @ SIGCONT-bit and 0<> if
        cr ." handle-signal SIGCONT " 
        handle-SIGCONT
    then
    signals @ SIGCHLD-bit and 0<> if
\         cr ." handle-signal SIGCHLD " 
        handle-SIGCHLD
    then
    signals @ SIGTTIN-bit and 0<> if
        cr ." handle-signal SIGTTIN " 
        handle-SIGTTIN
    then
    signals @ SIGTTOU-bit and 0<> if
        cr ." handle-signal SIGTTOU " 
        handle-SIGTTOU
    then
    signals @ SIGIO-bit and 0<> if
        cr ." handle-signal SIGIO " 
        handle-SIGIO
    then
    signals @ SIGXCPU-bit and 0<> if
        cr ." handle-signal SIGXCPU " 
        handle-SIGXCPU
    then
    signals @ SIGXFSZ-bit and 0<> if
        cr ." handle-signal SIGXFSZ " 
        handle-SIGXFSZ
    then
    signals @ SIGVTALRM-bit and 0<> if
        cr ." handle-signal SIGVTALRM " 
        handle-SIGVTALRM
    then
    signals @ SIGPROF-bit and 0<> if
        cr ." handle-signal SIGPROF " 
        handle-SIGPROF
    then
    signals @ SIGWINCH-bit and 0<> if
\         cr ." handle-signal SIGWINCH " 
        handle-SIGWINCH
    then
    signals @ SIGINFO-bit and 0<> if
        cr ." handle-signal SIGINFO " 
        handle-SIGINFO
    then
    signals @ SIGUSR1-bit and 0<> if
        cr ." handle-signal SIGUSR1 " 
        handle-SIGUSR1
    then
    signals @ SIGUSR2-bit and 0<> if
        cr ." handle-signal SIGUSR2 " 
        handle-SIGUSR2
    then
    0 signals !
;

' (handle-signal) is handle-signal

: signal.set ( sig -- , sig is SIGINT, etc )
    1 swap LSHIFT sys::set_signal
;

: .pid sys::getpid ." [" .# ." ] " ;

trace-include @ [if]
cr ." end signals "
[then]