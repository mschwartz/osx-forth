\
\ PHRed editor
\
\ programmed by Mike Schwartz
\
\ Syntax Highlighting
\
private{
    create token 512 allot
}private

\ caller must MEM.free new-text returned
: Highlight.c++ { text | comment? str ch src dst pos -- highlighted , syntax highlight line based upon fancy strings }
    text CString.new -> str
    false -> comment?
    regex.default

    \ comments
    str c" //" s\" \x8c//\z" CString.$regex_replace 0 >= if 
        true -> comment? 
    then
    comment? not if
        str c" /\*" s\" \x8c/*\z" CString.$regex_replace 0 >= if true -> comment? then
    then
    comment? not if
        str c" \*\*" s\" \x8c**\z" CString.$regex_replace 0 >= if true -> comment? then
    then
    comment? not if
        str c" \*/" s\" \x8c*/\z" CString.$regex_replace 0 >= if true -> comment? then
    then

    \ preprocessor directives
    regex.global
    comment? not if
        str c" #include" s\" \x8b#include\x88\z" CString.$regex_replace drop
        str c" #define"  s\" \x8b#define\x88\z" CString.$regex_replace drop
        str c" #if " s\" \x8b#if \x88\z" CString.$regex_replace drop
        str c" #ifdef " s\" \x8b#ifdef \x88\z" CString.$regex_replace drop
        str c" #ifndef"  s\" \x8b#ifndef\x88\z" CString.$regex_replace drop
        str c" #else"  s\" \x8b#else\x88\z" CString.$regex_replace drop
        str c" #endif" s\" \x8b#endif\x88\z" CString.$regex_replace drop
        \ keywords
        str c" extern " s\" \x8bextern \x88\z" CString.$regex_replace drop
        str c" const " s\" \x8bconst \x88\z" CString.$regex_replace drop
        str c" break" s\" \x8bbreak\x88\z" CString.$regex_replace drop
        str c" void" s\" \x8bvoid\x88\z" CString.$regex_replace drop
        str c" char " s\" \x8bchar \x88\z" CString.$regex_replace drop
        str c" int " s\" \x8bint \x88\z" CString.$regex_replace drop
        str c" struct " s\" \x8bstruct \x88\z" CString.$regex_replace drop
        str c" cell_t" s\" \x8bcell_t\x88\z" CString.$regex_replace drop
        str c" uint8_t " s\" \x8buint8_t \x88\z" CString.$regex_replace drop
        str c" while" s\" \x8bwhile\x88\z" CString.$regex_replace drop
        str c" switch" s\" \x8bswitch\x88\z" CString.$regex_replace drop
        str c" case " s\" \x8bcase \x88\z" CString.$regex_replace drop
        str c"  if" s\" \x8b if\x88\z" CString.$regex_replace drop
        str c" else" s\" \x8belse\x88\z" CString.$regex_replace drop
        str c" return " s\" \x8breturn \x88\z" CString.$regex_replace drop
    then
    str CString.zstring string::strdup 
    str CString.destroy
;

privatize
\ ncurses-uninstall
\ see highlight.forth
\ 0 sys::exit