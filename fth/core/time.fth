\ Time methods
\
\ Author: Mike Schwartz
\

ANEW TASK-TIME.FTH
decimal

: time>seconds time::time-ms 1000 / ;

: time time>seconds ;

: seconds>date ( time-in-seconds -- caddr u )
    time::ctime
    zcount
;

: milliseconds>date ( time-in-milliseconds -- c-addr )
    1000 / seconds>date
;

: microseconds>date ( time-in-microseconds -- c-addr )
    1000000 / seconds>date
;

: time>date
    time::time-ms  milliseconds>date
;

: date cr time>date type ; immediate
