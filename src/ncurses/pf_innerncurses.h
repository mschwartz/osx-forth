#ifdef PF_SUPPORT_NCURSES
case ID_NCURSES_VERSION: { // ( -- num-pairs )
  PUSH_TOS;
  TOS = (cell_t)curses_version();
}
  endcase;
case ID_NCURSES_COLOR_PAIRS: { // ( -- num-pairs )
  PUSH_TOS;
  TOS = (cell_t)COLOR_PAIRS;
}
  endcase;
case ID_NCURSES_COLORS: { // ( -- num-colors )
  PUSH_TOS;
  TOS = (cell_t)COLORS;
}
  endcase;
case ID_NCURSES_COLS: { // ( -- num-cols )
  PUSH_TOS;
  TOS = (cell_t)COLS;
}
  endcase;
case ID_NCURSES_ESCDELAY: { // ( -- delay )
  PUSH_TOS;
  TOS = (cell_t)ESCDELAY;
}
  endcase;
case ID_NCURSES_LINES: { // ( -- num-lines )
  PUSH_TOS;
  TOS = (cell_t)LINES;
}
  endcase;
case ID_NCURSES_TABSIZE: { // ( -- tabsize )
  PUSH_TOS;
  TOS = (cell_t)TABSIZE;
}
  endcase;
case ID_NCURSES_curscr: { // ( -- curscr )
  PUSH_TOS;
  TOS = (cell_t)curscr;
}
  endcase;
case ID_NCURSES_newscr: { // ( -- newscr )
  PUSH_TOS;
  TOS = (cell_t)newscr;
}
  endcase;
case ID_NCURSES_stdscr: { // ( -- stdscr )
  PUSH_TOS;
  TOS = (cell_t)stdscr;
}
  endcase;
case ID_NCURSES_set_escdelay: { // ( delay -- )
  int delay = (int)TOS;
  TOS = M_POP;
  set_escdelay(delay);
}
  endcase;
case ID_NCURSES_set_tabsize: { // ( tabsize -- )
  int tabsize = (int)TOS;
  TOS = M_POP;
  set_tabsize(tabsize);
}
  endcase;

case ID_NCURSES_initscr: { // ( -- window )
  PUSH_TOS;
  TOS = (cell_t)initscr();
}
  endcase;
case ID_NCURSES_endwin: { // ( -- )
  endwin();
}
  endcase;
case ID_NCURSES_focus_mode: { // ( mode -- )
  int mode = (int)TOS;
  TOS = M_POP;
  if (mode) {
    printf("\033[?1004h");
  }
  else {
    printf("\033[?1004l");
  }
#define KEY_FOCUS_IN 1001
#define KEY_FOCUS_OUT 1002

  define_key("\033[I", KEY_FOCUS_IN);
  define_key("\033[O", KEY_FOCUS_OUT);
}
  endcase;
case ID_NCURSES_define_key: { // ( key ch -- )
  int ch = (int)TOS;
  TOS = M_POP;
  const char *key = (const char *)TOS;
  TOS = (cell_t)define_key(key, ch);
}
  endcase;
case ID_NCURSES_resize_term: { // ( nlines ncols -- err )
  int ncols = (int)TOS;
  TOS = M_POP;
  int nlines = (int)TOS;
  TOS = (cell_t)resizeterm(nlines, ncols);
}
  endcase;
case ID_NCURSES_flushinp: { // ( -- )
  flushinp();
}
  endcase;
case ID_NCURSES_newwin: { // ( nlines ncols  top left -- win )
  int left = (int)TOS;
  TOS = M_POP;
  int top = (int)TOS;
  TOS = M_POP;
  int ncols = (int)TOS;
  TOS = M_POP;
  int nlines = (int)TOS;
  TOS = (cell_t)newwin(nlines, ncols, top, left);
}
  endcase;
case ID_NCURSES_delwin: { // ( win -- err )
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)delwin(win);
}
  endcase;
case ID_NCURSES_mvwin: { // ( win top left -- err )
  WINDOW *win = (WINDOW *)TOS;
  TOS = M_POP;
  int top = (int)TOS;
  TOS = M_POP;
  int left = (int)TOS;
  TOS = (cell_t)mvwin(win, top, left);
}
  endcase;
case ID_NCURSES_subwin: { // ( win top left nlines ncols -- subwin )
  int nlines = (int)TOS;
  TOS = M_POP;
  int ncols = (int)TOS;
  TOS = M_POP;
  int left = (int)TOS;
  TOS = M_POP;
  int top = (int)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)subwin(win, nlines, ncols, top, left);
}
  endcase;
case ID_NCURSES_derwin: { // ( win top left nlines ncols -- subwin )
  int nlines = (int)TOS;
  TOS = M_POP;
  int ncols = (int)TOS;
  TOS = M_POP;
  int left = (int)TOS;
  TOS = M_POP;
  int top = (int)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)subwin(win, nlines, ncols, top, left);
}
  endcase;
case ID_NCURSES_mvderwin: { // ( win top left -- err )
  WINDOW *win = (WINDOW *)TOS;
  TOS = M_POP;
  int top = (int)TOS;
  TOS = M_POP;
  int left = (int)TOS;
  TOS = (cell_t)mvderwin(win, top, left);
}
  endcase;
case ID_NCURSES_dupwin: { // ( win -- win )
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)dupwin(win);
}
  endcase;
case ID_NCURSES_wsyncup: { // ( win -- )
  WINDOW *win = (WINDOW *)TOS;
  TOS = M_POP;
  wsyncup(win);
}
  endcase;
case ID_NCURSES_syncok: { // ( win bool -- err )
  WINDOW *win = (WINDOW *)TOS;
  TOS = M_POP;
  bool ok = (bool)TOS;
  TOS = (cell_t)syncok(win, ok);
}
  endcase;
case ID_NCURSES_wcursyncup: { // ( win -- )
  WINDOW *win = (WINDOW *)TOS;
  TOS = M_POP;
  wcursyncup(win);
}
  endcase;
case ID_NCURSES_wsyncdown: { // ( win -- )
  WINDOW *win = (WINDOW *)TOS;
  TOS = M_POP;
  wsyncdown(win);
}
  endcase;
case ID_NCURSES_doupdate: { // ( -- )
  doupdate();
}
  endcase;
case ID_NCURSES_wredrawln: { // ( win beg_line, num_lines -- err )
  int num_lines = (int)TOS;
  TOS = M_POP;
  int beg_line = (int)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)wredrawln(win, beg_line, num_lines);
}
  endcase;
case ID_NCURSES_refresh: { // ( -- )
  refresh();
}
  endcase;
case ID_NCURSES_wnoutrefresh: { // ( win -- err )
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)wnoutrefresh(win);
}
  endcase;
case ID_NCURSES_wrefresh: { // ( win -- err )
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)wrefresh(win);
}
  endcase;
case ID_NCURSES_erase: { // ( -- err )
  PUSH_TOS;
  TOS = (cell_t)erase();
}
  endcase;
case ID_NCURSES_werase: { // ( win -- err )
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)werase(win);
}
  endcase;
case ID_NCURSES_clear: { // ( -- err )
  PUSH_TOS;
  TOS = (cell_t)clear();
}
  endcase;
case ID_NCURSES_wclear: { // ( win -- err )
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)wclear(win);
}
  endcase;
case ID_NCURSES_clrtobot: { // (  -- err )
  PUSH_TOS;
  TOS = (cell_t)clrtobot();
}
  endcase;
case ID_NCURSES_wclrtobot: { // ( win -- err )
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)wclrtobot(win);
}
  endcase;
case ID_NCURSES_clrtoeol: { // ( -- err )
  PUSH_TOS;
  TOS = (cell_t)clrtoeol();
}
  endcase;
case ID_NCURSES_wclrtoeol: { // ( win -- )
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)wclrtoeol(win);
}
  endcase;
case ID_NCURSES_winsertln: { // ( win -- )
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)winsertln(win);
}
  endcase;
case ID_NCURSES_wdeleteln: { // ( win -- )
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)wdeleteln(win);
}
  endcase;
case ID_NCURSES_move: { // ( y x -- )
  int x = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = (cell_t)move(y, x);
}
  endcase;
case ID_NCURSES_wmove: { // ( win y x -- )
  int x = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)wmove(win, y, x);
}
  endcase;
case ID_NCURSES_addch: { // ( ch -- add char/attr to screen )
  int ch = (int)TOS;
  TOS = addch(ch);
}
  endcase;
case ID_NCURSES_mvaddch: { // ( y x ch -- add char/attr to screen )
  int ch = (int)TOS;
  TOS = M_POP;
  int x = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = (cell_t)mvaddch(y, x, ch);
}
  endcase;
case ID_NCURSES_waddch: { // ( win ch -- )
  int ch = (int)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)waddch(win, ch);
}
  endcase;
case ID_NCURSES_addstr: { // ( win str -- )
  char *str = (char *)TOS;
  TOS = (cell_t)addstr(str);
}
  endcase;
case ID_NCURSES_mvaddstr: { // ( y x str -- )
  char *str = (char *)TOS;
  TOS = M_POP;
  int x = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = (cell_t)mvaddstr(y, x, str);
}
  endcase;
case ID_NCURSES_waddstr: { // ( win str -- )
  char *str = (char *)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)waddstr(win, str);
}
  endcase;
case ID_NCURSES_waddnstr: { // ( win str n -- )
  int n = (int)TOS;
  TOS = M_POP;
  char *str = (char *)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)waddnstr(win, str, n);
}
  endcase;

  // without wrap
case ID_NCURSES_addchstr: { // ( str -- )
  chtype *str = (chtype *)TOS;
  TOS = (cell_t)addchstr(str);
}
  endcase;
case ID_NCURSES_addchnstr: { // ( str n -- )
  int n = (int)TOS;
  TOS = M_POP;
  chtype *str = (chtype *)TOS;
  TOS = (cell_t)addchnstr(str, n);
}
  endcase;
case ID_NCURSES_waddchstr: { // ( win str -- )
  chtype *str = (chtype *)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)waddchstr(win, str);
}
  endcase;
case ID_NCURSES_waddchnstr: { // ( win str n -- )
  int n = (int)TOS;
  TOS = M_POP;
  chtype *str = (chtype *)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)waddchnstr(win, str, n);
}
  endcase;
case ID_NCURSES_mvaddchstr: { // ( y x str -- )
  chtype *str = (chtype *)TOS;
  TOS = M_POP;
  int x = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = (cell_t)mvaddchstr(y, x, str);
}
  endcase;
case ID_NCURSES_mvaddchnstr: { // ( y x str n -- )
  int n = (int)TOS;
  TOS = M_POP;
  chtype *str = (chtype *)TOS;
  TOS = M_POP;
  int x = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = (cell_t)mvaddchnstr(y, x, str, n);
}
  endcase;
case ID_NCURSES_mvwaddchstr: { // ( win y x str -- )
  chtype *str = (chtype *)TOS;
  TOS = M_POP;
  int x = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)mvwaddchstr(win, y, x, str);
}
  endcase;
case ID_NCURSES_mvwaddchnstr: { // ( win y x str n -- )
  int n = (int)TOS;
  TOS = M_POP;
  chtype *str = (chtype *)TOS;
  TOS = M_POP;
  int x = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)mvwaddchnstr(win, y, x, str, n);
}
  endcase;

  // WINDOW ATTRIBUTES
case ID_NCURSES_getattrs: { // ( win -- attrs )
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)getattrs(win);
}
  endcase;
case ID_NCURSES_getbegx: { // ( win -- x )
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)getbegx(win);
}
  endcase;
case ID_NCURSES_getbegy: { // ( win -- y )
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)getbegy(win);
}
  endcase;
case ID_NCURSES_getmaxx: { // ( win -- x )
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)getmaxx(win);
}
  endcase;
case ID_NCURSES_getmaxy: { // ( win -- y )
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)getmaxy(win);
}
  endcase;
case ID_NCURSES_getparx: { // ( win -- x )
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)getparx(win);
}
  endcase;
case ID_NCURSES_getpary: { // ( win -- y )
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)getpary(win);
}
  endcase;
case ID_NCURSES_getcurx: { // ( win -- x )
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)getcurx(win);
}
  endcase;
case ID_NCURSES_getcury: { // ( win -- y )
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)getcury(win);
}
  endcase;
case ID_NCURSES_getch: { // ( -- ch )
  PUSH_TOS;
  TOS = (cell_t)getch();
}
  endcase;
case ID_NCURSES_wgetch: { // ( win -- ch )
  TOS = (cell_t)wgetch((WINDOW *)TOS);
}
  endcase;
case ID_NCURSES_inch: { // (  -- ch )
  PUSH_TOS;
  TOS = (cell_t)inch();
}
  endcase;
case ID_NCURSES_winch: { // ( win -- ch )
  TOS = (cell_t)winch((WINDOW *)TOS);
}
  endcase;
case ID_NCURSES_mvinch: { // ( y x -- ch )
  int x = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = (cell_t)mvinch(y, x);
}
  endcase;
case ID_NCURSES_mvwinch: { // ( win y x -- ch )
  int x = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)mvwinch(win, y, x);
}
  endcase;
case ID_NCURSES_inchstr: { // ( str -- )
  chtype *str = (chtype *)TOS;
  TOS = (cell_t)inchstr(str);
}
  endcase;
case ID_NCURSES_inchnstr: { // ( str n -- )
  int n = (int)TOS;
  TOS = M_POP;
  chtype *str = (chtype *)TOS;
  TOS = (cell_t)inchnstr(str, n);
}
  endcase;

case ID_NCURSES_getnstr: { // ( str n -- )
  int n = (int)TOS;
  TOS = M_POP;
  char *str = (char *)TOS;
  TOS = (cell_t)getnstr(str, n);
}
  endcase;
case ID_NCURSES_getstr: { // ( str -- )
  char *str = (char *)TOS;
  TOS = (cell_t)getstr(str);
}
  endcase;
case ID_NCURSES_getmaxyx: { // ( win -- y x )
  int y, x;
  WINDOW *win = (WINDOW *)TOS;
  getmaxyx(win, y, x);
  TOS = (cell_t)y;
  PUSH_TOS;
  TOS = (cell_t)x;
}
  endcase;
case ID_NCURSES_getyx: { // ( win -- y x )
  int y, x;
  WINDOW *win = (WINDOW *)TOS;
  getyx(win, y, x);
  TOS = (cell_t)y;
  PUSH_TOS;
  TOS = (cell_t)x;
}
  endcase;
case ID_NCURSES_mvgetch: { // ( y x -- ch )
  int x = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = (cell_t)mvgetch(y, x);
}
  endcase;
case ID_NCURSES_mvgetnstr: { // ( y x str n -- )
  int n = (int)TOS;
  TOS = M_POP;
  char *str = (char *)TOS;
  TOS = M_POP;
  int x = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = (cell_t)mvgetnstr(y, x, str, n);
}
  endcase;
case ID_NCURSES_mvgetstr: { // ( y x str -- )
  char *str = (char *)TOS;
  TOS = M_POP;
  int x = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = (cell_t)mvgetstr(y, x, str);
}
  endcase;
// case ID_NCURSES_mvgetyx: { // ( y x -- y x )
//   int x = (int)TOS;
//   TOS = M_POP;
//   int y = (int)TOS;
//   TOS = M_POP;
//   WINDOW *win = (WINDOW *)TOS;
//   TOS = (cell_t)mvgetyx(win, y, x);
// }
//   endcase;
case ID_NCURSES_mvinnstr: { // ( y x str n -- )
  int n = (int)TOS;
  TOS = M_POP;
  int x = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = M_POP;
  char *str = (char *)TOS;
  TOS = (cell_t)mvinnstr(y, x, str, n);
}
  endcase;
case ID_NCURSES_mvinstr: { // ( y x str -- )
  char *str = (char *)TOS;
  TOS = M_POP;
  int x = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = (cell_t)mvinstr(y, x, str);
}
  endcase;
case ID_NCURSES_mvwgetch: { // ( win y x -- ch )
  int x = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)mvwgetch(win, y, x);
}
  endcase;
case ID_NCURSES_ungetch: { // ( ch -- )
  int ch = (int)TOS;
  TOS = (cell_t)ungetch(ch);
}
  endcase;
case ID_NCURSES_has_key: { // ( ch -- )
  int ch = (int)TOS;
  TOS = (cell_t)has_key(ch);
}
  endcase;
// case ID_NCURSES_getwin: { // ( -- win )
//   PUSH_TOS;
//   TOS = (cell_t)getwin();
// }
//   endcase;
case ID_NCURSES_printw: { // ( str -- )
  char *str = (char *)TOS;
  TOS = (cell_t)printw("%s", str);
}
  endcase;
case ID_NCURSES_wprintw: { // ( win str -- err , print to window )
  char *str = (char *)TOS;
  TOS = M_POP;
  TOS = (cell_t)wprintw((WINDOW *)TOS, "%s", str);
}
  endcase;
case ID_NCURSES_mvprintw: { // ( y x str -- )
  char *str = (char *)TOS;
  TOS = M_POP;
  int x = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)mvprintw(y, x, str, win);
}
  endcase;
case ID_NCURSES_mvwprintw: { // ( win y x str -- )
  char *str = (char *)TOS;
  TOS = M_POP;
  int x = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)mvwprintw(win, y, x, "%s", str);
}
  endcase;

case ID_NCURSES_cbreak: { // ( -- )
  PUSH_TOS;
  TOS = (cell_t)cbreak();
}
  endcase;
case ID_NCURSES_nocbreak: { // ( -- )
  PUSH_TOS;
  TOS = (cell_t)nocbreak();
}
  endcase;
case ID_NCURSES_echo: { // ( -- )
  PUSH_TOS;
  TOS = (cell_t)echo();
}
  endcase;
case ID_NCURSES_noecho: { // ( -- )
  PUSH_TOS;
  TOS = (cell_t)noecho();
}
  endcase;
case ID_NCURSES_halfdelay: { // ( n -- )
  int n = (int)TOS;
  TOS = (cell_t)halfdelay(n);
}
  endcase;
case ID_NCURSES_intrflush: { // ( win bool -- )
  bool ok = (bool)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)intrflush(win, ok);
}
  endcase;
case ID_NCURSES_keypad: { // ( win bool -- )
  bool ok = (bool)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)keypad(win, ok);
}
  endcase;
case ID_NCURSES_meta: { // ( win bool -- )
  bool ok = (bool)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)meta(win, ok);
}
  endcase;
case ID_NCURSES_nodelay: { // ( win bool -- )
  bool ok = (bool)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)nodelay(win, ok);
}
  endcase;
case ID_NCURSES_raw: { // ( -- )
  PUSH_TOS;
  TOS = (cell_t)raw();
}
  endcase;
case ID_NCURSES_noraw: { // ( -- )
  PUSH_TOS;
  TOS = (cell_t)noraw();
}
  endcase;
case ID_NCURSES_noqiflush: { // ( -- )
  noqiflush();
}
  endcase;
case ID_NCURSES_qiflush: { // ( -- )
  qiflush();
}
  endcase;
case ID_NCURSES_notimeout: { // ( win bool -- )
  bool ok = (bool)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = M_POP;
  notimeout(win, ok);
}
  endcase;
case ID_NCURSES_timeout: { // ( n -- )
  int n = (int)TOS;
  TOS = M_POP;
  timeout(n);
}
  endcase;
case ID_NCURSES_wtimeout: { // ( win n -- )
  int n = (int)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = M_POP;
  wtimeout(win, n);
}
  endcase;
case ID_NCURSES_typeahead: { // ( n -- )
  int n = (int)TOS;
  TOS = (cell_t)typeahead(n);
}
  endcase;
case ID_NCURSES_clearok: { // ( win bool -- )
  bool ok = (bool)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)clearok(win, ok);
}
  endcase;

case ID_NCURSES_idlok: { // ( win bool -- )
  bool ok = (bool)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)idlok(win, ok);
}
  endcase;

case ID_NCURSES_idcok: { // ( win bool -- )
  bool ok = (bool)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = M_POP;
  idcok(win, ok);
}
  endcase;

case ID_NCURSES_immedok: { // ( win bool -- )
  bool ok = (bool)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = M_POP;
  immedok(win, ok);
}
  endcase;

case ID_NCURSES_leaveok: { // ( win bool -- )
  bool ok = (bool)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)leaveok(win, ok);
}
  endcase;
case ID_NCURSES_setscrreg: { // ( top bottom -- )
  int bottom = (int)TOS;
  TOS = M_POP;
  int top = (int)TOS;
  TOS = M_POP;
  TOS = (cell_t)setscrreg(top, bottom);
}
  endcase;

case ID_NCURSES_wsetscrreg: { // ( win top bottom -- )
  int bottom = (int)TOS;
  TOS = M_POP;
  int top = (int)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)wsetscrreg(win, top, bottom);
}

case ID_NCURSES_scrollok: { // ( win bool -- )
  bool ok = (bool)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)scrollok(win, ok);
}
  endcase;

case ID_NCURSES_nl: { // ( -- )
  PUSH_TOS;
  TOS = (cell_t)nl();
}
  endcase;
case ID_NCURSES_nonl: { // ( -- )
  PUSH_TOS;
  TOS = (cell_t)nonl();
}
  endcase;

case ID_NCURSES_A_NORMAL: { // ( -- A_NORMAL )
  PUSH_TOS;
  TOS = (cell_t)A_NORMAL;
}
  endcase;
case ID_NCURSES_A_ATTRIBUTES: { // ( -- A_ATTRIBUTES )
  PUSH_TOS;
  TOS = (cell_t)A_ATTRIBUTES;
}
  endcase;
case ID_NCURSES_A_COLOR: { // ( -- A_COLOR )
  PUSH_TOS;
  TOS = (cell_t)A_COLOR;
}
  endcase;
case ID_NCURSES_A_STANDOUT: { // ( -- A_STANDOUT )
  PUSH_TOS;
  TOS = (cell_t)A_STANDOUT;
}
  endcase;
case ID_NCURSES_A_UNDERLINE: { // ( -- A_UNDERLINE )
  PUSH_TOS;
  TOS = (cell_t)A_UNDERLINE;
}
  endcase;
case ID_NCURSES_A_REVERSE: { // ( -- A_REVERSE )
  PUSH_TOS;
  TOS = (cell_t)A_REVERSE;
}
  endcase;
case ID_NCURSES_A_BLINK: { // ( -- A_BLINK )
  PUSH_TOS;
  TOS = (cell_t)A_BLINK;
}
  endcase;
case ID_NCURSES_A_DIM: { // ( -- A_DIM )
  PUSH_TOS;
  TOS = (cell_t)A_DIM;
}
  endcase;
case ID_NCURSES_A_BOLD: { // ( -- A_BOLD )
  PUSH_TOS;
  TOS = (cell_t)A_BOLD;
}
  endcase;
case ID_NCURSES_A_ALTCHARSET: { // ( -- A_ALTCHARSET )
  PUSH_TOS;
  TOS = (cell_t)A_ALTCHARSET;
}
  endcase;
case ID_NCURSES_A_INVIS: { // ( -- A_INVIS )
  PUSH_TOS;
  TOS = (cell_t)A_INVIS;
}
  endcase;
case ID_NCURSES_A_PROTECT: { // ( -- A_PROTECT )
  PUSH_TOS;
  TOS = (cell_t)A_PROTECT;
}
  endcase;
case ID_NCURSES_A_HORIZONTAL: { // ( -- A_HORIZONTAL )
  PUSH_TOS;
  TOS = (cell_t)A_HORIZONTAL;
}
  endcase;
case ID_NCURSES_A_LEFT: { // ( -- A_LEFT )
  PUSH_TOS;
  TOS = (cell_t)A_LEFT;
}
  endcase;
case ID_NCURSES_A_LOW: { // ( -- A_LOW )
  PUSH_TOS;
  TOS = (cell_t)A_LOW;
}
  endcase;
case ID_NCURSES_A_RIGHT: { // ( -- A_RIGHT )
  PUSH_TOS;
  TOS = (cell_t)A_RIGHT;
}
  endcase;
case ID_NCURSES_A_TOP: { // ( -- A_TOP )
  PUSH_TOS;
  TOS = (cell_t)A_TOP;
}
  endcase;
case ID_NCURSES_A_VERTICAL: { // ( -- A_VERTICAL )
  PUSH_TOS;
  TOS = (cell_t)A_VERTICAL;
}
  endcase;
case ID_NCURSES_A_ITALIC: { // ( -- A_ITALIC )
  PUSH_TOS;
  TOS = (cell_t)A_ITALIC;
}
  endcase;

case ID_NCURSES_attroff: { // ( attr -- ret )
  int attr = (int)TOS;
  TOS = (cell_t)attroff(attr);
}
  endcase;
case ID_NCURSES_wattroff: { // ( win attr -- ret )
  int attr = (int)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)wattroff(win, attr);
}
  endcase;

case ID_NCURSES_attron: { // ( attr -- ret )
  int attr = (int)TOS;
  TOS = (cell_t)attron(attr);
}
  endcase;
case ID_NCURSES_wattron: { // ( win attr -- ret )
  int attr = (int)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)wattron(win, attr);
}
  endcase;

case ID_NCURSES_attrset: { // ( attr -- ret )
  int attr = (int)TOS;
  TOS = (cell_t)attrset(attr);
}
  endcase;
case ID_NCURSES_wattrset: { // ( win attr -- ret )
  int attr = (int)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)wattrset(win, attr);
}
  endcase;

case ID_NCURSES_color_set: { // ( pair# opts -- ret )
  void *opts = (void *)TOS;
  TOS = M_POP;
  int pair = (int)TOS;
  TOS = (cell_t)color_set(pair, opts);
}
  endcase;

case ID_NCURSES_wcolor_set: { // ( win pair# opts -- ret )
  void *opts = (void *)TOS;
  TOS = M_POP;
  int pair = (int)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)wcolor_set(win, pair, opts);
}
  endcase;
case ID_NCURSES_standend: { // ( -- ret )
  PUSH_TOS;
  TOS = (cell_t)standend();
}
  endcase;
case ID_NCURSES_wstandend: { // ( win -- ret )
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)wstandend(win);
}
  endcase;
case ID_NCURSES_standout: { // ( -- ret )
  PUSH_TOS;
  TOS = (cell_t)standout();
}
  endcase;
case ID_NCURSES_wstandout: { // ( win -- ret )
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)wstandout(win);
}
  endcase;
case ID_NCURSES_attr_get: { // ( attrs pair opts -- ret )
  attr_t *attrs = (attr_t *)TOS;
  TOS = M_POP;
  short *pair = (short *)TOS;
  TOS = M_POP;
  void *opts = (void *)TOS;
  TOS = (cell_t)attr_get(attrs, pair, opts);
}
  endcase;
case ID_NCURSES_wattr_get: { // ( win attrs pair opts -- ret )
  attr_t *attrs = (attr_t *)TOS;
  TOS = M_POP;
  short *pair = (short *)TOS;
  TOS = M_POP;
  void *opts = (void *)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)wattr_get(win, attrs, pair, opts);
}
  endcase;
case ID_NCURSES_attr_off: { // ( attrs opts -- ret )
  attr_t attrs = (attr_t)TOS;
  TOS = M_POP;
  void *opts = (void *)TOS;
  TOS = (cell_t)attr_off(attrs, opts);
}
  endcase;
case ID_NCURSES_wattr_off: { // ( win attrs opts -- ret )
  attr_t attrs = (attr_t)TOS;
  TOS = M_POP;
  void *opts = (void *)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)wattr_off(win, attrs, opts);
}
  endcase;
case ID_NCURSES_attr_on: { // ( attrs opts -- ret )
  attr_t attrs = (attr_t)TOS;
  TOS = M_POP;
  void *opts = (void *)TOS;
  TOS = (cell_t)attr_on(attrs, opts);
}
  endcase;
case ID_NCURSES_wattr_on: { // ( win attrs opts -- ret )
  attr_t attrs = (attr_t)TOS;
  TOS = M_POP;
  void *opts = (void *)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)wattr_on(win, attrs, opts);
}
  endcase;
case ID_NCURSES_attr_set: { // ( attrs pair opts -- ret )
  attr_t attrs = (attr_t)TOS;
  TOS = M_POP;
  short pair = (short)TOS;
  TOS = M_POP;
  void *opts = (void *)TOS;
  TOS = (cell_t)attr_set(attrs, pair, opts);
}
  endcase;
case ID_NCURSES_wattr_set: { // ( win attrs pair opts -- ret )
  attr_t attrs = (attr_t)TOS;
  TOS = M_POP;
  short pair = (short)TOS;
  TOS = M_POP;
  void *opts = (void *)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)wattr_set(win, attrs, pair, opts);
}
  endcase;

case ID_NCURSES_chgat: { // ( n attr pair opts -- ret )
  const void *opts = (const void *)TOS;
  TOS = M_POP;
  short pair = (short)TOS;
  TOS = M_POP;
  attr_t attr = (attr_t)TOS;
  TOS = M_POP;
  int n = (int)TOS;
  TOS = (cell_t)chgat(n, attr, pair, opts);
}
  endcase;
case ID_NCURSES_wchgat: { // ( win n attr color opts -- ret )
  const void *opts = (const void *)TOS;
  TOS = M_POP;
  short color = (short)TOS;
  TOS = M_POP;
  attr_t attr = (attr_t)TOS;
  TOS = M_POP;
  int n = (int)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)wchgat(win, n, attr, color, opts);
}
  endcase;
case ID_NCURSES_mvchgat: { // ( y x n attr pair opts -- ret )
  const void *opts = (const void *)TOS;
  TOS = M_POP;
  short pair = (short)TOS;
  TOS = M_POP;
  attr_t attr = (attr_t)TOS;
  TOS = M_POP;
  int n = (int)TOS;
  TOS = M_POP;
  int x = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = (cell_t)mvchgat(y, x, n, attr, pair, opts);
}
  endcase;
case ID_NCURSES_mvwchgat: { // ( win y x n attr pair opts -- ret )
  const void *opts = (const void *)TOS;
  TOS = M_POP;
  short pair = (short)TOS;
  TOS = M_POP;
  attr_t attr = (attr_t)TOS;
  TOS = M_POP;
  int n = (int)TOS;
  TOS = M_POP;
  int x = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)mvwchgat(win, y, x, n, attr, pair, opts);
}
  endcase;
case ID_NCURSES_start_color: { // ( -- ret )
  PUSH_TOS;
  TOS = (cell_t)start_color();
}
  endcase;
case ID_NCURSES_init_pair: { // ( pair fg bg -- ret )
  short bg = (short)TOS;
  TOS = M_POP;
  short fg = (short)TOS;
  TOS = M_POP;
  short pair = (short)TOS;
  TOS = (cell_t)init_pair(pair, fg, bg);
}
  endcase;
case ID_NCURSES_init_color: { // ( color r g b -- ret )
  short b = (short)TOS;
  TOS = M_POP;
  short g = (short)TOS;
  TOS = M_POP;
  short r = (short)TOS;
  TOS = M_POP;
  short color = (short)TOS;
  TOS = (cell_t)init_color(color, r, g, b);
}
  endcase;
case ID_NCURSES_has_colors: { // ( -- ret )
  PUSH_TOS;
  TOS = (cell_t)has_colors();
}
  endcase;
case ID_NCURSES_can_change_color: { // ( -- ret )
  PUSH_TOS;
  TOS = (cell_t)can_change_color();
}
  endcase;
case ID_NCURSES_color_content: { // ( color -- r g b )
  short r, g, b;
  short color = (short)TOS;
  color_content(color, &r, &g, &b);
  TOS = r;
  PUSH_TOS;
  TOS = g;
  PUSH_TOS;
  TOS = b;
}
  endcase;
case ID_NCURSES_pair_content: { // ( pair -- fg bg )
  short fg, bg;
  short pair = (short)TOS;
  pair_content(pair, &fg, &bg);
  TOS = fg;
  PUSH_TOS;
  TOS = bg;
}
  endcase;
case ID_NCURSES_border: { // ( ls rs ts bs tl tr bl br -- ret )
  int br = (int)TOS;
  TOS = M_POP;
  int bl = (int)TOS;
  TOS = M_POP;
  int tr = (int)TOS;
  TOS = M_POP;
  int tl = (int)TOS;
  TOS = M_POP;
  int bs = (int)TOS;
  TOS = M_POP;
  int ts = (int)TOS;
  TOS = M_POP;
  int rs = (int)TOS;
  TOS = M_POP;
  int ls = (int)TOS;
  TOS = (cell_t)border(ls, rs, ts, bs, tl, tr, bl, br);
}
  endcase;
case ID_NCURSES_wborder: { // ( win ls rs ts bs tl tr bl br -- ret )
  int br = (int)TOS;
  TOS = M_POP;
  int bl = (int)TOS;
  TOS = M_POP;
  int tr = (int)TOS;
  TOS = M_POP;
  int tl = (int)TOS;
  TOS = M_POP;
  int bs = (int)TOS;
  TOS = M_POP;
  int ts = (int)TOS;
  TOS = M_POP;
  int rs = (int)TOS;
  TOS = M_POP;
  int ls = (int)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)wborder(win, ls, rs, ts, bs, tl, tr, bl, br);
}
  endcase;
case ID_NCURSES_box: { // ( win verch horch -- ret)
  chtype horch = (chtype)TOS;
  TOS = M_POP;
  chtype verch = (chtype)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)box(win, verch, horch);
}
  endcase;
case ID_NCURSES_hline: { // ( ch n -- ret )
  int n = (int)TOS;
  TOS = M_POP;
  chtype ch = (chtype)TOS;
  TOS = (cell_t)hline(ch, n);
}
  endcase;
case ID_NCURSES_whline: { // ( win ch n -- ret )
  int n = (int)TOS;
  TOS = M_POP;
  chtype ch = (chtype)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)whline(win, ch, n);
}
  endcase;
case ID_NCURSES_vline: { // ( ch n -- ret )
  int n = (int)TOS;
  TOS = M_POP;
  chtype ch = (chtype)TOS;
  TOS = (cell_t)vline(ch, n);
}
  endcase;
case ID_NCURSES_wvline: { // ( win ch n -- ret )
  int n = (int)TOS;
  TOS = M_POP;
  chtype ch = (chtype)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)wvline(win, ch, n);
}
  endcase;
case ID_NCURSES_mvhline: { // ( ch x y n -- ret )
  int n = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = M_POP;
  int x = (int)TOS;
  TOS = M_POP;
  chtype ch = (chtype)TOS;
  TOS = (cell_t)mvhline(ch, x, y, n);
}
  endcase;
case ID_NCURSES_mvvline: { // ( ch x y n -- ret )
  int n = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = M_POP;
  int x = (int)TOS;
  TOS = M_POP;
  chtype ch = (chtype)TOS;
  TOS = (cell_t)mvvline(ch, x, y, n);
}
  endcase;
case ID_NCURSES_mvwhline: { // ( win ch x y n -- ret )
  int n = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = M_POP;
  int x = (int)TOS;
  TOS = M_POP;
  chtype ch = (chtype)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)mvwhline(win, ch, x, y, n);
}
  endcase;
case ID_NCURSES_mvwvline: { // ( win ch x y n -- ret )
  int n = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = M_POP;
  int x = (int)TOS;
  TOS = M_POP;
  chtype ch = (chtype)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)mvwvline(win, ch, x, y, n);
}
  endcase;
case ID_NCURSES_curs_set: { // ( new -- old , set cursor state and return old state )
  int n = (int)TOS;
  TOS = (cell_t)curs_set(n);
}
  endcase;
case ID_NCURSES_newline: { // ( -- , emit newline )
  printw("\r\n");
}
  endcase;
case ID_NCURSES_bkgdset: { // ( ch -- , set character and attribute of background )
  chtype ch = (chtype)TOS;
  TOS = M_POP;
  bkgdset(ch);
}
  endcase;
case ID_NCURSES_wbkgdset: { // ( win ch -- , set character and attribute of window background )
  chtype ch = (chtype)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = M_POP;
  wbkgdset(win, ch);
}
  endcase;
case ID_NCURSES_bkgd: { // ( ch -- , set background property and apply to every character )
  chtype ch = (chtype)TOS;
  TOS = (cell_t)bkgd(ch);
}
  endcase;
case ID_NCURSES_wbkgd: { // ( win ch -- , set window background property and apply to every character )
  chtype ch = (chtype)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)wbkgd(win, ch);
}
  endcase;
case ID_NCURSES_getbkgd: { // ( win -- ch , get window's current background char/attribute pair )
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)getbkgd(win);
}
  endcase;
case ID_NCURSES_beep: { // ( -- f , beep terminal )}
  PUSH_TOS;
  TOS = (cell_t)beep();
}
  endcase;
case ID_NCURSES_flash: { // ( -- f , flash terminal )}
  PUSH_TOS;
  TOS = (cell_t)flash();
}
  endcase;

case ID_NCURSES_cursor_color: { // ( color -- )
  char *color = (char *)TOS;
  TOS = M_POP;
  printf("\033]12;%s\007", color);
}
  endcase;

case ID_NCURSES_BUTTON1_PRESSED: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON1_PRESSED;
}
  endcase;
case ID_NCURSES_BUTTON1_RELEASED: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON1_RELEASED;
}
  endcase;
case ID_NCURSES_BUTTON1_CLICKED: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON1_CLICKED;
}
  endcase;
case ID_NCURSES_BUTTON1_DOUBLE_CLICKED: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON1_DOUBLE_CLICKED;
}
  endcase;
case ID_NCURSES_BUTTON1_TRIPLE_CLICKED: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON1_TRIPLE_CLICKED;
}
  endcase;

case ID_NCURSES_BUTTON2_PRESSED: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON2_PRESSED;
}
  endcase;
case ID_NCURSES_BUTTON2_RELEASED: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON2_RELEASED;
}
  endcase;
case ID_NCURSES_BUTTON2_CLICKED: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON2_CLICKED;
}
  endcase;
case ID_NCURSES_BUTTON2_DOUBLE_CLICKED: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON2_DOUBLE_CLICKED;
}
  endcase;
case ID_NCURSES_BUTTON2_TRIPLE_CLICKED: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON2_TRIPLE_CLICKED;
}
  endcase;

case ID_NCURSES_BUTTON3_PRESSED: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON3_PRESSED;
}
  endcase;
case ID_NCURSES_BUTTON3_RELEASED: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON3_RELEASED;
}
  endcase;
case ID_NCURSES_BUTTON3_CLICKED: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON3_CLICKED;
}
  endcase;
case ID_NCURSES_BUTTON3_DOUBLE_CLICKED: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON3_DOUBLE_CLICKED;
}
  endcase;
case ID_NCURSES_BUTTON3_TRIPLE_CLICKED: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON3_TRIPLE_CLICKED;
}
  endcase;

case ID_NCURSES_BUTTON4_PRESSED: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON4_PRESSED;
}
  endcase;
case ID_NCURSES_BUTTON4_RELEASED: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON4_RELEASED;
}
  endcase;
case ID_NCURSES_BUTTON4_CLICKED: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON4_CLICKED;
}
  endcase;
case ID_NCURSES_BUTTON4_DOUBLE_CLICKED: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON4_DOUBLE_CLICKED;
}
  endcase;
case ID_NCURSES_BUTTON4_TRIPLE_CLICKED: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON4_TRIPLE_CLICKED;
}
  endcase;

case ID_NCURSES_BUTTON5_PRESSED: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON5_PRESSED;
}
  endcase;
case ID_NCURSES_BUTTON5_RELEASED: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON5_RELEASED;
}
  endcase;
case ID_NCURSES_BUTTON5_CLICKED: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON5_CLICKED;
}
  endcase;
case ID_NCURSES_BUTTON5_DOUBLE_CLICKED: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON5_DOUBLE_CLICKED;
}
  endcase;
case ID_NCURSES_BUTTON5_TRIPLE_CLICKED: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON5_TRIPLE_CLICKED;
}
  endcase;

case ID_NCURSES_BUTTON_CTRL: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON_CTRL;
}
  endcase;
case ID_NCURSES_BUTTON_SHIFT: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON_SHIFT;
}
  endcase;
case ID_NCURSES_BUTTON_ALT: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)BUTTON_ALT;
}
  endcase;
case ID_NCURSES_REPORT_MOUSE_POSITION: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)REPORT_MOUSE_POSITION;
}
  endcase;
case ID_NCURSES_ALL_MOUSE_EVENTS: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)ALL_MOUSE_EVENTS;
}
  endcase;

case ID_NCURSES_has_mouse: { // ( -- f )
  PUSH_TOS;
  TOS = (cell_t)has_mouse();
}
  endcase;
case ID_NCURSES_getmouse: { // ( -- id x y b err )
  MEVENT e;
  int err = getmouse(&e);

  PUSH_TOS
  TOS = (cell_t)e.id;
  PUSH_TOS
  TOS = (cell_t)e.y;
  PUSH_TOS
  TOS = (cell_t)e.x;
  PUSH_TOS
  TOS = (cell_t)e.bstate;
  PUSH_TOS
  TOS = (cell_t)err;
}
  endcase;
case ID_NCURSES_ungetmouse: { // ( id x y b -- )
  MEVENT e;
  e.bstate = (int)TOS;
  TOS = M_POP;
  e.y = (int)TOS;
  TOS = M_POP;
  e.x = (int)TOS;
  TOS = M_POP;
  e.id = (int)TOS;
  TOS = (cell_t)ungetmouse(&e);
}
  endcase;
case ID_NCURSES_wenclose: { // ( win y x -- f )
  int x = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)wenclose(win, y, x);
}
  endcase;
case ID_NCURSES_mouse_trafo: { // ( y x b -- f )
  bool b = (bool)TOS;
  TOS = M_POP;
  int x = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = (cell_t)mouse_trafo(&y, &x, b);
}
  endcase;
case ID_NCURSES_wmouse_trafo: { // ( win y x b -- y x err )
  bool b = (bool)TOS;
  TOS = M_POP;
  int x = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  int err = wmouse_trafo(win, &y, &x, b);
  TOS = (cell_t)y;
  PUSH_TOS;
  TOS = (cell_t)x;
  PUSH_TOS;
  TOS = (cell_t)err;
}
  endcase;

case ID_NCURSES_mouseinterval: { // ( n -- )
  int n = (int)TOS;
  TOS = (cell_t)mouseinterval(n);
}
  endcase;
case ID_NCURSES_mousemask: { // ( newmask -- oldmask acceptedmask )
  mmask_t mask = (mmask_t)TOS;
  mmask_t oldmask;
  mmask_t amask;
  amask = (cell_t)mousemask(mask, &oldmask);
  TOS = (cell_t)oldmask;
  PUSH_TOS;
  TOS = (cell_t)amask;
}
  endcase;

case ID_NCURSES_mouseon: {
  keypad(stdscr, TRUE);
  mousemask(ALL_MOUSE_EVENTS | REPORT_MOUSE_POSITION, NULL);
  keypad(stdscr, TRUE);
  //  printf("\033[?1003h\n"); // Makes the terminal report mouse movement events
}
  endcase;
case ID_NCURSES_mouseoff: {
  mousemask(~(ALL_MOUSE_EVENTS | REPORT_MOUSE_POSITION), NULL);
  //  printf("\033[?1003l\n"); // Makes the terminal report mouse movement events
}
  endcase;

  // wide characters

case ID_NCURSES_mvinvert_wch: { // ( y x -- )
  cchar_t ch;
  int x = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = M_POP;
  mvin_wch(y, x, &ch);
  setcchar(&ch, 0, A_REVERSE, 0, 0);
  mvadd_wch(y, x, &ch);
}
  endcase;

case ID_NCURSES_add_wch: { // ( ch -- err )
  cchar_t *ch = (cchar_t *)TOS;
  TOS = (cell_t)add_wch(ch);
}
  endcase;

case ID_NCURSES_wadd_wch: { // ( win ch -- err )
  cchar_t *ch = (cchar_t *)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)wadd_wch(win, ch);
}
  endcase;

case ID_NCURSES_mvadd_wch: { // ( y x ch -- err )
  cchar_t *ch = (cchar_t *)TOS;
  TOS = M_POP;
  int x = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = (cell_t)mvadd_wch(y, x, ch);
}
  endcase;

case ID_NCURSES_mvwadd_wch: { // ( win y x ch -- err )
  cchar_t *ch = (cchar_t *)TOS;
  TOS = M_POP;
  int x = (int)TOS;
  TOS = M_POP;
  int y = (int)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)mvwadd_wch(win, y, x, ch);
}
  endcase;

case ID_NCURSES_echo_wchar: { // ( ch -- err )
  cchar_t *ch = (cchar_t *)TOS;
  TOS = (cell_t)echo_wchar(ch);
}
  endcase;
case ID_NCURSES_wecho_wchar: { // ( win ch -- err )
  cchar_t *ch = (cchar_t *)TOS;
  TOS = M_POP;
  WINDOW *win = (WINDOW *)TOS;
  TOS = (cell_t)wecho_wchar(win, ch);
}
  endcase;

case ID_NCURSES_add_unicode: { // ( ch -- err )
  cchar_t cch;
  wchar_t wstr[256];
  wchar_t wch = (wchar_t)TOS;
  swprintf(wstr, 256, L"%lc", wch);

  setcchar(&cch, wstr, 0, 0, NULL);
  TOS = (cell_t)add_wch(&cch);
}
  endcase;
  // macros
case ID_NCURSES_COLOR_PAIR: { // ( n -- attr )
  int n = (int)TOS;
  TOS = (cell_t)COLOR_PAIR(n);
}
  endcase;
#endif
