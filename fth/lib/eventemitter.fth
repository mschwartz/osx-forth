anew task-lib/eventemitter.fth

require lib/arrays.fth
require lib/hashmap.fth

:STRUCT EventEmitter
    APTR EventEmitter.subscribers
;STRUCT

: EventEmitter.destroy-sub ( subscriber -- , destroy a subscriber entry ) 
;

: EventEmitter.new { | ee -- ee , new EventEmitter }
    sizeof() EventEmitter mem::malloc -> ee
    ['] EventEmitter.destroy-sub HashMap.new ee s! EventEmitter.subscribers
\ ee s@ EventEmitter.subscribers ." map " .hex
    ee
;

: EventEmitter.subscribe { ee $topic 'subscriber | a m -- , add subscription }
    ee s@ EventEmitter.subscribers  -> m
    m $topic HashMap.get -> a
    a 0= if
        10 Array.new -> a
        m $topic a HashMap.set drop
    then
    a 'subscriber Array.push
;

: EventEmitter.unsubscribe { ee $topic 'subscriber | a m ndx  -- remove subscriber }
    ee s@ EventEmitter.subscribers  -> m
    m $topic HashMap.get -> a
    a 0<> if
        a 'subscriber Array.index-of -> ndx
        ndx 0 >= if
            a ndx Array.delete-at
        then
    then
;

: EventEmitter.subscriber-count { ee $topic | m a -- n , get number of subscribers to topic }
    ee s@ EventEmitter.subscribers  -> m
    m $topic HashMap.get -> a
    a 0<> if
        a Array.length
    else
        0
    then
;

: EventEmitter.emit { ee $topic payload | func m a d -- , emit payload to $topic subscribers }
    ee s@ EventEmitter.subscribers  -> m
    m $topic HashMap.get -> a 
    a 0<> if
        a s@ Array.next 1 < if exit then
        a s@ Array.data -> d
        a s@ Array.next 0 do
            d i cells + @ -> func 
            func if 
                $topic payload func execute false = if exit then 
            then
        loop
    then
;
