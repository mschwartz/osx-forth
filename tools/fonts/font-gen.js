const fonts = require('./icons.json');

const toName = (n) => {
  return 'fa-' + n.replace(/ /g, '-').toLowerCase();
};

for (const key of Object.keys(fonts)) {
  console.log(`$ ${fonts[key].unicode} constant ${toName(fonts[key].label)}`);
}
//console.dir(fonts.accusoft);

//console.dir(fonts);
