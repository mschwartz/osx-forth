/* @(#) pf_main.c 98/01/26 1.2 */
/***************************************************************
** Forth based on 'C'
**
** main() routine that demonstrates how to call PForth as
** a module from 'C' based application.
** Customize this as needed for your application.
**
** Author: Phil Burk
**         and Mike Schwartz
**
** Copyright 1994 3DO, Phil Burk, Larry Polansky, David Rosenboom
**
** Permission to use, copy, modify, and/or distribute this
** software for any purpose with or without fee is hereby granted.
**
** THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
** WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL
** THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
** CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING
** FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
** CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
** OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
**
***************************************************************/

#if (defined(PF_NO_STDIO) || defined(PF_EMBEDDED))
#define NULL ((void *)0)
#define EMSG(msg) /* { printf msg; } */
#else
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/ioctl.h>

#define EMSG(msg) \
  { printf msg; }

#endif

#include "pforth.h"

#ifndef PF_DEFAULT_DICTIONARY
#define PF_DEFAULT_DICTIONARY "pforth.dic"
#endif

#ifndef TRUE
#define TRUE (1)
#define FALSE (0)
#endif

#ifdef PF_EMBEDDED
int main(void) {
  char IfInit = 0;
  const char *DicName = NULL;
  const char *SourceName = NULL;
  pfMessage("\npForth Embedded\n");
  return pfDoForth(DicName, SourceName, IfInit);
}
#else

extern cell_t gVarSignalsReceived;

// uint64_t signals = 0;

static void handle_signal(int sig) {
  gVarSignalsReceived |= 1 << sig;
}

static pid_t main_pid;
static void handle_sigint(int sig) {
  extern cell_t gVarRAW;
  if (gVarSignalsReceived & (1 << SIGINT)) {
    printf("About to exit on SIGINT\n");
    exit(0);
  }
  gVarSignalsReceived |= 1 << SIGINT;
  if (getpid() == main_pid && !gVarRAW) {
    printf("^C\n");
  }
}

static void handle_sigwinch(int sig) {
  extern cell_t gVarTerminalRows, gVarTerminalCols;
  struct winsize size;
  ioctl(0, TIOCGWINSZ, &size);
  gVarTerminalRows = size.ws_row;
  gVarTerminalCols = size.ws_col;
  gVarSignalsReceived |= 1 << SIGWINCH;
}

#if 0
static void handle_sigchld(int sig) {
  gVarSignalsReceived |= 1 << SIGCHLD;
  pid_t pid;
  int status;
  while ((pid = waitpid(-1, &status, WNOHANG)) > 0) {
    printf("Child %d exited with status %d\n", pid, status);
    //    unregister_child(pid, status); // Or whatever you need to do with the PID
  }
}
#endif
#ifdef PF_SUPPORT_SDL2
#include <SDL2/SDL.h>
static void cleanup_sdl(void) {
  SDL_Quit();
}

#endif

static const int signals[] = {
  SIGHUP,
  SIGINT,
  SIGQUIT,
  SIGILL,
  SIGTRAP,
  SIGABRT,
#ifdef __APPLE__
  SIGEMT,
  SIGINFO,
#endif
  SIGFPE,
  SIGKILL,
  SIGBUS,
  SIGSEGV,
  SIGSYS,
  SIGPIPE,
  SIGALRM,
  SIGTERM,
  SIGURG,
  SIGSTOP,
  SIGTSTP,
  SIGCONT,
  SIGCHLD,
  SIGTTIN,
  SIGTTOU,
  SIGIO,
  SIGXCPU,
  SIGXFSZ,
  SIGVTALRM,
  SIGPROF,
  SIGWINCH,
  SIGUSR1,
  SIGUSR2,
  -1,
};
int main(int argc, char **argv) {
  extern cell_t gVarArgC, gVarArgV, gVarArgV0, gVarSourceName;
  setlocale(LC_ALL, "");
#ifdef PF_SUPPORT_SDL2
  // SDL_Init(SDL_INIT_EVERYTHING);
  atexit(cleanup_sdl);
#endif
  for (int sig = 0; signals[sig] != -1; sig++) {
    if (sig != SIGINT) {
      signal(sig, handle_signal);
    }
  }
  main_pid = getpid();
  struct sigaction a;
  a.sa_handler = handle_sigint;
  a.sa_flags = 0;
  sigemptyset(&a.sa_mask);
  sigaction(SIGINT, &a, NULL);
  //  signal(SIGINT, handle_sigint);
  signal(SIGWINCH, handle_sigwinch);
  handle_sigwinch(SIGWINCH);

#ifdef PF_STATIC_DIC
  const char *DicName = NULL;
#else  /* PF_STATIC_DIC */
  const char *DicName = PF_DEFAULT_DICTIONARY;
#endif /* !PF_STATIC_DIC */

  const char *SourceName = NULL;
  char IfInit = FALSE;
  ThrowCode Result;
  int ch;

  pfSetQuiet(FALSE);
  /* Parse command line. */

  gVarArgV0 = (cell_t)argv[0];
  while ((ch = getopt(argc, argv, "hiqd:")) != -1) {
    switch (ch) {
      case 'i':
        IfInit = TRUE;
        DicName = NULL;
        break;
      case 'q':
        pfSetQuiet(TRUE);
        break;
      case 'd':
        if (optarg == NULL) {
          DicName = PF_DEFAULT_DICTIONARY;
        }
        else {
          printf("DIC %s\n", optarg);
          DicName = strdup(optarg);
        }
        break;
      case '?':
      case 'h':
        EMSG(("Usage:\n"));
        EMSG(("\tpforth {-i} {-q} {-d filename.dic} {sourcefilename} [...args]\n"));
        Result = 1;
        goto on_error;
        break;
      default:
        EMSG(("Unrecognized option! (%c)\n", ch));
        EMSG(("\tpforth {-i} {-q} {-dfilename.dic} {sourcefilename} [...args]\n"));
        Result = 1;
        goto on_error;
        break;
    }
  }
  argc -= optind;
  argv += optind;

  if (argc > 0) {
    if (strstr(argv[0], ".fth") != NULL) {
      SourceName = strdup(argv[0]);
      argc -= 1;
      argv += 1;
    }
  }

#ifdef __APPLE__
  optreset = 1;
#endif
  optind = 1;
  gVarSourceName = (cell_t)SourceName;
  gVarArgC = argc;
  gVarArgV = (cell_t)argv;

/* Force Init */
#ifdef PF_INIT_MODE
  IfInit = TRUE;
  DicName = NULL;
#endif

#ifdef PF_UNIT_TEST
  if ((Result = pfUnitTest()) != 0) {
    EMSG(("pForth stopping on unit test failure.\n"));
    goto on_error;
  }
#endif

  Result = pfDoForth(DicName, SourceName, IfInit);

on_error:
  return (int)Result;
}

#endif /* PF_EMBEDDED */
