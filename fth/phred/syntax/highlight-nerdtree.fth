\
\ PHRed editor
\
\ programmed by Mike Schwartz
\
\ nerdtree Syntax Highlighting
\

: Highlight.nerdtree { text -- , new-text }
    text string::strdup 
;
