# pForth - Portable Forth in 'C'

Forth is a public domain, portable ANS Forth based on a kernel written in ANSI 'C'. This makes it easy to port pForth to multiple platforms. So far, pForth has run on Macintosh, Windows, Linux, Beagle Board, Raspberry Pi, SUNs, Amigas, BeOS, Nokia Communicator, SGI Indys, 3DO ARM systems, 3DO PowerPC systems, WebTV systems, Hitachi SH4, OpenTV prototypes, Compaq Ipaq 3970, Sharp LH79520 ARM processor, Ciena Systems networking hardware, and some internal projects at Lucent. If you build pForth for an embedded system, please let me know and I will add your machine to the list of machines that pForth has run on.

PForth was developed by Phil Burk while working at 3DO. PForth is open source, and may be used for free.

## PForth Features

* ANS standard support for Core, Core Extensions, File-Access, Floating-Point, Locals, Programming-Tools, Strings word sets.
* Compiles from simple ANSI 'C' code with no special pre-processing needed. Also compiles under C++.
* INCLUDE reads source from normal files, not BLOCKs.
* Precompiled dictionaries can be saved and reloaded.
* Custom 'C' code can be easily linked with pForth.
* Handy words like ANEW  INCLUDE? SEE  WORDS.LIKE  FILE?
* Single Step Debugger
* Smart conditionals.  10 0 DO I . LOOP works in outer interpreter.
* Conditional compilation.  [IF]   [ELSE]   [THEN]
* Local variables using { }
* 'C' like structure defining words.
* Vectored execution using DEFER
* Can be compiled without any stdlib calls for embedded systems. Only needs custom KEY and EMIT equivalents in 'C'.

## pForth Documentation

* See the README.txt file in the pForthV* directory for version information.
* For information on pForth see the pForth Reference Manual.
* To learn the Forth language see the Forth Tutorial. 
* Peruse the pForth FAQ (Frequently Asked Questions)
* For a glossary of Forth words refer to the draft ANS Forth spec. https://www.taygeta.com/forthlit.html
