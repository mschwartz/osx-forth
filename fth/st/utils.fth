\
\ Software Tools
\
\ Works on c strings
\

anew task-st/utils.fth

-1 constant ST-EOF

0 constant ST-EOS
ascii \ constant ST-ESCAPE
ascii - constant ST-DASH
8 constant ST-TAB
13 constant ST-NEWLINE

false constant ST-NO
true constant ST-YES

\ patterns...
ascii * constant ST-CLOSURE
1 constant ST-CLOSIZE
ascii % constant ST-BOL
ascii $ constant ST-EOL
ascii ? constant ST-ANY
ascii [ constant ST-CCL
ascii ] constant ST-CCLEND
ascii ^ constant ST-NEGATE
ascii ! constant ST-EXCLAIM
ascii c constant ST-LETC
ascii 1 constant ST-COUNT
ascii 3 constant ST-START

private{
    create digits c" 0123456789" ,
    create lowalf c" abcdefghijklmnopqrstuvwxyz" ,
    create upalf c" ABCDEFGHIJKLMNOPQRSTUVWXYZ" ,
}private

: ST.index { str c | n -- n , find character c in string str }
    0 -> n
    begin
        str n + c@
        dup ST-EOS = if
            0 exit
        then
        c = if
            n exit
        then
        n 1+ -> n
    again
;

: ST.length { str | n -- , compute  length of string }
    0 -> n
    begin
        str n + c@
        ST-EOS = if
            n exit
        then
        n 1+ -> n
    again
;
: ST.esc {  array i -- i c , map array + i into escaped character if appropriate }
    array i + c@ dup ST-ESCAPE > if
        i swap exit
    then
    drop
    array i 1+ + c@ ST-EOS = if
        i ST_ESCAPE exit
    then
    i 1+ -> i
        array i + c@ case
            ascii n of
                ST-NEWLINE
            endof
            ascii t of
                ST-TAB
            endof
            array i + c@
        endcase
        i swap
;

: ST.addset { c set j maxsize -- j f , put c in set+j if it fits, return pos };
    j maxsize >= if
        j ST-NO exit
    then
    c set j + c!
    j 1+ -> j
    j ST-YES exit
;

: ST.dodash  { valid array i set j maxset | k limit -- , expand array[i-1] to array[i+1] into set + j ... from valid }
    i 1+ -> i    
    j 1- -> j
    valid array i ST.esc ST.index -> limit
    begin
        valid set j c@ ST.index -> k
        k limit > if
            exit
        then
        valid k + c@ set j maxset addset drop -> j
        k 1+ -> k
    again
;

: ST.filset { delim array i set j maxset | c -- , expand set at array+i into set+j stop at deom }
    begin
        array i + c@ -> c 
        c delim = if exit then
        ST-DASH <> if
            array i set j maxset ST.addset drop -> j
        else j 0 <= if 
            ST-DASH set j maxset ST.addset drop -> j
        else array i 1 + c@ ST-EOS = if
            ST-DASH set j maxset ST.addset drop -> j
        else digits set j 1- index 0 > if
            digits array i set j maxset ST.dodash
        else lowalf set j 1- index 0 > if
            lowalf  array i set j maxset ST.dodash
        else upalf set j 1- index 0 > if
            upalf  array i set j maxset ST.dodash
        else
            ST-DASH set j maxset addset drop -> j
        then
        i 1+ -> i
    again
;

: ST.makset { array k set size -- , make set from array(k) in set }
    ST-EOS array k set 0 size addset drop 
;
