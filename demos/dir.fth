#!/usr/local/bin/pforth_standalone -q
anew task-dir-demo.fth

0 trace-include !

require sys/memory.fth
require lib/debug.fth
require lib/strings.fth
require lib/lists.fth
require lib/icons.fth
require sys/fcntl.fth
require sys/path.fth

:STRUCT DirNode
    struct ListNode DirNode.node
    APTR DirNode.name
    UINT64 DirNode.type
    APTR DirNode.statbuf
;STRUCT

List dir-list
List sorted-list

: sort-insert { n | nam p -- , find place to insert node and insert it }
    n s@ DirNode.name -> nam
    sorted-list s@ List.next -> p
    begin
        p sorted-list <>
    while
        nam p s@ DirNode.name sys::strcmp 0> if
            \ nam < DirNode
            n p List.InsertBefore \ insert n before p
            exit
        then
        p s@ ListNode.next -> p
    repeat
    n sorted-list List.addTail
;

: sort-list { | n nxt -- , sort dir-list by filename }
    sorted-list List.init 
    dir-list s@ List.next -> n
    begin
        n dir-list <>
    while
        n s@ ListNode.next -> nxt
        n List.remove
        n sort-insert
        nxt -> n
    repeat
;

\ drwxrwxrwx
\ 9876543210
: bit-set? { n bit -- flg , is bit set in n? }
    n bit rshift 1 and 
;
: print-bit { ch mode bit -- , print ch if bit is set or ascii - }
    mode bit bit-set? if
\     mode bit rshift 1 and if
        ch emit
    else
        ascii - emit
    then
;

: print-mode { mode -- , print mode as drwxrwx, etc. }
    ascii d mode 9 print-bit
    ascii r mode 8 print-bit
    ascii w mode 7 print-bit
    ascii x mode 6 print-bit
    ascii r mode 5 print-bit
    ascii w mode 4 print-bit
    ascii x mode 3 print-bit
    ascii r mode 2 print-bit
    ascii w mode 1 print-bit
    ascii x mode 0 print-bit
;

: print-number { n w | str len -- , print number right aligned to width }
    n number>string -> len -> str
    w len - dup 0> if
        spaces 
    else
        drop
    then
    str len type
    space
;

: print-time { str -- , print time string from ctime format }
    str 4 + -> str
    3 0 do
        str c@ emit str 1+ -> str
    loop
    space
    str 1+ -> str

    str c@ emit str 1+ -> str
    str c@ emit str 1+ -> str

    str c@ emit str 1+ -> str \ blank
    5 0 do
        str c@ emit str 1+ -> str
    loop
;

variable max-uname-length 0 max-uname-length !
variable max-gname-length 0 max-gname-length !

: print-node { nod | stat len  str -- , print node }
    220 220 220 TIO.FG-RGB
    nod 0= if exit then
    nod s@ DirNode.statbuf -> stat

    stat sys::st_mode print-mode ."  "

    stat sys::st_nlink 3 print-number
\     stat sys::st_blocks 4 print-number

    stat sys::st_uid getuidname zcount max-uname-length @ 1+ type-aligned space
    stat sys::st_gid getgidname zcount max-gname-length @ 1+  type-aligned space

    stat sys::st_size 6 .R.human  space
    stat sys::st_mtime time::ctime print-time space

\     nod s@ DirNode.type DirEnt.type type space

    nod s@ DirNode.type case 
        DT_DIR of
            192 96 0 TIO.FG-RGB
            icon-folder emit-icon
        endof
        DT_LNK of
            0 255 0 TIO.FG-RGB
            icon-link emit-icon
        endof
        \ default
        stat sys::st_mode 6 bit-set? if
            0 0 192 TIO.FG-RGB
        else
            220 220 220 TIO.FG-RGB
        then
        icon-file emit-icon
    endcase
    space

    nod s@ DirNode.name zcount type
\     nod s@ DirNode.type DT_DIR = if
\         ." /"
\     then

    cr
;

variable total 0 total !
create fn-buffer 512 allot

: directory { $path | nod stat DIR de -- , main program }
    \ read directory entries into DirNodes so we can sort
    dir-list List.init 

    $path Path.opendir -> DIR
    DIR 0= if
        cr c" ** opendir " sys::perror
        exit
    then

    begin
        DIR Path.readdir -> de
        de nullptr <>
    while
        \ allocate DirNode and fill it in
        sizeof() DirNode MEM.malloc -> nod
        de sys::d_name sys::strdup nod s! DirNode.name
        de sys::d_type nod s! DirNode.type
        sizeof() statbuf MEM.malloc nod s! DirNode.statbuf

        $path Path.add-slash?
        $path count fn-buffer zplace        

        fn-buffer nod s@ DirNode.name sys::strcat drop
        fn-buffer nod s@ DirNode.statbuf Path.statz 
        stat 0< if
\             cr c" *** stat " sys::perror 
        else
            nod s@ DirNode.statbuf -> stat
            stat sys::st_blocks total @ + total !
            stat sys::st_uid getuidname zcount dup max-uname-length @ > if
                max-uname-length !
            else
                drop
            then
            stat sys::st_gid getgidname zcount dup max-gname-length @ > if
                max-gname-length !
            else
                drop
            then

            dup 
        then
        nod dir-list List.addTail
    repeat

    sort-list
    ." total  " total @ . cr
    sorted-list ['] print-node List.iterate
\     cr ." max uname " max-uname-length @ 2+ .
\     cr ." max gname " max-gname-length @ 2+ .
;

: demo { $path | nod st -- , do directory listing }
    sizeof() statbuf MEM.malloc -> st
    $path count pad zplace
    pad st sys::statz
    0< if
        cr c" *** " sys::perror
        exit
    then
    st sys::st_mode S_ISDIR if
        $path directory exit
    then
    \ fake a node so we can print it as we would in a directlry listing


    sizeof() DirNode MEM.malloc -> nod
    st nod s! DirNode.statbuf
    pad sys::strdup nod s! DirNode.name 
    st sys::st_mode st_mode>de_type nod s! DirNode.type
\     nod s@ DirNode.name 20 dump bye
    nod print-node
;

: main ( -- )
    0 argc @ = if
        cr ." Usage dir.fth <path>""
        cr ."  prints directory of <path> "
        exit
    then
    cr next-arg 
    dup here c!  ( -- zstr zcount )
    here 1+     ( -- zstr zcount dst )
    swap
    cmove
\     cr ." dir of  " pad $type cr
    here demo
;

main
\ time::time time::ctime ztype
cr

