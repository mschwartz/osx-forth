anew task-sdl-events.fth

include sdl/keyboard.fth

\
\ Event types
\

$  000 constant SDL_FIRSTEVENT
$  100 constant SDL_EVENT_QUIT
$  101 constant SDL_EVENT_APP_TERMINATING
$  102 constant SDL_EVENT_APP_LOWMEMORY
\ $  103 constant SDL_EVENT_APP_WILLENTERBACKGROUND
$  104 constant SDL_EVENT_APP_DIDENTERBACKGROUND
\ $  105 constant SDL_EVENT_APP_DIDENTERFOREGROUND
$  106 constant SDL_EVENT_LOCALECHANGED
\ Display events
$  150 constant SDL_EVENT_DISPLAYEVENT
\ Window events
$  200 constant SDL_EVENT_WINDOWEVENT
$  201 constant SDL_EVENT_SYSWMEVENT
\ Keyboard events
$  300 constant SDL_EVENT_KEYDOWN
$  301 constant SDL_EVENT_KEYUP
$  303 constant SDL_EVENT_TEXTEDITING
$  304 constant SDL_EVENT_TEXTINPUT
$  305 constant SDL_EVENT_KEYMAPCHANGED
$  306 constant SDL_EVENT_TEXTEDITING_EXT
\ Mouse events
$  400 constant SDL_EVENT_MOUSEMOTION
$  401 constant SDL_EVENT_MOUSEBUTTONDOWN
$  402 constant SDL_EVENT_MOUSEBUTTONUP
$  403 constant SDL_EVENT_MOUSEWHEEL
\ Joystick events
$  600 constant SDL_EVENT_JOYAXISMOTION
$  601 constant SDL_EVENT_JOYBALLMOTION
$  602 constant SDL_EVENT_JOYBUTTONDOWN
$  603 constant SDL_EVENT_JOYBUTTONUP
$  604 constant SDL_EVENT_JOYDEVICEADDED
\ $  605 constant SDL_EVENT_JOYDEVICEREMOVED
$  606 constant SDL_EVENT_JOYBATTERYUPDATED
\ Game controller events
$  650 constant SDL_EVENT_CONTROLLERAXISMOTION
$  651 constant SDL_EVENT_CONTROLLERBUTTONDOWN
$  652 constant SDL_EVENT_CONTROLLERBUTTONUP
$  653 constant SDL_EVENT_CONTROLLERDEVICEADDED
\ $  654 constant SDL_EVENT_CONTROLLERDEVICEREMOVED
\ $  655 constant SDL_EVENT_CONTROLLERDEVICEREMAPPED
$  656 constant SDL_EVENT_TOUCHPADDOWN
$  657 constant SDL_EVENT_TOUCHPADMOTION
$  658 constant SDL_EVENT_TOUCHPADUP
$  659 constant SDL_EVENT_ONTROLLERSENSORUPDATE
\     SDL_EVENT_CONTROLLERSENSORUPDATE,        /**< Game controller sensor was updated */
\ Touch events
$  700 constant SDL_EVENT_FINGERDOWN
$  701 constant SDL_EVENT_FINGERUP
$  702 constant SDL_EVENT_FINGERMOTION
\ Gesture events
$  800 constant SDL_EVENT_DOLLARGESTURE
$  801 constant SDL_EVENT_DOLLARRECORD
$  802 constant SDL_EVENT_MULTIGESTURE
\ Clipboard events
$  900 constant SDL_EVENT_CLIPBOARDUPDATE
\ Drag and drop events
$ 1000 constant SDL_EVENT_DROPFILE
$ 1001 constant SDL_EVENT_DROPTEXT
$ 1002 constant SDL_EVENT_DROPBEGIN
$ 1003 constant SDL_EVENT_DROPCOMPLETE
\ Audio hotplug events
$ 1100 constant SDL_EVENT_AUDIODEVICEADDED
$ 1001 constant SDL_EVENT_AUDIODEVICEREMOVED
\ Sensor events
$ 1200 constant SDL_EVENT_SENSORUPDATE
\ Render events
$ 2000 constant SDL_EVENT_RENDER_TARGETS_RESET
$ 2001 constant SDL_EVENT_RENDER_DEVICE_RESET
\ Internal events
$ 7f00 constant SDL_EVENT_POLLSENTINAL
\ Events SDL_EVENT_USEREVENT through SDL_EVENT_LASTEVENT are for programmer use
$ 8000 constant SDL_EVENT_USEREVENT
$ ffff constant SDL_EVENT_LAST_EVENT

\ for buttons
0 constant SDL_RELEASED
1 constant SDL_PRESSED

\ TODO finish this!
0 [if]
: .EventType ( type -- , print human readable string for event type)
    CASE
        SDL_FIRSTEVENT                    of ." SDL_FIRSTEVENT" endof
        SDL_EVENT_QUIT                    of ." SDL_EVENT_QUIT" endof
        SDL_EVENT_APP_TERMINATING         of ." SDL_EVENTAPP_TERMINATING" endof
        SDL_EVENT_APP_LOWMEMORY           of ." SDL_EVENT_APP_LOWMEMORY" endof
        SDL_EVENT_APP_WILLENTERBACKGROUND of ." SDL_EVENT_APP_WILLENTERBACKGROUND" endof
        SDL_EVENT_APP_DIDENTERBACKGROUND  of ." SDL_EVENT_APP_DIDENTERBACKGROUND" endof
        SDL_EVENT_APP_DIDENTERFOREGROUND  of ." SDL_EVENT_APP_DIDENTERFOREGROUND" endof
        SDL_EVENT_LOCALECHANGED           of ." SDL_EVENT_LOCALECHANGED" endof
        SDL_EVENT_DISPLAYEVENT            of ." SDL_EVENT_DISPLAYEVENT" endof
        SDL_EVENT_WINDOWEVENT             of ." SDL_EVENT_WINDOWEVENT" endof
        SDL_EVENT_SYSWMEVENT              of ." SDL_EVENT_SYSWMEVENT" endof
        SDL_EVENT_KEYDOWN                 of ." SDL_EVENT_EVENT_KEYDOWN" endof
        SDL_EVENT_KEYUP                   of ." SDL_EVENT_EVENT_KEYUP" endof
    ENDCASE
;
[then]

:STRUCT CommonEvent
    UINT32 CommonEvent.type
    UINT32 CommonEvent.timestamp
;STRUCT

:STRUCT DisplayEvent
    UINT32 DisplayEvent.type
    UINT32 DisplayEvent.timestamp
    UINT32 DisplayEvent.display      \ the associated display index
    UBYTE  DisplayEvent.event        \ SDL_DisplayEventID
    UBYTE  DisplayEvent.padding1
    UBYTE  DisplayEvent.padding2
    UBYTE  DisplayEvent.padding3
    INT32  DisplayEvent.data1        \ event dependent data
;STRUCT

:STRUCT WindowEvent
    UINT32 WindowEvent.type
    UINT32 WindowEvent.timestamp
    UINT32 WindowEvent.windowID      \ the associated window
    UINT8  WindowEvent.event         \ SDL_WindowEventID
    UBYTE  WindowEvent.padding1
    UBYTE  WindowEvent.padding2
    UBYTE  WindowEvent.padding3
    INT32  WindowEvent.data1         \ event dependent data
    INT32  WindowEvent.data2         \ event dependent data
;STRUCT

:STRUCT KeyboardEvent
    UINT32 KeyboardEvent.type
    UINT32 KeyboardEvent.timestamp
    UINT32 KeyboardEvent.windowID     \ the associated window
    UINT8 KeyboardEvent.state         \ SDL_PRESSED or SDL_RELEASED
    UINT8 KeyboardEvent.repeat        \ non-zero if this is a key repeat
    UBYTE KeyboardEvent.padding1
    UBYTE KeyboardEvent.padding2
    STRUCT keysym KeyboardEvent.key
;STRUCT

:STRUCT TextEditingEvent
    UINT32 TextEditingEvent.type
    UINT32 TextEditingEvent.timestamp
    UINT32 TextEditingEvent.windowID
    INT32 TextEditingEvent.start
    INT32 TextEditingEvent.length
;STRUCT

:STRUCT TextEditingExtEvent
    UINT32 TextEditingExtEvent.type
    UINT32 TextEditingExtEvent.timestamp
    UINT32 TextEditingExtEvent.windowID
    APTR   TextEditingExtEvent.text
    INT32  TextEditingExtEvent.start
    INT32  TextEditingExtEvent.length
;STRUCT

32 constant SDL_TEXTINPUTEVENT_TEXT_SIZE
:STRUCT TextInputEvent
    UINT32 TextInputEvent.type
    UINT32 TextInputEvent.timestamp
    UINT32 TextInputEvent.windowID
    SDL_TEXTINPUTEVENT_TEXT_SIZE ARRAY TextInputEvent.text
;STRUCT

:STRUCT MouseMotionEvent
    UINT32 MouseMotionEvent.type
    UINT32 MouseMotionEvent.timestamp
    UINT32 MouseMotionEvent.windowID
    UINT32 MouseMotionEvent.which
    UINT32 MouseMotionEvent.state
    INT32  MouseMotionEvent.x
    INT32  MouseMotionEvent.y
    INT32  MouseMotionEvent.xrel
    INT32  MouseMotionEvent.yrel
;STRUCT

:STRUCT MouseButtonEvent
    UINT32 MouseButtonEvent.type
    UINT32 MouseButtonEvent.timestamp
    UINT32 MouseButtonEvent.windowID
    UINT32 MouseButtonEvent.which
    UINT8  MouseButtonEvent.button
    UINT8  MouseButtonEvent.state         \ SDL_PRESSED or SDL_RELEASED
    UINT8  MouseButtonEvent.clicks        \ 1 for single click, 2 for doubleclick
    UINT8  MouseButtonEvent.padding1
    INT32  MouseButtonEvent.x
    INT32  MouseButtonEvent.y
;STRUCT

:STRUCT MouseWheelEvent
    UINT32 MouseWheelEvent.type
    UINT32 MouseWheelEvent.timestamp
    UINT32 MouseWheelEvent.windowID
    UINT32 MouseWheelEvent.which
    INT32  MouseWheelEvent.x
    INT32  MouseWheelEvent.y
    FLPT   MouseWheelEvent.precixeX
    FLPT   MouseWheelEvent.precixeY
    INT32  MouseWheelEvent.mouseX
    INT32  MouseWheelEvent.mouseY
;STRUCT

:STRUCT JoyAxisEvent
    UINT32 JoyAxisEvent.type
    UINT32 JoyAxisEvent.timestamp
    INT32  JoyAxisEvent.which
    UINT8  JoyAxisEvent.axis
    UINT8  JoyAxisEvent.padding1
    UINT8  JoyAxisEvent.padding2
    UINT8  JoyAxisEvent.padding3
    INT16  JoyAxisEvent.value
    INT16  JoyAxisEvent.padding4
;STRUCT

:STRUCT JoyBallEvent
    UINT32 JoyBallEvent.type
    UINT32 JoyBallEvent.timestamp
    INT32  JoyBallEvent.which
    UINT8  JoyBallEvent.ball
    UINT8  JoyBallEvent.padding1
    UINT8  JoyBallEvent.padding2
    UINT8  JoyBallEvent.padding3
    INT16  JoyBallEvent.xrel
    INT16  JoyBallEvent.yrel
;STRUCT

:STRUCT JoyHatEvent
    UINT32 JoyHatEvent.type
    UINT32 JoyHatEvent.timestamp
    INT32  JoyHatEvent.which
    UINT8  JoyHatEvent.hat
    UINT8  JoyHatEvent.value
    UINT8  JoyHatEvent.padding1
    UINT8  JoyHatEvent.padding2
;STRUCT

:STRUCT JoyButtonEvent
    UINT32 JoyButtonEvent.type
    UINT32 JoyButtonEvent.timestamp
    INT32  JoyButtonEvent.which
    UINT8  JoyButtonEvent.button
    UINT8  JoyButtonEvent.state
    UINT8  JoyButtonEvent.padding1
    UINT8  JoyButtonEvent.padding2
;STRUCT


:STRUCT JoyDeviceEvent
    UINT32 JoyDeviceEvent.type
    UINT32 JoyDeviceEvent.timestamp
    INT32  JoyDeviceEvent.which
;STRUCT


-1 constant SDL_JOYSTICK_POWER_UNKNOWN
 0 constant SDL_JOYSTICK_POWER_EMPTY   \ /* <= 5% */
 1 constant SDL_JOYSTICK_POWER_LOW,     \ /* <= 20% */
 2 constant SDL_JOYSTICK_POWER_MEDIUM,  \ /* <= 70% */
 3 constant SDL_JOYSTICK_POWER_FULL,    \ /* <= 100% */
 4 constant SDL_JOYSTICK_POWER_WIRED,
 5 constant SDL_JOYSTICK_POWER_MAX

:STRUCT JoyBatteryEvent
    UINT32 JoyBatteryEvent.type
    UINT32 JoyBatteryEvent.timestamp
    INT32  JoyBatteryEvent.which
    INT    JoyBatteryEvent.powerlevel
;STRUCT

:STRUCT ControllerAxisEvent
    UINT32 ControllerAxisEvent.type
    UINT32 ControllerAxisEvent.timestamp
    INT32  ControllerAxisEvent.which
    UINT8  ControllerAxisEvent.axis
    UINT8  ControllerAxisEvent.padding1
    UINT8  ControllerAxisEvent.padding2
    UINT8  ControllerAxisEvent.padding3
    INT16  ControllerAxisEvent.value
    INT16  ControllerAxisEvent.padding4
;STRUCT

:STRUCT ControllerButtonEvent
    UINT32 ControllerButtonEvent.type
    UINT32 ControllerButtonEvent.timestamp
    INT32  ControllerButtonEvent.which
    UINT8  ControllerButtonEvent.button
    UINT8  ControllerButtonEvent.state
    UINT8  ControllerButtonEvent.padding1
    UINT8  ControllerButtonEvent.padding2
;STRUCT

:STRUCT ControllerDeviceEvent
    UINT32 ControllerDeviceEvent.type
    UINT32 ControllerDeviceEvent.timestamp
    INT32  ControllerDeviceEvent.which
;STRUCT

:STRUCT TouchPadpadEvent
    UINT32 TouchPadpadEvent.type
    UINT32 TouchPadpadEvent.timestamp
    INT32  TouchPadpadEvent.which
    INT32  TouchPadpadEvent.finger
    FLPT   TouchPadpadEvent.x
    FLPT   TouchPadpadEvent.y
    FLPT   TouchPadpadEvent.pressure
;STRUCT

:STRUCT ControllerSensorEvent
    UINT32 ControllerSensorEvent.type
    UINT32 ControllerSensorEvent.timestamp
    INT32  ControllerSensorEvent.which
    INT32  ControllerSensorEvent.sensor
    FLPT  ControllerSensorEvent.data0
    FLPT  ControllerSensorEvent.data1
    FLPT  ControllerSensorEvent.data2
    UINT64 ControllerSensorEvent.timestamp_us
;STRUCT

:STRUCT AudioDeviceEvent
    UINT32 AudioDeviceEvent.type
    UINT32 AudioDeviceEvent.timestamp
    INT32  AudioDeviceEvent.which
    UINT8  AudioDeviceEvent.iscapture       \ 0 if an output device, 1 if a capture device
    UINT8  AudioDeviceEvent.padding1
    UINT8  AudioDeviceEvent.padding2
    UINT8  AudioDeviceEvent.padding3
;STRUCT

:STRUCT TouchFingerEvent
    UINT32 TouchFingerEVent.type
    UINT32 TouchFingerEvent.timestamp
    UINT32 TouchFingerEvent.touchId
    UINT32 TouchFingerEvent.fingerId
    FLPT   TouchFingerEvent.x
    FLPT   TouchFingerEvent.y
    FLPT   TouchFingerEvent.dx
    FLPT   TouchFingerEvent.dy
    UINT32 TouchFIngerEvent.windowID
;STRUCT

:STRUCT MultiGestureEvent
    UINT32 MultiGestureEVent.type
    UINT32 MultiGestureEvent.timestamp
    UINT32 MultiGestureEvent.touchId
    FLPT   MultiGestureEvent.dTheta
    FLPT   MultiGestureEvent.dDist
    FLPT   MultiGestureEvent.x
    FLPT   MultiGestureEvent.y
    UINT16 MultiGestureEvent.numFingers
    UINT16 MultiGestureEvent.padding
;STRUCT


:STRUCT DollarGestureEvent
    UINT32 DollarGestureEvent.type
    UINT32 DollarGestureEvent.timestamp
    UINT32 DollarGestureEvent.touchId
    UINT64 DollarGestureEvent.gestureId
    UINT32 DollarGestureEvent.numFingers
    FLPT   DollarGestureEvent.error
    FLPT   DollarGestureEvent.x
    FLPT   DollarGestureEvent.y
;STRUCT

:STRUCT DropEvent
    UINT32 DropEvent.type
    UINT32 DropEvent.timestamp
    APTR   DropEvent.file       \ filename, should be freed with SDL_Free()
    UINT32 DropEvent.windowID
;STRUCT

:STRUCT SensorEvent
    UINT32 SensorEvent.type
    UINT32 SensorEvent.timestamp
    INT32  SensorEvent.which
    FLPT   SensorEvent.data0
    FLPT   SensorEvent.data1
    FLPT   SensorEvent.data2
    FLPT   SensorEvent.data3
    FLPT   SensorEvent.data4
    FLPT   SensorEvent.data5
    FLPT   SensorEvent.data6
    UINT64 SensorEvent.timestamp_us
;STRUCT

:STRUCT QuitEvent
    UINT32 QuitEvent.type
    UINT32 QuitEvent.timestamp
;STRUCT

:STRUCT OSEvent
    UINT32 OSEvent.type
    UINT32 OSEvent.timestamp
;STRUCT

:STRUCT UserEvent
    UINT32 UserEvent.type
    UINT32 UserEvent.timestamp
    UINT32 UserEvent.windowID
    INT32  UserEvent.code
    APTR   UserEvent.data1
    APTR   UserEvent.data2
;STRUCT

\ see SDL_syswm.h
:STRUCT SysWMEvent
    UINT32 SysWMEvent.type
    UINT32 SysWMEvent.timestamp
    APTR   SysWMEvent.msg
;STRUCT
