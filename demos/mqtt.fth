#!/usr/local/bin/pforth_standalone -q

\
\ MQTT demo
\
\ Usage: 
\   mqtt subscribe "<topic>"
\   mqtt publish "<topic>" "<message>"
\

false trace-include !
true quiet !

require lib/mqtt.fth

\
\ SUBSCRIBE DEMO
\
: handle-raw-message { $topic payload -- , handle raw MQTT message }
    cr ." raw message topic " $topic $type
    cr ." payload " payload zcount 100 min type ."  ..."
    cr
;

: handle-message { $topic payload -- , handle incoming MQTT message }
    cr ." message topic " $topic $type
    cr ." payload " payload zcount 100 min type ."  ..."
    cr
;

: subscribe { | mosq err -- , }
    argc @ 2 <> if 
        cr ." Usage: "
        cr ."    " sys::argv0 @ zcount type space sys::sourcename @ zcount type ."  subscribe hostname topic"
        cr
        1 sys::exit
    then

    cr ." SUBSCRIBE DEMO " cr

    cr ." new MQTT ... "
    next-arg pad place-cstr pad 1883 MQTT.new -> mosq
    ." ok mosq = $" mosq .hex ." err = " mosq s@ MQTT.error .

    ['] handle-raw-message mosq s! MQTT.onMessage

    cr ." Connecting to host " pad ztype ."  ... "
    mosq MQTT.connect -> err
    err MOSQ_ERR_SUCCESS <> if
        cr cr ." *** connect error " err MQTT::strerror zcount type
        bye
    then
    ." connected err = "  err .
    next-arg pad place
    cr ." Subscribing to " pad $type ."  ... "
    mosq pad ['] handle-message MQTT.subscribe -> err
    err MOSQ_ERR_SUCCESS <> if
        cr cr ." *** subscribe error " err MQTT::strerror zcount type
        bye
    then
    ." ok " err .

    cr ." waiting for messages "
    begin
        key? if key exit then
        mosq MQTT.loop
    again
;

\
\ PUBLISH DEMO
\
variable publish-done false publish-done !
: handle-publish { -- , onPublish handler }
    cr ." handle-publish PUBLISHED! " cr
    true publish-done !
;
create publish-pad 512 allot
: publish { | mosq err payload -- , }
    argc @ 3 <> if 
        cr ." Usage: "
        cr ."    " sys::argv0 @ zcount type space sys::sourcename @ zcount type ."  publish hostname topic message"
        cr
        1 sys::exit
    then
    cr ." PUBLISH DEMO" cr

    cr ." new MQTT ... "
    next-arg pad place-cstr pad 1883 MQTT.new -> mosq
    ." ok mosq = $" mosq .hex ." err = " mosq s@ MQTT.error .
    ['] handle-publish mosq s! MQTT.onPublish

    cr ." Connecting to host " pad ztype ."  ... "
    mosq MQTT.connect -> err
    err MOSQ_ERR_SUCCESS <> if
        cr cr ." *** connect error " err MQTT::strerror zcount type
        bye
    then
    ." connected err = "  err .

    next-arg publish-pad place \ $topic
    next-arg drop -> payload
    ." Publishing topic '" publish-pad $type ." ' payload = " payload 10 dump
    mosq publish-pad payload true MQTT.publish -> err
    err MOSQ_ERR_SUCCESS <> if
        cr cr ." *** connect error " err MQTT::strerror zcount type
        bye
    then
    cr ." waiting for publish "
    begin
        publish-done @ false =
    while
        key? if key exit then
        mosq MQTT.loop
    repeat    

;

\
\ ROBODOMO DEMO
\

\ Note that RoboDomo is my own custom/home brewed Home Automation system that is
\ fully driven by MQTT for state and commands.  You certainly don't have it installed,
\ so this test is meant for me to exercise and test assorted MQTT functionality.
: robodomo-handle-position { $topic payload -- , handle incoming MQTT message }
    cr ." position topic " $topic $type
    cr ." payload " payload zcount type
    cr
;

: robodomo-handle-info { $topic payload -- , handle incoming MQTT message }
    cr ." info topic " $topic $type
    cr ." payload " payload zcount 100 min type ."  ..."
    cr
;

: Robodomo { | mosq err msg -- , MQTT demo }
    cr ." new MQTT"
    z" mqtt" 1+ 1883 MQTT.new -> mosq
    ." ok mosq = $" mosq .hex

    cr ." Connecting... "
    mosq MQTT.connect
    err MOSQ_ERR_SUCCESS <> if
        cr cr ." *** connect error " err MQTT::strerror zcount type
        bye
    then
    ." connected "
    cr ." Subscribing... " 
    mosq c" appletv/appletv-theater/status/info" ['] robodomo-handle-info MQTT.subscribe -> err
    mosq c" appletv/appletv-theater/status/position" ['] robodomo-handle-position MQTT.subscribe -> err
    err MOSQ_ERR_SUCCESS <> if
        cr cr ." *** subscribe error " err MQTT::strerror zcount type
        bye
    then
    ." ok" 

    begin
        key? if key exit then
        mosq MQTT.loop
    again
;

\
\ MAIN PROGRAM
\
: usage ( -- , print usage and exit )
    cr ." Usage: "
    cr ."    " sys::argv0 @ zcount type space sys::sourcename @ zcount type ."  <command> [...opts] "
    cr ." Where command and opts may be:"
    cr ."    subscribe hostname topic"
    cr ."    publish hostname topic message"
    cr ."    robodomo"
    1 sys::exit
;

: demo { | caddr u -- , MQTT demo }
    next-arg -> u -> caddr
    caddr u s" robodomo" compare 0= if
        RoboDomo
        exit
    then
    caddr u s" subscribe" compare 0= if
        subscribe 0 sys::exit
        exit
    then
    caddr u s" publish" compare 0= if
        argc 3 < if usage then
        publish
        exit
    then
    cr cr ." *** Invalid arguments "
    usage
;

demo 
cr
cr
cr
0 sys::exit
