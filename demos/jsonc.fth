#!/usr/local/bin/pforth_standalone -q

anew demos/jsonc.fth

require lib/jsonc.fth

: demo { | o oo -- , }
    cr cr ." JSON-C DEMO "

    cr cr ." TESTING create JSON from code "
    JSON.object.new -> o  
    JSON.object.new -> oo 
        o c" foo" oo JSON.object.add 
            oo c" baz" c" another test string" JSON.string.new JSON.object.add
        o c" bar" c" test string" JSON.string.new JSON.object.add

    cr oo c" baz" JSON.get_object_ex -> oo
    o JSON.pretty-print cr zcount type
    cr ." get foo.baz as string = " oo JSON.get_string ztype
    o JSON.free

    cr cr ." TESTING read JSON from file demos/sample.json "
    c" demos/sample.json" JSON.parse-file -> o
    o JSON.pretty-print cr zcount type
    o JSON.free

    cr cr ." TESTING read JSON from string"
    s\" {\"foo\": 10, \"bar\": \"demo string for bar\", \"baz\": { \"n\": 100}}\z" drop JSON.parse -> o
    o JSON.pretty-print cr zcount type
    o JSON.free
;

demo
cr
