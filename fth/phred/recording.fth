0 constant RECORDING-EMPTY
1 constant RECORDING-INPROGRESS
2 constant RECORDING-PLAYBACK
3 constant RECORDING-READY
4 constant RECORDING-PAUSED

:STRUCT Recording
    ULONG Recording.mode
    ULONG Recording.count
    APTR Recording.string
;STRUCT

: Recording.dump { r -- , dump recording }
    cr ." Recording $" r .hex
    cr ."        mode =  " r s@ Recording.mode .
    cr ."       count =  " r s@ Recording.count .
    cr ."      string =  " r s@ Recording.string .hex
    cr ."    (string) =  " r s@ Recording.string s@ CString.data 20 dump
;

: Recording.data { r -- ptr , get data/string of recording }
    r s@ Recording.string s@ CString.data
;

: Recording.new  { | r -- r , new recording }
    sizeof() Recording mem::malloc -> r
    CString.new-empty r s! Recording.string
    RECORDING-EMPTY r s! Recording.mode
    0 r s! Recording.count
    r
;

: Recording.paused? { r -- flg , is recording paused? }
    r s@ Recording.mode RECORDING-PAUSED =
;

: Recording.recording? { r -- flg , is recording started? }
    r s@ Recording.mode RECORDING-INPROGRESS = r Recording.paused? or 
;

: Recording.ready? { r -- flg , is recording ready to play? }
    r s@ Recording.mode RECORDING-READY = 
;

: Recording.playing? { r -- flg , is recording ready to play? }
    r s@ Recording.mode RECORDING-PLAYBACK <> if false exit then
    r s@ Recording.string CString.eol?
;

: Recording.ended? { r -- flg , is recording at end? }
    r s@ Recording.string CString.eol?
;

: Recording.clear { r -- , clear recording }
    RECORDING-EMPTY r s! Recording.mode
    r s@ Recording.string CString.truncate
;

: Recording.can-start? { r -- flg , can/should recording be started? }
    r s@ Recording.mode RECORDING-INPROGRESS = if true exit then
    r s@ Recording.mode RECORDING-PLAYBACK = if true exit then
    false
;

: Recording.start-playback { r -- , start playback }
\     r s@ Recording.mode RECORDING-EMPTY <> if
        RECORDING-PLAYBACK r s! Recording.mode 
        r s@ RECORDING.string CString.reset-index
\     then
;
: Recording.end-playback { r -- , }
    RECORDING-READY r s! Recording.mode
;

: Recording.start { r -- , }
    RECORDING-INPROGRESS r s! Recording.mode 
    r s@ RECORDING.string CString.reset-index
;
: Recording.end { r -- , }
    RECORDING-READY r s! Recording.mode 
;
: Recording.pause { r -- , }
    RECORDING-PAUSED r s! Recording.mode 
;
: Recording.resume { r -- , }
    RECORDING-INPROGRESS r s! Recording.mode ! 
;

: Recording.append-char { r ch -- , append char to recording }
    r s@ Recording.mode RECORDING-INPROGRESS = if
        r s@ Recording.string ch CString.append-char
    then
;

: Recording.append-string { r caddr u -- , append string to recording }
    r s@ Recording.string caddr u CString.append-string
;

: Recording.getch { r -- ch , get next char from recording }
\     r s@ Recording.string CString.eol? if 0 exit then
    r s@ Recording.string CString.getch
;

: Recording.ungetch { r -- , unget last getch }
    r s@ Recording.string CString.back-index
;

variable dot-recording Recording.new dot-recording !
