#ifdef PF_SUPPORT_SDL2
case ID_SDL_GetError:
  PUSH_TOS;
  TOS = (cell_t)SDL_GetError();
  endcase;

case ID_SDL_Init:
  PUSH_TOS;
  if (gVarInitializedSDL == 0) {
    printf("-- initializsing SDL\n");
    gVarInitializedSDL = 1;
    SDL_SetMainReady();
    TOS = (cell_t)SDL_Init(SDL_INIT_EVERYTHING);
  }
  else {
    printf("-- already initailized SDL\n");
    TOS = 0;
  }
  endcase;

case ID_SDL_Quit:
  SDL_Quit();
  printf("SDL_Quit()\n");
  endcase;

case ID_SDL_CreateWindow: { // ( title x y width height flags -- handle, create window )
  SDL_Window *window = NULL;
  int flags = TOS,
      height = M_POP,
      width = M_POP,
      y = M_POP,
      x = M_POP;

  ForthStringToC(gScratch, (char *)M_POP, sizeof(gScratch));

  window = SDL_CreateWindow(gScratch, x, y, width, height, flags);
  if (window == NULL) {
    TOS = 0;
    printf("*** Create window '%s' %dx%d -> %p ERROR: %s\n", gScratch, width, height, (void *)window, SDL_GetError());
  }
  else {
    printf("--- Create window '%s' %dx%d -> %p %s\n", gScratch, width, height, (void *)window, SDL_GetError());
    TOS = (cell_t)window;
  }
}
  endcase;

case ID_SDL_GetWindowID:
  TOS = (cell_t)SDL_GetWindowID((SDL_Window *)TOS);
  endcase;

case ID_SDL_GetWindowFromID:
  TOS = (cell_t)SDL_GetWindowFromID(TOS);
  endcase;

case ID_SDL_GetWindowFlags: // ( handle -- flags , get window flags: see video.fth )
  TOS = (cell_t)SDL_GetWindowFlags((SDL_Window *)TOS);
  endcase;

case ID_SDL_SetWindowTitle: // ( handle title -- )
{
  ForthStringToC(gScratch, (char *)TOS, sizeof(gScratch));
  TOS = M_POP;
  SDL_SetWindowTitle((SDL_Window *)TOS, gScratch);
  TOS = M_POP;
}
  endcase;

case ID_SDL_GetWindowTitle: // ( handle -- title )
{
  strcpy(gScratch, SDL_GetWindowTitle((SDL_Window *)TOS));
  PUSH_TOS;
  TOS = (cell_t)gScratch;
}
  endcase;

case ID_SDL_SetWindowIcon: // ( handle icon -- )
{
  SDL_Surface *icon = (SDL_Surface *)TOS;
  TOS = M_POP;
  SDL_SetWindowIcon((SDL_Window *)TOS, icon);
  TOS = M_POP;
}
  endcase;
case ID_SDL_SetWindowData: // ( data name window -- , set named window data )
{
  SDL_Window *win = (SDL_Window *)TOS;
  TOS = M_POP;
  char *name = (char *)TOS;
  TOS = M_POP;
  SDL_SetWindowData(win, name, (void *)TOS);
  TOS = M_POP;
}
  endcase;

case ID_SDL_GetWindowData: // ( data name window -- , set named window data )
{
  SDL_Window *win = (SDL_Window *)TOS;
  TOS = M_POP;
  char *name = (char *)TOS;
  TOS = (cell_t)SDL_GetWindowData(win, name);
}
  endcase;

case ID_SDL_SetWindowPosition: // ( handle x y -- )
{
  int y = (int)TOS;
  TOS = M_POP;
  int x = (int)TOS;
  TOS = M_POP;
  SDL_SetWindowPosition((SDL_Window *)TOS, x, y);
}
  endcase;

case ID_SDL_GetWindowPosition: // ( handle -- x y )
{
  int x, y;
  SDL_GetWindowPosition((SDL_Window *)TOS, &x, &y);
  TOS = x;
  PUSH_TOS;
  TOS = y;
}
  endcase;

case ID_SDL_SetWindowSize: // ( handle w h -- )
{
  int w = (int)TOS;
  TOS = M_POP;
  int h = (int)TOS;
  TOS = M_POP;
  SDL_SetWindowSize((SDL_Window *)TOS, w, h);
}
  endcase;

case ID_SDL_GetWindowSize: // ( handle -- w h )
{
  int w, h;
  SDL_GetWindowSize((SDL_Window *)TOS, &w, &h);
  TOS = w;
  PUSH_TOS;
  TOS = h;
}
  endcase;

case ID_SDL_GetWindowBorderSize: // ( handle -- top left bottom right )
{
  int top, left, bottom, right;
  SDL_GetWindowBordersSize((SDL_Window *)TOS, &top, &left, &bottom, &right);
  TOS = top;
  PUSH_TOS;
  TOS = left;
  PUSH_TOS;
  TOS = bottom;
  PUSH_TOS;
  TOS = right;
}
  endcase;

case ID_SDL_GetWindowSizeInPixels: // ( handle -- w h )
{
  int w, h;
  SDL_GetWindowSizeInPixels((SDL_Window *)TOS, &w, &h);
  TOS = w;
  PUSH_TOS;
  TOS = h;
}
  endcase;

case ID_SDL_SetWindowMinimumSize: // ( handle w h -- )
{
  int w = (int)TOS;
  TOS = M_POP;
  int h = (int)TOS;
  TOS = M_POP;
  SDL_SetWindowMinimumSize((SDL_Window *)TOS, w, h);
}
  endcase;

case ID_SDL_GetWindowMinimumSize: // ( handle -- w h )
{
  int w, h;
  SDL_GetWindowMinimumSize((SDL_Window *)TOS, &w, &h);
  TOS = w;
  PUSH_TOS;
  TOS = h;
}
  endcase;

case ID_SDL_SetWindowMaximumSize: // ( handle w h -- )
{
  int w = (int)TOS;
  TOS = M_POP;
  int h = (int)TOS;
  TOS = M_POP;
  SDL_SetWindowMaximumSize((SDL_Window *)TOS, w, h);
}
  endcase;

case ID_SDL_GetWindowMaximumSize: // ( handle -- w h )
{
  int w, h;
  SDL_GetWindowMaximumSize((SDL_Window *)TOS, &w, &h);
  TOS = w;
  PUSH_TOS;
  TOS = h;
}
  endcase;

case ID_SDL_SetWindowBordered: // ( handle flag -- , set window bordered to flag )
{
  SDL_bool flag = (SDL_bool)TOS;
  TOS = M_POP;
  SDL_SetWindowBordered((SDL_Window *)TOS, flag);
}
  endcase;

case ID_SDL_SetWindowResizable: // ( handle flag -- , set window resizable to flag )
{
  SDL_bool flag = (SDL_bool)TOS;
  TOS = M_POP;
  SDL_SetWindowResizable((SDL_Window *)TOS, flag);
}
  endcase;

case ID_SDL_SetWindowAlwaysOnTop: // ( handle flag -- , set window always on top to flag )
{
  SDL_bool flag = (SDL_bool)TOS;
  TOS = M_POP;
  SDL_SetWindowAlwaysOnTop((SDL_Window *)TOS, flag);
}
  endcase;

case ID_SDL_ShowWindow: // ( window -- , show window)
  SDL_ShowWindow((SDL_Window *)TOS);
  TOS = M_POP;
  endcase;

case ID_SDL_HideWindow: // ( window -- , hide window)
  SDL_HideWindow((SDL_Window *)TOS);
  TOS = M_POP;
  endcase;

case ID_SDL_RaiseWindow:
  SDL_RaiseWindow((SDL_Window *)TOS);
  TOS = M_POP;
  endcase;

case ID_SDL_MaximizeWindow: // ( window -- , maximize window)
  SDL_MaximizeWindow((SDL_Window *)TOS);
  TOS = M_POP;
  endcase;

case ID_SDL_MinimizeWindow: // ( window -- , minimize window)
  SDL_MinimizeWindow((SDL_Window *)TOS);
  TOS = M_POP;
  endcase;

case ID_SDL_RestoreWindow:
  SDL_RestoreWindow((SDL_Window *)TOS);
  TOS = M_POP;
  endcase;

case ID_SDL_SetWindowFullScreen: // ( handle flags -- , set window full screen to flag )
{
  uint32_t flags = (uint32_t)TOS;
  TOS = M_POP;
  SDL_SetWindowFullscreen((SDL_Window *)TOS, flags);
}
  endcase;

case ID_SDL_HasWindowSurface: // ( handle -- bool , check if window has surface)
  TOS = (cell_t)SDL_HasWindowSurface((SDL_Window *)TOS);
  endcase;

case ID_SDL_GetWindowSurface: // ( handle -- surface , return window surface)
  TOS = (cell_t)SDL_GetWindowSurface((SDL_Window *)TOS);
  printf("GetWindowSurface %lx\n", TOS);
  endcase;

case ID_SDL_DumpSurface: { // ( surface -- , dump surface struct )
  SDL_Surface *s = (SDL_Surface *)TOS;
  TOS = M_POP;
  printf("Surface %p\n", (void *)s);
  printf("   flags: 0x%x\n", s->flags);
  printf("  format: %p\n", (void *)s->format);
  printf("    w, h: %d, %dn", s->w, s->h);
  printf("  pixels: %p\n", (void *)s->pixels);
  printf("\n");
  SDL_PixelFormat *f = (SDL_PixelFormat *)s->format;
  printf("  PixelFormat $%p\n", (void *)f);
  printf("           format: 0x%x (%s)\n", f->format, SDL_GetPixelFormatName(f->format));
  printf("          palette: %p\n", (void *)f->palette);
  printf("     BitsPerPixel: %d\n", f->BitsPerPixel);
  printf("    BytesPerPixel: %d\n", f->BytesPerPixel);
}
  endcase;

case ID_SDL_UpdateWindowSurface: // ( handle -- )
  TOS = (cell_t)SDL_UpdateWindowSurface((SDL_Window *)TOS);
  endcase;

  // case ID_SDL_UpdateWindowSurfaceRects: // (handle rects num-rects -- )
  //  TOS = (cell_t)SDL_UpdateWindowSurfaceRects((SDL_Window *)TOS);
  //  endcase;

case ID_SDL_DestroyWindowSurface: // (handle -- )
  TOS = (cell_t)SDL_DestroyWindowSurface((SDL_Window *)TOS);
  endcase;

case ID_SDL_SetWindowGrab: // ( handle flag -- , set window grab to grab or release depending on flag )
{
  SDL_bool flag = (SDL_bool)TOS;
  TOS = M_POP;
  SDL_SetWindowGrab((SDL_Window *)TOS, flag);
}
  endcase;

case ID_SDL_SetWindowKeyboardGrab: // ( handle flag -- , set window grab to grab or release depending on flag )
{
  SDL_bool flag = (SDL_bool)TOS;
  TOS = M_POP;
  SDL_SetWindowKeyboardGrab((SDL_Window *)TOS, flag);
}
  endcase;

case ID_SDL_SetWindowMouseGrab: // ( handle flag -- , set window grab to grab or release depending on flag )
{
  SDL_bool flag = (SDL_bool)TOS;
  TOS = M_POP;
  SDL_SetWindowMouseGrab((SDL_Window *)TOS, flag);
}
  endcase;

case ID_SDL_GetWindowGrab: // ( handle -- bool , get window grab state )
  TOS = (cell_t)SDL_GetWindowGrab((SDL_Window *)TOS);
  endcase;

case ID_SDL_GetWindowKeyboardGrab: // ( handle -- bool , get window grab state )
  TOS = (cell_t)SDL_GetWindowKeyboardGrab((SDL_Window *)TOS);
  endcase;

case ID_SDL_GetWindowMouseGrab: // ( handle -- bool , get window grab state )
  TOS = (cell_t)SDL_GetWindowMouseGrab((SDL_Window *)TOS);
  endcase;

case ID_SDL_GetGrabbedWindow: // ( -- handle )
  TOS = (cell_t)SDL_GetGrabbedWindow();
  endcase;

case ID_SDL_SetWindowMouseRect: // ( handle rect -- , set rect of window to bind mouse cursor )
{
  SDL_Rect *rect = (SDL_Rect *)TOS;
  TOS = (cell_t)SDL_SetWindowMouseRect((SDL_Window *)TOS, rect);
}
  endcase;

case ID_SDL_GetWindowMouseRect: // ( handle -- rect )
  TOS = (cell_t)SDL_GetWindowMouseRect((SDL_Window *)TOS);
  endcase;

case ID_SDL_SetWindowBrightness: // ( handle  --  ) ( brightness , set brightness of window )
{
  PF_FLOAT brightness = (PF_FLOAT)FP_TOS;
  FP_TOS = M_FP_POP;
  TOS = (cell_t)SDL_SetWindowBrightness((SDL_Window *)TOS, brightness);
}
  endcase;
case ID_SDL_GetWindowBrightness: // ( handle  --  ) ( -- brightness , get brightness of window )
{
  PUSH_FP_TOS;
  FP_TOS = SDL_GetWindowBrightness((SDL_Window *)TOS);
}
  endcase;

case ID_SDL_SetWindowOpacity: // ( handle  --  ) ( opacity , set opacity of window )
{
  PF_FLOAT opacity = (PF_FLOAT)FP_TOS;
  FP_TOS = M_FP_POP;
  TOS = (cell_t)SDL_SetWindowOpacity((SDL_Window *)TOS, opacity);
}
  endcase;
case ID_SDL_GetWindowOpacity: // ( handle  --  ) ( -- opacity , get opacity of window )
{
  float opacity;
  PUSH_FP_TOS;
  SDL_GetWindowOpacity((SDL_Window *)TOS, &opacity);
  FP_TOS = (PF_FLOAT)opacity;
}
  endcase;
case ID_SDL_SetWindowModalFor: { // ( handle parent -- )
  SDL_Window *parent = (SDL_Window *)TOS;
  TOS = M_POP;
  TOS = (cell_t)SDL_SetWindowModalFor((SDL_Window *)TOS, parent);
}
  endcase;

case ID_SDL_SetWindowInputFocus: { // ( handle -- )
  TOS = SDL_SetWindowInputFocus((SDL_Window *)TOS);
}
  endcase;

case ID_SDL_FlashWindow: // ( window -- , flash window)
  SDL_FlashWindow((SDL_Window *)TOS, SDL_FLASH_UNTIL_FOCUSED);
  TOS = M_POP;
  endcase;

case ID_SDL_DestroyWindow: { // (handle -- )
  SDL_DestroyWindow((SDL_Window *)TOS);
  TOS = M_POP;
}
  endcase;

case ID_SDL_IsScreensaverEnabled: // ( -- bool , test if screensaver is enabled )
  PUSH_TOS;
  TOS = (cell_t)SDL_IsScreenSaverEnabled();
  endcase;
case ID_SDL_EnableScreensaver: // ( -- , screensaver is enabled )
  SDL_EnableScreenSaver();
  endcase;
case ID_SDL_DisableScreensaver: // ( -- , screensaver is disabled )
  SDL_EnableScreenSaver();
  endcase;

  /* // \\//\\//\\ \\//\\//\\ \\//\\//\\ \\//\\//\\ */

  //
case ID_SDL_MapRGB: // ( r g b surface -- color )
{
  int blue = M_POP;
  int green = M_POP;
  int red = M_POP;
  SDL_Surface *surface = (SDL_Surface *)TOS;
  TOS = (cell_t)SDL_MapRGB(surface->format, red, green, blue);
}
  endcase;

case ID_SDL_MapRGBA: // ( r g b a surface -- color )
{
  int alpha = M_POP;
  int blue = M_POP;
  int green = M_POP;
  int red = M_POP;
  SDL_Surface *surface = (SDL_Surface *)TOS;
  TOS = (cell_t)SDL_MapRGBA(surface->format, red, green, blue, alpha);
}
  endcase;

case ID_SDL_PollEvent: { // ( -- event | 0 , poll for an event, return event or 0 if none )
  SDL_Event e;
  e.type = 0;
  PUSH_TOS;
  TOS = SDL_PollEvent(&e) ? (cell_t)&e : 0;
}
  endcase;

case ID_SDL_CreateRGBSurface: {
}
  endcase;
case ID_SDL_CreateRGBSurfaceWithFormat: {
}
  endcase;
case ID_SDL_CreateRGBSurfaceFrom: {
}
  endcase;
case ID_SDL_CreateRGBSurfaceWithFormatFrom: {
}
  endcase;
case ID_SDL_FreeSurface: {
}
  endcase;
case ID_SDL_SetSurfacePalette: {
}
  endcase;
case ID_SDL_LockSurface: { // ( surface -- , lock surface)
  TOS = (cell_t)SDL_LockSurface((SDL_Surface *)TOS);
}
  endcase;
case ID_SDL_UnlockSurface: {
  SDL_UnlockSurface((SDL_Surface *)TOS);
  TOS = M_POP;
}
  endcase;
case ID_SDL_LoadBMP_RW: {
}
  endcase;
case ID_SDL_LoadBMP: {
}
  endcase;
case ID_SDL_SaveBMP_RW: {
}
  endcase;
case ID_SDL_SaveBMP: {
}
  endcase;
case ID_SDL_SetSurfaceRLE: {
}
  endcase;
case ID_SDL_HasSurfaceRLE: {
}
  endcase;
case ID_SDL_SetColorKey: {
}
  endcase;
case ID_SDL_HasColorKey: {
}
  endcase;
case ID_SDL_GetColorKey: {
}
  endcase;
case ID_SDL_SetSurfaceColorMod: {
}
  endcase;
case ID_SDL_GetSurfaceColorMod: {
}
  endcase;
case ID_SDL_SetSurfaceAlphaMod: {
}
  endcase;
case ID_SDL_GetSurfaceAlphaMod: {
}
  endcase;
case ID_SDL_SetSurfaceBlendMode: {
}
  endcase;
case ID_SDL_GetSurfaceBlendMode: {
}
  endcase;
case ID_SDL_SetClipRect: {
}
  endcase;
case ID_SDL_GetClipRect: {
}
  endcase;
case ID_SDL_DuplicateSurface: {
}
  endcase;
case ID_SDL_ConvertSurface: {
}
  endcase;
case ID_SDL_ConvertSurfaceFormat: {
}
  endcase;
case ID_SDL_ConvertPixels: {
}
  endcase;
case ID_SDL_PremultiplyAlpha: {
}
  endcase;
// case ID_SDL_FillRect: {
// }
//   endcase;
case ID_SDL_FillRects: {
}
  endcase;
case ID_SDL_UpperBlit: {
}
  endcase;
case ID_SDL_LowerBlit: {
}
  endcase;
case ID_SDL_SoftStretch: {
}
  endcase;
case ID_SDL_UpperBlitScaled: {
}
  endcase;
case ID_SDL_LowerBlitScaled: {
}
  endcase;
case ID_SDL_SetYUVConversionMode: {
}
  endcase;
case ID_SDL_GetYUVConversionMode: {
}
  endcase;

case ID_SDL_FillRect: // ( surface mapped-rgb -- , fill rect in surface)
{
  SDL_Surface *surface = (SDL_Surface *)M_POP;
  SDL_FillRect(surface, NULL, (uint32_t)TOS);
  M_DROP;
}
  endcase;

  /* // \\//\\//\\ \\//\\//\\ \\//\\//\\ \\//\\//\\ */
  /* // \\//\\//\\ \\//\\//\\ \\//\\//\\ \\//\\//\\ */
  /* // \\//\\//\\ \\//\\//\\ \\//\\//\\ \\//\\//\\ */

  // RENDERER

case ID_SDL_CreateRenderer: { // ( window index flags -- renderer , index of renderer or -1 to use best fit for flags )
  uint32_t flags = (uint32_t)TOS;
  TOS = M_POP;
  int index = (int)TOS;
  TOS = M_POP;
  TOS = (cell_t)SDL_CreateRenderer((SDL_Window *)TOS, index, flags);
}
  endcase;

case ID_SDL_CreateSoftwareRenderer: { // ( surface -- renderer , create 2D software renderer for surface )
  TOS = (cell_t)SDL_CreateSoftwareRenderer((SDL_Surface *)TOS);
}
  endcase;

case ID_SDL_CreateTexture: { // ( renderer format access width height -- texture )
  int h = (int)TOS;
  TOS = M_POP;
  int w = (int)TOS;
  TOS = M_POP;
  int access = (int)TOS;
  TOS = M_POP;
  uint32_t format = (uint32_t)TOS;
  TOS = M_POP;
  TOS = (cell_t)SDL_CreateTexture((SDL_Renderer *)TOS, format, access, w, h);
}
  endcase;
case ID_SDL_CreateTextureFromSurface: { // ( renderer surface -- texture )
  SDL_Surface *surface = (SDL_Surface *)TOS;
  TOS = M_POP;
  TOS = (cell_t)SDL_CreateTextureFromSurface((SDL_Renderer *)TOS, surface);
}
  endcase;

  // TODO: figure out what to do with the window and renderer structures that need to be filled in/returned.
  //       maybe have an AllocWindow() and AllocRenderer() method for Forth to use to allocate these structs
  // case ID_SDL_CreateWindowAndRenderer: { // ( width height flags window renderer --  )
  //    // not implemented for now
  // }
  //   endcase;

// TODO: figure out what to do with the blendmode structure that need to be filled in/returned.
//       maybe have an AllocBlendMode()  method for Forth to use to allocate these structs
// case ID_SDL_GetRenderDrawBlendMode: { // ( renderer blendmode -- )
// }
// endcase;
case ID_SDL_GetRenderDrawColor: { // ( renderer -- rgba ,  Get the color used for drawing operations [Rect, Line and Clear] )
  uint8_t a, b, g, r;
  SDL_GetRenderDrawColor((SDL_Renderer *)TOS, &r, &g, &b, &a);
  TOS = (cell_t)(r << 24 | g << 16 | b << 8 | a);
}
  endcase;

  // TODO: figure out what to do with the blendmode structure that need to be filled in/returned.
  //       maybe have an AllocBlendMode()  method for Forth to use to allocate these structs
  // case ID_SDL_GetRendererInfo: { // ( renderer info -- , Get information about a rendering context )
  // }
  // endcase;

case ID_SDL_DestroyRenderer: { // ( renderer -- )
  SDL_DestroyRenderer((SDL_Renderer *)TOS);
  TOS = M_POP;
}
  endcase;
case ID_SDL_DestroyTexture: { // ( texture -- )
  SDL_DestroyTexture((SDL_Texture *)TOS);
  TOS = M_POP;
}
  endcase;
case ID_SDL_GetRendererOutputSize: // ( renderer -- w h )
{
  int w, h;
  SDL_GetRendererOutputSize((SDL_Renderer *)TOS, &w, &h);
  TOS = w;
  PUSH_TOS;
  TOS = h;
}
  endcase;
case ID_SDL_GetRenderTarget: { // ( renderer -- texture )
  TOS = (cell_t)SDL_GetRenderTarget((SDL_Renderer *)TOS);
}
  endcase;
case ID_SDL_RenderClear: { // ( renderer -- , Clear the current rendering target with the drawing color )
  TOS = (cell_t)SDL_RenderClear((SDL_Renderer *)TOS);
}
  endcase;
case ID_SDL_RenderCopy: { // ( renderer texture srcrect dstrect -- , Copy/blend a portion of the texture to the current rendering target )
  SDL_Rect *srcRect, *dstRect;
  srcRect = (SDL_Rect *)TOS;
  TOS = M_POP;
  dstRect = (SDL_Rect *)TOS;
  TOS = M_POP;
  SDL_Texture *t = (SDL_Texture *)TOS;
  TOS = M_POP;
  TOS = (cell_t)SDL_RenderCopy((SDL_Renderer *)TOS, t, srcRect, dstRect);
}
  endcase;

case ID_SDL_RenderCopyEx: { // ( renderer texture srcrect dstrect center flip ) ( angle -- , Copy a portion of the texture to the current rendering target )
  SDL_Rect *srcRect, *dstRect;
  srcRect = (SDL_Rect *)TOS;
  TOS = M_POP;
  dstRect = (SDL_Rect *)TOS;
  TOS = M_POP;
  SDL_Texture *t = (SDL_Texture *)TOS;
  TOS = M_POP;
  SDL_Point *center = (SDL_Point *)TOS;
  TOS = M_POP;
  SDL_RendererFlip flip = (SDL_RendererFlip)TOS;
  TOS = M_POP;
  float angle = (float)FP_TOS;
  FP_TOS = M_FP_POP;
  TOS = (cell_t)SDL_RenderCopyEx((SDL_Renderer *)TOS, t, srcRect, dstRect, angle, center, flip);
}
  endcase;
  // TODO: convert doubles to float in rects and points and angles structs
  // case ID_SDL_RenderCopyExF: { // ( renderer texture srcrect dstrect center flip ) ( angle -- , Copy a portion of the texture to the current rendering target )
  //   SDL_Rect *srcRect, *dstRect;
  //   srcRect = (SDL_Rect *)TOS;
  //   TOS = M_POP;
  //   dstRect = (SDL_Rect *)TOS;
  //   TOS = M_POP;
  //   SDL_Texture *t = (SDL_Texture *)TOS;
  //   TOS = M_POP;
  //   SDL_Point *center = (SDL_Point *)TOS;
  //   TOS = M_POP;
  //   SDL_RendererFlip flip = (SDL_RendererFlip)TOS;
  //   TOS = M_POP;
  //   float angle = (float)FP_TOS;
  //   FP_TOS = M_FP_POP;
  //   TOS = (cell_t)SDL_RenderCopyEx((SDL_Renderer *)TOS, t, srcRect, dstRect, angle, center, flip);
  // }
  //   endcase;

  // TODO deal with floats in structs
  // case ID_SDL_RenderCopyF: { // ( renderer texture srcrect dstrect -- , Copy a portion of the texture to the current rendering target )
  //   SDL_Rect *srcRect, *dstRect;
  //   srcRect = (SDL_Rect *)TOS;
  //   TOS = M_POP;
  //   dstRect = (SDL_Rect *)TOS;
  //   TOS = M_POP;
  //   SDL_Texture *t = (SDL_Texture *)TOS;
  //   TOS = M_POP;
  //   TOS = (cell_t)SDL_RenderCopyF((SDL_Renderer *)TOS, t, srcRect, dstRect);
  // }
  //   endcase;

case ID_SDL_RenderDrawLine: { // ( renderer x1 y1 x2 y2 -- ; draw line from x1,y1 to x2,y2 )
  int y2 = (int)TOS;
  TOS = M_POP;
  int x2 = (int)TOS;
  TOS = M_POP;
  int y1 = (int)TOS;
  TOS = M_POP;
  int x1 = (int)TOS;
  TOS = M_POP;
  TOS = SDL_RenderDrawLine((SDL_Renderer *)TOS, x1, y1, x2, y2);
}
  endcase;

case ID_SDL_RenderDrawLineF: { // ( renderer -- ) ( f: x1 y1 x2 y2 -- ; draw line from x1,y1 to x2,y2 )
  float y2 = (int)FP_TOS;
  FP_TOS = M_FP_POP;
  float x2 = (int)TOS;
  FP_TOS = M_FP_POP;
  float y1 = (int)TOS;
  FP_TOS = M_FP_POP;
  float x1 = (int)TOS;
  FP_TOS = M_FP_POP;
  TOS = SDL_RenderDrawLineF((SDL_Renderer *)TOS, x1, y1, x2, y2);
}
  endcase;

case ID_SDL_RenderDrawLines: { // ( renderer points count -- ; draw count lines )
  int count = (int)TOS;
  TOS = M_POP;
  SDL_Point *points = (SDL_Point *)TOS;
  TOS = M_POP;
  TOS = SDL_RenderDrawLines((SDL_Renderer *)TOS, points, count);
}
  endcase;

case ID_SDL_RenderDrawLinesF: { // ( renderer points count -- ; draw count lines )
  int count = (int)TOS;
  TOS = M_POP;
  SDL_FPoint *points = (SDL_FPoint *)TOS;
  TOS = M_POP;
  TOS = SDL_RenderDrawLinesF((SDL_Renderer *)TOS, points, count);
}
  endcase;

case ID_SDL_RenderDrawPoint: { // ( renderer x y-- ; draw point at x,y )
  int y = (int)TOS;
  TOS = M_POP;
  int x = (int)TOS;
  TOS = M_POP;
  TOS = SDL_RenderDrawPoint((SDL_Renderer *)TOS, x, y);
}
  endcase;
case ID_SDL_RenderDrawPointF: { // ( renderer -- ) ( f: x y-- ; draw point at x,y )
  float y = (float)FP_TOS;
  FP_TOS = M_FP_POP;
  float x = (float)FP_TOS;
  FP_TOS = M_FP_POP;
  TOS = SDL_RenderDrawPointF((SDL_Renderer *)TOS, x, y);
}
  endcase;
case ID_SDL_RenderDrawPoints: { // ( renderer points count -- ; draw count points )
  int count = (int)TOS;
  TOS = M_POP;
  SDL_Point *points = (SDL_Point *)TOS;
  TOS = M_POP;
  TOS = SDL_RenderDrawPoints((SDL_Renderer *)TOS, points, count);
}
  endcase;
case ID_SDL_RenderDrawPointsF: { // ( renderer points count -- ; draw count points )
  int count = (int)TOS;
  TOS = M_POP;
  SDL_FPoint *points = (SDL_FPoint *)TOS;
  TOS = M_POP;
  TOS = SDL_RenderDrawPointsF((SDL_Renderer *)TOS, points, count);
}
  endcase;
case ID_SDL_RenderDrawRect: { // ( renderer rect -- ; draw rect )
  SDL_Rect *rect = (SDL_Rect *)TOS;
  TOS = M_POP;
  TOS = SDL_RenderDrawRect((SDL_Renderer *)TOS, rect);
}
  endcase;
case ID_SDL_RenderDrawRectF: { // ( renderer rect -- ; draw rect with float coords )
  SDL_FRect *rect = (SDL_FRect *)TOS;
  TOS = M_POP;
  TOS = SDL_RenderDrawRectF((SDL_Renderer *)TOS, rect);
}
  endcase;
case ID_SDL_RenderDrawRects: { // ( renderer rects count -- ; draw count rects )
  int count = (int)TOS;
  TOS = M_POP;
  SDL_Rect *rects = (SDL_Rect *)TOS;
  TOS = M_POP;
  TOS = SDL_RenderDrawRects((SDL_Renderer *)TOS, rects, count);
}
  endcase;
case ID_SDL_RenderDrawRectsF: { // ( renderer points count -- ; draw count points )
  int count = (int)TOS;
  TOS = M_POP;
  SDL_FRect *points = (SDL_FRect *)TOS;
  TOS = M_POP;
  TOS = SDL_RenderDrawRectsF((SDL_Renderer *)TOS, points, count);
}
  endcase;
case ID_SDL_RenderFillRect: { // ( renderer rect -- ; draw rect )
  SDL_Rect *rect = (SDL_Rect *)TOS;
  TOS = M_POP;
  TOS = SDL_RenderFillRect((SDL_Renderer *)TOS, rect);
}
  endcase;
case ID_SDL_RenderFillRectF: { // ( renderer rect -- ; draw rect with float coords )
  SDL_FRect *rect = (SDL_FRect *)TOS;
  TOS = M_POP;
  TOS = SDL_RenderFillRectF((SDL_Renderer *)TOS, rect);
}
  endcase;
case ID_SDL_RenderFillRects: { // ( renderer rects count -- ; draw count rects )
  int count = (int)TOS;
  TOS = M_POP;
  SDL_Rect *rects = (SDL_Rect *)TOS;
  TOS = M_POP;
  TOS = SDL_RenderFillRects((SDL_Renderer *)TOS, rects, count);
}
  endcase;
case ID_SDL_RenderFillRectsF: { // ( renderer points count -- ; draw count points )
  int count = (int)TOS;
  TOS = M_POP;
  SDL_FRect *points = (SDL_FRect *)TOS;
  TOS = M_POP;
  TOS = SDL_RenderFillRectsF((SDL_Renderer *)TOS, points, count);
}
  endcase;
case ID_SDL_RenderFlush: { // ( renderer -- , flush all rendering operations )
  TOS = (cell_t)SDL_RenderFlush((SDL_Renderer *)TOS);
}
  endcase;
case ID_SDL_RenderGetClipRect: { // ( renderer rect -- f , Get clipping rectangle.)
  SDL_Rect *rect = (SDL_Rect *)TOS;
  TOS = M_POP;
  SDL_RenderGetClipRect((SDL_Renderer *)TOS, rect);
}
  endcase;

case ID_SDL_RenderGetIntegerScale: { // ( renderer -- f , Get whether integer scales are forced for resolution-independent rendering.)
  TOS = (cell_t)SDL_RenderGetIntegerScale((SDL_Renderer *)TOS);
}
  endcase;

case ID_SDL_RenderGetLogicalSize: { // ( renderer -- w h , Get device independent resolution for rendering. )
  int w, h;
  SDL_RenderGetLogicalSize((SDL_Renderer *)TOS, &w, &h);
  TOS = w;
  PUSH_TOS;
  TOS = h;
}
  endcase;

case ID_SDL_RenderGetScale: { // ( renderer -- ) ( f: -- x y , Get the drawing scale for the current target )
  float scaleX, scaleY;
  SDL_RenderGetScale((SDL_Renderer *)TOS, &scaleX, &scaleY);
  PUSH_FP_TOS;
  FP_TOS = scaleX;
  PUSH_FP_TOS;
  FP_TOS = scaleY;
}
  endcase;

case ID_SDL_RenderGetViewport: { // ( renderer rect -- f , Get viewport )
  SDL_Rect *rect = (SDL_Rect *)TOS;
  TOS = M_POP;
  SDL_RenderGetViewport((SDL_Renderer *)TOS, rect);
}
  endcase;
case ID_SDL_RenderGetWindow: { // ( renderer -- handle, Get window )
  TOS = (cell_t)SDL_RenderGetWindow((SDL_Renderer *)TOS);
}
  endcase;
case ID_SDL_RenderIsClipEnabled: { // ( renderer -- f, is clipping enabled? )
  TOS = (cell_t)SDL_RenderIsClipEnabled((SDL_Renderer *)TOS);
}
  endcase;
case ID_SDL_RenderLogicalToWindow: { // ( renderer -- x y ) ( f: x y -- , Convert logical coordinates to window coordinates )
  float _x, _y;
  int x, y;
  _y = (int)FP_TOS;
  FP_TOS = M_FP_POP;
  _x = (int)FP_TOS;
  FP_TOS = M_FP_POP;
  SDL_RenderLogicalToWindow((SDL_Renderer *)TOS, _x, _y, &x, &y);
  PUSH_TOS;
  TOS = (cell_t)y;
  PUSH_TOS;
  TOS = (cell_t)x;
}
  endcase;
case ID_SDL_RenderPresent: { // ( renderer -- , Update the screen with any rendering performed since the previous call. )
  SDL_RenderPresent((SDL_Renderer *)TOS);
  TOS = M_POP;
}
  endcase;

case ID_SDL_GetPixelFormatName: { // ( format -- zname , Get human readable name of a pixel format )
  TOS = (cell_t)SDL_GetPixelFormatName((uint32_t)TOS);
}
  endcase;

case ID_SDL_Get_Surface_flags: { // ( surface -- flags )
  SDL_Surface *s = (SDL_Surface *)TOS;
  TOS = (cell_t)s->flags;
}
  endcase;
case ID_SDL_Get_Surface_format: { // ( surface -- format )
  SDL_Surface *s = (SDL_Surface *)TOS;
  TOS = (cell_t)s->format;
}
  endcase;
case ID_SDL_Get_Surface_w: { // ( surface -- w , width )
  SDL_Surface *s = (SDL_Surface *)TOS;
  TOS = (cell_t)s->w;
}
  endcase;
case ID_SDL_Get_Surface_h: { // ( surface -- h , height )
  SDL_Surface *s = (SDL_Surface *)TOS;
  TOS = (cell_t)s->h;
}
  endcase;
case ID_SDL_Get_Surface_pitch: { // ( surface -- pitch , pitch )
  SDL_Surface *s = (SDL_Surface *)TOS;
  TOS = (cell_t)s->pitch;
}
  endcase;
case ID_SDL_Get_Surface_pixels: { // ( surface -- pixels , raw bitmap )
  SDL_Surface *s = (SDL_Surface *)TOS;
  TOS = (cell_t)s->pixels;
}
  endcase;
case ID_SDL_Get_Surface_userdata: { // ( surface -- userdata , TANY * )
  SDL_Surface *s = (SDL_Surface *)TOS;
  TOS = (cell_t)s->userdata;
}
  endcase;
case ID_SDL_Get_Surface_locked: { // ( surface -- locked , is locked? )
  SDL_Surface *s = (SDL_Surface *)TOS;
  TOS = (cell_t)s->locked;
}
  endcase;
case ID_SDL_Get_Surface_cliprect: { // ( surface -- cliprect , SDL_Rect & )
  SDL_Surface *s = (SDL_Surface *)TOS;
  TOS = (cell_t)&s->clip_rect;
}
  endcase;
#endif
