#!/usr/local/bin/pforth_standalone -q

." hello from fork.fth" cr

2000 constant sleep-time
: demo
    sys::fork dup 0= if
        drop
        cr ." child: started"
        cr ." child: sleeping " sleep-time . ." milliseconds"
        sleep-time  msec
        cr ." child: exiting"
        0 sys::exit
    else
        cr ." parent: forked child pid " dup .
        cr ." parent: waiting..."
        sys::wait
        swap
        cr ." parent: process " . ." exited with code " .
    then
;

demo