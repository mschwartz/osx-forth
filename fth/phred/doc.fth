\
\ PHRed editor
\
\ programmed by Mike Schwartz
\
\ Docuementation processing functionality
\

:STRUCT DocEntry
    APTR DocEntry.filename
    APTR DocEntry.signature
    ULONG DocEntry.line#
;STRUCT

private{
    create dpad 4096 allot
    create dpad2 4096 allot
}private

Array DocumentationStack DocumentationStack 256 Array.construct

: Documentation.get { zname | caddr u buf lin -- line , get documentation for word zname }A
    zname zcount dpad2 place
    dpad2 defined-in-file? 
    dup if
        -> u -> caddr
        s" 'C' kernel" caddr u compare 0= if
            z" 'C' kernel" 1+ string::strdup 
            exit
        then
        caddr u BufferList.find-or-create -> buf
        buf not if
            messageline{ ."  *** Documentation.get: can't find or create " caddr u type }messageline
            nullptr exit
        then
        dpad2 z" ^: " 1+ string::strcpy drop
        dpad2 zname string::strcat drop
        dpad2 z"  " 1+ string::strcat drop
        regex.ignore-case
        dpad2 regex.escape
        buf dpad2 Buffer.search -> lin
        lin if 
            lin s@ Line.text string::strdup
        else
            z" NOT FOUND." 1+ string::strdup
        then
    else
        2drop 
        z" NOT FOUND." 1+ string::strdup
    then
;

: Documentation.word { caddr u | b lin -- caddr u , get documentation for word }
    caddr u dpad place-cstr
    dpad Documentation.get -> lin
    lin if
        lin 
    else
        z" " 1+ 
    then
;

privatize
