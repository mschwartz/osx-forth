anew task-net.fth

trace-include @ [if]
cr ." Compiling net.fth... "
[then]

require net/constants.fth
require sys/memory.fth
require sys/fcntl.fth


: sa_family_t UBYTE ;
: in_port_t UBYTE ;
: in_addr_t UINT32 ;

:STRUCT in_addr 
    in_addr_t saddr
;STRUCT

:STRUCT sockaddr_in 
    UBYTE sin_len
    sa_family_t  sin_family
    in_port_t sin_port
    struct in_addr sin_addr
    8 BYTES sin_zero
;STRUCT

\ declarations
: NET.socket ( protocol type domain -- fd )
    net::socket
;

: perror ( caddr -- , print io error )
    sys::perror
;

: NET.bind ( address_len address fd -- error )
    net::bind
;

: NET.listen ( backlog fd -- )
    net::listen
;

: NET.setsockopt ( option_value level optname fd -- error )
    net::setsockopt
;

: NET.accept ( fd -- sockaddr fd , accept a connection )
    net::accept 
;

: NET.recv { flags caddr u fd -- actual , recv: receive bytes from fd }
    flags caddr u fd net::recv 
;

: NET.read { caddr u fd -- actual , read bytes from fd }
    caddr u fd sys::read
;

: NET.send { caddr u fd -- , write buffer to fd }
    caddr u fd FILE.write
;

: NET.print { caddr u fd -- , print string to fd }
    caddr u fd net.send
;

create crlf 13 c, 10 c, 
: NET.newln { fd -- , send cr/lf to fd }
    crlf 2 fd NET.send
    dup 0 <= if
        c" net.newln " sys::perror
    then
    cr ." end newln" .s
;

: NET.writeln { caddr u fd -- , print string to fd }
    caddr u fd NET.send
    cr caddr .hex u . fd . ." NET.writeln  " .s
    dup 0 <= if
        c" net.writeln " sys::perror
        -1 exit
    then
    fd NET.newln
    0 <= if
        c" net.writeln " sys::perror
        drop
        -1 exit
    then
    2+
;

: NET.sendfile ( flags len offset s fd -- err , send file specified by fd to socket s )
;

trace-include @ [if]
cr ." END net.fth"
[then]
