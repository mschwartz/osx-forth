#ifndef MQTTUTILS_HH
#define MQTTUTILS_HH

#include <mosquitto.h>
#include <string.h>
#include <unistd.h>

const uint64_t MQTTMessageTypeUnknown = 0;
const uint64_t MQTTMessageTypeConnect = 1;
const uint64_t MQTTMessageTypeDisconnect = 2;
const uint64_t MQTTMessageTypeMessage = 3;
const uint64_t MQTTMessageTypePublish = 4;

struct MQTTMessage {
  MQTTMessage *next;
  uint64_t type;
  char *topic;
  char *payload;
  MQTTMessage(uint64_t type = MQTTMessageTypeUnknown);
  ~MQTTMessage();
};

void addMessage(MQTTMessage *m);
// MQTTMessage *removeMessage();

void onConnect(struct mosquitto *, void *opts, int rc, int flags);
void onDisconnect(struct mosquitto *mosq, void *opts, int rc);
void onMessage(struct mosquitto *mosq, void *opts, const struct mosquitto_message *msg);
void onPublish(struct mosquitto *mosq, void *opts, int mid);

// these are called from Forth
struct mosquitto *mqttInit();
void mqttCleanup();
MQTTMessage *mqttRemoveMessage();
bool mqttHasMessage();
void freeMessage(MQTTMessage *e);

#endif