: keymap.common { w ch n | l b -- handled? , run editor common keymap }
    \ returns true if key handled
    ch case 
        ascii : of
            w CommandLine.run
            true exit
        endof
        ncurses.key_delete of     \ delete key
            n 0 ?do w Window.cursor-left loop
            true exit
        endof
        ncurses.ctrl_n of
            Screen.toggle-nerdtree
            true exit
        endof
        3 of die{ cr ." control-c " }die endof
        \ windows
        ncurses.ctrl_w of
            w Keymap.window exit
        endof
        ncurses.key_backspace of \ ctrl-h
            Screen.left-window
            exit
        endof
        key.ctrl_l of
            Screen.right-window
            exit
        endof
        key.ctrl_j of
            Screen.next-window
            true exit
        endof
        key.ctrl_k of
            Screen.prev-window
            true exit
        endof
        \ motions
        ncurses.ctrl_d of
            n 0 do w Window.page-down loop
            true exit
        endof
        key.ctrl_u of
            n 0 ?do w Window.page-up loop
            true exit
        endof
        ncurses.key_left of
            n 0 ?do w Window.cursor-left loop
            true exit
        endof
        ncurses.key_down of
            n 0 ?do w Window.cursor-down loop
\             w Window.last-line? not if
\                 w Window.line-down
\             then
            true exit
        endof
        ascii j of
            n 0 ?do w Window.cursor-down loop
\             w Window.last-line? not if
\                 w Window.line-down
\             then
            true exit
        endof
        ncurses.key_up of
            n 0 do w Window.cursor-up loop
\             w s@ Window.cursor-row 2 > if
\                 w Window.line-up
\             then
            true exit
        endof
        ascii k of
            n 0 do w Window.cursor-up loop
\             w s@ Window.cursor-row 2 > if
\                 w Window.line-up
\             then
            true exit
        endof
        ncurses.key_right of
            n 0 do w Window.cursor-right loop
            true exit
        endof
        ascii l of
            n 0 do w Window.cursor-right loop
            true exit
        endof
        ascii 0 of
            w Window.beginning-of-line
            true exit
        endof
        ascii $ of
            w Window.end-of-line
            true exit
        endof
        ascii H of
            w Window.top-of-screen
            true exit
        endof
        ascii L of
            w Window.bottom-of-screen
            true exit
        endof
        ascii M of
            w Window.middle-of-screen
            true exit
        endof
        ascii b of
            n 0 do w true Window.word-backward loop
            true exit
        endof
        ascii B of
            n 0 do w false Window.word-backward loop
            true exit
        endof
        ascii w of
            n 0 do w false Window.word-forward loop
            true exit
        endof
        ascii W of
            n 0 do w true Window.word-forward loop
            true exit
        endof
        ascii e of
            n 0 do w false Window.endword-forward loop
            true exit
        endof
        ascii E of
            n 0 do w true Window.endword-forward loop
            true exit
        endof
        ascii / of
            \ search forward
            w ascii / Window.search
            true exit
        endof
    endcase
    false
;