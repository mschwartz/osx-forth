anew task-sdl-keyboard.fth

include sdl/keycodes.fth
include sdl/scancodes.fth

:STRUCT Keysym
    SHORT ks_scancode        \ SDL physical key code
    SHORT ks_sym             \ SDL virtual key code
    UINT16 ks_mod          \ current key modifiers
    UINT32 ks_unused
;STRUCT

: SDL_GetKetyboardFocus ( -- window, query the window which currently has keyboard focus )
    0
;

\ /**
\  * Get a snapshot of the current state of the keyboard.
\  *
\  * The pointer returned is a pointer to an internal SDL array. It will be
\  * valid for the whole lifetime of the application and should not be freed by
\  * the caller.
\  *
\  * A array element with a value of 1 means that the key is pressed and a value
\  * of 0 means that it is not. Indexes into this array are obtained by using
\  * SDL_Scancode values.
\  *
\  * Use SDL_PumpEvents() to update the state array.
\  *
\  * This function gives you the current state after all events have been
\  * processed, so if a key or button has been pressed and released before you
\  * process events, then the pressed state will never show up in the
\  * SDL_GetKeyboardState() calls.
\  *
\  * Note: This function doesn't take into account whether shift has been
\  * pressed or not.
\  *
\  * \param numkeys if non-NULL, receives the length of the returned array
\  * \returns a pointer to an array of key states.
\  *
\  * \since This function is available since SDL 2.0.0.
\  *
\  * \sa SDL_PumpEvents
\  * \sa SDL_ResetKeyboard
\  */
: SDL_GetKeyboardState ( -- state-array )
    0
;

: SDL_ResetKeyobard ( -- , clear stateu of keyboard, generating key up for all pressed keys)
;