anew task-sdl-render.fth

\ Renderer Flags
$ 00000001 constant SDL_RENDER_SOFTWARE         \ renderer is software fallback
$ 00000002 constant SDL_RENDER_ACCELERATED      \ renderer uses hardware acceleration
$ 00000004 constant SDL_RENDER_PRESENTVSYNC     \ resent is synchronized with refresh rate
$ 00000008 constant SDL_RATGETTEXTURE           \ renderer supports rendering to texture

:STRUCT SDL_RenderInfo 
    APTR ri_name           \ the name of the renderer
    UINT32 ri_flags        \ a mask of supported renderer flags
    UINT32 ri_num_textures
    \ this is really an array of 16 uint32s:
    UINT32 ri_format1
    UINT32 ri_format2
    UINT32 ri_format3
    UINT32 ri_format4
    UINT32 ri_format5
    UINT32 ri_format6
    UINT32 ri_format7
    UINT32 ri_format8
    UINT32 ri_format9
    UINT32 ri_format10
    UINT32 ri_format11
    UINT32 ri_format12
    UINT32 ri_format13
    UINT32 ri_format14
    UINT32 ri_format15
    UINT32 ri_format16
    UINT32 ri_max_width
    UINT32 ri_max_height
;STRUCT

$ 00000000 constant SDL_FLIP_NONE
$ 00000001 constant SDL_FLIP_HORIZONTAL
$ 00000002 constant SDL_FLIP_VERTICAL
