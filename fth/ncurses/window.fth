: ncurses-window.fth ;

: ncurses.newwin ( nlines ncols begin_y begin_x -- win , create new window  )
    ncurses::newwin
\     dup true ncurses.keypad
\     ncurses.endwin 
;

: ncurses.delwin ( win -- , delete window )
    ncurses::delwin
    drop
;

: ncurses.winch ( win -- ch , get character/attr at cursor )
    ncurses::winch
;

: ncurses.mvwinch { w y x -- ch , move and get character at cursor }
    ncurses::mvwinch
;

: ncurses.waddch ( w ch -- , add character/attributes )
    ncurses::waddch
    drop
;

: ncurses.wprintw { w caddr u -- , print string to window }
    caddr u pad  place-cstr
    w pad  ncurses::wprintw
    drop
;

: ncurses.mvwprintw { win row col caddr u -- , move to row,col of win and print string }
    caddr u pad place-cstr
    win row col pad ncurses::mvwprintw
;
: ncurses.wemit ( w ch -- , )
    pad c!
    0 pad 1+ c!
    pad 2 ncurses.wprintw
;

: ncurses.wattron ( w attr -- , enable attribute for window )
    ncurses::wattron
    drop
;


: ncurses.wattroff ( w attr -- , disable attribute for window )
    ncurses::wattroff
    drop
;

: ncurses.wcleartoeol ( w -- , clear to end of line )
    ncurses::wclrtoeol
    drop
;

: ncurses.werase ( w -- , erase screen )
    ncurses::erase
    drop
;

: ncurses.wmove ( w y x -- , move cursor to y,x )
    ncurses::wmove
    drop
;

: ncurses.wborder ( win ls rs ts bs tl tr bleft br -- , draw windwow borders )
    ncurses::wborder
    drop
;

: ncurses.wrefresh ( win -- , refresh window )
    ncurses::wrefresh
    drop
;

: ncurses.wspaces { w n -- print n spaces to window }
    n 0 do 
        w bl ncurses.wemit
    loop
;

: ncurses.wprint# { w n l | str ww -- , print n to window, min width }
    n (.) -> ww -> str
    w l ww - ncurses.wspaces
    w str ww ncurses.wprintw
;
