\
\ phred editor
\
\ programmed by Mike Schwartz
\
\ Command Line handling
\
\ TODO: see https://www.baeldung.com/linux/vi-ex-mode
\

trace-include @ [if]
cr ." Compiling phred/commandline.fth" cr
[then]

variable CommandLine-start
variable CommandLine-end

: CommandLine.clear { w -- , clear command line for window }
    CommandLine.move-to ncurses.cleartoeol
;

: CommandLine.parse-one { str w | ch n m op -- n , parse one part of range }
    str CString.peek-ch  -> ch
    ch ctype::isdigit if
        str CString.parse-number drop exit
    then
    ch case
        ascii . of w s@ Window.line# -> n endof
        ascii $ of w Window.last-line# -> n endof
        0 of drop -1 exit endof
        drop
        -1 exit
    endcase
    str CString.advance-index
    str CString.peek-ch  -> op
    op ascii - <> op ascii + <> and if n exit then
    str CString.advance-index
    str CString.peek-ch  -> ch
    ch ctype::isdigit not if -2 exit then
    str CString.parse-number  drop -> m
    n
    op ascii - = if
        m -
    else
        m +
    then
;

: Commandline.parse-range { str w | ll strt end -- start-line end-line , parse range from start of CString }
    str CString.skip-blanks
    str w CommandLine.parse-one -> strt
    strt -1 = if \ no range present
        w s@ Window.line# dup exit
    then
    strt -2 = if \ syntax error
        -2 -2 exit
    then
    str CString.peek-ch ascii , <> if
        strt strt exit
    then
    str CString.advance-index
    str w CommandLine.parse-one -> end
    end -2 = if \ syntax error
        -2 -2 exit
    then
    w Window.last-line# -> ll
    strt ll > if ll -> strt then
    end ll > if ll -> end then
    strt end
    end strt < if swap then \ make sure end > start
;


include phred/commands/searchreplace.fth 
include phred/commands/quit.fth
include phred/commands/todo.fth
include phred/commands/grep.fth
include phred/commands/git.fth
include phred/commands/bufed.fth
include phred/commands/join.fth
include phred/commands/edit.fth
include phred/commands/write.fth
include phred/commands/help.fth
include phred/commands/words.fth
include phred/commands/dired.fth
include phred/commands/delete.fth
include phred/commands/man.fth
include phred/commands/project.fth

private{
variable input
c" none" Readline.new input !
}private

: (CommandLine.erase)
    CommandLine.move-to ncurses.cleartoeol
;
' (CommandLine.erase) is CommandLine.erase


: (CommandLine.run) { w | cmd len str ch strt end tok toku -- , run command line for window }
    CommandLine.move-to ncurses.cleartoeol ." : "

    input @ Readline.edit 
    input @ Readline.text  -> len -> cmd
    cmd CString.new -> str
    CommandLine.move-to ncurses.cleartoeol ." : "  
    cmd len type \ erase our faux cursor
    len 0= if 
        \ blank line
		exit 
	then

    str w CommandLine.parse-range -> end -> strt
    strt -2 = if
        MessageLine.start
        ." *** Invalid range '" CommandLine.text cmd len type ." '."
        MessageLine.end
        exit
    then

    strt commandline-start !
    end commandline-end !

    str CString.skip-blanks
    str CString.eol? if 
        w end Window.goto-line exit
    then

    str CString.peek-ch ascii ! = if
        str CString.advance-index
        str CString.skip-blanks
        w str CString.rest Cmded.begin
        w Window.cmded-mode
        exit
    then

    str CString.peek-ch ascii # = if
        str CString.advance-index
        str CString.skip-blanks
        w str CString.rest true Screen.execute
        exit
    then

    str CString.peek-ch ascii s = if
        str CString.advance-index
        w str strt end CommandLine.search&replace
        exit
    then
    str CString.skip-blanks
    str CString.parse-token -> toku -> tok

    tok toku s" pwd" compare 0= if
        current-directory MAX-PATH-LENGTH sys::getcwd drop
        current-directory string::strlen current-directory-length !
        messageline{ current-directory zcount type }messageline
        exit
    then

    tok toku s" cd" compare 0= if
        str CString.skip-blanks
        str CString.rest Path.realpathz Path.chdir
        exit
    then

    tok toku s" chdir" compare 0= if
        str CString.skip-blanks
        str CString.rest Path.realpath Path.chdir
        exit
    then

    tok toku s" noh" compare 0= if
        0 search-pad c!
        exit
    then

    tok toku s" map" compare 0= if
        ncurses::endwin
        ansi.cls 0 0 ansi.move-to
        map
        ansi.newline
        ansi.newline
        ansi.newline
        s" press any key " type-save @ execute
        sys::key

        ncurses::refresh
        exit
    then

    tok toku s" file?" compare 0= if
        str CString.skip-blanks
        str CString.rest pad place
        messageline{
            pad defined-in-file? ?dup if
                pad $type ." defined in " type ." ."
            else
                pad $type ." not known."
            then
        }messageline
        exit
    then

    tok toku s" debug" compare 0= if
        debug-on
        exit
    then

    tok toku s" info" compare 0= if
        die{
            ansi.clear-scrollback
            Screen.dump
        }die
    then

    tok toku s" ro"  compare 0= if
        w Window.toggle-read-only
        exit
    then

    w str tok toku CommandLine.quit if exit then
    w str tok toku CommandLine.todo if exit then
    w str tok toku CommandLine.grep if exit then
    w str tok toku CommandLine.man if exit then
    w str tok toku CommandLine.git if exit then
    w str tok toku CommandLine.bufed if exit then
    w str tok toku CommandLine.join if exit then
    w str tok toku CommandLine.edit if exit then
    w str tok toku CommandLine.help if exit then
    w str tok toku CommandLine.words if exit then
    w str tok toku CommandLine.write if exit then
    w str tok toku CommandLine.dired if exit then
    w str tok toku CommandLine.delete if exit then
    w str tok toku CommandLine.project if exit then

    messageline{ ." *** Invalid command '" cmd len type ." '" }messageline
    exit
;

' (CommandLine.run) is CommandLine.run

privatize

trace-include @ [if]
cr ." End phred/commandline.fth"
[then]
