\
\ ncurses library
\
\ programmed by Mike Schwartz
\
\ ReadLine handling
\

require ncurses.fth

private{
    64 constant DEFAULT_HISTORY_MAX
    variable editmode editmode.insert editmode !

    \ for using our readline for forth's query/interpret loop
    variable history.rlaccept 0 history.rlaccept !
    variable under-cursor

}private

:STRUCT ReadLine 
    APTR ReadLine.filename
    struct List ReadLine.history
    APTR ReadLine.currentHistory        \ current line in history
    APTR ReadLine.lastHistory           \ last history in list
    UINT64 ReadLine.historyLength       \ current number of entries in history
    UINT64 ReadLine.historyMax          \ MAX number of entries in history
    APTR ReadLine.suggestFn             \ autosuggest function
    BOOL ReadLine.insert-only           \ only allow insert mode
    APTR ReadLine.userData              \ per ReadLine instance application defined data
;STRUCT

: ReadLine.historyList { rl -- keystone , get keystone of readline struct's history }
    rl ReadLine.history +
;

: ReadLine.dump { rl -- , dump readline }
    " ReadLine at $" rl .hex
    cr ."   . currentHistory = " rl s@ ReadLine.currentHistory .hex
    cr ."       .lastHistory = " rl s@ ReadLine.lastHistory .hex
    cr ."     .historyLength = " rl s@ ReadLine.historyLength .
    cr ."        .historyMax = " rl s@ ReadLine.historyMax .
    cr ."         .suggestFn = " rl s@ ReadLine.suggestFn .hex
    cr ."       .insert-only = " rl s@ ReadLine.insert-only .hex
    cr ."          .userData = " rl s@ ReadLine.userData .hex
;

\ TODO read and write history to/from file
: ReadLine.new { $filename | rl -- , initialize readline }
    sizeof() ReadLine MEM.malloc -> rl
    $filename $duplicate rl s! ReadLine.filename
    rl ReadLine.historyList  List.init 
    nullptr rl s! ReadLine.currentHistory 
    nullptr rl s! ReadLine.suggestFn
    nullptr rl s! ReadLine.userData
    false rl s! ReadLine.insert-only
    0 rl s! ReadLine.historyLength
    DEFAULT_HISTORY_MAX rl s! ReadLine.historyMax
    rl
;

: ReadLine.destroy { rl -- , destroy readline instance }
    \ TODO: implement this
;

: ReadLine.last { rl -- lin , get last line in history }
    rl ReadLine.history List.last
;
: ReadLine.text { rl -- caddr u }
    rl s@ ReadLine.lastHistory
    s@ Line.text 
    dup string::strlen
;

: ReadLine.handle-common { k -- f , handle keystrokes common to both modes }
;

\ internal method called by ReadLine.edit, could be made private
: ReadLine.editLine { rl r c | k len r c start ecol lin txt handled -- , read characters from console into line }
    c -> ecol    \ editing column 

    s" " pad place-cstr             \ PLACE-CSTR  ( c-addr1 u1 c-addr2 -- )
    pad Line.new -> lin             \ new empty line
    lin rl s! ReadLine.lastHistory  \ is new last history
    lin rl ReadLine.historyList     \ add to end of history list
    List.addTail

    lin s@ Line.text -> txt
    0 -> len

    lin rl s! ReadLine.currentHistory

    r c ncurses.move ncurses.cleartoeol
\     true debug!
    begin
        WindowList.active-window Window.paint-statusline
        WindowList.active-window Window.refresh
        \ draw/redraw line
        lin s@ Line.text -> txt
        r c ncurses.move
        txt ncurses.addstr
        ncurses.cleartoeol
        r ecol ncurses::mvinch under-cursor !
        under-cursor @ ncurses::A_REVERSE OR ncurses::addch drop
        ncurses.refresh

        ncurses.getch -> k 

		false -> handled

        \ handle keys common to both edit modes
		k KEY.CTRL_D = if
			bye
		then

        k ncurses.KEY_MOUSE = if
            true -> handled
        then

		k ncurses.KEY_LEFT = if
			true -> handled
			ecol c > if
				ecol 1- -> ecol
			else
                ncurses.bell
			then
		then

		k ncurses.KEY_RIGHT = if
			true -> handled
			ecol c - len < if
				ecol 1+ -> ecol
			else
                ncurses.bell
			then
		then

		k ncurses.KEY_UP = if
			true -> handled
			rl ReadLine.historyList List.first lin <> if
				lin s@ ListNode.prev rl s! ReadLine.currentHistory
				rl s@ ReadLine.currentHistory -> lin
				lin s@ Line.text string::strlen -> len
				len c + -> ecol
			then
		then

		k ncurses.KEY_DOWN = if
			true -> handled
			rl ReadLine.historyList List.last lin <> if
				lin s@ ListNode.next rl s! ReadLine.currentHistory
				rl s@ ReadLine.currentHistory -> lin
				lin s@ Line.text string::strlen -> len
				len c + -> ecol
			then
		then

		k KEY.NEWLINE  = if
			true -> handled
			0 lin s@ Line.text len + c!	\ null terminate it
			rl s@ ReadLine.lastHistory lin <> if
				\ assure selected line is lastHistory (new/added history)
				rl s@ ReadLine.lastHistory s@ Line.text MEM.free \ free old
				lin s@ Line.text 
				string::strdup 
				rl s@ ReadLine.lastHistory  s! Line.text \ copy new
			then
			exit
		then

        handled not if
			false -> handled
            editmode @ editmode.insert = if
				k KEY.ESCAPE = if
					rl s@ ReadLine.insert-only false = if
						editmode.visual editmode !
					then
                    ncurses.bell
					true -> handled
				then
				k KEY.DEL = if
					ecol c > if
						lin 
						ecol c - 1- 
						Line.delete-at-index drop
						ecol 1- -> ecol
					then
					true -> handled
				then
				k KEY.BACKSPACE = if
					ecol c > if
						lin ecol c - 1- Line.delete-at-index drop
						ecol 1- -> ecol
					then
					true -> handled
				then
				handled not if
					k bl >= if
						\ add character to input
						lin ecol c - k 
                        Line.insert-at-index  
						lin s@ Line.text string::strlen -> len
						ecol 1+ -> ecol
						rl s@ ReadLine.suggestFn ?dup if
							k swap execute 
						then
					then
				then
            else
                \ visual mode
                k case
                    KEY.DEL of
                        ecol c > if
                            ecol 1- -> ecol
                        then
                    endof
                    ascii h of
                        ecol c > if
                            ecol 1- -> ecol
                        else
                            ncurses.bell
                        then
                    endof
                    ascii l of
                        ecol c - len < if
                            ecol 1+ -> ecol
                        else
                            ncurses.bell
                        then
                    endof
                    ascii 0 of
                        c -> ecol
                    endof
                    ascii $ of
                        lin s@ Line.text string::strlen c + -> ecol
                    endof
                    ascii I of
                        c -> ecol
                        editmode.insert editmode !
                    endof
                    ascii A of
                        lin s@ Line.text string::strlen c + -> ecol
                        editmode.insert editmode !
                    endof
                    ascii i of
                        editmode.insert editmode !
                    endof
                    ascii a of
                        ecol c - len < if
                            ecol 1+ -> ecol
                        then
                        editmode.insert editmode !
                    endof
                    ascii x of
                        lin ecol c - Line.delete-at-index drop
                    endof
                    ascii D of
                        \ delete to end of line
                        begin
                            txt ecol c - + c@ 0 <> and
                            len 0> and
                        while
                            lin ecol c - Line.delete-at-index drop
                            len 1- -> len
                        repeat
                    endof
                    ascii d of
                        sys::key case
                            ascii d of
                                \ clear line
                                s" " txt place-cstr       \ PLACE-CSTR  ( c-addr1 u1 c-addr2 -- )
                                0 -> len 
                                c -> ecol
                            endof
                            ascii w of
                                \ delete word
                                begin
                                    txt ecol c - + c@ bl <> 
                                    txt ecol c - + c@ 0 <> and
                                    len 0> and
                                while
                                    lin ecol c - Line.delete-at-index drop
                                    len 1- -> len
                                repeat
                                begin
                                    txt ecol c - + c@ bl =
                                    txt ecol c - + c@ 0 <> and
                                    len 0> and
                                while
                                    lin ecol c - Line.delete-at-index drop
                                    len 1- -> len
                                repeat
                                len 0= if
                                    s" " txt place-cstr       \ PLACE-CSTR  ( c-addr1 u1 c-addr2 -- )
                                then
                            endof
                        endcase
                    endof
                    ascii b of    \ back word
                        ecol c <> if
                            ecol 1- -> ecol
                            begin
                                txt ecol c - + c@ bl = 
                            while
                                ecol 1- -> ecol
                                ecol c - < if
                                    c -> ecol
                                    leave
                                then
                            repeat

                            begin
                                txt ecol c - + c@ bl <> 
                                ecol c <> and
                            while
                                ecol 1- -> ecol
                            repeat
                            ecol c <> if
                                ecol 1+ -> ecol
                            then
                        then
                    endof
                    ascii w of    \ next word
                        begin
                            txt ecol c - + c@ bl <> 
                            ecol c - len < and
                        while
                            ecol 1+ -> ecol
                        repeat
                        begin
                            txt ecol c - + c@ bl =
                            ecol c - len < and
                        while
                            ecol 1+ -> ecol
                        repeat
                    endof
                endcase
            then
        then
        ncurses.refresh
    again
;

: ReadLine.edit { rl | r c -- , edit / read a line from terminal }
    ncurses::stdscr ncurses::getyx 
    -> c -> r
	rl r c ReadLine.editLine 
;

\ history! we can use our readline to replace accept so we get history and readline
\ . editing facilities in our forth query/interpret loop

: ReadLine.history-init ( -- , initialize readline for history )
    cr ." initializing readline history"
    history.rlaccept @ ?dup if
        ReadLine.destroy
    then
    cr ." initializing readline history"
    c" none" ReadLine.new history.rlaccept !
    cr ." initializing readline history"
;

: ReadLine.accept { addr amax -- numchars , accept replacement using readline for history }
    history.rlaccept @ ReadLine.edit
    history.rlaccept @ ReadLine.text 
    addr place-cstr
    addr string::strlen
;

: ReadLine.history-on ( -- , enable history )
    ReadLine.history-init
    ['] ReadLine.accept is accept
    cr ." Installed ReadLine history" cr
;

: ReadLine.history-off
    history.rlaccept @ ?dup if
        ReadLine.destroy
    then
    ['] (accept) is accept
;

if.forgotten ReadLine.history-off

\ : AUTO.INIT
\     AUTO.INIT
\     ReadLine.history-on
\ ;

\ ReadLine.history-on
privatize

trace-include @ [if]
cr ." End lib/readline.fth " cr
[then]
