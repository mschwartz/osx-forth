anew task-lib/vocabulary.fth

\ fn returns true to continue, false to stop the iteration
: dictionary.iterate { fn | nam -- , iterate dicionary and call fn with each name }
    \ see WORDS - the source is almost the same 
    latest
    BEGIN  
        dup 0<>
    WHILE 
        dup c@ flag_smudge and 0=
        IF
            dup fn execute not if drop exit then
\             swap 1+ swap
        THEN
        prevname
    REPEAT 
    drop 
;
