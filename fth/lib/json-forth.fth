anew task-lib/json-forth.fth

require lib/c-strings.fth
require lib/arrays.fth
require lib/hashmap.fth

0 constant JObjType.number
1 constant JObjType.string
2 constant JObjType.bool
3 constant JObjType.array
4 constant JObjType.object

:STRUCT JObj
    APTR JObj.struct_type
    ULONG JObj.type
    APTR JObj.value 
;STRUCT

variable json-indent 4 json-indent !
variable json-str

: JObj.dump { o -- , dump JObj }
    cr ." JObj $" o .hex 
    cr ."    .struct_type " o s@ JObj.struct_type count type
    cr ."    .type " o s@ JObj.type .
    cr ."    .value " o s@ JObj.value .hex
    o s@ JObj.type case
        JObjType.string of
            o s@ JObj.value ?dup if
                zcount type
            then
        endof
        JObjType.object of
            o s@ JObj.value s@ HashMap.struct_type $type
        endof
        cr ." type? " o s@ JObj.type .
    endcase
    cr
;

: JObj.new { typ val | o -- o , new JObj }
    sizeof() JObj mem::malloc -> o
    c" JObj" $duplicate o s! JObj.struct_type
    typ o s! JObj.type
    val o s! JObj.value
    o
;

defer JObj.destroy
: (JOBj.destroy) { o -- , release resources }
    o s@ JObj.type case
        JObjType.object of
            ['] JObj.destroy o s@ JObj.value HashMap.destroy
        endof
        JObjType.array of
            ['] JObj.destroy o s@ JObj.value Array.destroy
        endof
        JObjType.number of
            \ value is not allocated, do nothing!
        endof
        JObjType.bool of
            \ value is not allocated, do nothing!
        endof
        \ default
        o s@ JObj.value mem::free
    endcase
;
' (JObj.destroy) is JObj.destroy

\ json-stack is used to track the last kind of thing added ot the json object
variable json-stack 
50 Array.new json-stack !

: JSON.type ( -- typ , get current defining obj type )
    json-stack @ Array.peek s@ JObj.type 
;

: JSON.new { | o -- o , new JSON Object }
\     50 Array.new json-stack !
    sizeof() JObj mem::malloc -> o
    c" JSON" $duplicate o s! JObj.struct_type
    JObjType.object o s! JObj.type
    ['] JObj.destroy  HashMap.new o s! JObj.value
    json-stack @ o Array.push
    o
;

: JSON.iterate { 'callback jo -- , iterate JSON object }
    jo s@ JObj.value 'callback HashMap.iterate
;

: JSON.addKey { $key o -- , add o to current object }
    json-stack @ Array.peek -1 = if
        cr ." json-stack empty " bye
    then
    json-stack @ Array.peek s@ JObj.type
    JObjType.object <> if
        cr ." NOT OBJECT " bye
        throw ERR_RESULT_OUT_OF_RANGE
    then
    json-stack @ Array.peek s@ JObj.value $key o HashMap.set drop
;

: JSON{ {  $key | new -- o , start new JSON obj }
    JOBjType.object ['] JObj.destroy HashMap.new  JObj.new -> new
    $key new JSON.addKey
    json-stack @ new Array.push
; 

: }JSON { | o -- , end defining JSON object }
    json-stack @ Array.pop -> o
    o s@ JObj.type JObjType.object <> if
        throw ERR_RESULT_OUT_OF_RANGE
    then
; 

: JSON[ { | o -- o }
    JObjType.array  10 Array.new  JObj.new -> o
    json-stack @ o Array.push
    o
;

: ]JSON { | o -- , end defining JSON array }
    json-stack @ Array.pop -> o
    o s@ JObj.type JObjType.object <> if
        throw ERR_RESULT_OUT_OF_RANGE
    then
;

: JSON# { $key n | o -- o , return JSON number }
    JObjType.number n JObj.new -> o
    $key o JSON.addKey
;

: JSON<> { $key n | o -- o , return JSON bool }
    JOBjType.bool n JOBj.new -> o
    $key o JSON.addKey
;

: JSON" { $key caddr u | str o -- o , return JSON string }
    caddr u pad place-cstr pad string::strdup -> str
    JOBjType.string str JObj.new -> o
    $key o JSON.addKey
;

: JSON.push { o -- , add o to array }
    json-stack @ Array.peek s@ JObj.type
    JObjType.array <> if
        throw ERR_RESULT_OUT_OF_RANGE
    then
    json-stack @ Array.peek o Array.push
;

defer JSON.concat
: (JSON.concat) { ho | str $k v -- , }
    json-str @ -> str
    ho s@ HashMapNode.key -> $k
    ho s@ HashMapNode.value -> v
    json-indent @ spaces
    str json-indent @ CString.append-spaces
    str ascii " CString.append-char
    str $k CString.append-$string
    str ascii " CString.append-char
    str ascii : CString.append-char
    str bl CString.append-char
    v s@ JObj.type case
        JobjType.number of
            str v s@ JObj.value CString.append-number
        endof
        JObjType.string of
            str ascii " CString.append-char
            str v s@ JObj.value zcount CString.append-string
            str ascii " CString.append-char
        endof
        JObjType.object of
            str ascii { CString.append-char str CString.newline
            json-indent @ 4 + json-indent !
            ['] JSON.concat v JSON.iterate
            json-indent @ 4 - json-indent !
            str json-indent @ CString.append-spaces
            str ascii } CString.append-char
        endof
        cr ." unknown type "
        v s@ JObj.type .hex
    endcase
    str ascii , CString.append-char
    str CString.newline
;
' (JSON.concat) is JSON.concat

: JSON.stringify { str jo -- caddr u , return jo as JSON string }
    str json-str !
    str ascii { CString.append-char str CString.newline
        ['] JSON.concat jo JSON.iterate
    str ascii } CString.append-char str CString.newline
;

: dump-one { o -- , }
cr ." dump-one " o .hex
;
: JSON.dump { jo -- , dump JSON object }
    ['] dump-one jo JSON.iterate
;