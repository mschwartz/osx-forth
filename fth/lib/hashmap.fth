\
\ HashMap functions
\
\ Programmed by Mike Schwartz
\
\ key/value store
\

anew task-lib/hashmap.fth

0 [if]
Hash map is 1024 buckets long.  A bucket is a single linked list.

Key string is hashed to a number from 0-1023 and used as an index to
a linked list where the key/value should be found.

[then]

: lib/hashmap ;

require sys/memory.fth
require lib/lists.fth
require lib/strings.fth

:STRUCT HashmapNode
    APTR HashMapNode.struct_type
    APTR HashMapNode.next
    APTR HashMapNode.key      \ as counted string
    APTR HashMapNode.value    \ as counted string
;STRUCT

: HashMapNode.dump  { e -- , dump entry }
    cr ." HashMapNode " e .hex
    cr ."   next " e s@ HashMapNode.next .hex
    cr ."   key " e s@ HashMapNode.key count type
    cr ."   value " e s@ HashMapNode.value count type
;

: HashMapNode.new { $k v | entry -- , create and initialize a new HashMapNode } 
    sizeof() HashMapNode mem::malloc -> entry
    $k $duplicate entry s! HashMapNode.key
    c" HashMapNode" $duplicate entry s! HashMapNode.struct_type
    v entry s! HashMapNode.value
    entry
;

: HashMapNode.destroy { destructor entry -- , release memory used by entry }
    entry s@ HashMapNode.key MEM.free
    entry s@ HashMapNode.value destructor execute
    entry MEM.free
;

:STRUCT HashMap
    APTR HashMap.struct_type
    APTR HashMap.buckets
    APTR HashMap.destructor         \ method to call to destroy a HashMapNode
;STRUCT

: HashMap.hash { $caddr | ptr cnt sum -- hash , compute hash for string }
    0 -> sum
    $caddr count -> cnt -> ptr 
    cnt 0 do
        ptr c@ sum + -> sum
        ptr 1+ -> ptr
    loop
    sum 1023 MOD
;

: HashMap.init { m destructor -- , initialize hashmap }
    destructor m s! HashMap.destructor
    1024 cells mem::malloc  m s! HashMap.buckets
    m s@ HashMap.buckets 1024 cells erase
    c" HashMap" $duplicate m s! HashMap.struct_type
;
: HashMap.new { destructor | m -- map , create a new hashmap }
    sizeof() HashMap mem::malloc -> m
    m destructor HashMap.init
    m
;

: HashMap.destroy-bucket { destructor bucket | entry -- , destroy all HashMapNode in bucket }
    begin
        bucket 0= if
            exit \ at end of list
        then
        bucket @ -> entry \ first in list
        entry 0= if
            exit \ at end of list
        then
        entry s@ HashMapNode.next -> bucket \ unlink
        destructor entry HashMapNode.destroy
    again
;

: HashMap.destroy { m | destructor lst -- , }
    m s@ HashMap.buckets -> lst \ address of first bucket
    m s@ HashMap.destructor -> destructor
    1023 0 do
        lst @ 0<> if
            destructor lst HashMap.destroy-bucket
        then
        lst cell+ -> lst
    loop
    m s@ HashMap.buckets @ mem::free
    m mem::free
;

: HashMap.iterate-bucket { bucket 'callback | entry -- , iterate over bucket }
    begin
        bucket 0= if
            exit \ at end of list
        then
        bucket @ -> entry \ first in list
        entry 0= if
            exit \ at end of list
        then
        entry 'callback execute
        entry s@ HashMapNode.next -> bucket \ unlink
    again
;

: HashMap.iterate { m 'callback | lst -- , iterate over map }
    m s@ HashMap.buckets -> lst \ address of first bucket
    1023 0 do
        lst 0<> if
            lst 'callback HashMap.iterate-bucket
        then
        lst cell +  -> lst
    loop
;

: HashMap>bucket { m $k -- addr , return address of bucket }
    $k HashMap.hash 
    cells  \ compute offset into buckets
    m s@ HashMap.buckets + \ address of bucket
;

: HashMap.lookup { m $k | entry bucket -- entry , lookup key in hashmap and return the entry if found }
    m $k HashMap>bucket @ -> entry   
    begin
        entry 0= if 0 exit then
        $k count entry s@ HashMapNode.key count compare
        0= if \ found
            entry exit
        then
        entry s@ HashMapNode.next -> entry
    again
;

: HashMap.get { m $k | entry bucket -- caddr or null , lookup key in hashmap and return the value if found }
    m $k HashMap.lookup -> entry
    entry 0= if
        0
    else
        entry s@ HashMapNode.value
    then
;

: HashMap.set { m $k v | bucket entry -- n , set key in hashmap to value, return true if existing entry }
    m $k HashMap.lookup -> entry
    entry 0<> if
        entry s@ HashMapNode.value m s@ HashMap.destructor execute
        v entry s! HashMapNode.value
        true exit
    then
    \ not found
    $k v HashMapNode.new -> entry
    m $k HashMap>bucket -> bucket
    bucket @ entry s! HashMapNode.next
    entry bucket ! 
    false
;
