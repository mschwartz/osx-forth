\
\ Forth information function
\
\ Programmed by Mike Schwartz
\
\ core words, extensions, help text
\

anew task-lib/forth-keywords.fth

require lib/lists.fth
require lib/hashmap.fth
require lib/strings.fth


: ForthInfo.destructor { fi -- , destroy ForthInfo }
;

private{
HashMap core-words
core-words ' ForthInfo.destructor HashMap.init
create core-buf 512 allot

: add-core-word { $name $help  -- add word and help to ForthInfo }
    core-words $name $help HashMap.set drop
;

here s" ABORT"          ",  here s" help text"  ", add-core-word
here s\" ABORT\""       ",  here s" help text"  ", add-core-word
here s" ABS"            ",  here s" help text"  ", add-core-word
here s" ACCEPT"         ",  here s" help text"  ", add-core-word
here s" ACTION-OF"      ",  here s" help text"  ", add-core-word
here s" AGAIN"          ",  here s" help text"  ", add-core-word
here s" ALIGN"          ",  here s" help text"  ", add-core-word
here s" ALIGNED"        ",  here s" help text"  ", add-core-word
here s" ALLOT"          ",  here s" help text"  ", add-core-word
here s" AND"            ",  here s" help text"  ", add-core-word
here s" BASE"           ",  here s" help text"  ", add-core-word
here s" BEGIN"          ",  here s" help text"  ", add-core-word
here s" BL"             ",  here s" help text"  ", add-core-word
here s" BUFFER:"        ",  here s" help text"  ", add-core-word
here s" ["              ",  here s" help text"  ", add-core-word
here s" [CHAR]"         ",  here s" help text"  ", add-core-word
here s" [COMPILE]"      ",  here s" help text"  ", add-core-word
here s" [']"            ",  here s" help text"  ", add-core-word
here s" CASE"           ",  here s" help text"  ", add-core-word
here s" C,"             ",  here s" help text"  ", add-core-word
here s" CELL+"          ",  here s" help text"  ", add-core-word
here s" CELLS"          ",  here s" help text"  ", add-core-word
here s" C@"             ",  here s" help text"  ", add-core-word
here s" CHAR"           ",  here s" help text"  ", add-core-word
here s" CHAR+"          ",  here s" help text"  ", add-core-word
here s" CHARS"          ",  here s" help text"  ", add-core-word
here s" COMPILE,"       ",  here s" help text"  ", add-core-word
here s" CONSTANT"       ",  here s" help text"  ", add-core-word
here s" COUNT"          ",  here s" help text"  ", add-core-word
here s" CR"             ",  here s" help text"  ", add-core-word
here s" CREATE"         ",  here s" help text"  ", add-core-word
here s" C!"             ",  here s" help text"  ", add-core-word
here s" :"              ",  here s" help text"  ", add-core-word
here s" :NONAME"        ",  here s" help text"  ", add-core-word
here s" ,"              ",  here s" help text"  ", add-core-word
here s\" C\""           ",  here s" help text"  ", add-core-word
here s" DECIMAL"        ",  here s" help text"  ", add-core-word
here s" DEFER"          ",  here s" help text"  ", add-core-word
here s" DEFER@"         ",  here s" help text"  ", add-core-word
here s" DEPTH"          ",  here s" help text"  ", add-core-word
here s" DO"             ",  here s" help text"  ", add-core-word
here s" DOES>"          ",  here s" help text"  ", add-core-word
here s" DROP"           ",  here s" help text"  ", add-core-word
here s" DUP"            ",  here s" help text"  ", add-core-word
here s" /"              ",  here s" help text"  ", add-core-word
here s" /MOD"           ",  here s" help text"  ", add-core-word
here s" .R"             ",  here s" help text"  ", add-core-word
here s" .("             ",  here s" help text"  ", add-core-word
here s\" .\""           ",  here s" help text"  ", add-core-word
here s" ELSE"           ",  here s" help text"  ", add-core-word
here s" EMIT"           ",  here s" help text"  ", add-core-word
here s" ENDCASE"        ",  here s" help text"  ", add-core-word
here s" ENVIRONMENT?"   ",  here s" help text"  ", add-core-word
here s" ERASE"          ",  here s" help text"  ", add-core-word
here s" EVALUATE"       ",  here s" help text"  ", add-core-word
here s" EXECUTE"        ",  here s" help text"  ", add-core-word
here s" EXIT"           ",  here s" help text"  ", add-core-word
here s" ="              ",  here s" help text"  ", add-core-word
here s" FALSE"          ",  here s" help text"  ", add-core-word
here s" FILL"           ",  here s" help text"  ", add-core-word
here s" FIND"           ",  here s" help text"  ", add-core-word
here s" FM/MOD"         ",  here s" help text"  ", add-core-word
here s" @"              ",  here s" help text"  ", add-core-word
here s" here"           ",  here s" help text"  ", add-core-word
here s" HEX"            ",  here s" help text"  ", add-core-word
here s" HOLD"           ",  here s" help text"  ", add-core-word
here s" HOLDS"          ",  here s" help text"  ", add-core-word
here s" I"              ",  here s" help text"  ", add-core-word
here s" IF"             ",  here s" help text"  ", add-core-word
here s" IMMEDIATE"      ",  here s" help text"  ", add-core-word
here s" INVERT"         ",  here s" help text"  ", add-core-word
here s" IS"             ",  here s" help text"  ", add-core-word
here s" J"              ",  here s" help text"  ", add-core-word
here s" KEY"            ",  here s" help text"  ", add-core-word
here s" LEAVE"          ",  here s" help text"  ", add-core-word
here s" LITERAL"        ",  here s" help text"  ", add-core-word
here s" LOOP"           ",  here s" help text"  ", add-core-word
here s" LSHIFT"         ",  here s" help text"  ", add-core-word
here s" MARKER"         ",  here s" help text"  ", add-core-word
here s" MAX"            ",  here s" help text"  ", add-core-word
here s" MIN"            ",  here s" help text"  ", add-core-word
here s" MOD"            ",  here s" help text"  ", add-core-word
here s" MOVE"           ",  here s" help text"  ", add-core-word
here s" M*"             ",  here s" help text"  ", add-core-word
here s" -"              ",  here s" help text"  ", add-core-word
here s" NEGATE"         ",  here s" help text"  ", add-core-word
here s" NIP"            ",  here s" help text"  ", add-core-word
here s" OF"             ",  here s" help text"  ", add-core-word
here s" OR"             ",  here s" help text"  ", add-core-word
here s" OVER"           ",  here s" help text"  ", add-core-word
here s" 1-"             ",  here s" help text"  ", add-core-word
here s" 1+"             ",  here s" help text"  ", add-core-word
here s" PAD"            ",  here s" help text"  ", add-core-word
here s" PARSE-NAME"     ",  here s" help text"  ", add-core-word
here s" PARSE"          ",  here s" help text"  ", add-core-word
here s" PICK"           ",  here s" help text"  ", add-core-word
here s" POSTPONE"       ",  here s" help text"  ", add-core-word
here s" +"              ",  here s" help text"  ", add-core-word
here s" +LOOP"          ",  here s" help text"  ", add-core-word
here s" +!"             ",  here s" help text"  ", add-core-word
here s" QUIT"           ",  here s" help text"  ", add-core-word
here s" RECURSE"        ",  here s" help text"  ", add-core-word
here s" REFILL"         ",  here s" help text"  ", add-core-word
here s" REPEAT"         ",  here s" help text"  ", add-core-word
here s" RESTORE-INPUT"  ",  here s" help text"  ", add-core-word
here s" R@"             ",  here s" help text"  ", add-core-word
here s" ROLL"           ",  here s" help text"  ", add-core-word
here s" ROT"            ",  here s" help text"  ", add-core-word
here s" RSHIFT"         ",  here s" help text"  ", add-core-word
here s" R>"             ",  here s" help text"  ", add-core-word
here s" SAVE-INPUT"     ",  here s" help text"  ", add-core-word
here s" SIGN"           ",  here s" help text"  ", add-core-word
here s" SM/REM"         ",  here s" help text"  ", add-core-word
here s" SOURCE-ID"      ",  here s" help text"  ", add-core-word
here s" SOURCE"         ",  here s" help text"  ", add-core-word
here s" SPACE"          ",  here s" help text"  ", add-core-word
here s" SPACES"         ",  here s" help text"  ", add-core-word
here s" STATE"          ",  here s" help text"  ", add-core-word
here s" SWAP"           ",  here s" help text"  ", add-core-word
here s" ;"              ",  here s" help text"  ", add-core-word
here s\" S\""           ",  here s" help text"  ", add-core-word
here s\" S\""           ",  here s" help text"  ", add-core-word
here s" S>D"            ",  here s" help text"  ", add-core-word
here s" !"              ",  here s" help text"  ", add-core-word
here s" THEN"           ",  here s" help text"  ", add-core-word
here s" TO"             ",  here s" help text"  ", add-core-word
here s" TRUE"           ",  here s" help text"  ", add-core-word
here s" TUCK"           ",  here s" help text"  ", add-core-word
here s" TYPE"           ",  here s" help text"  ", add-core-word
here s" '"              ",  here s" help text"  ", add-core-word
here s" '"              ",  here s" help text"  ", add-core-word
here s" *"              ",  here s" help text"  ", add-core-word
here s" */"             ",  here s" help text"  ", add-core-word
here s" */MOD"          ",  here s" help text"  ", add-core-word
here s" 2DROP"          ",  here s" help text"  ", add-core-word
here s" 2DUP"           ",  here s" help text"  ", add-core-word
here s" 2/"             ",  here s" help text"  ", add-core-word
here s" 2@"             ",  here s" help text"  ", add-core-word
here s" 2OVER"          ",  here s" help text"  ", add-core-word
here s" 2R@"            ",  here s" help text"  ", add-core-word
here s" 2R>"            ",  here s" help text"  ", add-core-word
here s" 2SWAP"          ",  here s" help text"  ", add-core-word
here s" 2!"             ",  here s" help text"  ", add-core-word
here s" 2*"             ",  here s" help text"  ", add-core-word
here s" 2>R"            ",  here s" help text"  ", add-core-word
here s" U.R"            ",  here s" help text"  ", add-core-word
here s" UM/MOD"         ",  here s" help text"  ", add-core-word
here s" UM*"            ",  here s" help text"  ", add-core-word
here s" UNLOOP"         ",  here s" help text"  ", add-core-word
here s" UNTIL"          ",  here s" help text"  ", add-core-word
here s" UNUSED"         ",  here s" help text"  ", add-core-word
here s" U."             ",  here s" help text"  ", add-core-word
here s" U<"             ",  here s" help text"  ", add-core-word
here s" U>"             ",  here s" help text"  ", add-core-word
here s" VALUE"          ",  here s" help text"  ", add-core-word
here s" VARIABLE"       ",  here s" help text"  ", add-core-word
here s" WHILE"          ",  here s" help text"  ", add-core-word
here s" WITHIN"         ",  here s" help text"  ", add-core-word
here s" WORD"           ",  here s" help text"  ", add-core-word
here s" XOR"            ",  here s" help text"  ", add-core-word
here s" 0="             ",  here s" help text"  ", add-core-word
here s" 0<"             ",  here s" help text"  ", add-core-word
here s" 0>"             ",  here s" help text"  ", add-core-word
here s" 0<>"            ",  here s" help text"  ", add-core-word
here s" \"              ",  here s" help text"  ", add-core-word
here s" ."              ",  here s" help text"  ", add-core-word
here s" <"              ",  here s" help text"  ", add-core-word
here s" >"              ",  here s" help text"  ", add-core-word
here s" <>"             ",  here s" help text"  ", add-core-word
here s" #>"             ",  here s" help text"  ", add-core-word
here s" <#"             ",  here s" help text"  ", add-core-word
here s" #"              ",  here s" help text"  ", add-core-word
here s" #S"             ",  here s" help text"  ", add-core-word
here s" ("              ",  here s" help text"  ", add-core-word
here s" ?DO"            ",  here s" help text"  ", add-core-word
here s" ?DUP"           ",  here s" help text"  ", add-core-word
here s" >BODY"          ",  here s" help text"  ", add-core-word
here s" >IN"            ",  here s" help text"  ", add-core-word
here s" >R"             ",  here s" help text"  ", add-core-word

\ other (non-core)
here s" REQUIRE"        ",  here s" help text"  ", add-core-word
here s" [IF]"           ",  here s" help text"  ", add-core-word
here s" [ELSE]"         ",  here s" help text"  ", add-core-word
here s" [THEN]"         ",  here s" help text"  ", add-core-word
here s" CATCH"          ",  here s" help text"  ", add-core-word

: string>upper { caddr u -- , uppercase string at caddr }
    u 0 do
        caddr c@ toupper caddr c!
        caddr  1+ -> caddr
    loop
;
}private


: forth-core-word? { caddr u -- , is caddr u a core word? }
    caddr u core-buf place
    core-buf count string>upper
    core-words core-buf HashMap.get 0<>
\ ." word? " caddr u dump .s 
;

privatize
