\
\ phred editor
\
\ programmed by Mike Schwartz
\
\ Command Line handling
\   Man(page) Buffer
\
private{
    variable maned-buf
    create maned-pad 2048 allot
}private

: (Maned.begin) { w caddr u | buf -- , initialize and attach man output }
    w s@ Window.buffer -> buf
    s" man " maned-pad place-cstr
    caddr u pad place-cstr
    maned-pad pad string::strcat drop
    s"  | col -bx | tr -dc '\007-\011\012-\015\040-\376'" pad place-cstr
    maned-pad pad string::strcat drop

    w maned-pad zcount Cmded.begin
    Buffer.filetype.man buf s! Buffer.filetype
\     buf ['] filter Buffer.iterate
    true buf s! Buffer.read-only
;

' (Maned.begin) is Maned.begin

privatize