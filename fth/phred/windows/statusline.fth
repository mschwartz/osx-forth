\
\ PHRed editor
\
\ programmed by Mike Schwartz
\
\ Phred editor Window status line functions
\

private{
    variable current-branch 0 current-branch !
    variable branch-length 0 branch-length !
    variable poll-time 0 poll-time !
    create statusline-pad 512 allot

    : poll-branch 
        git.current-branch 
        branch-length ! 
        current-branch !
    ;
    : print-branch 
        poll-time @ 0= if
            git.current-branch branch-length ! current-branch !
            time::time poll-time !
        else
            time::time poll-time @ - 1 > if
                git.current-branch branch-length ! current-branch !
                time::time poll-time !
            then
        then

        current-branch @ if
            statusline-pad OPCODE.ICON String.append.char
            statusline-pad ICON.branch String.append.char
            statusline-pad bl String.append.char
            statusline-pad current-branch @ String.append 
        then
    ;

}private

: (Window.paint-statusline) { w -- , paint window status line }
    w WindowList.active-window = if
        ncurses::stdscr Theme.status-line
    else
        ncurses::stdscr Theme.current-line
    then
        w s@ Window.height w s@ Window.top + 1-  \ row
        w s@ Window.left                         \ col
    ncurses.move
    w s@ Window.type WindowType.nerdtree = if
        ."  [NERDTree] " 
        w Window.erase-eol
        exit
    then

    statusline-pad String.init
    statusline-pad c"  Phred | " String.$append
    WindowList.active-window w = if
        m-recording @ if
            statusline-pad ascii q String.append.char
            statusline-pad m-register c@ String.append.char
            statusline-pad c"  | " String.$append
        then
        m-playing @ if
            statusline-pad ascii @ String.append.char
            statusline-pad m-register c@ String.append.char
            statusline-pad c"  | " String.$append
        then
        print-branch statusline-pad c"  | " String.$append

    \     editor-mode @ .
        editor-mode @ case
            editmode.insert of
                statusline-pad c" [INSERT] " String.$append
            endof
            editmode.visual of
                statusline-pad c" [NORMAL] " String.$append
            endof
            editmode.bufed of
                statusline-pad c" [BUFED] " String.$append
            endof
            editmode.dired of
                statusline-pad c" [DIRED] " String.$append
            endof
            editmode.worded of
                statusline-pad c" [WORDS] " String.$append
            endof
            editmode.helped of
                statusline-pad c" [HELP] " String.$append
            endof
            editmode.cmded of
                statusline-pad c" [COMMAND] " String.$append
            endof
            editmode.greped of
                statusline-pad c" [GREP] " String.$append
            endof
            editmode.gited of
                statusline-pad c" [GIT] " String.$append
            endof
            editmode.selection of
                statusline-pad c" [SELECT] "  String.$append
            endof
        endcase

        depth 0> if 
            statusline-pad c" | dpth=" String.$append
            statusline-pad depth (u.) String.append-string
            statusline-pad bl String.append.char
        then
        depth 0< if
            statusline-pad c" | UNDERFLOW! " String.$append
        then
    then

    statusline-pad w s@ Window.cursor-row 1+ (u.) String.append-string
    statusline-pad c" :" String.$append
    statusline-pad w s@ Window.cursor-col 1+ (u.) String.append-string
    statusline-pad bl String.append.char
    
\     statusline-pad terminal-rows @ (u.) String.append-string
\     statusline-pad ascii x String.append.char
\     statusline-pad terminal-cols @ (u.) String.append-string

    statusline-pad w s@ Window.height (u.) String.append-string
    statusline-pad ascii x String.append.char
    statusline-pad w s@ Window.width  (u.) String.append-string
    
    
    WindowList.active-window w = if
        statusline-pad bl String.append.char
        statusline-pad w Window.read-only? if c" RO" else c" RW" then String.$append
    then
    statusline-pad bl String.append.char

    statusline-pad w s@ Window.buffer Buffer.modified? if ascii + String.append.char else bl  String.append.char then 
    statusline-pad w s@ Window.name Path.relative zcount String.append-string

    statusline-pad w s@ Window.width 2- String.print
    w Window.erase-eol

\     ncurses.cleartoeol

    ncurses::stdscr Theme.default
    w Window.address-cursor
    w Window.refresh
;
' (Window.paint-statusline) is Window.paint-statusline

privatize
