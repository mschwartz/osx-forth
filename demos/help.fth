#!/usr/local/bin/pforth_standalone -q

\
\ Demonstrate the use of the nixforth help words
\
\ Programmed by Mike Schwartz
\

false trace-include !
require help.fth

: demo { | caddr u -- , help demo }
    next-arg dup 0= if
        cr ." useage: help <word> "
        0 sys::exit
    then
    -> u -> caddr
    caddr u (help) false = if
        ." The word '" caddr u type ." ' is not defined in the dictionary."
    then
;

demo