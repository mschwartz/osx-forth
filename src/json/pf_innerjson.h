#ifdef PF_SUPPORT_JSON

case ID_JSON_free: { // ( json -- free JSON instance )
  json_object *json = (json_object *)TOS;
  TOS = M_POP;
  json_object_put(json);
}
  endcase;

case ID_JSON_stringify: { // ( json -- string )
  TOS = (cell_t)json_object_to_json_string((json_object *)TOS);
}
  endcase;

case ID_JSON_stringify_pretty: { // ( json -- string )
  TOS = (cell_t)json_object_to_json_string_ext((json_object *)TOS, JSON_C_TO_STRING_PRETTY);
}
  endcase;

case ID_JSON_parse: { // ( string -- json )
  TOS = (cell_t)json_tokener_parse((char *)TOS);
}
  endcase;

case ID_JSON_parse_file: { // ( filename -- json )
  TOS = (cell_t)json_object_from_file((char *)TOS);
}
  endcase;

case ID_JSON_get_type: { // ( json -- type )
  TOS = (cell_t)json_object_get_type((json_object *)TOS);
}
  endcase;

case ID_JSON_iterate: { // { obj 'callback -- , iterate JSON object }
                        //  struct json_object *array, *object, *tmp, *secu_code;

  ThrowCode exception = 0;
  ExecToken xt = (ExecToken)TOS;
  TOS = M_POP;
  json_object *object = (json_object *)TOS;
  TOS = M_POP;
  json_object_object_foreach(object, key, val) {
    TOS = (cell_t)object;
    PUSH_TOS;
    TOS = (cell_t)key;
    PUSH_TOS;
    TOS = (cell_t)val;
    exception = pfCatch(xt);
    if (exception != 0) {
      return exception;
    }
  }
}
  endcase;

case ID_JSON_get_object: { // ( json -- object )
  TOS = (cell_t)json_object_get_object((json_object *)TOS);
}
  endcase;

  // JSON_EXPORT json_bool json_object_object_get_ex(const struct json_object *obj, const char *key,
  //                                              struct json_object **value);
case ID_JSON_get_object_ex: { // ( json key -- object )
  const char *key = (const char *)TOS;
  TOS = M_POP;
  json_object *ret;
  json_object_object_get_ex((json_object *)TOS, key, &ret);
  TOS = (cell_t)ret;
}
  endcase;

case ID_JSON_get_array: { // ( json -- array )
  TOS = (cell_t)json_object_get_array((json_object *)TOS);
}
  endcase;

case ID_JSON_array_length: { // ( array -- length )
  TOS = (cell_t)json_object_array_length((json_object *)TOS);
}
  endcase;

case ID_JSON_array_get_idx: { // { array index -- value }
  TOS = (cell_t)json_object_array_get_idx((json_object *)TOS, TOS);
}
  endcase;

case ID_JSON_get_string: { // ( json -- string )
  TOS = (cell_t)json_object_get_string((json_object *)TOS);
}
  endcase;

case ID_JSON_get_string_len: { // ( json -- length )
  TOS = (cell_t)json_object_get_string_len((json_object *)TOS);
}
  endcase;

case ID_JSON_get_int64: { // ( json -- int64 )
  TOS = (cell_t)json_object_get_int64((json_object *)TOS);
}
  endcase;

case ID_JSON_get_boolean: { // ( json -- boolean )
  TOS = (cell_t)json_object_get_boolean((json_object *)TOS);
}
  endcase;

case ID_JSON_get_double: { // ( json -- double )
  PUSH_FP_TOS;
  FP_TOS = (cell_t)json_object_get_double((json_object *)TOS);
  TOS = M_POP;
}
  endcase;

case ID_JSON_new_object: { // ( -- json )
  PUSH_TOS;
  TOS = (cell_t)json_object_new_object();
}
  endcase;

  // void json_object_object_add(struct json_object *obj, const char *key, struct json_object *val);
case ID_JSON_object_add: { // { object key value -- }
  json_object *value = (json_object *)TOS;
  TOS = M_POP;
  char *key = (char *)TOS;
  TOS = M_POP;
  json_object *object = (json_object *)TOS;
  TOS = M_POP;
  json_object_object_add(object, key, value);
}
  endcase;
case ID_JSON_new_array: { // ( -- json )
  PUSH_TOS;
  TOS = (cell_t)json_object_new_array();
}
  endcase;

case ID_JSON_array_put_idx: { // { array index value -- ret }
  json_object *value = (json_object *)TOS;
  TOS = M_POP;
  int index = (int)TOS;
  TOS = M_POP;
  TOS = (cell_t)json_object_array_put_idx((json_object *)TOS, index, value);
}

case ID_JSON_array_add: { // { array value -- ret }
  json_object *value = (json_object *)TOS;
  TOS = M_POP;
  TOS = (cell_t)json_object_array_add((json_object *)TOS, value);
}
  endcase;

case ID_JSON_new_string: { // ( string -- json )
  TOS = (cell_t)json_object_new_string((char *)TOS);
}
  endcase;

case ID_JSON_new_int64: { // ( int64 -- json )
  TOS = (cell_t)json_object_new_int64((int64_t)TOS);
}
  endcase;

case ID_JSON_new_boolean: { // ( boolean -- json )
  TOS = (cell_t)json_object_new_boolean((bool)TOS);
}
  endcase;

case ID_JSON_new_double: { // ( double -- json )
  double d = (double)FP_TOS;
  FP_TOS = M_FP_POP;
  PUSH_TOS;
  TOS = (cell_t)json_object_new_double((d));
}
  endcase;

case ID_JSON_TYPE_null: { // ( -- JSON_TYPE_null )
  PUSH_TOS;
  TOS = json_type_null;
}
  endcase;

case ID_JSON_TYPE_boolean: { // ( -- JSON_TYPE_boolean )
  PUSH_TOS;
  TOS = json_type_boolean;
}
  endcase;

case ID_JSON_TYPE_double: { // ( -- JSON_TYPE_double )
  PUSH_TOS;
  TOS = json_type_double;
}
  endcase;

case ID_JSON_TYPE_int: { // ( -- JSON_TYPE_int64 )
  PUSH_TOS;
  TOS = json_type_int;
}
  endcase;

case ID_JSON_TYPE_object: { // ( -- JSON_TYPE_object )
  PUSH_TOS;
  TOS = json_type_object;
}
  endcase;

case ID_JSON_TYPE_array: { // ( -- JSON_TYPE_array )
  PUSH_TOS;
  TOS = json_type_array;
}
  endcase;

case ID_JSON_TYPE_string: { // ( -- JSON_TYPE_string )
  PUSH_TOS;
  TOS = json_type_string;
}
  endcase;

#endif
