\
\ phred editor
\
\ programmed by Mike Schwartz
\
\ Command Line handling
\   :be command
\   :bufed command
\

anew task-phred/commands/bufed.fth

: CommandLine.bufed { w str tok toku -- flg , return true if executed }
    tok toku s" be" compare 0= if
        w Bufed.begin
        w Window.bufed-mode
        true exit
    then
    tok toku s" bufed" compare 0= if
        w Bufed.begin
        w Window.bufed-mode
        true exit
    then
    tok toku s" bd" compare 0= if
        w s@ Window.buffer API.delete-buffer drop
        true exit
    then
    tok toku s" bdelete" compare 0= if
        w s@ Window.buffer API.delete-buffer drop
        true exit
    then
    false
;

