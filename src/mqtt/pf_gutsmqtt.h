#ifdef PF_SUPPORT_MQTT

ID_MQTT_lib_version,
  ID_MQTT_lib_init,
  ID_MQTT_lib_cleanup,
  ID_MQTT_new,
  ID_MQTT_destroy,
  ID_MQTT_reinitialize,
  ID_MQTT_connect,
  ID_MQTT_reconnect,
  ID_MQTT_disconnect,
  ID_MQTT_publish,
  ID_MQTT_subscribe,
  ID_MQTT_unsubscribe,
  ID_MQTT_loop,
  ID_MQTT_strerror,
  ID_MQTT_init,
  ID_MQTT_cleanup,
  ID_MQTT_get_message,
  ID_MQTT_has_message,

#endif
