\ \ nixforth
\
\ ncursesw interface
\    ncursesw provides wide character support for ncurses
\
: ncurses.add_wch ( ch -- , put complex character on screen and advance cursor )
    ncurses::add_wch drop
;

: ncurses.wadd_wch ( win ch -- , put complex character on screen in window and advance cursor )
    ncurses::wadd_wch drop
;

: ncurses.mvadd_wch ( y x ch -- , put complex character on screen at position and advance cursor )
    ncurses::mvadd_wch drop
;


: ncurses.mvwadd_wch ( win y x ch -- , put complex character on screen in window at position and advance cursor )
    ncurses::mvwadd_wch drop
;


: ncurses.echo_wchar ( ch -- , put complex character on screen in window at position and advance cursor )
    ncurses::echo_wchar drop
;

: ncurses.wecho_wchar ( win y x ch -- , put complex character on screen in window at position and advance cursor )
    ncurses::wecho_wchar drop
;

: ncurses.add_unicode ( uch -- , output unicode character )
    ncurses::add_unicode drop
;

\ Line Graphics (Unicode) - single lines, same VT100 line-drawing set as addch
$ 25ae constant WACS_BLOCK      \ solid square block 
$ 2592 constant WACS_BOARD      \ board of squares
$ 2534 constant WACS_BTEE       \ bottom tee
$ 00b7 constant WACS_BULLET     \ bullet
$ 2592 constant WACS_CKBOARD    \ checker board (stipple)
$ 2193 constant WACS_DARROW     \ arrow pointing down
$ 00b0 constant WACS_DEGREE     \ degree symbol
$ 25c6 constant WACS_DIAMOND    \ diamond
$ 2265 constant WACS_GEQUAL     \ greater-than-or-equal-to
$ 2500 constant WACS_HLINE      \ horizontal line
$ 2603 constant WACS_LANTERN    \ lantern symbol
$ 2190 constant WACS_LARROW     \ arrow pointing left
$ 2264 constant WACS_LEQUAL     \ less-than-or-equal-to
$ 2514 constant WACS_LLCORNER   \ lower left-hand corner
$ 2518 constant WACS_LRCORNER   \ lower right-hand corner
$ 2524 constant WACS_LTEE       \ left tee
$ 2260 constant WACS_NEQUAL     \ not-equal
$ 03c0 constant WACS_PI         \ greek pi
$ 00b1 constant WACS_PLMINUS    \ plus/minus
$ 253c constant WACS_PLUS       \ plus
$ 2192 constant WACS_RARROW     \ arrow pointing right
$ 251c constant WACS_RTEE       \ right tee
$ 23ba constant WACS_S1         \ scan line 1
$ 23bb constant WACS_S3         \ scan line 3
$ 23bc constant WACS_S7         \ scan line 7
$ 23bd constant WACS_S9         \ scan line 9
$ 00a3 constant WACS_STERLING   \ pound-sterling symbol
$ 252c constant WACS_TTEE       \ top tee
$ 2191 constant WACS_UARROW     \ arrow pointing up
$ 250c constant WACS_ULCORNER   \ upper left-hand corner
$ 2510 constant WACS_URCORNER   \ upper right-hand corner
$ 2502 constant WACS_VLINE      \ vertical line

\ Thick and double lines
$ 250f constant WACS_T_ULCORNER \ thick upper left corner
$ 2517 constant WACS_T_LLCORNER \ thick lower left corner
$ 2513 constant WACS_T_URCORNER \ thick upper right corner
$ 251b constant WACS_T_LRCORNER \ thick lower right corner
$ 252b constant WACS_T_LTEE     \ thick tee pointing right
$ 2523 constant WACS_T_RTEE     \ thick tee pointing left
$ 253b constant WACS_T_BTEE     \ thick tee pointing up
$ 2533 constant WACS_T_TTEE     \ thick tee pointing down
$ 2501 constant WACS_T_HLINE    \ thick horizontal line
$ 2503 constant WACS_T_VLINE    \ thick vertical line
$ 254b constant WACS_T_PLUS     \ thick large plus or crossover
$ 2554 constant WACS_D_ULCORNER \ double upper left corner
$ 255a constant WACS_D_LLCORNER \ double lower left corner
$ 2557 constant WACS_D_URCORNER \ double upper right corner
$ 255d constant WACS_D_LRCORNER \ double lower right corner
$ 2563 constant WACS_D_RTEE     \ double tee pointing left
$ 2560 constant WACS_D_LTEE     \ double tee pointing right
$ 2569 constant WACS_D_BTEE     \ double tee pointing up
$ 2566 constant WACS_D_TTEE     \ double tee pointing down
$ 2550 constant WACS_D_HLINE    \ double horizontal line
$ 2551 constant WACS_D_VLINE    \ double vertical line
$ 256c constant WACS_D_PLUS     \ double large plus or crossover
