\
\ PHRed editor
\
\ programmed by Mike Schwartz
\
\ Markdown Syntax Highlighting
\

anew TASK-syntax/highlight-markdown.fth

: Highlight.markdown { txt | dst new-text -- highlighted }
    txt string::strlen 2* MEM.malloc -> new-text
    new-text -> dst
    0 dst c!

    txt c@ bl = if
        txt s"     " index-of 0= if
            OPCODE.code dst c! 
            dst 1+ -> dst
            0 dst c!
            dst txt string::strcat drop
            new-text exit
        then
    then

    txt c@ ascii # = if
        txt s" ##### " index-of -1 <> if
            OPCODE.H5 dst c! 
            dst 1+ -> dst
            0 dst c!
            dst txt string::strcat drop
            new-text exit
        then
        txt s" #### " index-of -1 <> if
            OPCODE.H4 dst c! 
            dst 1+ -> dst
            0 dst c!
            dst txt string::strcat drop
            new-text exit
        then
        txt s" ### " index-of -1 <> if
            OPCODE.H3 dst c! 
            dst 1+ -> dst
            0 dst c!
            dst txt string::strcat drop
            new-text exit
        then
        txt s" ## " index-of -1 <> if
            OPCODE.H2 dst c! 
            dst 1+ -> dst
            0 dst c!
            dst txt string::strcat drop
            new-text exit
        then
        txt s" # " index-of -1 <> if
            OPCODE.H1 dst c! 
            dst 1+ -> dst
            0 dst c!
            dst txt string::strcat drop
            new-text exit
        then
    then
    new-text ?dup if mem::free then
    txt string::strdup
;
