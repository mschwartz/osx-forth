#ifdef PF_SUPPORT_JSON

// see https://github.com/nlohmann/json?tab=readme-ov-file

ID_JSON_new,
  ID_JSON_free,
  ID_JSON_stringify,
  ID_JSON_stringify_pretty,

  // parsing json from string or file
  ID_JSON_parse,
  ID_JSON_parse_file,

  // accessing json object
  ID_JSON_get_type,
  ID_JSON_iterate,
  ID_JSON_get_object,
  ID_JSON_get_object_ex,
  ID_JSON_get_array,
  ID_JSON_array_length,
  ID_JSON_array_get_idx,
  ID_JSON_get_string,
  ID_JSON_get_string_len,
  ID_JSON_get_int64,
  ID_JSON_get_boolean,
  ID_JSON_get_double,

  // creating and setting the json object values
  ID_JSON_new_object,
  ID_JSON_object_add,
  ID_JSON_new_array,
  ID_JSON_array_put_idx,
  ID_JSON_array_add,
  ID_JSON_new_string,
  ID_JSON_new_int64,
  ID_JSON_new_boolean,
  ID_JSON_new_double,

  // JSON types
  ID_JSON_TYPE_null,
  ID_JSON_TYPE_boolean,
  ID_JSON_TYPE_double,
  ID_JSON_TYPE_int,
  ID_JSON_TYPE_object,
  ID_JSON_TYPE_array,
  ID_JSON_TYPE_string,

#endif