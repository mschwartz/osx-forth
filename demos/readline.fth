#!/usr/local/bin/pforth_standalone -q

\
\ Program to exercise and demonstrate lib/Readline.fth
\

anew task-demos/readline.fth

false trace-include !
true quiet !

require lib/readline.fth

create foo 512 allot

: demo2 ( -- )
    Readline.history-init
    cr ." prompt> "
    foo 256 Readline.accept
    cr hex .s bye
;

: demo { | rl --  }
cr ." This is a demo of lib/readline.fth "
cr 
cr ."   * Note that you can't backspace over the prompt> ."
cr ."   * You can use arrow keys to move around and edit in insert mode."
cr ."   * If you hit escape you enter visual mode where vi key bindings work."
cr ."   * in visual mode, you can use i, a, etc., vim keys to enter insert mode again."
cr ."   * Up/Down arrows recall and navigate through history."
cr
cr ." HIT ^D TO QUIT"
cr
    c" none" Readline.new -> rl
    begin
        cr
        cr ." prompt> " 
        rl Readline.edit 
        rl Readline.text
        cr ." got  " type
    again
;

: test
    begin 
        sys::key 
        dup 4 = if bye then
        dup 20 >= if
            cr dup .hex dup emit
        else
            cr .hex
        then
    again 
;

\ test
demo
