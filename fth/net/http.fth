anew task-net/http.fth 

\ TODO: use body instead of headers-line (reentrant)

trace-include @ [if]
cr ." Compiling net/http.fth... "
[then]

require lib/hashmap.fth
require lib/buffer.fth
require sys/fcntl.fth
require net/netdb.fth
require net/socket.fth

variable http-verbose true http-verbose !
: http-verbose@ http-verbose @ ;

\ ***************************
\ HTTPRequest
\ ***************************

1024 1024 * constant max-body-size
:STRUCT HTTPRequest 
    APTR HTTPRequest.sock
    APTR HTTPRequest.headers
    APTR HTTPRequest.body
    LONG HTTPRequest.body-size
;STRUCT

: HTTPRequest.header.destroy { n -- , }
    cr ." destroy  " n .hex
;
: HTTPRequest.new { sock | r -- req , create HTTPRequest }
    sizeof() HTTPRequest MEM.malloc -> r 
    sock r s! HTTPRequest.sock 
    ['] MEM.free HashMap.new r s! HTTPRequest.headers
    max-body-size MEM.malloc r s! HTTPRequest.body
    0 r s! HTTPRequest.body-size
    r
;

: HTTPRequest.destroy { req -- , free resources }
    req s@ HTTPRequest.body mem::free
    req s@ HTTPRequest.headers HashMap.destroy
    req mem::free
;

: HTTPRequest.dump { req -- , dump request }
    cr .pid ." DUMP of HTTPRequest at $" req .hex
    cr .pid ."   .sock = " req s@ HTTPRequest.sock .hex
    cr .pid ."   .headers = " req s@ HTTPRequest.headers .hex
    cr .pid ."   .body = " req s@ HTTPRequest.body .hex
    cr .pid ."   .body-size = " req s@ HTTPRequest.body-size .hex
;

: HttpRequest.set-uri { caddr u req  -- , set request method and URI }
    caddr u req TCPSocket.writeln drop
;

: HTTPRequest.send-header { caddr req | sock -- , send header to file/socket }
    req s@ HTTPRequest.sock -> sock 
    caddr count sock TCPSocket.writeln 
    drop
;

: HTTPRequest.end-headers { req -- , end headers }
    req s@ HTTPRequest.sock TCPSocket.newline
    req S@ HTTPRequest.sock TCPSocket.flush
    drop
;

private{

1024 1024 * constant max-size
create headers-line max-size allot
variable headers-line-length 

: find-char { c strz | ndx ptr  -- index ,  return index in string of char }
    strz -> ptr
    0 -> ndx
    begin
        ndx 1+ -> ndx
        ndx headers-line-length >= if
            -1 exit
        then
        ptr c@
        dup 0= if   \ end of string, not found
            drop    \ drop character
            -1 exit
        then
        c = if
            ptr strz -
            exit             
        then
        ptr 1+ -> ptr
    again
;

\ Take a pointer to string and skip past any blanks and return pointer
\ to the string past the blanks.
: skip-blanks ( ptr -- ptr2 , skip blanks  )
    begin
        dup c@
        bl <> if
            exit
        then
        1+
    again
;
: skip-!blanks ( ptr -- ptr2 , skip non blanks  )
    begin
        dup c@
        bl = if
            exit
        then
        1+
    again
;

: parse-header { m line | u1 caddr2 u2 -- , parse header from headers-line and add to headers }
    ascii : headers-line 1+ find-char -> u1
    u1 0< if 
        exit \ not a header!
    then

    u1 line c!
    line u1 + 2+ skip-blanks -> caddr2
    caddr2 0= if
        cr .pid ." not a header " caddr2 count type
        exit  \ not a header
    then
    caddr2 string::strlen caddr2 1- c! 
    m line caddr2 1- $duplicate HashMap.set drop
;
}private

: HTTPRequest.read-headers { req | sock headers -- , read headers into HTTPRequest struct }
    req s@ HTTPRequest.sock -> sock
    req s@ HTTPRequest.headers -> headers
    begin
        sock TCPSocket.eof? if
            cr .pid ." AT EOF" .s
            true exit
        then
        headers-line max-size 1- sock TCPSocket.read-line
        dup 0= if
            cr .pid c" read-line " sys::perror
            exit
        then
        headers-line-length !

        headers-line 1+ string::strlen headers-line c!
        headers-line c@ 0= if
            true 
            exit
        then
        http-verbose @ if
            cr .pid ."    " headers-line count type
        then
        headers headers-line parse-header
    again
;

\ ***************************
\ HTTPResponse
\ ***************************

:STRUCT HTTPResponse 
    UINT HTTPResponse.statusCode    \ http status code (e.g. 200)
    APTR HTTPResponse.protocol      \ http protocol (e.g. HTTP/1.0)
    APTR HTTPResponse.statusText    \ http status text (e.g. OK)
    APTR HTTPResponse.sock
    APTR HTTPResponse.headers
    APTR HTTPResponse.body
    LONG HTTPResponse.body-size
;STRUCT

: HTTPResponse.new { sock | r -- response , create HTTPResonse }
    sizeof() HTTPResponse MEM.malloc -> r
    sock r s! HTTPResponse.sock
    ['] MEM.free  HashMap.new r s! HTTPResponse.headers
    max-body-size MEM.malloc r s! HTTPResponse.body
    0 r s! HTTPResponse.body-size
    r
;

: HTTPResponse.destroy { res -- , free resources }
    res s@ HTTPResponse.body mem::free
    res s@ HTTPResponse.headers HashMap.destroy
    res mem::free
;

: HTTPResponse.dump { req -- , dump response }
    cr .pid ." DUMP of HTTPResponse at $" req .hex
    cr .pid ."   .sock = " req s@ HTTPResponse.sock .hex
    cr .pid ."   .headers = " req s@ HTTPResponse.headers .hex
    cr .pid ."   .body = " req s@ HTTPResponse.body .hex
    cr .pid ."   .body-size = " req s@ HTTPResponse.body-size .hex
    cr .pid ."   .statusCode = " req s@ HTTPResponse.statusCode .
    cr .pid ."   .statusText = " req s@ HTTPResponse.statusText $type
    cr .pid ."   .protocol = " req s@ HTTPResponse.protocol $type
;

: HTTPResponse.get-body { res -- caddr u , get body }
    res s@ HTTPResponse.body
    res s@ HTTPResponse.body-size
;
: HttpResponse.set-status { caddr u res | sock -- , set response status header }
    res s@ HTTPResponse.sock -> sock
    caddr u sock TCPSocket.writeln
;

: HTTPResponse.send-header { $caddr res | sock -- send header to socket }
    res s@ HTTPResponse.sock -> sock 
    $caddr count sock TCPSocket.writeln drop
;

: HTTPResponse.add-header { caddr2 u2 caddr1 u1 res | sock -- f , set caddr2 value for key caddr1 }
\     cr ." response add-header " caddr2 u2 type ."  <=  " caddr1 u1 type
    res s@ HTTPResponse.sock -> sock
    caddr1 u1 sock TCPSocket.write 
    0< if -1 cr .pid c" write " sys::perror -1 exit then
    ascii : sock TCPSocket.write-char  drop
    bl sock TCPSocket.write-char  drop
    caddr2 u2 sock TCPSocket.write
    0< if -1 cr .pid c" write " sys::perror -1 exit then
    sock TCPSocket.newline drop
    u1 u2 +
;

: HTTPResponse.end-headers { req | sock  -- , end headers }
    req s@ HTTPResponse.sock -> sock
    sock TCPSocket.newline drop
    sock s@ TCPSocket.fp sys::fflush
;

: HTTPResponse.read-body { res | sock actual -- , read body from server } 
    res s@ HTTPResponse.sock -> sock
    res s@ HTTPResponse.body max-body-size sock TCPSocket.read
    -> actual
    actual res s! HTTPResponse.body-size
    actual
;

( 
test of multi
line comment
)

: HTTPResponse.read-status { res | ptr ptr2 status sock -- , read status line }
    res s@ HTTPResponse.sock -> sock
    sock TCPSocket.eof? 0<> if
        false exit
    then
    headers-line max-size sock TCPSocket.read-line
    0<> if 
        headers-line 1+ string::strlen headers-line c!
        headers-line c@ 0= if
            false exit
        then
        http-verbose @ if
            cr .pid ." status line " headers-line count type
        then
        0 -> status
        headers-line count drop -> ptr
        \ parse protocol
        ptr skip-!blanks -> ptr2 
        ptr2 ptr - 
        ptr 1- c! 

        ptr 1- $duplicate 
        res s! HTTPResponse.protocol 
        ptr2 -> ptr

        \ parse status code
        ptr skip-blanks -> ptr
        10 0 do
            ptr c@ 
            dup '0' < if leave then
            dup '9' > if leave then
            '0' -
            status 10 * + -> status 
            ptr 1+ -> ptr
        loop

        status res s! HTTPResponse.statusCode

        ptr skip-blanks -> ptr
        ptr string::strlen ptr 1- c!
        ptr 1- $duplicate res s! HTTPResponse.statusText
    then
;

: HTTPResponse.read-headers { res | headers sock -- f , read headers into HTTPRequest struct }
    res s@ HTTPResponse.sock -> sock
    res s@ HTTPResponse.headers -> headers
    res HTTPResponse.read-status
    sock TCPSocket.eof? 0<> if
        false exit
    then
    begin
        sock TCPSocket.eof? 0<> if
            false exit
        then

        headers-line max-size sock TCPSocket.read-line
        0<> if 
            headers-line 1+ string::strlen headers-line c!
            headers-line c@ 0= if
                exit
            then
            http-verbose @ if
                cr .pid ." header line " headers-line count type
            then
            headers headers-line parse-header
        then
    again
;

: HTTPResponse.write { caddr u res | sock -- actual , write data to response  socket }
    res s@ HTTPResponse.sock -> sock
    caddr u sock TCPSocket.write
;

privatize

trace-include @ [if]
cr ." end net/http.fth "
[then]