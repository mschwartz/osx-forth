#!/usr/local/bin/pforth_standalone -q

\
\ getopt demo
\
\ see: https://www.gnu.org/software/libc/manual/html_node/Example-of-Getopt.html;
\

require sys/getopt.fth

: usage ( -- , print usage and exit )
    cr
    cr ." Usage:"
    cr ."   getopt.fth args"
    1 sys::exit
;

: demo { | done aflag bflag cvalue ch fd -- , }
cr ." getopt demo "
cr ." implements example at https://www.gnu.org/software/libc/manual/html_node/Example-of-Getopt.html "
cr
    0 -> bflag
    0 -> aflag
    nullptr -> cvalue

    false -> done
    begin
        done false =
    while
        argc @ argv z" abc:" 1+ getopt -> ch
        ch case
            -1 of 
                true -> done 
            endof
            ascii a of 
                1 -> aflag 
            endof
            ascii b of 
                1 -> bflag 
            endof
            ascii c of
                optarg @ -> cvalue
            endof
            ascii ? of
                true -> done
                optopt c@ ascii c = if
                    cr ." Option -c requires and argument."
                else
                    optopt c@ ctype::isprint if
                        cr ." Unknown option -" optopt c@ emit
                    else
                        cr ." Uknown option character " getopt c@ .hex
                    then
                then
            endof
            cr ." ch is " ch .hex
            cr ." aborting" 1 sys::exit
        endcase
    repeat
    cr ." aflag = " aflag .hex
    ." , bflag = " bflag .hex
    ." , cvalue = " cvalue nullptr <> if cvalue ztype else ." nullptr" then

    optind @ argc @ < if
        argc @ optind @ do
            cr ." Non-option argument " i . argv @ i cell * + @ ztype
        loop
    then
;

demo
cr
