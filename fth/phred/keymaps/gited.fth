
: keymap.leader.gited  { w -- f , handle leader commands }
    keymap.key case
        ascii f of
            \ format selection
        endof
        ascii s of
            \ save buffer
        endof
        ascii b of
            keymap.key case
                ascii e of
                    w Bufed.begin
                    w Window.bufed-mode
                    drop
                    true exit
                endof
                ncurses.bell
            endcase
        endof
        ascii g of
            w Gited.begin
            w Window.gited-mode
            exit
        endof
        ascii d of
            keymap.key case
                ascii e of
                    w Dired.begin
                    w Window.dired-mode
                    exit
                endof
                ascii a of              \ da introducer
                    keymap.key ascii w <> if exit then
                    \ delete (cut) word under the cursor and the space after or before it
                    begin
                        w Window.char-at-cursor whitespace? NOT
                    while
                        w false Window.delete-at-cursor
                    repeat
                    w true Window.delete-at-cursor
                endof
                ascii d of              
                    \ dd = delete line
                    w Window.delete-line
                endof
                ascii w of              
                    \ dw = delete word forward
                    w Window.del-word-forward
                    w Window.fix-cursor
                endof
                ascii i of              \ di introducer
                    keymap.key ascii w <> if true exit then
                    \ delete (cut) word under the cursor
                    w Window.delete-word-at-cursor
                    w Window.fix-cursor
                endof
                ncurses.bell
            endcase
        endof
        ncurses.bell
    endcase
;

private{
create filename 512 allot

: parse-filename { w | l txt -- , parse filename from current line }
    0 filename c!
    w s@ Window.current-line -> l
    l s@ Line.text -> txt
    begin txt c@ bl = while txt 1+ -> txt repeat
    begin txt c@ bl <> while txt 1+ -> txt repeat
    begin txt c@ bl = while txt 1+ -> txt repeat
    txt zcount filename place-cstr
;

}private

: Keymap.gited { w | n ch -- , run gited keymap for window }
    Keymap.input# -> n
    begin
        w Window.paint
        w Window.refresh

        w Window.address-cursor
        Keymap.key -> ch
        Window.end-search

        begin
            ch ncurses.key_mouse = 
        while
            Keymap.mouse if exit then \ because a different window may have been clicked on (activated)
            Keymap.key -> ch
        repeat

        MessageLine.clear
        w Window.address-cursor

        w ch n keymap.common if
            exit
        then

        ch case
            ansi_escape of \ escape key
                ncurses.beep
                drop exit
            endof
            ascii , of
                w Keymap.leader.gited
                exit
            endof
            \ GIT COMMANDS
            ascii a of 
                \ git add
                w parse-filename 
                filename zcount git.add
                w Gited.begin
            endof
            ascii c of 
                \ git add
                w parse-filename 
                filename zcount git.commit
                w Gited.begin
            endof
            ascii r of
                \ refresh
                w gited.begin
            endof
            ascii ? of
                gited-help @ not gited-help !
                w gited.begin
            endof
            \ default
            MessageLine.move-to w hex . decimal
        endcase
    again
;

privatize
