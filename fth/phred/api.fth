\
\ Phred editor
\
\ programmed by Mike Schwartz
\
\ Phred Editor API
\
\ General purpose routines for interacting with the editor, buffers and windows
\

private{

create api-pad 4096 allot

variable old-buffer
variable new-buffer
: fix-buffer { w -- , }
    w s@ Window.buffer old-buffer @ <> if exit then
    w new-buffer @ Window.set-buffer
;
: is-separator { ch -- flg }
    ch whitespace? if true exit then
    ch case
        ascii . of true exit endof
        ascii - of true exit endof
        ascii _ of true exit endof
        ascii : of true exit endof
        ascii / of true exit endof
    endcase
    false
;
}private

: Window.save-undo { w -- , save undo buffer for window }
    w s@ Window.buffer Buffer.save-undo
;

: Window.end-of-line? { w -- f , is cursor at end of line? }
    w Window.current-line-index w Window.current-line-length 1- =
;


: Window.beginning-of-file? { w -- f , is cursor at start of file? }
    w Window.first-line? not if false exit then
    w Window.current-line-index 0<> if false exit then
    true
;

: Window.end-of-file? { w -- f , is cursor at end of file? }
    w Window.last-line? not if false exit then
    w Window.end-of-line? not if false exit then
    true
;

: Window.first-char { p c | cx -- p , return pointer to first character 
                            starting at column  to render of line }
    0 -> cx
    begin
        cx c >= if
            c exit
        then
        p c@ ASCII_TAB = if
            cx Window.tab-to-col -> cx
        else
            cx 1+ -> cx
        then
    again
;
: Window.print-tab { col | added -- col , expand tab using spaces }
	col GUTTER-WIDTH -  1+ -> col
    begin
		col TAB-WIDTH @ mod 0= if col exit then
        space
        col 1+ -> col
    again
;

: Window.char-at-cursor { w -- c , get character under cursor }
    \ TODO: tab expansion
    w Window.current-text 
    w s@ Window.offset-left + 
    w s@ Window.cursor-col + 
    c@
;

: Window.move-to { w row col | lin# lin last-lin -- , move cursor to row,col }
    \ todo past end of file
    \ todo before min-line#
    w s@ Window.top-line# -> lin#
    0 w s! Window.cursor-row
    w s@ Window.top-line -> lin
    lin w s! Window.current-line
    w s@ Window.type WindowType.nerdtree = if
        0 w s! Window.cursor-col
    else
        col GUTTER-WIDTH - dup 0< if drop 0 then w s! Window.cursor-col
    then
    row 0<> if
        row 0 do
            w s@ Window.cursor-row 1+ w s! Window.cursor-row
            w Window.last-line? if
                w Window.fix-cursor
                w Window.address-cursor
                w Window.refresh
                exit
            then
            lin# 1+ -> lin#
            lin s@ ListNode.next -> lin
            lin w s! Window.current-line
        loop
    then
    begin
        lin# w s@ Window.min-line# <
    while
        w Window.line-down
        lin# 1+ -> lin#
    repeat
    lin# w s! Window.line#

    w Window.fix-cursor
    w Window.address-cursor
    w Window.refresh
;

: Window.cursor-up { w -- , move cursor up }
    w Window.line-up 
;

: Window.page-up { w -- , move cusor up one page }
    w s@ Window.height 2/ 0 do 
        w Window.line-up 
    loop
;

: Window.goto-line { w line# | cnt -- , move cursor to line # }
    w s@ Window.line# line# - -> cnt
    cnt 0< if
        cnt -1 * -> cnt
        cnt 0 do
            w Window.line-down
        loop
        exit
    then
    cnt 0> if
        cnt 0 do
            w Window.line-up
        loop
    then
;

: Window.goto-eof { w | b -- , move cursor to end of file }
    begin
        w Window.last-line? not
    while
        w Window.line-down
    repeat
;

: Window.cursor-down { w | b -- , move cursor down }
    w Window.line-down
;

: Window.page-down { w -- , move cusor down one page }
    w s@ Window.height 2/ 0 do 
        w Window.line-down
    loop
;

: Window.top-of-screen { w | col -- , move cursor to top of screen }
    w s@ Window.cursor-col -> col
    begin
        w s@ window.cursor-row 0>
    while
        w window.cursor-up
    repeat
    col w s! Window.cursor-col
    w Window.fix-cursor
;

: Window.bottom-of-screen { w | col -- , move cursor to bottom of screen }
    w s@ Window.cursor-col -> col
    begin
        w Window.last-line? not
        w s@ Window.cursor-row w s@ Window.height 1- <= and
    while
        w window.cursor-down
    repeat
    col w s! Window.cursor-col
    w Window.fix-cursor
;

: Window.middle-of-screen { w | target col -- , move cursor to middle of screen }
    w s@ Window.cursor-col -> col
    w s@ Window.height 2/  -> target
    target w s@ Window.cursor-row > if
        begin
            w Window.last-line? not
            w s@ Window.cursor-row target < and
        while
            w window.cursor-down
        repeat
    else
        begin
            w s@ window.cursor-row 0>
            w s@ Window.cursor-row target > and
        while
            w window.cursor-up
        repeat
    then
    col w s! Window.cursor-col
    w Window.fix-cursor
;
: Window.cursor-left { w -- , move cursor left }
    w Window.line-left
;

: Window.end-of-line { w -- , move cursor to end of line }
    w Window.current-line-length 
    w Window.client-width - 0 max 
    w s! Window.offset-left
    w s@ Window.offset-left 0> if
        w Window.client-width 1 - w s! Window.cursor-col
    else
        w Window.current-line-length 1- w s! Window.cursor-col
    then
;

: Window.cursor-right { w -- , move cursor right }
    w Window.line-right
;

: Window.skip-blanks { w dir ignore-sep? -- , move cursor past blanks }
    w Window.save-cursor
    begin
        dir if
            w Window.end-of-line? if
                w Window.restore-cursor
                exit
            then
            w Window.char-at-cursor whitespace? not if exit then
            w Window.line-right
        else
            w Window.beginning-of-line? if
                exit
            then
            w Window.char-at-cursor whitespace? not if exit then
            w Window.line-left
        then
    again
;

: Window.skip-nonblanks { w dir ignore-sep? | ch delim -- , move cursor past blanks }
    w Window.char-at-cursor -> delim
    dir if
        w Window.end-of-line? if w Window.line-right exit then
    else
        w Window.beginning-of-line? if w Window.line-left exit then
    then
    begin
        dir if
            w Window.end-of-line? if exit then
            w Window.char-at-cursor -> ch
            w Window.line-right
            ignore-sep? if
                ch whitespace? if exit then
            else
                ch is-separator if exit then
            then
        else
            w Window.beginning-of-line? if exit then
            w Window.char-at-cursor -> ch
            w Window.line-left
            ch is-separator if 
                ch whitespace? if
                    w Window.line-right 
                then
                exit 
            then
        then
    again
;

: Window.word-forward { w ignore-sep? -- , move cursor right to start of next word }
    w true ignore-sep? Window.skip-nonblanks 
    w true ignore-sep? Window.skip-blanks
;

: Window.endword-forward { w ignore-sep? -- , move cursor right to last character of word }
    w Window.line-right
    w Window.line-right
    w true ignore-sep? Window.skip-nonblanks 
    w true ignore-sep? Window.skip-blanks
\     w true ignore-sep? Window.skip-nonblanks 
    w Window.line-left 
    ignore-sep? if
        w Window.char-at-cursor whitespace? if w Window.line-left then
    else
        w Window.char-at-cursor is-separator if w Window.line-left then
    then
;

: Window.word-backward { w ignore-sep? -- , move cursor left to start of previous word }
    w Window.line-left
    w Window.beginning-of-line? if
        exit
    then
    w false ignore-sep? Window.skip-blanks 
    w false ignore-sep? Window.skip-nonblanks  
     w Window.char-at-cursor whitespace? if w Window.line-right then
;

: Window.endword-backward { w ignore-sep? -- , move cursor right to last character of word }
    w false ignore-sep? Window.skip-nonblanks 
    w false ignore-sep? Window.skip-blanks
    w false ignore-sep? Window.skip-nonblanks 
    w Window.char-at-cursor whitespace? if w Window.line-right then
;

: Window.join-lines { w flg -- , join current line with next line adding a space between if flag is set }
    w Window.save-undo
    w s@ Window.buffer w s@ Window.current-line flg Buffer.join
;

: Window.delete-at-cursor { w flg | lin -- , delete character at cursor and repaint if flag }
    w Window.address-cursor
    w s@ Window.current-line  -> lin
    lin w s@ Window.offset-left w s@ Window.cursor-col + Line.delete-at-index drop
    flg if
        w Window.fix-cursor
    then
    w s@ Window.buffer Buffer.touch
;

: Window.del-word-forward { w -- , delete from cursor to end of word and following spaces }
    w Window.save-undo
    begin
        w Window.char-at-cursor 
        is-separator NOT
    while
        w false Window.delete-at-cursor
    repeat
    begin
        w Window.char-at-cursor whitespace?
    while
        w false Window.delete-at-cursor
    repeat
;

: Window.delete-line { w | b lin nxt -- , delete current line }
    w s@ Window.buffer -> b
    w s@ Window.current-line -> lin
    lin s@ ListNode.next -> nxt

    b w s@ Window.current-line Buffer.delete-line
    b s@ Buffer.#lines 0= if 
        z" " 1+ b Buffer.append-text -> lin
        lin w s! Window.current-line
        lin w s! Window.top-line
        w s@ Window.buffer Buffer.touch
        exit
    then
    w s@ Window.current-line w s@ Window.top-line = if
        nxt w s! Window.top-line
    then
    nxt w s! Window.current-line
    w s@ Window.buffer Buffer.touch
;

: Window.delete-word-at-cursor { w -- , delete entire word under cursor }
    begin
        w Window.char-at-cursor is-separator NOT
    while
        w Window.line-left
    repeat
    w Window.char-at-cursor whitespace? if
        w Window.line-right
    then
    w Window.del-word-forward
    w s@ Window.buffer Buffer.touch
;

\ TODO finish this word
: Window.word-under-cursor { w | ch done lin str -- caddr u , get word under cursor }
    w s@ Window.current-line -> lin
    lin s@ Line.text CString.new -> str
    str w Window.current-line-index 
    CString.set-index
    \ scan backwards for start of word
    false -> done
    begin
        str CString.bol? not
        done not and
    while
        str CString.peek-ch  -> ch
        ch ctype::isblank if
            str CString.advance-index
            true -> done
        else
            str CString.back-index
        then
    repeat
    str CString.parse-token
;

: Window.insert-character { w ch | lin -- , insert character at cursor, advance cursor }
    w s@ Window.current-line  -> lin
    lin w Window.current-line-index ch Line.insert-at-index
    w Window.line-right
    w s@ Window.buffer Buffer.touch
;

: Window.append-character { w c -- , append character after cursor, advance cursor }
    w s@ Window.current-line  
    w Window.current-line-index 1+
    c 
    Line.insert-at-index
    w s@ Window.buffer Buffer.touch
;

: Window.split-at-cursor { w -- , split line at cursor into two lines }
    w Window.save-undo
        w s@ Window.buffer
        w  s@ Window.current-line 
        w Window.current-line-index 
    Buffer.split-at-index
    w Window.line-right
;

: Window.insert-mode { w -- , set insert before cursor mode }
    w Window.save-undo
    editmode.insert editor-mode !
\     ncurses.cursor-insert drop
    WindowType.editor w s! Window.type    
    ncurses.refresh
    0 w s! Window.min-line#
;

: Window.append-mode { w | l -- , set append after cursor mode }
    w Window.save-undo
    w s@ Window.current-line -> l
        l 
        w Window.current-text string::strlen
        bl 
    Line.insert-at-index
    w Window.line-right
    w Window.insert-mode
;

: Window.bufed-mode { w -- , set bufed mode }
    editmode.bufed editor-mode !
    WindowType.bufed w s! Window.type    
    w Window.refresh
    3 w s! Window.min-line#
;

: Window.dired-mode { w -- , set dired mode }
    editmode.dired editor-mode !
    WindowType.dired w s! Window.type    
    w Window.refresh
    3 w s! Window.min-line#
;

: Window.worded-mode { w -- , set worded mode }
    editmode.worded editor-mode !
    WindowType.worded w s! Window.type    
    w Window.refresh
;

: Window.helped-mode { w -- , set helped mode }
    editmode.helped editor-mode !
    WindowType.helped w s! Window.type    
    w Window.refresh
    3 w s! Window.min-line#
;

: Window.cmded-mode { w -- , set cmded mode }
    editmode.cmded editor-mode !
    WindowType.cmded w s! Window.type    
    w Window.refresh
    3 w s! Window.min-line#
;

: Window.gited-mode { w -- , set gited mode }
    editmode.gited editor-mode !
    WindowType.gited w s! Window.type    
    w Window.refresh
    gited-help @ if
        8 w s! Window.min-line#
    else
        0 w s! Window.min-line#
    then
;

: Window.greped-mode { w -- , set greped mode }
    editmode.greped editor-mode !
    WindowType.greped w s! Window.type    
    w Window.refresh
\     3 w s! Window.min-line#
;
: Window.maned-mode { w -- , set greped mode }
    editmode.maned editor-mode !
    WindowType.maned w s! Window.type    
    w Window.refresh
\     3 w s! Window.min-line#
;

: Window.selection-mode { w -- , set selection mode }
    editmode.selection editor-mode !
    w Window.refresh
;

: Window.nerdtree-mode { w -- , set nerdtree mode }
    editmode.nerdtree editor-mode !
    w Window.refresh
    3 w s! Window.min-line#
;

: Window.set-editmode { w -- , set mode based upon window type }
    w s@ Window.type case
        WindowType.editor of w Window.visual-mode endof
        WindowType.bufed of w Window.bufed-mode endof
        WindowType.dired of w Window.dired-mode endof
        WindowType.worded of w Window.worded-mode endof
        WindowType.helped of w Window.helped-mode endof
        WindowType.cmded of w Window.cmded-mode endof
        WindowType.greped of w Window.greped-mode endof
        WindowType.gited of w Window.gited-mode endof
        WindowType.nerdtree of w Window.nerdtree-mode endof
        \ default
        die{ ." Window.set-editmode illegal Window.type " w s@ Window.type . }die
    endcase
;

\
\ API methods!
\   These are named as the keyboard commands that invoke them.
\
\   However, the API.* methods can be called in any context to affect changes in the editor
\

: API.delete-buffer { b | nxt  -- flg , }
    b s@ Buffer.permanent if
        errorline{ ."  *** Buffer is permanent " }errorline
        ncurses.bell
        false exit
    then

    \ get next buffer to show instead of current one
    b s@ ListNode.prev BufferList.list <> if
        b s@ ListNode.prev
    else
        b s@ ListNode.next
    then
    -> nxt

    \ assure all windows pointing to buffer view the next buffer
    b -> old-buffer
    nxt -> new-buffer
    ['] fix-buffer WindowList.iterate
    b List.remove
    b Buffer.destroy
    Screen.paint
    true
;
: API.% { w | delim dir ch -- , find matching paren, brace, bracket, etc. }
    \ todo ignore comments!
    \ todo count nested delims - e.g. ((foo))
    \                                 ^<--->^
    w Window.save-cursor
    true -> dir \ forwards
    w Window.char-at-cursor -> delim
    delim case
        ascii { of ascii } -> delim true -> dir endof
        ascii } of ascii { -> delim false -> dir endof
        ascii ( of ascii ) -> delim true -> dir endof
        ascii ) of ascii ( -> delim false -> dir endof
        ascii [ of ascii ] -> delim true -> dir endof
        ascii ] of ascii [ -> delim false -> dir endof
        ascii : of ascii ; -> delim true -> dir endof
        ascii ; of ascii : -> delim false -> dir endof
        exit
    endcase
    begin
        dir if 
            w Window.end-of-file? if w Window.restore-cursor exit then
            w Window.line-right 
        else 
            w Window.beginning-of-file? if w Window.restore-cursor exit then
            W Window.line-left 
        then
        w Window.char-at-cursor -> ch
        ch delim = if 
            ch ascii : <> if exit then
            w Window.beginning-of-line? if
                exit
            then
        then
\             delim ascii : = if
\             then
\         then
    again
;

: API.<< { w | lin -- , unindent line }
    w s@ Window.current-line -> lin
    lin 0 Line.character-at-index ctype::isspace if
        w Window.save-undo
        TAB-WIDTH @ 0 do
            lin 0 Line.character-at-index ctype::isspace if
                lin 0 Line.delete-at-index
                w s@ Window.cursor-col 0<> if
                    w Window.line-left
                then
            then
        loop
    then
;
: API.>> { w | lin -- , indent line }
    w Window.save-undo
    w s@ Window.current-line -> lin

    TAB-WIDTH @ 0 do 
        lin 0 bl Line.insert-at-index
        w Window.line-right
    loop
;

: API.ctrl_ax { w delta | lin n str ndx-save -- , increment number by delta at or right of cursor on current-line }
    w s@ Window.current-line -> lin
    lin s@ Line.text CString.new -> str
    str w Window.current-line-index CString.set-index
    str CString.skip-to-number not if 
        exit 
    then

    str CString.get-index -> ndx-save
    str CString.parse-number not if 
        exit 
    then 
    w Window.save-undo
    delta + -> n             \ increment it
    str CString.get-index ndx-save do
        str ndx-save CString.delete-char
    loop
    str n (.) ndx-save CString.insert-string
    lin str s@ CString.data string::strdup Line.replace-text
    str CString.destroy
;

: API.cw { w -- , change word }
    w Window.del-word-forward
    w Window.fix-cursor
    w Window.insert-mode
;

: API.daw { w -- , delete word under cursor and space before/after it }
    \ delete (cut) word under the cursor and the space after or before it
    w Window.save-undo
    begin
        w Window.char-at-cursor whitespace? NOT
    while
        w false Window.delete-at-cursor
    repeat
    w true Window.delete-at-cursor
;

: API.dd { w -- , delete line that cursor is on }
    w Window.save-undo
    w Window.delete-line
;

: API.dw { w -- , delete word forward }
    w Window.save-undo
    w Window.del-word-forward
    w Window.fix-cursor
;

: API.diw { w -- , delete/cut word under cursor }
    w Window.save-undo
    w Window.delete-word-at-cursor
    w Window.fix-cursor
;

: API.di{ { w -- , delete between open and close curly brace }

;
: API.di} API.di{ ;     \ alias

: API.di( { w -- , delete between open and close parenthesis }
;
: API.di) API.di( ;     \ alias
: API.di[ { w -- , delete between open and close brackets }
;
: API.di] API.di[ ;     \ alias

: API.x { w -- , delete char under cursor }
    w Window.save-undo
    w true Window.delete-at-cursor
\     w Window.paint
;
: API.i { w -- , enter insert mode }
    w Window.save-undo
    w Window.insert-mode
;
: API.a { w -- , enter append mode }
    w Window.save-undo
    w Window.append-mode
;

: API.paste { w n | b done dst src new nxt head -- , paste selection n times after window current line }
    w Window.save-undo
    n 1 < if 1 -> n then
    w s@ Window.buffer -> b
    w s@ Window.current-line -> dst
    n 0 do
        Selection.head -> head
        head s@ List.next -> src
        false -> done
        begin
            done not
        while
                b 
                src s@ Line.text Line.new       \ src
                dst                             \ dst
            Buffer.insert-after
            src s@ ListNode.next -> src
            dst s@ ListNode.next -> dst
            src head = if true -> done then
        repeat
    loop
;

: API.yank { w | b s1 s2 done -- , copy selection-start# through selection-end# to selection buffer }
    w Window.save-undo
    w s@ Window.buffer -> b
    b selection-start# @ Buffer.goto-line# -> s1
    b selection-end# @ Buffer.goto-line# -> s2

    Selection.reset

    false -> done 
    begin
        done not
    while
        s1 s@ Line.text Selection.append
        s1 s2 = if 
            true -> done
        else
            s1 s@ ListNode.next -> s1
        then
    repeat
;

: API.delete-selection { w | b done s1 s2 -- , delete selection } 
    w Window.save-undo
    w s@ Window.buffer -> b

    selection-start# @ -> s1
    selection-end# @ -> s2

    w s1 Window.goto-line

    s2 s1 = if
        w API.dd
        exit
    then
    s2 1+ s1 do
        w API.dd
    loop
;



privatize
