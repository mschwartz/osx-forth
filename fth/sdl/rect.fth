anew task-sdl-rect.fth

:STRUCT SDL_Point
    INT p_x
    INT p_y
;STRUCT

:STRUCT SDL_FPoint
    FLPT fp_x
    FLPT fp_y
;STRUCT

:STRUCT SDL_Rect
    INT r_x
    INT r_y
    INT r_w
    INT r_h
;STRUCT

:STRUCT SDL_FRect
    FLPT r_fx
    FLPT r_fy
    FLPT r_fw
    FLPT r_fh
;STRUCT
