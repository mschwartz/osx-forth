\
\ C++ information function
\
\ Programmed by Mike Schwartz
\
\ core words, extensions, help text
\
anew task-lib/c++-keywords.fth

require lib/lists.fth
require lib/hashmap.fth
require lib/strings.fth

: c++Info.destructor { ci -- , destroy c++ info }
;

private{
HashMap keywords
keywords ' c++Info.destructor HashMap.init
create keyword-buf 512 allot
variable max-length 0 max-length !

: add-keyword { $name $help | cnt -- add word and help to ForthInfo }
    $name count -> cnt  drop
    cnt max-length @ > if
        cnt max-length !
    then
    keywords $name $help HashMap.set drop
;

here s" #include" ",                here s" help text" ", add-keyword
here s" #if" ",                      here s" help text" ", add-keyword
here s" #ifdef" ",                      here s" help text" ", add-keyword
here s" #ifndef" ",                      here s" help text" ", add-keyword
here s" #else" ",                      here s" help text" ", add-keyword
here s" #endif" ",                      here s" help text" ", add-keyword

here s" alignas" ",                 here s" (C++11)" ", add-keyword
here s" alignof" ",                 here s" (C++11)" ", add-keyword
here s" and" ",                     here s" help text" ", add-keyword
here s" and_eq" ",                  here s" help text" ", add-keyword
here s" asm" ",                     here s" help text" ", add-keyword
here s" atomic_cancel" ",           here s" (TM TS)" ", add-keyword
here s" atomic_commit" ",           here s" (TM TS)" ", add-keyword
here s" atomic_noexcept" ",         here s" (TM TS)" ", add-keyword
here s" auto" ",                    here s" help text" ", add-keyword
here s" bitand" ",                  here s" help text" ", add-keyword
here s" bitor" ",                   here s" help text" ", add-keyword
here s" bool" ",                    here s" help text" ", add-keyword
here s" break" ",                   here s" help text" ", add-keyword
here s" case" ",                    here s" help text" ", add-keyword
here s" catch" ",                   here s" help text" ", add-keyword
here s" char" ",                    here s" help text" ", add-keyword
here s" char8_t" ",                  here s" (C++20)" ", add-keyword
here s" char16_t" ",                 here s" (C++11)" ", add-keyword
here s" char32_t" ",                 here s" (C++11)" ", add-keyword
here s" class" ",                    here s" help text"  ",  add-keyword
here s" compl" ",                   here s" help text" ", add-keyword
here s" concept"  ",                  here s" (C++20)" ", add-keyword
here s" const" ",                   here s" help text" ", add-keyword
here s" consteval" ",               here s" (C++20)" ", add-keyword
here s" constexpr" ",               here s" (C++11)" ", add-keyword
here s" constinit" ",               here s" (C++20)" ", add-keyword
here s" const_cast" ",              here s" help text" ", add-keyword
here s" continue" ",                here s" help text" ", add-keyword
here s" co_await" ",                here s" (C++20)" ", add-keyword
here s" co_return" ",               here s" (C++20)" ", add-keyword
here s" co_yield" ",                here s" (C++20)" ", add-keyword
here s" ecltype" ",                 here s" (C++11)" ", add-keyword
here s" default" ",                 here s" help text" ", add-keyword
here s" delete" ",                  here s" help text" ", add-keyword
here s" do" ",                      here s" help text" ", add-keyword
here s" double" ",                  here s" help text" ", add-keyword
here s" dynamic_cast" ",            here s" help text" ", add-keyword
here s" else" ",                    here s" help text" ", add-keyword
here s" enum" ",                    here s" help text" ", add-keyword
here s" explicit" ",                here s" help text" ", add-keyword
here s" export" ",                  here s" help text" ", add-keyword
here s" extern" ",                  here s" help text" ", add-keyword
here s" false" ",                   here s" help text" ", add-keyword
here s" float" ",                   here s" help text" ", add-keyword
here s" for" ",                     here s" help text" ", add-keyword
here s" friend" ",                  here s" help text" ", add-keyword
here s" goto" ",                    here s" help text" ", add-keyword
here s" if" ",                      here s" help text" ", add-keyword
here s" inline" ",                  here s" help text" ", add-keyword
here s" int" ",                     here s" help text" ", add-keyword
here s" long" ",                    here s" help text" ", add-keyword
here s" mutable" ",                  here s" help text" ", add-keyword
here s" namespace" ",               here s" help text" ", add-keyword
here s" new" ",                     here s" help text" ", add-keyword
here s" noexcept" ",                 here s" (C++11)" ", add-keyword
here s" not" ",                     here s" help text" ", add-keyword
here s" not_eq" ",                  here s" help text" ", add-keyword
here s" nullptr" ",                  here s" (C++11)" ", add-keyword
here s" operator" ",                 here s" help text" ", add-keyword
\ cr .s bye
here s" or" ",                      here s" help text" ", add-keyword
here s" or_eq" ",                   here s" help text" ", add-keyword
here s" private" ",                  here s" help text" ", add-keyword
here s" protected" ",               here s" help text" ", add-keyword
here s" public" ",                  here s" help text" ", add-keyword
here s" reflexpr" ",                 here s" (reflection TS)" ", add-keyword
here s" register" ",                 here s" help text" ", add-keyword
here s" reinterpret_cast" ",        here s" help text" ", add-keyword
here s" requires" ",                 here s" (C++20)" ", add-keyword
here s" return" ",                  here s" help text" ", add-keyword
here s" short" ",                   here s" help text" ", add-keyword
here s" signed" ",                  here s" help text" ", add-keyword
here s" sizeof" ",                   here s" help text" ", add-keyword
here s" static" ",                  here s" help text" ", add-keyword
here s" static_assert" ",            here s" (C++11)" ", add-keyword
here s" static_cast" ",             here s" help text" ", add-keyword
here s" struct" ",                   here s" help text" ", add-keyword
here s" switch" ",                  here s" help text" ", add-keyword
here s" synchronized" ",             here s" (TM TS)" ", add-keyword
here s" template" ",                here s" help text" ", add-keyword
here s" thi"s ",                     here s" help text" ", add-keyword
here s" thread_local" ",             here s" (C++11)" ", add-keyword
here s" throw" ",                   here s" help text" ", add-keyword
here s" true" ",                    here s" help text" ", add-keyword
here s" try" ",                     here s" help text" ", add-keyword
here s" typedef" ",                 here s" help text" ", add-keyword
here s" typeid" ",                  here s" help text" ", add-keyword
here s" typename" ",                here s" help text" ", add-keyword
here s" union" ",                   here s" help text" ", add-keyword
here s" unsigned" ",                here s" help text" ", add-keyword
here s" using" ",                    here s" help text" ", add-keyword
here s" virtual" ",                 here s" help text" ", add-keyword
here s" void" ",                    here s" help text" ", add-keyword
here s" volatile" ",                here s" help text" ", add-keyword
here s" wchar_t" ",                 here s" help text" ", add-keyword
here s" while" ",                   here s" help text" ", add-keyword
here s" xor" ",                     here s" help text" ", add-keyword
here s" xor_eq" ",                  here s" help text" ", add-keyword
here s" final" ",                    here s" (C++11)" ", add-keyword
here s" override" ",                 here s" (C++11)" ", add-keyword
here s" transaction_safe" ",         here s" (TM TS)" ", add-keyword
here s" transaction_safe_dynamic" ", here s" (TM TS)" ", add-keyword
here s" import" ",                   here s" (C++20)" ",  add-keyword
here s" module" ",                   here s" (C++20)" ",  add-keyword
here s" if" ",                      here s" help text" ", add-keyword
here s" elif" ",                    here s" help text" ", add-keyword
here s" else" ",                    here s" help text" ", add-keyword
here s" endif" ",                   here s" help text" ", add-keyword
here s" ifdef" ",                   here s" help text" ", add-keyword
here s" ifndef" ",                  here s" help text" ", add-keyword
here s" elifdef" ",                  here s" (C++23)" ",  add-keyword
here s" elifndef" ",                 here s" (C++23)" ", add-keyword
here s" define" ",                  here s" help text" ", add-keyword
here s" undef" ",                   here s" help text" ", add-keyword
here s" include" ",                 here s" help text" ", add-keyword
here s" line" ",                    here s" help text" ", add-keyword
here s" error" ",                   here s" help text" ", add-keyword
here s" warning" ",                  here s" (C++23)" ", add-keyword
here s" pragma" ",                  here s" help text" ", add-keyword
here s" defined" ",                 here s" help text" ", add-keyword
here s" __has_include" ",            here s" (C++17)" ", add-keyword
here s" __has_cpp_attribute" ",      here s" (C++20)" ", add-keyword
here s" export" ",                   here s" (C++20)" ", add-keyword
here s" import" ",                   here s" (C++20)" ", add-keyword
here s" module" ",                   here s" (C++20)" ", add-keyword
here s" _Pragma" ",                  here s" (C++11)" ", add-keyword
}private

: c++-keyword? { caddr u -- flg , is caddr u a core word? }
    caddr u keyword-buf place
    keyword-buf count 
    string>upper
    keywords keyword-buf HashMap.get 
    0<>
;

: c++-keywords.iterate { 'handler -- , iterate c++ keywords }
    keywords 'handler HashMap.iterate
;

privatize
