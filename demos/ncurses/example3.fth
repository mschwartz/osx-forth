false trace-include !

require lib/ncurses.fth

create newline 13 c, 10 c, 0 c,     \ crlf c string

\ Example 3: A Simple printw example per https://tldp.org/HOWTO/NCURSES-Programming-HOWTO/printw.html
: demo { | row col y x -- , printw example }
    s" Just a string " pad place-cstr
    ncurses.initscr drop
    ncurses.stdscr ncurses.getmaxyx -> col -> row
    row 2/ -> y
    col 2/ pad strlen -  -> x
    y x s" This screen has " ncurses.mvprintw 
        row ncurses.# s"  rows and " ncurses.printw 
        col ncurses.# s"  columns  " ncurses.printw
    newline count ncurses.printw
    newline count ncurses.printw
    s"  Try resizing the window and run this program again" ncurses.printw
    ncurses.refresh
    ncurses.getch drop
    ncurses.endwin
;

demo