#ifdef PF_SUPPORT_STDIO

case ID_SYS_clearerr: { //
  clearerr((FILE *)TOS);
  TOS = M_POP;
}
  endcase;
case ID_SYS_fready: { // ( fp -- f )
  FILE *fp = (FILE *)TOS;
  int fd = fileno(fp);

  int select_retval;
  fd_set readfds;
  struct timeval tv;

  FD_ZERO(&readfds);
  FD_SET(fd, &readfds);

  /* Set timeout to zero so that we just poll and return. */
  tv.tv_sec = 0;
  tv.tv_usec = 1; // enough time for ansi escape sequence to be encountered
  select_retval = select(fd + 1, &readfds, NULL, NULL, &tv);
  if (select_retval < 0) {
    perror("sdTerminalInit: select");
  }
  //  printf("key? %d\n", FD_ISSET(STDIN_FILENO, &readfds));
  TOS = (cell_t)FD_ISSET(fd, &readfds) ? FTRUE : FFALSE;
}
  endcase;

case ID_SYS_fclose: { //
  TOS = (cell_t)fclose((FILE *)TOS);
}
  endcase;
case ID_SYS_feof: { // ( fp -- f , return true if file is at EOF )
  TOS = (cell_t)feof((FILE *)TOS);
}
  endcase;
case ID_SYS_ferror: { //
  TOS = (cell_t)ferror((FILE *)TOS);
}
  endcase;
case ID_SYS_fflush: { //
  TOS = (cell_t)fflush((FILE *)TOS);
}
  endcase;
case ID_SYS_fgetc: { //
  TOS = (cell_t)fgetc((FILE *)TOS);
}
  endcase;
case ID_SYS_fgets: { // ( caddr u FILE -- result , read string from FILE )
  FILE *fp = (FILE *)TOS;
  TOS = M_POP;
  int size = (int)TOS;
  TOS = M_POP;
  char *src = (char *)TOS;
  *src = '\0';
  TOS = (cell_t)fgets(src, size, fp);

  // put null terminator wherever newline is
  while (*src != '\0') {
    if (*src == 13 || *src == 10) {
      *src = '\0';
      break;
    }
    src++;
  }
}
  endcase;
case ID_SYS_fopen: { // (  mode filename -- result ,  open file for I/O using mode )
  char *filename = (char *)TOS;
  TOS = M_POP;
  char *mode = (char *)TOS;
  TOS = (cell_t)fopen(filename, mode);
}
  endcase;
case ID_SYS_fputc: { // (  c FILE -- result )
  FILE *fp = (FILE *)TOS;
  TOS = M_POP;
  TOS = (cell_t)fputc((int)TOS, fp);
}
  endcase;
case ID_SYS_fputs: { // ( string FILE -- result )
  FILE *fp = (FILE *)TOS;
  TOS = M_POP;
  char *string = (char *)TOS;
  TOS = (cell_t)fputs(string, fp);
}
  endcase;
case ID_SYS_fread: { // ( buffer size nitems FILE -- result , read from FILE into buffer )
  FILE *fp = (FILE *)TOS;
  TOS = M_POP;
  int nitems = (int)TOS;
  TOS = M_POP;
  int size = (int)TOS;
  TOS = M_POP;
  TOS = (cell_t)fread((char *)TOS, size, nitems, fp);
}
  endcase;
case ID_SYS_freopen: { // (  mode filename FILE -- result ,  reopen file for I/O using mode )
  FILE *fp = (FILE *)TOS;
  TOS = M_POP;
  char *filename = (char *)TOS;
  TOS = M_POP;
  TOS = (cell_t)freopen(filename, (char *)TOS, fp);
}
  endcase;
case ID_SYS_fseek: { // ( offset whence FILE -- result , seek in FILE )
  FILE *fp = (FILE *)TOS;
  TOS = M_POP;
  int whence = (int)TOS;
  TOS = M_POP;
  TOS = (cell_t)fseek(fp, (long)TOS, whence);
}
  endcase;
case ID_SYS_fsetpos: { // ( position FILE -- result , set position in FILE )
  FILE *fp = (FILE *)TOS;
  TOS = M_POP;
  TOS = (cell_t)fsetpos(fp, (fpos_t *)TOS);
}
  endcase;
case ID_SYS_ftell: { //
  TOS = (cell_t)ftell((FILE *)TOS);
}
  endcase;
case ID_SYS_fwrite: { // ( buffer size nitems FILE -- result , write buffer to FILE )
  FILE *fp = (FILE *)TOS;
  TOS = M_POP;
  int nitems = (int)TOS;
  TOS = M_POP;
  int size = (int)TOS;
  TOS = M_POP;
  //  printf("fwrite: %p %d, %d, %p\n", (void *)TOS, size, nitems, (void *)fp);
  TOS = (cell_t)fwrite((char *)TOS, size, nitems, fp);
  //  printf("RETURNS %ld\n", TOS);
}
  endcase;
case ID_SYS_getc: { //
  TOS = (cell_t)getc((FILE *)TOS);
}
  endcase;
case ID_SYS_getchar: { //
  PUSH_TOS;
  TOS = (cell_t)getchar();
}
  endcase;
case ID_SYS_perror: { // (  caddr -- , print system error message)
  ForthStringToC(gScratch, (char *)TOS, sizeof(gScratch));
  perror(gScratch);
  TOS = M_POP;
}
  endcase;
case ID_SYS_putc: { // ( c FILE -- result )
  FILE *fp = (FILE *)TOS;
  TOS = M_POP;
  TOS = (cell_t)putc((int)TOS, fp);
}
  endcase;
case ID_SYS_putchar: { // ( c -- result )
  TOS = (cell_t)putchar((int)TOS);
}
  endcase;
case ID_SYS_puts: { //
  ForthStringToC(gScratch, (char *)TOS, sizeof(gScratch));
  TOS = (cell_t)puts(gScratch);
}
  endcase;
case ID_SYS_remove: { //
  ForthStringToC(gScratch, (char *)TOS, sizeof(gScratch));
  TOS = (cell_t)remove(gScratch);
}
  endcase;
case ID_SYS_rename: { //
  ForthStringToC(gScratch, (char *)TOS, sizeof(gScratch));
  TOS = M_POP;
  ForthStringToC(gScratch2, (char *)TOS, sizeof(gScratch2));
  TOS = (cell_t)rename(gScratch, gScratch2);
}
  endcase;
case ID_SYS_rewind: { // ( FILE -- )
  rewind((FILE *)TOS);
  TOS = M_POP;
}
  endcase;
case ID_SYS_setbuf: { // ( buffer FILE -- , set buffer for FILE )
  FILE *fp = (FILE *)TOS;
  TOS = M_POP;
  setbuf(fp, (char *)TOS);
  TOS = M_POP;
}
  endcase;
case ID_SYS_setvbuf: { // ( buffer size type FILE -- , set buffer for FILE )
  FILE *fp = (FILE *)TOS;
  TOS = M_POP;
  int type = (int)TOS;
  TOS = M_POP;
  size_t size = (size_t)TOS;
  TOS = (cell_t)setvbuf(fp, (char *)TOS, type, size);
}
  endcase;
case ID_SYS_tmpfile: { //
  PUSH_TOS;
  TOS = (cell_t)tmpfile();
}
  endcase;
case ID_SYS_ungetc: { // ( c FILE -- result , push character back to input stream )
  FILE *fp = (FILE *)TOS;
  TOS = M_POP;
  TOS = ungetc((int)TOS, fp);
}
  endcase;
case ID_SYS_fdopen: { // ( mode fd -- FILE , open FILE from descriptor )
  int fd = (int)TOS;
  TOS = M_POP;
  ForthStringToC(gScratch, (char *)TOS, sizeof(gScratch));
  TOS = (cell_t)fdopen(fd, gScratch);
}
  endcase;
case ID_SYS_fileno: { // ( FILE -- fd , get file descriptor for FILE )
  TOS = (cell_t)fileno((FILE *)TOS);
}
  endcase;

case ID_SYS_pclose: { //
  TOS = (cell_t)pclose((FILE *)TOS);
}
  endcase;
case ID_SYS_popen: { // ( mode command -- FILE , open pipe )
  char *command = (char *)TOS;
  TOS = M_POP;
  TOS = (cell_t)popen(command, (char *)TOS);
}
  endcase;
case ID_SYS_strerror: { // ( -- string )
  PUSH_TOS;
  TOS = (cell_t)strerror(errno);
}
  endcase;
case ID_SYS_flockfile: { // (FILE -- , lock FILE )
  flockfile((FILE *)TOS);
  TOS = M_POP;
}
  endcase;
case ID_SYS_ftrylockfile: { //
  TOS = (cell_t)ftrylockfile((FILE *)TOS);
}
  endcase;
case ID_SYS_funlockfile: { //
  funlockfile((FILE *)TOS);
  TOS = M_POP;
}
  endcase;
case ID_SYS_getc_unlocked: { //
  TOS = (cell_t)getc_unlocked((FILE *)TOS);
}
  endcase;
case ID_SYS_getchar_unlocked: { //
  PUSH_TOS;
  TOS = (cell_t)getchar_unlocked();
}
  endcase;
case ID_SYS_putc_unlocked: { // ( c FILE -- result )
  FILE *fp = (FILE *)TOS;
  TOS = M_POP;
  TOS = (cell_t)putc_unlocked((int)TOS, fp);
}
  endcase;
case ID_SYS_putchar_unlocked: { //
  TOS = (cell_t)putchar_unlocked((int)TOS);
}
  endcase;
case ID_SYS_fseeko: { // ( whence offset FILE -- seek in file )
  int whence = (int)TOS;
  TOS = M_POP;
  off_t offset = (off_t)TOS;
  TOS = M_POP;
  TOS = fseeko((FILE *)TOS, offset, whence);
}
  endcase;
case ID_SYS_ftello: { // ( FILE -- offset , return position in file )
  TOS = (cell_t)ftello((FILE *)TOS);
}
  endcase;
case ID_SYS_getdelim: { // ( buffer count delimiter FILE -- result )
  FILE *fp = (FILE *)TOS;
  TOS = M_POP;
  size_t count = (size_t)TOS;
  TOS = M_POP;
  int delimiter = (int)TOS;
  TOS = M_POP;
  TOS = getdelim((char **)TOS, &count, delimiter, fp);
}
  endcase;
case ID_SYS_getline: { // ( buffer count FILE -- result )
  FILE *fp = (FILE *)TOS;
  TOS = M_POP;
  size_t count = (size_t)TOS;
  TOS = M_POP;
  getline((char **)TOS, &count, fp);
}
  endcase;
case ID_SYS_fmemopen: { // ( mode buffer size -- FILE , open a memory FILE )
  size_t size = (size_t)TOS;
  TOS = M_POP;
  void *buf = (void *)TOS;
  TOS = M_POP;
  TOS = (cell_t)fmemopen(buf, size, (char *)TOS);
}
  endcase;
case ID_SYS_open_memstream: { // ( sizep bufferp -- FILE , open a memory stream for writing )
  char **bp = (char **)TOS;
  TOS = M_POP;
  TOS = (cell_t)open_memstream(bp, (size_t *)TOS);
}
  endcase;
case ID_SYS_ctermid: { // ( buf or NULL -- buf , generate pathname string for current terminal )
  TOS = (cell_t)ctermid((char *)TOS);
}
  endcase;
case ID_SYS_fpurge: { // ( FILE -- result, flush FILE stream, discarding anything buffered )
#ifdef __APPLE__
  TOS = (cell_t)fpurge((FILE *)TOS);
#else
  printf("fpurge not implemented on this platform.\n");
  exit(0);
#endif
}
  endcase;
case ID_SYS_setbuffer: { // ( buffer size FILE -- , set buffer for FILE )
  FILE *fp = (FILE *)TOS;
  TOS = M_POP;
  size_t size = (size_t)TOS;
  TOS = M_POP;
  setbuffer(fp, (char *)TOS, size);
  TOS = M_POP;
}
  endcase;
case ID_SYS_setlinebuf: { // ( FILE -- , set FILE line buffering )
  setlinebuf((FILE *)TOS);
  TOS = M_POP;
}
  endcase;
case ID_SYS_feof_unlocked: { // ( FILE -- result , check if FILE is at end of file )
  TOS = (cell_t)feof_unlocked((FILE *)TOS);
}
  endcase;
case ID_SYS_ferror_unlocked: { //
  TOS = (cell_t)ferror_unlocked((FILE *)TOS);
}
  endcase;
case ID_SYS_clearerr_unlocked: { //
  clearerr_unlocked((FILE *)TOS);
  TOS = M_POP;
}
  endcase;
case ID_SYS_fileno_unlocked: { //
  TOS = (cell_t)fileno_unlocked((FILE *)TOS);
}
  endcase;
case ID_SYS_stat: { // ( filename statbuf -- err , get file status )
  struct stat *b = (struct stat *)TOS;
  TOS = M_POP;
  ForthStringToC(gScratch, (char *)TOS, sizeof(gScratch));
  TOS = (cell_t)stat(gScratch, b);
}
  endcase;

case ID_SYS_statz: { // ( filename statbuf -- err , get file status )
  struct stat *b = (struct stat *)TOS;
  TOS = M_POP;
  TOS = (cell_t)stat((char *)TOS, b);
}
  endcase;
case ID_SYS_file_modified: { // ( filename -- mtime , file modified timestamp )
  struct stat b;
  if (stat((char *)TOS, &b) < 0) {
    TOS = 0;
  }
  else {
    TOS = (cell_t)b.st_mtime;
  }
}
  endcase;
case ID_SYS_st_size: { // ( statbuf -- size )
  TOS = (cell_t)((struct stat *)TOS)->st_size;
}
  endcase;
case ID_SYS_st_mode: { // ( statbuf -- mode )
  TOS = (cell_t)((struct stat *)TOS)->st_mode;
}
  endcase;
case ID_SYS_st_uid: { // ( statbuf -- uid )
  struct stat *b = (struct stat *)TOS;
  TOS = (cell_t)b->st_uid;
}
  endcase;
case ID_SYS_st_gid: { // ( statbuf -- gid )
  TOS = (cell_t)((struct stat *)TOS)->st_gid;
}
  endcase;
case ID_SYS_st_atime: { // ( statbuf -- atime )
  TOS = (cell_t)((struct stat *)TOS)->st_atime;
}
  endcase;
case ID_SYS_st_mtime: { // ( statbuf -- mtime )
  TOS = (cell_t)((struct stat *)TOS)->st_mtime;
}
  endcase;
case ID_SYS_st_ctime: { // ( statbuf -- ctime )
  TOS = (cell_t)((struct stat *)TOS)->st_ctime;
}
  endcase;
case ID_SYS_st_blocks: { // ( statbuf -- blocks )
  TOS = (cell_t)((struct stat *)TOS)->st_blocks;
}
  endcase;
case ID_SYS_st_nlink: { // ( statbuf -- nlink )
  TOS = (cell_t)((struct stat *)TOS)->st_nlink;
}
  endcase;

case ID_SYS_mkstemp: { // ( buf -- buf , generate pathname string for temporary file )
  TOS = (cell_t)mkstemp((char *)TOS);
}
  endcase;

case ID_SYS_mkdtemp: { // ( buf -- buf , generate pathname string for temporary directory )
  TOS = (cell_t)mkdtemp((char *)TOS);
}
  endcase;

case ID_SYS_mkdtempat_np: { // ( dfd buf -- buf , generate pathname string for temporary directory )
#ifdef __APPLE__
  int dfd = (int)TOS;
  TOS = M_POP;
  TOS = (cell_t)mkdtempat_np(dfd, (char *)TOS);
#else
  printf("mkdtempat_np not supported on this platform\n");
  exit(0);
#endif
}
  endcase;

case ID_SYS_mkstemps: { // ( template suffixlen -- ret , generate pathname string for temporary file )
  int suffixlen = (int)TOS;
  TOS = M_POP;
  TOS = (cell_t)mkstemps((char *)TOS, suffixlen);
}
  endcase;

case ID_SYS_mkstempsat_np: { // ( dfd template suffixlen -- ret , generate pathname string for temporary file )
#ifdef __APPLE__
  int dfd = (int)TOS;
  TOS = M_POP;
  char *tmplate = (char *)TOS;
  TOS = M_POP;
  TOS = (cell_t)mkstempsat_np(dfd, tmplate, (int)TOS);
#else
  printf("mkstempsat_np not supported on this platform\n");
  exit(0);
#endif
}
  endcase;

case ID_SYS_mkostemp: { // ( template flags  -- buf , generate pathname string for temporary file )
  int flags = (int)TOS;
  TOS = M_POP;
  TOS = (cell_t)mkostemp((char *)TOS, flags);
}
  endcase;

case ID_SYS_mkostemps: { // ( template suffixlen flags  -- buf , generate pathname string for temporary file )
  int flags = (int)TOS;
  TOS = M_POP;
  int suffixlen = (int)TOS;
  TOS = M_POP;
  TOS = (cell_t)mkostemps((char *)TOS, suffixlen, flags);
}
  endcase;

case ID_SYS_mkostempsat_np: { // ( dfd template suffixlen flags  -- buf , generate pathname string for temporary file )
#ifdef __APPLE__
  int dfd = (int)TOS;
  TOS = M_POP;
  char *tmplate = (char *)TOS;
  TOS = M_POP;
  int suffixlen = (int)TOS;
  TOS = M_POP;
  int flags = (int)TOS;
  TOS = (cell_t)mkostempsat_np(dfd, tmplate, suffixlen, flags);
#else
  printf("mkostempsat_np not supported on this platform\n");
  exit(0);
#endif
}
  endcase;

case ID_SYS_filesize: { // ( filename -- size , get file size )
  struct stat sbuf;
  const char *filename = (const char *)TOS;
  if (stat(filename, &sbuf) < 0) {
    TOS = -1;
    break;
  }
  TOS = (cell_t)sbuf.st_size;
}
  endcase;

case ID_SYS_fileexists: { // ( filename -- result , check if file exists )}
  const char *filename = (const char *)TOS;
  TOS = (cell_t)access(filename, F_OK) ? 0 : -1;
}
  endcase;

case ID_SYS_access: { // ( filename -- result , check if file exists )}
  const char *filename = (const char *)TOS;
  TOS = (cell_t)access(filename, F_OK);
}
  endcase;

case ID_SYS_TCP_NODELAY: {
  // ( sock -- , set TCP_NODELAY on socket )
  int sock = (int)TOS;
  int flag = 1;
  TOS = (cell_t)setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (void *)&flag, sizeof(int));
}
  endcase;

case ID_SYS_open: { // ( filename mode -- fd , open file )
  int ret;
  int mode = TOS;
  TOS = M_POP;
  char *filename = (char *)TOS;
  ret = open(filename, mode, 0644);
  TOS = (cell_t)ret;
}
  endcase;

case ID_SYS_close: { // ( fd -- , close file )
  TOS = (cell_t)close((int)TOS);
}
  endcase;
case ID_SYS_read: { // ( buffer count fd -- count , read from file )
  int ret;
  int fd = (int)TOS;
  TOS = M_POP;
  size_t count = (size_t)TOS;
  TOS = M_POP;
  void *buf = (void *)TOS;
  ret = read(fd, buf, count);
  TOS = (cell_t)ret;
}
  endcase;
case ID_SYS_write: { // ( buffer count fd -- count , write to file )
  int fd = (int)TOS;
  TOS = M_POP;
  size_t count = (size_t)TOS;
  TOS = M_POP;
  TOS = (cell_t)write(fd, (char *)TOS, count);
  //  perror("\n\nHERE\n\n");
}
  endcase;
case ID_SYS_flock: { // ( locktype fd -- , flock file )
  int fd = (int)TOS;
  TOS = M_POP;
  int locktype = (int)TOS;
  int ret;
  ret = flock(fd, locktype);
  TOS = (cell_t)ret;
}
  endcase;
case ID_SYS_lseek: { // ( offset whence fd -- offset , seek in file )
  int fd = (int)TOS;
  TOS = M_POP;
  int whence = (int)TOS;
  TOS = M_POP;
  TOS = (cell_t)lseek(fd, (long)TOS, whence);
}
  endcase;
case ID_SYS_creat: { // ( filename mode -- fd , create file )
  mode_t mode = (mode_t)TOS;
  TOS = M_POP;
  TOS = (cell_t)creat((char *)TOS, mode);
}
  endcase;
case ID_SYS_truncate: { // ( filename size -- err , truncate file )
  int off_t = (int)TOS;
  TOS = M_POP;
  ForthStringToC(gScratch, (char *)TOS, sizeof(gScratch));
  TOS = (cell_t)truncate(gScratch, off_t);
}
  endcase;
case ID_SYS_ftruncate: { // ( size fd -- err , truncate file )
  int fd1 = (int)TOS;
  TOS = M_POP;
  TOS = (cell_t)ftruncate(fd1, (off_t)TOS);
}
  endcase;
case ID_SYS_mkdir: { // ( filename mode -- err , create directory )
  mode_t mode = (mode_t)TOS;
  TOS = M_POP;
  ForthStringToC(gScratch, (char *)TOS, sizeof(gScratch));
  TOS = (cell_t)mkdir(gScratch, mode);
}
  endcase;
case ID_SYS_unlink: { // ( filename -- err , delete file )
  ForthStringToC(gScratch, (char *)TOS, sizeof(gScratch));
  TOS = (cell_t)unlink(gScratch);
}
  endcase;
case ID_SYS_realpath: { // ( $path -- $resolved , get base name )
  ForthStringToC(gScratch, (char *)TOS, sizeof(gScratch));
  char *resolved = realpath(gScratch, gScratch2);
  CStringToForth(gScratch, resolved, sizeof(gScratch));
  TOS = (cell_t)gScratch;
}
  endcase;
case ID_SYS_realpathz: { // ( zpath -- $resolved , get base name )
  strcpy(gScratch, (char *)TOS);
  realpath(gScratch, gScratch2);
  TOS = (cell_t)gScratch2;
}
  endcase;
case ID_SYS_basename: { // ( $path -- base , get base name )
  TOS = (cell_t)basename((char *)TOS);
}
  endcase;
case ID_SYS_dirname: { // ( path -- dir , get base name )
  ForthStringToC(gScratch, (char *)TOS, sizeof(gScratch));
  TOS = (cell_t)dirname(gScratch);
}
  endcase;
case ID_SYS_getcwd: { // ( buffer size -- buffer , get current working directory )
  size_t size = (size_t)TOS;
  TOS = M_POP;
  TOS = (cell_t)getcwd((char *)TOS, size);
}
  endcase;
case ID_SYS_chdir: { // ( path -- , change current working directory )
  ForthStringToC(gScratch, (char *)TOS, sizeof(gScratch));
  TOS = (cell_t)chdir(gScratch);
}
  endcase;
case ID_SYS_chroot: { // ( path -- , change root directory )
  ForthStringToC(gScratch, (char *)TOS, sizeof(gScratch));
  TOS = (cell_t)chroot(gScratch);
}
  endcase;

case ID_SYS_opendir: { // ( $path -- dir , open directory )
  ForthStringToC(gScratch, (char *)TOS, sizeof(gScratch));
  TOS = (cell_t)opendir(gScratch);
}
  endcase;
case ID_SYS_opendirz: { // ( zpath -- dir , open directory )
  TOS = (cell_t)opendir((char *)TOS);
}
  endcase;
case ID_SYS_rewinddir: { // ( dir -- , rewind directory )
  DIR *dp = (DIR *)TOS;
  TOS = M_POP;
  rewinddir(dp);
}
  endcase;
case ID_SYS_readdir: { // ( dir -- dirent , read directory )
  TOS = (cell_t)readdir((DIR *)TOS);
}
  endcase;
case ID_SYS_closedir: { // ( dir -- , close directory )
  TOS = (cell_t)closedir((DIR *)TOS);
}
  endcase;

case ID_SYS_d_ino: { // ( dirent -- type , get ino
  dirent *de = (dirent *)TOS;
  TOS = de->d_ino;
}
  endcase;
case ID_SYS_d_reclen: { // ( dirent -- reclen , get reclen )
  dirent *de = (dirent *)TOS;
  TOS = de->d_reclen;
}
  endcase;
case ID_SYS_d_type: { // ( dirent -- type , get type
  dirent *de = (dirent *)TOS;
  TOS = de->d_type;
}
  endcase;
case ID_SYS_d_namlen: { // ( dirent -- namelen , get namelen )
  dirent *de = (dirent *)TOS;
#ifdef __APPLE__
  TOS = de->d_namlen;
#else
  TOS = strlen(de->d_name);
#endif
}
  endcase;
case ID_SYS_d_name: { // ( dirent -- name , get name )
  dirent *de = (dirent *)TOS;
  TOS = (cell_t)de->d_name;
}
  endcase;
#endif
