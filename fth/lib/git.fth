\
\ nix-forth Git support words
\

anew task-lib/git.fth

require lib/c-strings.fth

private{
    4096 constant PADSIZE
    create git-pad PADSIZE allot

: append-string { str caddr u -- , append string to string }
    caddr u pad place-cstr 
    str pad string::strcat drop
;

}private

: git.command { caddr u | str fp -- output , execute git command  }
    CString.new-empty -> str

    caddr u git-pad place-cstr
    z" r" 1+ git-pad sys::popen -> fp
    fp 0= if
        sys::strerror zcount exit
    then

    begin
        git-pad PADSIZE fp sys::fgets
    while
        str git-pad CString.strcat
    repeat
    fp sys::pclose drop
    str CString.zstring string::strdup
    str CString.destroy
;

: git.root-path { -- caddr u , get path to root of repo }
    s" git rev-parse --show-toplevel" git.command zcount
;

: git.current-branch { -- caddr u , get current git branch }
    s" git rev-parse --abbrev-ref HEAD" git.command zcount
;

: git.rm { caddr u -- status , git rm file }
    s" git rm " pad place-cstr
    pad caddr string::strcat drop
    pad zcount false Screen.execute
;

: git.add { caddr u -- status , git add file }
    s" git add " pad place-cstr
    pad caddr string::strcat drop
    pad zcount false Screen.execute
;

\    git restore --staged file-to-unstage.txt
: git.unstage { caddr u -- status , git unstage file }
    s" git restore --staged " pad place-cstr
    pad caddr string::strcat drop
    pad zcount false Screen.execute
;

: git.commit { caddr u  -- , status }
    s" git commit " pad place-cstr
    pad caddr string::strcat drop
    pad zcount false Screen.execute
;

: git.push { caddr u | br -- , push to current branch }
    s" git push origin " pad place-cstr
    git.current-branch drop -> br
    pad br string::strcat drop
    pad zcount true Screen.execute
;

privatize
