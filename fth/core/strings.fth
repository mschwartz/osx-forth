\ @(#) strings.fth 98/01/26 1.2
\ String support for PForth
\
\ Copyright Phil Burk 1994
\ Modified by Mike Schwartz 2023

ANEW TASK-STRINGS.FTH

: -TRAILING  ( c-addr u1 -- c-addr u2 , strip trailing blanks )
    dup 0>
    IF
        BEGIN
            2dup 1- chars + c@ bl =
            over 0> and
        WHILE
            1-
        REPEAT
    THEN
;

\ Structure of string table
: $ARRAY  (  )
    CREATE  ( #strings #chars_max --  )
        dup ,
        2+ * even-up allot
    DOES>    ( index -- $addr )
        dup @  ( get #chars )
        rot * + cell+
;

\ Compare two strings
: $= ( $1 $2 -- flag , true if equal )
    -1 -rot
    dup c@ 1+ 0
    DO  dup c@ tolower
        2 pick c@ tolower -
        IF rot drop 0 -rot LEAVE
        THEN
        1+ swap 1+ swap
    LOOP 2drop
;

: TEXT=  ( addr1 addr2 count -- flag )
    >r -1 -rot
    r> 0
    ?DO  dup c@ tolower
        2 pick c@ tolower -
        IF rot drop 0 -rot LEAVE
        THEN
        1+ swap 1+ swap
    LOOP 2drop
;

: TEXT=?  ( addr1 count addr2 -- flag , for JForth compatibility )
    swap text=
;

: $MATCH?  ( $string1 $string2 -- flag , case INsensitive )
    dup c@ 1+ text=
;


: INDEX ( $string char -- false | address_char true , search for char in string )
    >r >r 0 r> r>
    over c@ 1+ 1
    DO  over i + c@ over =
        IF  rot drop
            over i + rot rot LEAVE
        THEN
    LOOP 2drop
    ?dup 0= 0=
;


: $APPEND.CHAR  ( $string char -- ) \ ugly stack diagram
    over count chars + c!
    dup c@ 1+ swap c!
;

\ ----------------------------------------------
: ($ROM)  ( index address -- $string )
    ( -- index address )
    swap 0
    ?DO dup c@ 1+ + aligned
    LOOP
;

: $ROM ( packed array of strings, unalterable )
    CREATE ( <name> -- )
    DOES> ( index -- $string )  ($rom)
;

: TEXTROM ( packed array of strings, unalterable )
    CREATE ( <name> -- )
    DOES> ( index -- address count )  ($rom) count
;

\ C / asciiz strings added by Mike Schwartz

: >z pad place pad count over swap chars + 0 swap c! ;

: z" ( "ccc<quote>" -- addr , " zero terminated string )
    state @
    IF 
        compile (c") [char] " parse 
        'word >r
        tuck 'word place 2+ dup >r allot align 
        r> \ length
        1-
        r> \ `word
        +
        0 swap c!
        \ ",
    ELSE
        [char] " parse pad place pad count
        over swap
        chars + 0 swap c! 
    THEN
; immediate

: zcount ( addr -- addr u ) 
    dup string::strlen
;

: ztype ( addr -- ~ ) ?dup if zcount type else ." NULLPTR" then ;

: z+ ( addr addr2 -- addr; adds zstring at addr2 to string at addr1)
    dup zcount +
;

\ Copy the string C-ADDR/U1 to C-ADDR2 and append a NUL.
: PLACE-CSTR  ( c-addr1 u1 c-addr2 -- )
    2dup 2>r          ( c-addr1 u1 c-addr2 )  ( r: u1 c-addr2 )
    swap cmove        ( ) ( r: u1 c-addr2 )
    0 2r> + c!        ( )
;

variable string>upper-address
: string>upper ( caddr u -- , uppercase string at caddr )
    swap string>upper-address !
    0 do
        string>upper-address @ c@ toupper swap c!
        string>upper-address @  1+ string>upper-address !
    loop
;

: number>string ( n -- caddr u )  s>d <# #s #> ;

: .# number>string type ;

\ -----------------------------------------------
