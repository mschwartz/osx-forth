\
\ Phred editor
\
\ programmed by Mike Schwartz
\
\ Phred editor Keymaps and command recording handling
\

anew task-phred/keymaps.fth

require phred/recording.fth 

create CommandLine.text 512 chars allot

require phred/keymaps/mouse.fth

\ ************************************************************
\ ************************************************************
\ ************************************************************

: Keymap.can-start?
    dot-recording @ Recording.can-start?
;

: Keymap.start-recording 
    dot-recording @ Recording.start
;
: Keymap.end-recording 
    dot-recording @ Recording.end
;
: Keymap.start-playback
    dot-recording @ Recording.start-playback
;
: Keymap.end-playback
    dot-recording @ Recording.end-playback
;

: Keymap.reset-recording 
    dot-recording @ Recording.clear
    dot-recording @ Recording.start
;

: keymap.recording? 
    dot-recording @ Recording.recording?
;

: Keymap.dump-Recording
    dot-recording @ Recording.dump
;

: Keymap.set-count { n -- , set count of recording }
    n dot-recording @ s! Recording.count
;

: Keymap.append-char { ch -- , add ch to recording }
    dot-recording @ Recording.recording? if
        dot-recording @ ch Recording.append-char
    then
;

: Keymap.append-string { caddr u -- , append string to recording }
    dot-recording @ Recording.recording? if
        dot-recording @ caddr u  Recording.append-string
    then
;

: Keymap.playing? dot-recording @ Recording.playing? ;

: Keymap.record-command { n cmd -- , set recording to n<cmd> }
    Keymap.playing? not if
        Keymap.reset-recording
        n 1 > if n (.) Keymap.append-string then
        cmd Keymap.append-string
    then
;
\ ************************************************************
\ ************************************************************
\ ************************************************************

variable m-playing 0 m-playing !
variable m-recording 0 m-recording !    \ the macro currently being recorded
variable m-register 0 m-register c!
variable m-count 1 m-count !

: Keymap.record-macro { ch -- , start recording a macro }
    m-playing @ if exit then        \ can't start recording if macro is playing
    m-recording @ if
        die{ ." *** already recording macro " }die
    then
    ch m-register c!    
    Macro.new m-recording !
;

: Keymap.end-macro { | ch -- , end recording of macro }
    m-recording @ not if exit then  \ can't end a macro if one isn't being recorded
    m-register c@ -> ch
    ch m-recording @ Macro.set
    0 m-recording !
;

: Keymap.play-macro { n ch | m -- , start macro playing }
    m-playing @ if exit then    \ can't play macro if playing a macro
    ch Macro.get -> m
    m not if 
        messageline{ ."  *** No macro recorded" }messageline
        exit
    then
    n m-count !
    ch m-register !
    m m-playing ! 
    m Macro.rewind
;

: Keymap.ungetch { ch | r -- , unget Recording.getch }
    dot-recording @ -> r
    r Recording.playing? if
        r Recording.ungetch 
    else
        ch ncurses::ungetch drop
    then
;

: Keymap.key { | r ch -- ch , get key }
    screen-resized @ if
        false screen-resized !
        screen.resize
        commandline.erase
        screen.paint
        messageline.clear
    then
    dot-recording @ -> r
    Keymap.playing? if
        r Recording.getch ?dup if exit then
    then
    m-recording @ if
        0 m-playing !
        ncurses.getch -> ch
        messageline.clear
        ch ascii q <> if
            m-recording @ ch Macro.addch
        then
        ch exit
    then
    m-playing @ if
        m-playing @ Macro.getch  -> ch
        ch 0= if 
            \ end of macro
            m-count @ 1- m-count !
            m-count @ 0> if
                m-playing @ Macro.rewind
                m-playing @ Macro.getch -> ch
            else
                ncurses.getch -> ch 
                messageline.clear
                0 m-playing !
            then
        then
        ch exit
    then
    ncurses.getch
    messageline.clear
;

: Keymap.peek { | ch -- ch , peek at key }
    Keymap.key -> ch
    ch Keymap.ungetch
    ch
;

: Keymap.input# { | ch n -- n , parse number from input }
    Keymap.key -> ch
    ch ctype::isdigit not if
        ch Keymap.ungetch 
        1 exit
    then

    0 -> n
    begin
        ch ascii 0 - n 10 * + -> n
        messageline{ n . }messageline
        Keymap.key -> ch
        ch ctype::isdigit not if
            ch ncurses::ungetch drop
            ncurses::refresh
            n exit
        then
    again
;

require phred/keymaps/window.fth
require phred/keymaps/common.fth
require phred/keymaps/bufed.fth
require phred/keymaps/dired.fth
require phred/keymaps/greped.fth
require phred/keymaps/gited.fth
require phred/keymaps/visual.fth
require phred/keymaps/insert.fth
require phred/keymaps/selection.fth
require phred/keymaps/nerdtree.fth

: Keymap.editor { w -- , run keymap for editor }
    begin
        editor-mode @ case
            editmode.visual of
                w Keymap.visual
                exit
            endof
            editmode.insert of
                w Keymap.insert
                exit
            endof
            editmode.bufed of
                w Keymap.bufed
                exit
            endof
            editmode.dired of
                w Keymap.dired
                exit
            endof
            editmode.worded of
                w Keymap.visual
                exit
            endof
            editmode.helped of
                w Keymap.visual
                exit
            endof
            editmode.cmded of
                w Keymap.visual
                exit
            endof
            editmode.greped of
                w Keymap.greped
                exit
            endof
            editmode.gited of
                w Keymap.gited
                exit
            endof
            editmode.selection of
                w Keymap.selection
                exit
            endof
            editmode.nerdtree of
                w Keymap.nerdtree
                exit
            endof
            editmode.maned of 
                w Keymap.visual
            endof
            \ default
                die{ ." bad editor mode " editor-mode . .s }die
        endcase
    again
;
