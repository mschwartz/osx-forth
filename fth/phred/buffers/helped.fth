\
\ Help Buffer
\

private{
    variable helped-buf
    create help-pad 512 allot

    : build-help { buf caddr u | tf lst lin txt caddr2 u2 instruct printing -- , build help in buf }
        caddr u help-pad place
        help-pad defined-in-file? ?dup if
            -> u2 -> caddr2
            caddr2 u2 help-pad place
            help-pad TextFile.new -> tf
            tf TextFile.revert not if
                s" fth/" help-pad place
                caddr2 u2 help-pad append
                tf help-pad TextFile.set-filename
                tf TextFile.revert not if
                    TAG_END
                        c"    *** No help available because " TAG_$STRING
                    caddr u TAG_STRING 
                    c"  is from " TAG_$STRING
                    pad TAG_$STRING
                    buf Buffer.format
                    tf TextFile.destroy
                    exit
                then
            then
            TAG_END
                tf s@ TextFile.filename zcount TAG_STRING 
                c"  *** defined in " TAG_$STRING
                caddr u TAG_STRING
            buf Buffer.format
            TAG_END c" " TAG_$STRING buf Buffer.format
            tf TextFile.list -> lst
            lst -> lin
            false -> printing
            false -> instruct
            begin
                lin s@ ListNode.next -> lin
                lin lst <>
            while
                lin s@ Line.text -> txt
                printing if 
                    TAG_END
                    txt c@ bl <> if
                            c"     " TAG_$STRING
                    then
                    txt zcount TAG_STRING
                    buf Buffer.format
                else
                    lin ascii : Line.starts-with-char if
                        TAG_END
                            txt zcount TAG_STRING
                            buf Buffer.format
                        false -> printing
                        lin ascii s Line.index-of 1 = if
                            true -> instruct
                            true -> printing
                        then
                        lin ascii S Line.index-of 1 = if
                            true -> instruct
                            true -> printing
                        then
                        lin ascii ( Line.index-of -1 <> if
                            true -> printing
                        then
                        lin ascii { Line.index-of -1 <> if
                            true -> printing
                        then
                    then
                then
                printing if
                    instruct if
                        lin ascii ; Line.starts-with-char if
                            lin ascii false Line.index-of 1 = if
                                false -> instruct
                                false -> printing
                            then
                            lin ascii S Line.index-of 1 = if
                                false -> instruct
                                false -> printing
                            then
                        then
                    else
                        lin ascii ) Line.ends-with-char if
                            false -> printing
                        then 
                        lin ascii } Line.ends-with-char if
                            false -> printing
                        then 
                    then
                then
            repeat
            tf TextFile.destroy
            true
        else
            drop
            false
        then
    ;
}private

: (Helped.begin) { w caddr u | buf -- , initialize and attach help editor }

    s" Help Editor " help-pad place
    caddr u help-pad append
    help-pad count BufferList.find ?dup if
        -> buf
        buf Buffer.clear
    else
        help-pad count Buffer.new -> buf
        buf BufferList.add
    then
    buf helped-buf !
    buf caddr u build-help not if
        messageline{ ."  *** Word not found '" help-pad $type }messageline
        exit
    then
    w Window.line-down
    w Window.line-down
    w buf Window.set-buffer
    true buf s! Buffer.read-only
    true buf s! Buffer.permanent
;

' (HelpEd.begin) is Helped.begin

privatize
