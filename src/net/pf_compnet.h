#ifdef PF_SUPPORT_NET

CreateDicEntryC(ID_NET_SOCKET, "net::socket", 0);
CreateDicEntryC(ID_NET_ACCEPT, "net::accept", 0);
CreateDicEntryC(ID_NET_CLOSE, "net-close", 0);
CreateDicEntryC(ID_NET_BIND, "net::bind", 0);
CreateDicEntryC(ID_NET_CONNECT, "net::connect", 0);
CreateDicEntryC(ID_NET_GETSOCKNAME, "net::getsockname", 0);
CreateDicEntryC(ID_NET_GETSOCKOPT, "net::getsockopt", 0);
CreateDicEntryC(ID_NET_SETSOCKOPT, "net::setsockopt", 0);
CreateDicEntryC(ID_NET_GETCONSTANT, "net::getconstant", 0);
CreateDicEntryC(ID_NET_IOCTL, "net::ioctl", 0);
CreateDicEntryC(ID_NET_LISTEN, "net::listen", 0);
CreateDicEntryC(ID_NET_READ, "net-read", 0);
CreateDicEntryC(ID_NET_RECV, "net::recv", 0);
CreateDicEntryC(ID_NET_RECVFROM, "net::recvfrom", 0);
CreateDicEntryC(ID_NET_ALLOC_FD_SET, "net::alloc-fd_set", 0);
CreateDicEntryC(ID_NET_FREE_FD_SET, "net::free-fd_set", 0);
CreateDicEntryC(ID_NET_FD_CLR, "net::FD_CLR", 0);
#ifdef __APPLE__
CreateDicEntryC(ID_NET_FD_COPY, "net::FD_COPY", 0);
#endif
CreateDicEntryC(ID_NET_FD_ISSET, "net::FD_ISSET", 0);
CreateDicEntryC(ID_NET_FD_SET, "net::FD_SET", 0);
CreateDicEntryC(ID_NET_FD_ZERO, "net::FD_ZERO", 0);
CreateDicEntryC(ID_NET_SELECT, "net::select", 0);
CreateDicEntryC(ID_NET_SEND, "net::send", 0);
CreateDicEntryC(ID_NET_SENDMSG, "net::sendmsg", 0);
CreateDicEntryC(ID_NET_SENDTO, "net::sendto", 0);
CreateDicEntryC(ID_NET_SHUTDOWN, "net::shutdown", 0);
CreateDicEntryC(ID_NET_SOCKETPAIR, "net::socketpair", 0);
CreateDicEntryC(ID_NET_WRITE, "net::write", 0);
CreateDicEntryC(ID_NET_SENDFILE, "net::sendfile", 0);
CreateDicEntryC(ID_NET_GETPROTOENT, "net::getprotoent", 0);
CreateDicEntryC(ID_NET_ALLOC_SOCKADDR, "net::allocsockaddr", 0);
CreateDicEntryC(ID_NET_FREE_SOCKADDR, "net::freesockaddr", 0);

#ifdef __APPLE__
CreateDicEntryC(ID_NET_HTONLL, "net::htonll", 0);
#endif
CreateDicEntryC(ID_NET_HTONL, "net::htonl", 0);
CreateDicEntryC(ID_NET_HTONS, "net::htons", 0);
#ifdef __APPLE__
CreateDicEntryC(ID_NET_NTOHLL, "net::ntohll", 0);
#endif
CreateDicEntryC(ID_NET_HTONL, "net::htonl", 0);
CreateDicEntryC(ID_NET_NTOHL, "net::ntohl", 0);
CreateDicEntryC(ID_NET_NTOHS, "net::ntohs", 0);

CreateDicEntryC(ID_NET_GETHOSTBYNAME, "net::gethostbyname", 0);
CreateDicEntryC(ID_NET_LOOKUPHOSTNAME, "net::lookuphostname", 0);
CreateDicEntryC(ID_NET_GETHOSTBYADDR, "net::gethostbyaddr", 0);

#endif
