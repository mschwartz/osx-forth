anew task-net/netdb.fth

trace-include @ [if]
cr ." Compiling net/netdb.fth"
[then]

: .ip-address ( address -- , prints cell IP address in xxx.xxx.xxx.xxx form )
    dup $ ff and .#
    ." ."
    dup 8 rshift $ ff and .#
    ." ."
    dup 16 rshift $ ff and .#
    ." ."
    24 rshift $ ff and .#
;

:STRUCT HOSTENT
    APTR HOSTENT.h_name
    APTR HOSTENT.h_aliases
    INT  HOSTENT.h_addrtype
    INT  HOSTENT.h_length
    APTR HOSTENT.h_addr_list
;STRUCT

: NET.lookuphostname ( caddr -- ip , get ip address from hostname )
    net::lookuphostname
;

: NET.gethostbyname ( caddr -- ip , get ip address [4 bytes] of host by name )
\    cr ." lookup " dup count type
    net::gethostbyname
    cr dup s@ HOSTENT.h_name ztype
    s@ HOSTENT.h_addr_list  \ addresses[]
    cr dup c@ . dup 1+ c@ . dup 2 + c@ . dup 3 + c@ .
    @  
    cr dup 24 rshift  $ ff and .
    dup 16 rshift  $ ff and .
    dup 8 rshift  $ ff and .
    dup 0 rshift $ ff and .
    dup s@ HOSTENT.h_length cr ." h_length = " .
;

trace-include @ [if]
cr ." End net/netdb.fth"
[then]