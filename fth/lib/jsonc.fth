anew task-lib/jsonc.fth

JSON::type_null constant json_type_null
JSON::type_boolean constant json_type_boolean
JSON::type_double constant json_type_double
JSON::type_int constant json_type_int
JSON::type_object constant json_type_object
JSON::type_array constant json_type_array
JSON::type_string constant json_type_string

: JSON.type { obj -- get type of json object }
    obj JSON::get_type
;

: JSON.parse { str -- o , parse JSON string and return as JSON object }
    str JSON::parse 
;

: JSON.parse-file { $filename -- o , parse JSON file and return as JSON object }
    $filename count pad place-cstr
    pad JSON::parse_file
;

: JSON.stringify { o -- str , convert object to string }
    o JSON::stringify
;

: JSON.pretty-print { o -- str , convert object to string }
    o JSON::stringify_pretty
;

: JSON.free { o -- , release json_object }
    o JSON::free
;

: JSON.iterate { obj 'callback -- , iterate over object calling 'callback with key/value }
    obj 'callback JSON::iterate
;

: JSON.get_object { obj -- obj , get object from json_obj }
    obj JSON::get_object
;

: JSON.get_object_ex { obj $key -- obj , get object from json_obj }
    $key count pad place-cstr
    obj pad JSON::get_object_ex
;

: JSON.get_array { obj -- obj , get array from json_obj }
    obj JSON::get_array
;

: JSON.array.length { obj -- , get length of array }
    obj JSON::array_length
;

: JSON.get_string { obj -- obj , get string from json_obj }
    obj JSON::get_string
;

: JSON.string.length { obj -- , get length of string }
    obj JSON::string_len
;

: JSON.get_int64 { obj -- obj , get int64 from json_obj }
    obj JSON::get_int64
;

: JSON.get_boolean { obj -- obj , get boolean from json_obj }
    obj JSON::get_int64
;

: JSON.get_double { obj -- , get double onto fp stack from json_obj }
    obj JSON::get_int64
;

: JSON.object.new (  -- o , new JSON object to be maniuplated )
    JSON::new_object 
;

: JSON.array.new (  -- o , new JSON array to be maniulated )
    JSON::new_array 
;

: JSON.string.new {  $str -- o , new JSON string to be maniulated }
    $str count pad place-cstr
    pad JSON::new_string
;

: JSON.int64.new {  val -- o , new JSON int64 to be maniulated }
    val JSON::new_int64
;

: JSON.boolean.new {  val -- o , new JSON boolean to be maniulated }
    val JSON::new_boolean
;

: JSON.double.new (  -- o , new JSON double to be maniulated val is on fp stack )
    JSON::new_double
;

: JSON.object.add { obj $key val -- , err }
    $key count pad place-cstr
    obj pad val JSON::object_add
;

: JSON.array.push { obj val -- , push val in array/obj }
    obj val JSON::array_add
    false = if
        cr ." *** failed to array.push " obj .hex val .hex
        bye
    then
;

: JSON.array.set { obj idx val -- , set idx in array/obj to val }
    obj idx val JSON::array_put_idx 
    false = if
        cr ." *** failed to array.set " obj .hex idx . val .hex
        bye
    then
;
