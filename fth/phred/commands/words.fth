\
\ phred editor
\
\ programmed by Mike Schwartz
\
\ Command Line handling
\   :words command
\

anew task-phred/commands/words.fth

private{
create words-pad 512 allot
}private
: CommandLine.words { w str tok toku -- flg , return true if executed }
    tok toku s" words"  compare 0= if
        w nullptr Worded.begin
        w Window.worded-mode
        true exit
    then
    tok toku s" words.like" compare 0= if
        str CString.skip-blanks
        w str CString.rest words-pad place-cstr
        words-pad Worded.begin
        w Window.worded-mode
        true exit
    then
    false
;

privatize
