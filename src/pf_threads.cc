#include "pf_all.h"
#include "pf_threads.h"
// #include "pf_float.h"
// #include "pf_guts.h"
// #include "pf_io.h"

static void thread_cleanup(void *arg) {
  struct ThreadDetail *detail = (ThreadDetail *)arg;
  delete detail;
  return;
}

void thread_join(void *arg) {
  //
}

typedef void (*funcPtr)(void);

pthread_t thread_start(cell_t func, void *arg) {
  struct ThreadDetail *detail = new ThreadDetail();
  detail->success = pthread_create(&detail->thread, NULL, (void *(*)(void *))func, arg);
  pthread_cleanup_push(thread_cleanup, (void *)detail);

  detail->taskData = (pfTaskData_t *)pfCreateTask(DEFAULT_USER_DEPTH, DEFAULT_RETURN_DEPTH);
  funcPtr f = (funcPtr)func;
  f();
#ifdef __APPLE__
  pthread_cleanup_pop(thread_cleanup);
#else
  pthread_cleanup_pop(true);
#endif
  return detail->thread;
}
