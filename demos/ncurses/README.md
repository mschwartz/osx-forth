# ncurses programming howto

This directory contains demo programs based upon the examples at the ncurses programming howto web pages.

The files are named example1.fth, example2.fth ..., where the number is the
example number on those web pages.

Obviously, those examples had to be converted to Forth.

The C code has sys::ncurses-method and sys::ncurses-variable exposed.  The ncurses.fth 
file defines an API that can be used to simplify using ncurses.

## Reference

* https://tldp.org/HOWTO/NCURSES-Programming-HOWTO