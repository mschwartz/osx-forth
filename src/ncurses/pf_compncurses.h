#ifdef PF_SUPPORT_NCURSES

CreateDicEntryC(ID_NCURSES_VERSION, "ncurses::VERSION", 0);
CreateDicEntryC(ID_NCURSES_COLOR_PAIRS, "ncurses::COLOR_PAIRS", 0);
CreateDicEntryC(ID_NCURSES_COLORS, "ncurses::COLORS", 0);
CreateDicEntryC(ID_NCURSES_COLS, "ncurses::COLS", 0);
CreateDicEntryC(ID_NCURSES_ESCDELAY, "ncurses::ESCDELAY", 0);
CreateDicEntryC(ID_NCURSES_LINES, "ncurses::LINES", 0);
CreateDicEntryC(ID_NCURSES_TABSIZE, "ncurses::TABSIZE", 0);
CreateDicEntryC(ID_NCURSES_curscr, "ncurses::curscr", 0);
CreateDicEntryC(ID_NCURSES_newscr, "ncurses::newscr", 0);
CreateDicEntryC(ID_NCURSES_stdscr, "ncurses::stdscr", 0);
CreateDicEntryC(ID_NCURSES_set_escdelay, "ncurses::set_escdelay", 0);
CreateDicEntryC(ID_NCURSES_set_tabsize, "ncurses::set_tabsize", 0);

CreateDicEntryC(ID_NCURSES_initscr, "ncurses::initscr", 0);
CreateDicEntryC(ID_NCURSES_endwin, "ncurses::endwin", 0);
CreateDicEntryC(ID_NCURSES_focus_mode, "ncurses::focus_mode", 0);
CreateDicEntryC(ID_NCURSES_define_key, "ncurses::define_key", 0);
CreateDicEntryC(ID_NCURSES_resize_term, "ncurses::resize_term", 0);
CreateDicEntryC(ID_NCURSES_flushinp, "ncurses::flushinp", 0);
CreateDicEntryC(ID_NCURSES_newwin, "ncurses::newwin", 0);
CreateDicEntryC(ID_NCURSES_delwin, "ncurses::delwin", 0);
CreateDicEntryC(ID_NCURSES_mvwin, "ncurses::mvwin", 0);
CreateDicEntryC(ID_NCURSES_subwin, "ncurses::subwin", 0);
CreateDicEntryC(ID_NCURSES_derwin, "ncurses::derwin", 0);
CreateDicEntryC(ID_NCURSES_mvderwin, "ncurses::mvderwin", 0);
CreateDicEntryC(ID_NCURSES_dupwin, "ncurses::dupwin", 0);
CreateDicEntryC(ID_NCURSES_wsyncup, "ncurses::wsyncup", 0);
CreateDicEntryC(ID_NCURSES_syncok, "ncurses::syncok", 0);
CreateDicEntryC(ID_NCURSES_wcursyncup, "ncurses::wcursyncup", 0);
CreateDicEntryC(ID_NCURSES_wsyncdown, "ncurses::wsyncdown", 0);
CreateDicEntryC(ID_NCURSES_doupdate, "ncurses::doupdate", 0);
CreateDicEntryC(ID_NCURSES_redrawwin, "ncurses::redrawwin", 0);
CreateDicEntryC(ID_NCURSES_wredrawln, "ncurses::wredrawln", 0);
CreateDicEntryC(ID_NCURSES_refresh, "ncurses::refresh", 0);
CreateDicEntryC(ID_NCURSES_wnoutrefresh, "ncurses::wnoutrefresh", 0);
CreateDicEntryC(ID_NCURSES_wrefresh, "ncurses::wrefresh", 0);
CreateDicEntryC(ID_NCURSES_erase, "ncurses::erase", 0);
CreateDicEntryC(ID_NCURSES_werase, "ncurses::werase", 0);
CreateDicEntryC(ID_NCURSES_clear, "ncurses::clear", 0);
CreateDicEntryC(ID_NCURSES_wclear, "ncurses::wclear", 0);
CreateDicEntryC(ID_NCURSES_clrtobot, "ncurses::clrtobot", 0);
CreateDicEntryC(ID_NCURSES_wclrtobot, "ncurses::wclrtobot", 0);
CreateDicEntryC(ID_NCURSES_clrtoeol, "ncurses::clrtoeol", 0);
CreateDicEntryC(ID_NCURSES_wclrtoeol, "ncurses::wclrtoeol", 0);
CreateDicEntryC(ID_NCURSES_winsertln, "ncurses::winsertln", 0);
CreateDicEntryC(ID_NCURSES_wdeleteln, "ncurses::wdeleteln", 0);
CreateDicEntryC(ID_NCURSES_move, "ncurses::move", 0);
CreateDicEntryC(ID_NCURSES_wmove, "ncurses::wmove", 0);

CreateDicEntryC(ID_NCURSES_addch, "ncurses::addch", 0);
CreateDicEntryC(ID_NCURSES_mvaddch, "ncurses::mvaddch", 0);
CreateDicEntryC(ID_NCURSES_waddch, "ncurses::waddch", 0);
CreateDicEntryC(ID_NCURSES_addstr, "ncurses::addstr", 0);
CreateDicEntryC(ID_NCURSES_mvaddstr, "ncurses::mvaddstr", 0);
CreateDicEntryC(ID_NCURSES_waddstr, "ncurses::waddstr", 0);
CreateDicEntryC(ID_NCURSES_waddnstr, "ncurses::waddnstr", 0);

CreateDicEntryC(ID_NCURSES_addchstr, "ncurses::addchstr", 0);
CreateDicEntryC(ID_NCURSES_addchnstr, "ncurses::addchnstr", 0);
CreateDicEntryC(ID_NCURSES_waddchstr, "ncurses::waddchstr", 0);
CreateDicEntryC(ID_NCURSES_waddchnstr, "ncurses::waddchnstr", 0);
CreateDicEntryC(ID_NCURSES_mvaddchstr, "ncurses::mvaddchstr", 0);
CreateDicEntryC(ID_NCURSES_mvaddchnstr, "ncurses::mvaddchnstr", 0);
CreateDicEntryC(ID_NCURSES_mvwaddchstr, "ncurses::mvwaddchstr", 0);
CreateDicEntryC(ID_NCURSES_mvwaddchnstr, "ncurses::mvwaddchnstr", 0);

CreateDicEntryC(ID_NCURSES_getattrs, "ncurses::getattrs", 0);
CreateDicEntryC(ID_NCURSES_getbegx, "ncurses::getbegx", 0);
CreateDicEntryC(ID_NCURSES_getbegy, "ncurses::getbegy", 0);
CreateDicEntryC(ID_NCURSES_getcurx, "ncurses::getcurx", 0);
CreateDicEntryC(ID_NCURSES_getcury, "ncurses::getcury", 0);
CreateDicEntryC(ID_NCURSES_getmaxx, "ncurses::getmaxx", 0);
CreateDicEntryC(ID_NCURSES_getmaxy, "ncurses::getmaxy", 0);
CreateDicEntryC(ID_NCURSES_getparx, "ncurses::getparx", 0);
CreateDicEntryC(ID_NCURSES_getpary, "ncurses::getpary", 0);
CreateDicEntryC(ID_NCURSES_getch, "ncurses::getch", 0);
CreateDicEntryC(ID_NCURSES_inch, "ncurses::inch", 0);
CreateDicEntryC(ID_NCURSES_winch, "ncurses::winch", 0);
CreateDicEntryC(ID_NCURSES_mvinch, "ncurses::mvinch", 0);
CreateDicEntryC(ID_NCURSES_mvwinch, "ncurses::mvwinch", 0);
CreateDicEntryC(ID_NCURSES_inchstr, "ncurses::inchstr", 0);
CreateDicEntryC(ID_NCURSES_inchnstr, "ncurses::inchnstr", 0);
CreateDicEntryC(ID_NCURSES_getnstr, "ncurses::getnstr", 0);
CreateDicEntryC(ID_NCURSES_getstr, "ncurses::getstr", 0);
CreateDicEntryC(ID_NCURSES_getmaxyx, "ncurses::getmaxyx", 0);
CreateDicEntryC(ID_NCURSES_getyx, "ncurses::getyx", 0);
CreateDicEntryC(ID_NCURSES_wgetch, "ncurses::wgetch", 0);
CreateDicEntryC(ID_NCURSES_mvwgetch, "ncurses::mvwgetch", 0);
CreateDicEntryC(ID_NCURSES_mvgetch, "ncurses::mvgetch", 0);
CreateDicEntryC(ID_NCURSES_mvgetnstr, "ncurses::mvgetnstr", 0);
CreateDicEntryC(ID_NCURSES_mvgetstr, "ncurses::mvgetstr", 0);
// CreateDicEntryC(ID_NCURSES_mvgetyx, "ncurses::mvgetyx", 0);
CreateDicEntryC(ID_NCURSES_mvwgetch, "ncurses::mvwgetch", 0);
CreateDicEntryC(ID_NCURSES_ungetch, "ncurses::ungetch", 0);
CreateDicEntryC(ID_NCURSES_has_key, "ncurses::has_key", 0);

CreateDicEntryC(ID_NCURSES_printw, "ncurses::printw", 0);
CreateDicEntryC(ID_NCURSES_wprintw, "ncurses::wprintw", 0);
CreateDicEntryC(ID_NCURSES_mvprintw, "ncurses::mvprintw", 0);
CreateDicEntryC(ID_NCURSES_mvwprintw, "ncurses::mvwprintw", 0);

CreateDicEntryC(ID_NCURSES_cbreak, "ncurses::cbreak", 0);
CreateDicEntryC(ID_NCURSES_nocbreak, "ncurses::nocbreak", 0);
CreateDicEntryC(ID_NCURSES_echo, "ncurses::echo", 0);
CreateDicEntryC(ID_NCURSES_noecho, "ncurses::noecho", 0);
CreateDicEntryC(ID_NCURSES_halfdelay, "ncurses::halfdelay", 0);
CreateDicEntryC(ID_NCURSES_intrflush, "ncurses::intrflush", 0);
CreateDicEntryC(ID_NCURSES_keypad, "ncurses::keypad", 0);
CreateDicEntryC(ID_NCURSES_meta, "ncurses::meta", 0);
CreateDicEntryC(ID_NCURSES_nodelay, "ncurses::nodelay", 0);
CreateDicEntryC(ID_NCURSES_raw, "ncurses::raw", 0);
CreateDicEntryC(ID_NCURSES_noraw, "ncurses::noraw", 0);
CreateDicEntryC(ID_NCURSES_noqiflush, "ncurses::noqiflush", 0);
CreateDicEntryC(ID_NCURSES_qiflush, "ncurses::qiflush", 0);
CreateDicEntryC(ID_NCURSES_notimeout, "ncurses::notimeout", 0);
CreateDicEntryC(ID_NCURSES_timeout, "ncurses::timeout", 0);
CreateDicEntryC(ID_NCURSES_wtimeout, "ncurses::wtimeout", 0);
CreateDicEntryC(ID_NCURSES_typeahead, "ncurses::typeahead", 0);

CreateDicEntryC(ID_NCURSES_clearok, "ncurses::clearok", 0);
CreateDicEntryC(ID_NCURSES_idlok, "ncurses::idlok", 0);
CreateDicEntryC(ID_NCURSES_idcok, "ncurses::idcok", 0);
CreateDicEntryC(ID_NCURSES_immedok, "ncurses::immedok", 0);
CreateDicEntryC(ID_NCURSES_leaveok, "ncurses::leaveok", 0);
CreateDicEntryC(ID_NCURSES_setscrreg, "ncurses::setscrreg", 0);
CreateDicEntryC(ID_NCURSES_wsetscrreg, "ncurses::wsetscrreg", 0);
CreateDicEntryC(ID_NCURSES_scrollok, "ncurses::scrollok", 0);
CreateDicEntryC(ID_NCURSES_nl, "ncurses::nl", 0);
CreateDicEntryC(ID_NCURSES_nonl, "ncurses::nonl", 0);

CreateDicEntryC(ID_NCURSES_A_NORMAL, "ncurses::A_NORMAL", 0);
CreateDicEntryC(ID_NCURSES_A_ATTRIBUTES, "ncurses::A_ATTRIBUTES", 0);
CreateDicEntryC(ID_NCURSES_A_COLOR, "ncurses::A_COLOR", 0);
CreateDicEntryC(ID_NCURSES_A_STANDOUT, "ncurses::A_STANDOUT", 0);
CreateDicEntryC(ID_NCURSES_A_UNDERLINE, "ncurses::A_UNDERLINE", 0);
CreateDicEntryC(ID_NCURSES_A_REVERSE, "ncurses::A_REVERSE", 0);
CreateDicEntryC(ID_NCURSES_A_BLINK, "ncurses::A_BLINK", 0);
CreateDicEntryC(ID_NCURSES_A_DIM, "ncurses::A_DIM", 0);
CreateDicEntryC(ID_NCURSES_A_BOLD, "ncurses::A_BOLD", 0);
CreateDicEntryC(ID_NCURSES_A_ALTCHARSET, "ncurses::A_ALTCHARSET", 0);
CreateDicEntryC(ID_NCURSES_A_INVIS, "ncurses::A_INVIS", 0);
CreateDicEntryC(ID_NCURSES_A_PROTECT, "ncurses::A_PROTECT", 0);
CreateDicEntryC(ID_NCURSES_A_HORIZONTAL, "ncurses::A_HORIZONTAL", 0);
CreateDicEntryC(ID_NCURSES_A_LEFT, "ncurses::A_LEFT", 0);
CreateDicEntryC(ID_NCURSES_A_LOW, "ncurses::A_LOW", 0);
CreateDicEntryC(ID_NCURSES_A_RIGHT, "ncurses::A_RIGHT", 0);
CreateDicEntryC(ID_NCURSES_A_TOP, "ncurses::A_TOP", 0);
CreateDicEntryC(ID_NCURSES_A_VERTICAL, "ncurses::A_VERTICAL", 0);
CreateDicEntryC(ID_NCURSES_A_ITALIC, "ncurses::A_ITALIC", 0);

CreateDicEntryC(ID_NCURSES_attroff, "ncurses::attroff", 0);
CreateDicEntryC(ID_NCURSES_wattroff, "ncurses::wattroff", 0);
CreateDicEntryC(ID_NCURSES_attron, "ncurses::attron", 0);
CreateDicEntryC(ID_NCURSES_wattron, "ncurses::wattron", 0);
CreateDicEntryC(ID_NCURSES_attrset, "ncurses::attrset", 0);
CreateDicEntryC(ID_NCURSES_wattrset, "ncurses::wattrset", 0);
CreateDicEntryC(ID_NCURSES_color_set, "ncurses::color_set", 0);
CreateDicEntryC(ID_NCURSES_wcolor_set, "ncurses::wcolor_set", 0);
CreateDicEntryC(ID_NCURSES_standend, "ncurses::standend", 0);
CreateDicEntryC(ID_NCURSES_wstandend, "ncurses::wstandend", 0);
CreateDicEntryC(ID_NCURSES_standout, "ncurses::standout", 0);
CreateDicEntryC(ID_NCURSES_wstandout, "ncurses::wstandout", 0);
CreateDicEntryC(ID_NCURSES_attr_get, "ncurses::attr_get", 0);
CreateDicEntryC(ID_NCURSES_wattr_get, "ncurses::wattr_get", 0);
CreateDicEntryC(ID_NCURSES_attr_off, "ncurses::attr_off", 0);
CreateDicEntryC(ID_NCURSES_wattr_off, "ncurses::wattr_off", 0);
CreateDicEntryC(ID_NCURSES_attr_on, "ncurses::attr_on", 0);
CreateDicEntryC(ID_NCURSES_wattr_on, "ncurses::wattr_on", 0);
CreateDicEntryC(ID_NCURSES_attr_set, "ncurses::attr_set", 0);
CreateDicEntryC(ID_NCURSES_wattr_set, "ncurses::wattr_set", 0);
CreateDicEntryC(ID_NCURSES_chgat, "ncurses::chgat", 0);
CreateDicEntryC(ID_NCURSES_wchgat, "ncurses::wchgat", 0);
CreateDicEntryC(ID_NCURSES_mvchgat, "ncurses::mvchgat", 0);
CreateDicEntryC(ID_NCURSES_mvwchgat, "ncurses::mvwchgat", 0);

CreateDicEntryC(ID_NCURSES_start_color, "ncurses::start_color", 0);
CreateDicEntryC(ID_NCURSES_init_pair, "ncurses::init_pair", 0);
CreateDicEntryC(ID_NCURSES_init_color, "ncurses::init_color", 0);
CreateDicEntryC(ID_NCURSES_has_colors, "ncurses::has_colors", 0);
CreateDicEntryC(ID_NCURSES_can_change_color, "ncurses::can_change_color", 0);
CreateDicEntryC(ID_NCURSES_color_content, "ncurses::color_content", 0);
CreateDicEntryC(ID_NCURSES_pair_content, "ncurses::pair_content", 0);

CreateDicEntryC(ID_NCURSES_border, "ncurses::border", 0);
CreateDicEntryC(ID_NCURSES_wborder, "ncurses::wborder", 0);
CreateDicEntryC(ID_NCURSES_box, "ncurses::box", 0);
CreateDicEntryC(ID_NCURSES_hline, "ncurses::hline", 0);
CreateDicEntryC(ID_NCURSES_whline, "ncurses::whline", 0);
CreateDicEntryC(ID_NCURSES_vline, "ncurses::vline", 0);
CreateDicEntryC(ID_NCURSES_mvhline, "ncurses::mvhline", 0);
CreateDicEntryC(ID_NCURSES_mvwhline, "ncurses::mvwhline", 0);
CreateDicEntryC(ID_NCURSES_mvvline, "ncurses::mvvline", 0);
CreateDicEntryC(ID_NCURSES_mvwvline, "ncurses::mvwvline", 0);

CreateDicEntryC(ID_NCURSES_curs_set, "ncurses::curs_set", 0);
CreateDicEntryC(ID_NCURSES_newline, "ncurses::newline", 0);

CreateDicEntryC(ID_NCURSES_bkgdset, "ncurses::bkgdset", 0);
CreateDicEntryC(ID_NCURSES_wbkgdset, "ncurses::wbkgdset", 0);
CreateDicEntryC(ID_NCURSES_bkgd, "ncurses::bkgd", 0);
CreateDicEntryC(ID_NCURSES_wbkgd, "ncurses::wbkgd", 0);
CreateDicEntryC(ID_NCURSES_getbkgd, "ncurses::getbkgd", 0);

CreateDicEntryC(ID_NCURSES_beep, "ncurses::beep", 0);
CreateDicEntryC(ID_NCURSES_flash, "ncurses::flash", 0);
CreateDicEntryC(ID_NCURSES_cursor_color, "ncurses::cursor-color", 0);

CreateDicEntryC(ID_NCURSES_BUTTON1_PRESSED, "ncurses::BUTTON1_PRESSED", 0);
CreateDicEntryC(ID_NCURSES_BUTTON1_RELEASED, "ncurses::BUTTON1_RELEASED", 0);
CreateDicEntryC(ID_NCURSES_BUTTON1_CLICKED, "ncurses::BUTTON1_CLICKED", 0);
CreateDicEntryC(ID_NCURSES_BUTTON1_DOUBLE_CLICKED, "ncurses::BUTTON1_DOUBLE_CLICKED", 0);
CreateDicEntryC(ID_NCURSES_BUTTON1_TRIPLE_CLICKED, "ncurses::BUTTON1_TRIPLE_CLICKED", 0);

CreateDicEntryC(ID_NCURSES_BUTTON2_PRESSED, "ncurses::BUTTON2_PRESSED", 0);
CreateDicEntryC(ID_NCURSES_BUTTON2_RELEASED, "ncurses::BUTTON2_RELEASED", 0);
CreateDicEntryC(ID_NCURSES_BUTTON2_CLICKED, "ncurses::BUTTON2_CLICKED", 0);
CreateDicEntryC(ID_NCURSES_BUTTON2_DOUBLE_CLICKED, "ncurses::BUTTON2_DOUBLE_CLICKED", 0);
CreateDicEntryC(ID_NCURSES_BUTTON2_TRIPLE_CLICKED, "ncurses::BUTTON2_TRIPLE_CLICKED", 0);

CreateDicEntryC(ID_NCURSES_BUTTON3_PRESSED, "ncurses::BUTTON3_PRESSED", 0);
CreateDicEntryC(ID_NCURSES_BUTTON3_RELEASED, "ncurses::BUTTON3_RELEASED", 0);
CreateDicEntryC(ID_NCURSES_BUTTON3_CLICKED, "ncurses::BUTTON3_CLICKED", 0);
CreateDicEntryC(ID_NCURSES_BUTTON3_DOUBLE_CLICKED, "ncurses::BUTTON3_DOUBLE_CLICKED", 0);
CreateDicEntryC(ID_NCURSES_BUTTON3_TRIPLE_CLICKED, "ncurses::BUTTON3_TRIPLE_CLICKED", 0);

CreateDicEntryC(ID_NCURSES_BUTTON4_PRESSED, "ncurses::BUTTON4_PRESSED", 0);
CreateDicEntryC(ID_NCURSES_BUTTON4_RELEASED, "ncurses::BUTTON4_RELEASED", 0);
CreateDicEntryC(ID_NCURSES_BUTTON4_CLICKED, "ncurses::BUTTON4_CLICKED", 0);
CreateDicEntryC(ID_NCURSES_BUTTON4_DOUBLE_CLICKED, "ncurses::BUTTON4_DOUBLE_CLICKED", 0);
CreateDicEntryC(ID_NCURSES_BUTTON4_TRIPLE_CLICKED, "ncurses::BUTTON4_TRIPLE_CLICKED", 0);

CreateDicEntryC(ID_NCURSES_BUTTON5_PRESSED, "ncurses::BUTTON5_PRESSED", 0);
CreateDicEntryC(ID_NCURSES_BUTTON5_RELEASED, "ncurses::BUTTON5_RELEASED", 0);
CreateDicEntryC(ID_NCURSES_BUTTON5_CLICKED, "ncurses::BUTTON5_CLICKED", 0);
CreateDicEntryC(ID_NCURSES_BUTTON5_DOUBLE_CLICKED, "ncurses::BUTTON5_DOUBLE_CLICKED", 0);
CreateDicEntryC(ID_NCURSES_BUTTON5_TRIPLE_CLICKED, "ncurses::BUTTON5_TRIPLE_CLICKED", 0);

CreateDicEntryC(ID_NCURSES_BUTTON_CTRL, "ncurses::BUTTON_CTRL", 0);
CreateDicEntryC(ID_NCURSES_BUTTON_SHIFT, "ncurses::BUTTON_SHIFT", 0);
CreateDicEntryC(ID_NCURSES_BUTTON_ALT, "ncurses::BUTTON_ALT", 0);
CreateDicEntryC(ID_NCURSES_REPORT_MOUSE_POSITION, "ncurses::REPORT_MOUSE_POSITION", 0);
CreateDicEntryC(ID_NCURSES_ALL_MOUSE_EVENTS, "ncurses::ALL_MOUSE_EVENTS", 0);

CreateDicEntryC(ID_NCURSES_has_mouse, "ncurses::has_mouse", 0);
CreateDicEntryC(ID_NCURSES_getmouse, "ncurses::getmouse", 0);
CreateDicEntryC(ID_NCURSES_ungetmouse, "ncurses::ungetmouse", 0);
CreateDicEntryC(ID_NCURSES_mousemask, "ncurses::mousemask", 0);
CreateDicEntryC(ID_NCURSES_mouseon, "ncurses::mouseon", 0);
CreateDicEntryC(ID_NCURSES_mouseoff, "ncurses::mouseoff", 0);
CreateDicEntryC(ID_NCURSES_wenclose, "ncurses::wenclose", 0);
CreateDicEntryC(ID_NCURSES_wmouse_trafo, "ncurses::wmouse_trafo", 0);
CreateDicEntryC(ID_NCURSES_mouse_trafo, "ncurses::mouse_trafo", 0);
CreateDicEntryC(ID_NCURSES_mouseinterval, "ncurses::mouseinterval", 0);

// wide characters

CreateDicEntryC(ID_NCURSES_mvinvert_wch, "ncurses::mvinvert_wch", 0);
CreateDicEntryC(ID_NCURSES_add_wch, "ncurses::add_wch", 0);
CreateDicEntryC(ID_NCURSES_wadd_wch, "ncurses::wadd_wch", 0);
CreateDicEntryC(ID_NCURSES_mvadd_wch, "ncurses::mvadd_wch", 0);
CreateDicEntryC(ID_NCURSES_mvwadd_wch, "ncurses::mvwadd_wch", 0);
CreateDicEntryC(ID_NCURSES_echo_wchar, "ncurses::echo_wchar", 0);
CreateDicEntryC(ID_NCURSES_wecho_wchar, "ncurses::wecho_wchar", 0);
CreateDicEntryC(ID_NCURSES_add_unicode, "ncurses::add_unicode", 0);

// macros
CreateDicEntryC(ID_NCURSES_COLOR_PAIR, "ncurses::COLOR_PAIR", 0);

#endif
