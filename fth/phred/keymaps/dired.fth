: keymap.leader.dired  { w -- f , handle leader commands }
    keymap.key case
        ascii b of
            keymap.key case
                ascii e of
                    w Bufed.begin
                endof
                ncurses.bell
            endcase
        endof
        ncurses.bell
    endcase
;

: Keymap.dired { w | n ch l b -- , run editor keymap for window }
    Keymap.input# -> n
    begin
        w Window.paint
        w Window.refresh
        Keymap.key -> ch
        Window.end-search

        begin
            ch ncurses.key_mouse = 
        while
            Keymap.mouse if exit then \ because a different window may have been clicked on (activated)
            Keymap.key -> ch
        repeat

        MessageLine.clear
        w Window.address-cursor

        w ch n keymap.common if
            exit
        then
        ch case
            ansi_escape of \ escape key
                ncurses.beep
                drop exit
            endof
            $ 0a of
                \ switch to selected buffer
                w s@ Window.current-line  -> l
                l s@ Line.data -> b
                b if
                    b s@ DirNode.type case
                        DT_DIR of 
                            \ change / open directory
                            b s@ DirNode.fullpath Path.realpathz zcount 1+ current-directory swap cmove
                            w Dired.begin
                            w Window.dired-mode
                            w Window.paint
                        endof
                        DT_REG of
                            w b s@ DirNode.fullpath zcount WindowList.find-or-create drop
                        endof
                        DT_LNK of
                            ncurses.bell
                        endof
                        \ invalid type
                        ncurses.bell
                    endcase
                then
                exit
            endof

            ascii , of
                w Keymap.leader.dired
                exit
            endof

            \ default
            MessageLine.move-to w hex . decimal
        endcase
        w Window.refresh
    again
;
