anew task-c/fcntl.fth

trace-include @ [if]
cr ." Compiling sys/fcntl.fth... "
[then]

$ 00000000 constant O_RDONLY            \ open for reading only 
$ 00000001 constant O_WRONLY            \ open for writing only 
$ 00000002 constant RDWR                \ open for reading and writing
$ 00000003 constant O_ACCMODE           \ mask for above modes

$ 00000004 constant O_NONBLOCK          \ no delay
$ 00000008 constant O_APPEND            \ set append mode
$ 00000010 constant O_SHLOCK            \ open with shared file lock
$ 00000020 constant O_EXLOCK            \ open with exclusive file lock
$ 00000040 constant O_ASYNC             \ signal pgrp when data ready
$ 00000100 constant O_NOFOLOOW          \ don't follow symlinks
$ 00000200 constant O_CREAT             \ create if nonexistant
$ 00000400 constant O_TRUNC             \ truncate to zero length
$ 00000800 constant O_EXCL              \ error if already exists

$ 00020000 constant O_NOCTTY            \ don't assign controlling terminal
$ 00400000 constant O_DSYNC             \ synch I/O data integrity
$ 00100000 constant O_DIRECTORY
$ 00200000 constant O_SYMLINK           \ allow open of a symlink
$ 01000000 constant O_CLOECXEC          \ implicitly set FD_CLOEXEC
$ 20000000 constant O_NOFOLLOW_ANY      \ no symlinks allowed in path
$ 40000000 constant O_EXEC              \ open file for execute only
O_EXEC O_DIRECTORY OR constant O_SEARCH \ open directory for search only

\ flags used for copyfile(2)
$ 0001 constant CPF_OVERWRITE
$ 0002 constant CPF_IGNORE_MODE
CPF_OVERWRITE CPF_IGNORE_MODE OR constant CPF_MASK

\ fcntl(2) constants
  0 constant F_DUPFD                    \ duplicate file descriptor
  1 constant F_GETFD                    \ get file descriptor flags
  2 constant F_SETFD                    \ set file descriptor flags
  3 constant F_GETFL                    \ get file status flags
  4 constant F_SETFL                    \ set file status flags
  5 constant F_GETOWN                   \ get SIGIO/SIGURG proc/pgrp
  6 constant F_SETOWN                   \ set SIGIO/SIGURG proc/prgrp
  7 constant F_GETLK                    \ get record locking information
  8 constant F_SETLK                    \ set record locking information
  9 constant F_SETLKW                   \ F_SETLK; wait if blocked
 10 constant F_SETLKWTIMEOUT            \ FL_SETLKW; wait if blocked, return on timeou
 40 constant F_FLUSH_DATA
 41 constant F_CHKCLEAN                 \ used for regression test
 42 constant F_PREALLOCATE              \ preallocate storage
 43 constant F_SETSIZE                  \ truncate a file, equivalent to truncate(2)
 44 constant F_RDADVISE                 \ turn on advisory read async with no copy to user
 45 constant F_RDAHEAD                  \ turn read ahead off/on for this fd
 
 48 constant F_NOCACHE                  \ turn caching off for this fd
 49 constant F_LOG2PHYS                 \ file offset to device offset
 50 constant F_GETPATH                  \ return the full path of the fd
 51 constant F_FULLFSYNC                \ fsync + ask the drive to flush to the media
 52 constant F_PATHPKG_CHECK            \ find which component (if any) is a package
 53 constant F_FREEZE_FS                \ freeze all fs operations
 54 constant F_THAW_FS                  \ thaw (unfreeze) all fs operations
 55 constant F_GLOBAL_NOCACHE           \ turn data caching off/on (globally) for this file
  
 59 constant F_ADDSIGS                  \ add detached signatures
 61 constant F_ADDFILESIGS              \ add signature from same file (used by dyld for shared libs)
 62 constant F_NODIRECT                 \ used in conjunction with F_NOCACHE to indicate that DIRECT, synchonous writes 
 63 constant F_GETPROTECTIONCLASS       \ Get the protection class of a file from the EA, returns int 
 64 constant F_SETPROTECTIONCLASS       \ Set the protection class of a file for the EA, requires int 
 65 constant F_LOG2PHYS_EXT             \ file offset to device offset, extended
 66 constant F_GETLKPID                 \ see man fcntl(2) F_GETLK by PID

 67 constant F_DUPFD_CLOEXEC

 70 constant F_SETBACKINGSTORE          \ Mark the file as being the backing store for another filesystem
 71 constant F_GETPATH_MTMINFO          \ return the full path of the FD, but error in specific mtmd circumstances 
 72 constant F_GETCODEDIR               \ Returns the code directory, with associated hashes, to the caller 
 73 constant F_SETNOSIGPIPE             \ No SIGPIPE generated on EPIPE
 74 constant F_GETNOSIGPIPE             \ Status of SIGPIPE for this fd
 75 constant F_TRANSCODEKEY             \ For some cases, we need to rewrap the key for AKS/MKB
 76 constant F_SINGLE_WRITER            \ file being written to a by single writer... 
 77 constant F_GETPROTECTIONLEVEL       \ Get the protection version number for this filesystem 
 78 constant F_FINDSIGS

 83 constant F_ADDFILESIGS_FOR_DYLD_SYM \ Add signature from same file, only if it is signed by Apple (used by dyld for simulator) 

 85 constant F_BARRIERFSYNC             \ fsync + issue barrier to drive

 90 constant F_OFD_SETLK                \ Acquire or release open file descriptor lock
 91 constant F_OFD_SETLKW               \ F_OFD_SETLK, block if conflicting locks
 92 constant F_OFD_GETLK                \ Examine OFD lock

 93 constant F_OFD_SETLKWTIMEOUT        \ F_OFD_SETLK, return if timeout

 97 constant F_ADDFILESIGS_RETURN       \ add signature from same file, return end offset
 98 constant F_CHECK_LV                 \ Check if library validation allows this Mach-O file to be mapped into the calling process
 99 constant F_PUNCHHOLE                \ deallocate a range of a file
100 constant F_TRIM_ACTIVE_FILE         \ trim an active file
101 constant F_SPECULATIVE_READ         \ async advisory read fcntl for regular and compressed file
102 constant F_GETPATH_NOFIRMLINK       \ return the full path without firmlinks of the fd 
103 constant F_ADDFILESIGS_INFO         \ Add signature from same file, return information 
104 constant F_ADDFILESUPPL             \ Add supplemental signature from same file with fd reference to original 
105 constant F_GETSIGINFO               \ Look up code signature information attached to a file or slice
106 constant F_SETLEASE                 \ acquire of release lease
107 constant F_GETLEASE                 \ Retrieve lease information

110 constant F_TRANSFEREXTENTS          \ Transfer allocated extents beyond leof to a different file 
111 constant F_ATTRIUTION_TAG           \ Based on flags, query/set/delete a file's attribution tag 

\ file descriptor flags (F_GETFD, F_SETFD)
1 constant FD_CLOEXEC

\ record locking flags (F_GETLK, F_SETLK...)
1 constant F_RDLCK
2 constant F_UNLOCK
3 constant F_WRLCK

\ lock operations for flock(2)
$ 01 constant LOCK_SH
$ 02 constant LOCK_EX
$ 04 constant LOCK_NB
$ 08 constant LOCK_UN

\ whence values for lseek
$ 00 constant SEEK_SET
$ 01 constant SEEK_CUR
$ 02 constant SEEK_END
$ 03 constant SEEK_HOLE
$ 04 constant SEEK_DATA

: FILE.creat ( filename perms -- fd , create file given permissions and open for writing )
    sys::creat
;

: FILE.ftruncate ( length fd -- result ,  truncate file to specified length)
    sys::ftruncate
;

: FILE.truncate ( filename length -- result , truncate file to specified length )
    sys::truncate
;

: FILE.open { filename mode -- fd , open file given mode and permissions }
    filename mode sys::open
;

: FILE.close ( fd -- , close file )
    sys::close
;

: FILE.read { buf len fd -- actual , read len bytes into buf from fd }
\    cr buf .hex len . fd . ." FILE.read"
    buf len fd sys::read
\    cr ." file.read " dup . ." bytes"
;

: FILE.write { caddr u fd -- actual , write len bytes from buf to fd }
    caddr u fd sys::write 
;

create write-byte-temp 0 ,

: FILE.write-byte { n fd -- actual , write a byte to fd }
    n write-byte-temp c!
    write-byte-temp swap FILE.write
;

: FILE.lseek ( whence offset fd -- offset , seek fd to offset )
    sys::lseek
;

: FILE.size ( filename -- size, return size of file )
    sys::filesize
;

: File.write-contents { filename caddr u | fd -- , actual }
    filename O_WRONLY O_CREAT or O_TRUNC or File.open -> fd
    fd 0< if fd exit then
    caddr u fd File.write 
    fd File.close drop
;

: FILE.contents { filename | fd size d -- caddr u , read entire file into memory }
    filename FILE.size -> size 
    size mem::malloc -> d
    filename O_RDONLY sys::open -> fd
    fd 0< if
        die{ c" file.contents " sys::perror filename ztype }die
    then
    d size fd sys::read
    dup 0 <= if
        die{ . c" File.contents read error" sys::perror }die
        fd FILE.close
        exit
    then
    fd FILE.close drop
    d swap
;

trace-include @ [if]
cr ." END sys/fcntl.fth"
[then]
