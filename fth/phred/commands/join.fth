\
\ phred editor
\
\ programmed by Mike Schwartz
\
\ Command Line handling
\   :j command
\   :join command
\

anew task-phred/commands/join.fth

: CommandLine.join { w str tok toku -- flg , return true if executed }
    tok toku s" j" compare 0= if
        w true Window.join-lines
        true exit
    then
    tok toku s" join" compare 0= if
        w true Window.join-lines
        true exit
    then
    false
;
