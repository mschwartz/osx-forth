#!/usr/local/bin/pforth_standalone -q

anew task-demos/http-client.fth

false trace-include !       \ disable messages about including

require net/http.fth
require net/socket.fth 

true http-verbose !        \ disable http diagnostic messages

: demo  { caddr u port | client req res }
    cr cr
    cr ." =================="
    cr ."  HTTP CLIENT DEMO"
    cr ." =================="

    caddr u port TCPSocket.client  -> client

    client HTTPRequest.new -> req
    client HTTPResponse.new -> res

    cr ." sending request headers... " 
    c" GET / HTTP/1.0" req HTTPRequest.send-header
    ." sent... "
    req HTTPRequest.end-headers drop
    ." sent " 

    cr ." reading headers... "
    res HTTPResponse.read-headers 
    false = if
        abort" failed to read-headers"
    then
    cr ." reading body ... "
    res HTTPResponse.read-body 
    ." got " dup . ." bytes"
    0= if
        abort" failed to read-body"
    then
    res HTTPResponse.get-body cr ." get-body returns ( body  count ) ".s
    http-verbose @ if
        cr cr cr ." ================ body (first 500 chars) follows: " cr cr cr 500 min type
        cr cr cr
    then

    cr ." about to destroy socket"
    client TCPSocket.destroy
    true
;

s" www.google.com" 80 demo

cr