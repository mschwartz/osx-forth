\ Load history and save new dictionary.
\ This is not part of the standard build because some computers
\ do not support ANSI terminal I/O.

include? ESC[ termio.fth
include? HISTORY history.fth
\ 1024 1024 * 20 * code-size !
\ 1024 1024 * 20 * headers-size !
\ 1500000 CODE-SIZE !         \ request code portion of new dictionary to be 500000 bytes
\ 1300000 HEADERS-SIZE !      \ request name portion of new dictionary to be 300000 bytes
c" pforth.dic" save-forth
