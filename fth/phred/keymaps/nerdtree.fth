private{
    : open-dirnode { n -- , open dirnode }
        true n s! DirNode.flags
    ;
    : close-dirnode { n -- , close dirnode }
        false n s! DirNode.flags
    ;
}private
: Keymap.nerdtree { w | ch dirnode -- , run nerdtree keymap }
    begin
        w Window.paint
        w Window.refresh
        w Window.address-cursor
        Keymap.key -> ch
        ch ncurses.key_mouse = if
            Keymap.mouse if exit then \ because a different window may have been clicked on (activated)
        then
        ch case
            -1 of die{ ." got -1?  " }die endof
            3 of die{ ." nerdtree exit  " }die endof
            ansi_escape of \ escape key
                ncurses.beep
            endof
            ascii : of
                w CommandLine.run 
                exit
            endof
            ncurses.ctrl_n of
                Screen.toggle-nerdtree
                exit
            endof
            ncurses.ctrl_l of
                Screen.right-window
                exit
            endof
            ncurses.ctrl_d of
                w Window.page-down
            endof
            key.ctrl_u of
                w Window.page-up
            endof
            ncurses.key_down of
                w Window.cursor-down
            endof
            ascii j of
                w Window.cursor-down
            endof
            ascii X of
                \ recursively close node
                w s@ Window.current-line s@ Line.data -> dirnode 
                dirnode s@ DirNode.type DT_DIR <> if exit then
                dirnode ['] close-dirnode DirNode.iterate
                w nerdtree.refresh
                w Window.paint
                w Window.refresh
                exit
            endof
            ascii O of
                \ recursively open node
                w s@ Window.current-line s@ Line.data -> dirnode 
                dirnode s@ DirNode.type DT_DIR <> if exit then
                dirnode ['] open-dirnode DirNode.iterate
                w nerdtree.refresh
                w Window.paint
                w Window.refresh
                exit
            endof
            ascii o of
                w s@ Window.current-line s@ Line.data -> dirnode 
                dirnode s@ DirNode.type case
                    DT_DIR of
                        dirnode s@ DirNode.flags not dirnode s! DirNode.flags
                        w nerdtree.refresh
                        w Window.paint
                        w Window.refresh
                        exit
                    endof
                    DT_REG of
                            WindowList.previous-window \ window
                            dirnode s@ DirNode.fullpath zcount  \ caddr u
                        WindowList.find-or-create drop
                        w NERDTree.deactivate
                        exit
                    endof
                endcase
            endof
            ncurses.key_up of
                w Window.cursor-up
            endof
            ascii k of
                w Window.cursor-up
            endof
        endcase
        messageline{ ." key " ch .hex }messageline
    again
;

privatize
