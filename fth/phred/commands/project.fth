\
\ phred editor
\
\ programmed by Mike Schwartz
\
\ Command Line handling
\   : pathalias @key value ( maps @key to value in require/include paths - create or update )
\   : pathalias @key    ( removes pathalias for key )
\   : pathalias ( print pathaliases )
\   : project   ( print project info )
\   : include-path <value> ( set include-path )
\   : include-path+ <value> ( append to include-path )
\

anew task-phred/commands/project.fth

private{
create project-pad 2048 allot
create project-pad2 2048 allot
}private

: CommandLine.project { w str tok toku -- flg , return true if executed }
    tok toku s" pathalias" compare 0= if
        str CString.skip-blanks

        str CString.parse-token -> toku -> tok
        toku 0= if
            messageline{
                theProject @ Project.print-aliases
            }messageline
            true exit
        then
        tok toku project-pad place-cstr

        str CString.skip-blanks
        str CString.parse-token -> toku -> tok
        toku 0= if
            \ TODO delete pathalias
            theProject @ project-pad ProjectAlias.remove
            true exit
        then
        tok toku project-pad2 place-cstr
        theProject @ project-pad project-pad2 Project.add-alias
        true exit
    then

    tok toku s" project" compare 0= if
        true exit
    then

    tok toku s" include-path" compare 0= if
        str CString.peek-ch if
            theProject @ ProjectIncludePath.clear
            debug-on
            begin
                str CString.skip-blanks
                str CString.parse-token -> toku -> tok
                toku 
            while
                tok toku project-pad place-cstr
                theProject @ project-pad Project.add-include-path
            repeat
        then
        messageline{ ." include-path: " theProject @ Project.print-include-path }messageline
        true exit
    then

    tok toku s" include-path+" compare 0= if
        str CString.peek-ch if
            begin
                str CString.skip-blanks
                str CString.parse-token -> toku -> tok
                toku 
            while
                tok toku project-pad place-cstr
                theProject @ project-pad Project.add-include-path
            repeat
            drop
        then
        messageline{ ." include-path: " theProject @ Project.print-include-path }messageline
        true exit
    then

    false
;

privatize
