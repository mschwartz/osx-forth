\
\ PHRed editor
\
\ programmed by Mike Schwartz
\
\ Buffer words
\

trace-include @ [if]
cr ." Compiling phred/buffer.fth"
[then]

0 constant Buffer.filetype.text
1 constant Buffer.filetype.forth
2 constant Buffer.filetype.c++
3 constant Buffer.filetype.md
4 constant Buffer.filetype.makefile
5 constant Buffer.filetype.git
6 constant Buffer.filetype.cmd
7 constant Buffer.filetype.dir
8 constant Buffer.filetype.help
10 constant Buffer.filetype.words
11 constant Buffer.filetype.nerdtree
12 constant Buffer.filetype.man
13 constant Buffer.filetype.bufed

:STRUCT Buffer
    struct ListNode Buffer.node
    APTR Buffer.fullpath                \ filename / path
    APTR Buffer.extension               \ filename extension (file type)
    ULONG Buffer.filetype
    APTR Buffer.name                    \ buffer name (can be **scratch**, etc.)
    BOOL Buffer.read-only               \ buffer can't be modified
    BOOL Buffer.permanent               \ buffer can't be deleted
    BOOL Buffer.hidden                  \ buffer should be hidden
    UINT64 Buffer.ctime                 \ timestamp of loaded/created
    UINT64 Buffer.mtime                 \ timestamp of modified
    UINT64 Buffer.#lines
    struct List Buffer.lines
    struct Array Buffer.undo-list
    struct Array Buffer.redo-list
;STRUCT

create buffer-last-line 512 allot

: Buffer.keystone ( b -- keystone , get keystone from Buffer )
    Buffer.lines + 
;

: Buffer.iterate { b handler -- , iterate each line in Buffer calling handler for each }
    b Buffer.keystone handler List.Iterate
;

: dump-one { n -- , dump one }
    n .hex ascii " emit n s@ Line.text zcount type ascii " emit cr
;
: Buffer.dump { buf | top lin -- , print buffer }
    cr
    cr ." dump of buf " buf .hex buf Buffer.keystone .hex cr
    ."  filetype: " buf s@ Buffer.filetype . cr
    ."  read-only: " buf s@ Buffer.read-only . cr
    ."  permanent: " buf s@ Buffer.permanent . cr
    ."  hidden: " buf s@ Buffer.hidden . cr
    buf ['] dump-one Buffer.iterate
;

private{
List buffer-list buffer-list List.init
variable found-buffer
variable find-name
variable find-type
variable find-name-length
create buffer-pad 512 allot

: buffer-find { n -- , iterator function for buffer list }
    n s@ Buffer.fullpath ?dup if
        zcount 
        find-name @ find-name-length @
        compare 0= if 
            n found-buffer !
        then
    then
    n s@ Buffer.name ?dup if
        zcount 
        find-name @ find-name-length @
        compare 0= if 
            n found-buffer !
        then
    then
;

}private

: BufferList.List ( -- l , return buffer-list )
    buffer-list 
;

: BufferList.find { caddr u -- buf , find buffer by filename }
    caddr find-name !
    u find-name-length !
    0 found-buffer !
    buffer-list ['] buffer-find List.iterate
    found-buffer @
;

: BufferList.first ( -- b , return first buffer in buffer-list )
    buffer-list List.first
;
: BufferList.first? { b -- f , is b first in list? }
    buffer-list List.first b =
;
: BufferList.last ( -- b , return first buffer in buffer-list )
    buffer-list List.last
;
: BufferList.last? { b  -- f , is buffer last in list? }
    buffer-list List.last b =
;

: BufferList.add ( b --, add buffer )
    buffer-list List.addTail
;

: BufferList.iterate { handler -- , }
    buffer-list handler List.Iterate
;

: Buffer.destroy { b | head lin nxt -- , destroy buffer }  
    b Buffer.keystone  -> head
    head s@ List.next -> lin
    begin 
        lin head <>
    while
        lin s@ ListNode.next -> nxt
        lin List.remove
        lin Line.destroy
        nxt -> lin
    repeat

    b mem::free
;

: (Buffer.new) { caddr u  | b -- b , create new buffer }
    sizeof() Buffer MEM.malloc -> b
    caddr u pad place-cstr pad string::strdup b s! Buffer.name
    caddr u pad place-cstr pad string::strdup b s! Buffer.fullpath
    0 b s! Buffer.#lines
    b Buffer.keystone List.init
    time::time-ms dup b s! Buffer.ctime 
    b s! Buffer.mtime
    b Buffer.undo-list + 20 Array.construct
    b Buffer.redo-list + 20 Array.construct
    Buffer.filetype b s! Buffer.filetype
    false b s! Buffer.hidden
    false b s! Buffer.read-only
    false b s! Buffer.permanent
    b
;
' (Buffer.new) is Buffer.new

: Buffer.touch { b -- , modify mtime of buffer }
    time::time-ms b s! Buffer.mtime
;


: Buffer.append-line { b l -- l , append line to buffer }
    l b Buffer.keystone List.addTail
    b s@ Buffer.#lines 1+ b s! Buffer.#lines
    b Buffer.touch
    l
;

: Buffer.append-text { txt b -- l , append zero terminated txt to buffer }
    b
    txt Line.new  \ create new Line
    Buffer.append-line
    b Buffer.touch
;

: Buffer.from { caddr u buf start# end# | head lin b -- b , create buffer from another buffer }
    \ create buffer from buf lines start# through end#
    caddr u Buffer.new -> b
    buf Buffer.keystone -> head
    head s@ List.next -> lin
    start# 0 do
        lin head = if
            \ start# is past end of buffer
            b exit
        then
        lin s@ ListNode.next -> lin
    loop
    end# start# do
        lin s@ Line.text b Buffer.append-text drop
        lin s@ ListNode.next -> lin
        lin head = if
            \ start# is past end of buffer
            b exit
        then
    loop
    b exit
;

: (Buffer.clone) { old | new head lin -- new , clone buffer }
    \ initialize new buffer
    old s@ Buffer.name zcount Buffer.new -> new
    new Buffer.keystone List.init
    \ copy old buffer fields
    old s@ Buffer.fullpath dup 
    if 
        string::strdup 
    then 
    new s! Buffer.fullpath 
    old s@ Buffer.extension ?dup 
    if
        string::strdup new s! Buffer.extension
    else
        0 new s! Buffer.extension
    then
    old s@ Buffer.filetype new s! Buffer.filetype
    old s@ Buffer.read-only new s! Buffer.read-only
    old s@ Buffer.permanent new s! Buffer.permanent
    old s@ Buffer.hidden new s! Buffer.hidden
    old s@ Buffer.ctime new s! Buffer.ctime
    old s@ Buffer.mtime new s! Buffer.mtime
    \ copy old buffer's lines
    old Buffer.keystone -> head
    head s@ List.next -> lin
    begin
        new lin Line.clone Buffer.append-line drop
        lin s@ ListNode.next -> lin
        lin head = if 
            new exit 
        then
    again
;
' (Buffer.clone) is Buffer.clone

: Buffer.replace-lines { buf line-list | blist l src dst -- , replace buf's lines }
    buf Buffer.keystone -> blist
    line-list s@ List.next -> src
    blist s@ List.next -> dst

    begin
        src line-list <>
    while
        dst blist <> if
            src s@ Line.text Line.new -> l
            dst l s@ Line.text Line.replace-text
            dst s@ List.next -> dst
        else
            src s@ Line.text buf Buffer.append-text drop
        then
        src s@ List.next -> src
    repeat
;

: (Buffer.save-undo) { buf -- , take undo snapshot of buffer }
    buf Buffer.undo-list + 
    buf Buffer.clone 
    Array.push
;
' (Buffer.save-undo) is Buffer.save-undo

: (Buffer.save-redo) { buf -- , take redo snapshot of buffer }
    buf Buffer.redo-list + 
    buf Buffer.clone     
    Array.push
;
' (Buffer.save-redo) is Buffer.save-redo

: (Buffer.undo) { buf | ary ubuf -- , undo changes to buffer }
    buf Buffer.save-redo \ current state is redo state
    buf Buffer.undo-list +  -> ary
    ary Array.empty? if 
        messageline{ ."  *** No more undo" }messageline
        exit 
    then
    ary Array.pop -> ubuf
    ubuf if
        buf ubuf Buffer.keystone Buffer.replace-lines
        ubuf Buffer.destroy
    then
    buf Buffer.touch
;
' (Buffer.undo) is Buffer.undo

: (Buffer.redo) { buf | ary rbuf -- , redo changes to buffer }
    buf Buffer.save-undo \ current state is undo state
    buf Buffer.redo-list + -> ary
    ary Array.empty? if 
        messageline{ ."  *** No more redo" }messageline
        exit 
    then
    ary Array.pop -> rbuf
    rbuf if
        buf rbuf Buffer.keystone Buffer.replace-lines
        rbuf Buffer.destroy
    then
    buf Buffer.touch
;
' (Buffer.redo) is Buffer.redo

: Buffer.read-only? ( b -- f , is buffer read only? )
    s@ Buffer.read-only
;

: Buffer.hidden? ( b -- f , is buffer hidden? )
    s@ Buffer.hidden
;

: Buffer.permanent? ( b -- f , is buffer permanent/undeletable )
    s@ Buffer.permanent
;

: Buffer.modified? { b -- f , is buffer modified? }
    b s@ Buffer.mtime 1000 / b s@ Buffer.ctime 1000 / <>
;

: BufferList.unsaved? { | b -- flg , is any buffer not saved? }
    BufferList.list -> b
    begin
        b s@ ListNode.next -> b
        b BufferList.list <>
    while
        b Buffer.modified? if
            b Buffer.hidden? not if
                b Buffer.permanent? not if
                    messageline{
                        ." No write since last change for buffer "
                        ascii " emit
                        b s@ Buffer.name zcount type
                        ascii " emit
                    }messageline
                    true exit
                then
            then
        then
    repeat
    false
;

: Buffer.first-line { b -- l , get first line of buffer }
    b Buffer.lines + List.First
;
: Buffer.last-line { b -- l , get last line of buffer }
    b Buffer.keystone List.Last
;
: Buffer.last-line? { b l -- f , is line end of buffer? }
    b Buffer.keystone l s@ List.next =
;

: Buffer.end? { b l -- f , is line end of buffer? }
    b Buffer.keystone l s@ List.next =
;

: Buffer.insert-before { b src dst -- , add src line before dst line }
    dst src List.InsertBefore
    b s@ Buffer.#lines 1+ b s! Buffer.#lines
    b Buffer.touch
;

: Buffer.insert-after { b src dst -- , add src line after dst line }
    src dst List.InsertAfter
    b s@ Buffer.#lines 1+ b s! Buffer.#lines
    b Buffer.touch
;

: Buffer.join { b l flg | nxt tmp t1 t2 -- , join line with next line adding blank between if flag set }
    l b Buffer.last-line? not if
        l flg Line.join
        b s@ Buffer.#lines 1- b s! Buffer.#lines
        b Buffer.touch
    then
;

: Buffer.split-at-index { b l ndx -- , split line at index }
    l ndx Line.split-at-index
    b s@ Buffer.#lines 1+ b s! Buffer.#lines
    b Buffer.touch
;

: Buffer.unlink-line { l -- , unlnk and remove line }
    l List.Remove
    l Line.destroy
;

: Buffer.delete-line { b l -- , delete line }
    b Buffer.keystone l = if 
        exit 
    then
    l Buffer.unlink-line
    b s@ Buffer.#lines 1- b s! Buffer.#lines
    b Buffer.touch
;

: Buffer.goto-line# { b line# | head lin -- l , get line# line from buffer }
    b Buffer.keystone -> head
    head s@ List.next -> lin
    line# 1 ?do
        lin head = if
            0 exit
        then
        lin s@ ListNode.next -> lin
    loop
    lin
;

: Buffer.search { b zpattern | head lin re -- lin }
    zpattern regex.new -> re
    b Buffer.keystone -> head
    head -> lin
    begin   
        lin s@ ListNode.next -> lin
        lin head <>
    while
        lin s@ Line.text re regex.search_re if
            re regex.delete
            lin exit
        then
    repeat
    re regex.delete
    nullptr
;

: Buffer.clear { b -- , remove all lines from Buffer }
    b Buffer.keystone List.empty? if exit then
    b Buffer.keystone ['] Buffer.unlink-line List.iterate
    0 b s! Buffer.#lines
    b Buffer.touch
;

: Buffer.read { b caddr u | fp -- f , read lines from file }
    \ TODO - read after line, not always to end of file
    c" r " caddr u stdio.fopen -> fp
    fp 0= if
        z" " 1+ b Buffer.append-text drop
        exit
    then

    begin 
        fp stdio.feof not
    while
        pad 4096 fp stdio.fgets
        0<> if
            pad b Buffer.append-text drop
        then
    repeat
    fp stdio.fclose drop
    time::time-ms b s! Buffer.mtime
;

: Buffer.revert { b -- , }
    b Buffer.clear
    b b s@ Buffer.fullpath zcount Buffer.read
    time::time-ms dup b s! Buffer.mtime b s! Buffer.ctime
;

: Buffer.file-modified { b -- flg , if flg is true, maybe revert buffer }
    b s@ Buffer.fullpath 0= if false exit then
    b s@ Buffer.fullpath sys::file-modified
    b s@ Buffer.mtime  1000 / >
\     > if
\         \ file has changed
\         messageline{ 
\             ." file has changed on disk! reload (y/n)? " 
\             sys::key 
\             die{ ." got key " }die
\         }messageline
\         TOUPPER ascii Y = if
\             b Buffer.revert
\             w b Window.set-buffer
\         then
\     then

;
: Buffer.set-filetype { b | ext -- , set filetype of buffer based on extension }
    b s@ Buffer.extension -> ext
    ext c@ 0= if
        Buffer.filetype.text b s! Buffer.filetype
        exit
    then
    ext s\" fth\z" drop string::strcasecmp 0= if
        Buffer.filetype.forth b s! Buffer.filetype
        exit
    then
    ext s\" fs\z" drop string::strcasecmp 0= if
        Buffer.filetype.forth b s! Buffer.filetype
        exit
    then
    ext s\" cpp\z" drop string::strcasecmp 0= if
        Buffer.filetype.c++ b s! Buffer.filetype
        exit
    then
    ext s\" cc\z" drop string::strcasecmp 0= if
        Buffer.filetype.c++ b s! Buffer.filetype
        exit
    then
    ext s\" h\z" drop string::strcasecmp 0= if
        Buffer.filetype.c++ b s! Buffer.filetype
        exit
    then
    ext s\" hpp\z" drop string::strcasecmp 0= if
        Buffer.filetype.c++ b s! Buffer.filetype
        exit
    then
    ext s\" md\z" drop string::strcasecmp 0= if
        Buffer.filetype.md b s! Buffer.filetype
        exit
    then
    b s@ Buffer.name s\" Makefile\z" drop string::strcmp 0= if
        Buffer.filetype.makefile b s! Buffer.filetype
        exit
    then
    Buffer.filetype.text b s! Buffer.filetype
;

: Buffer.set-filename { b caddr | ext -- , set filename of buffer }
    caddr string::strdup b s! Buffer.fullpath
    caddr Path.extension  -> ext
    ext 0= if
        s\" txt\z"  -> ext
    then
    ext b s! Buffer.extension
    b Buffer.set-filetype
;

create new-buffer-line 512 allot
variable new-buffer-data
: Buffer.format ( ...tags b -- , print message line w/tags into end of buffer ) { | b -- , } 
    0 new-buffer-line c!
    0 new-buffer-data !
    -> b
    b >r
    begin
        dup TAG_END <>
    while
        case
            TAG_NUMBER_JUSTIFIED of
                >r (U.) 
                r> 
                over -  
                ?dup if 0 do
                bl new-buffer-line addchar
                    loop
                then
                new-buffer-line append
            endof
            TAG_NUMBER of
                (.) new-buffer-line append
            endof
            TAG_NUMBERX of
                hex (.) new-buffer-line append decimal
            endof
            TAG_$STRING of
                count new-buffer-line append
            endof
            TAG_STRING of
                new-buffer-line append
            endof
            TAG_CHAR of
                new-buffer-line addchar
            endof
            TAG_BYTE of
                new-buffer-line addchar
            endof
            TAG_ICON of
                new-buffer-line addchar
            endof
            TAG_CHARX of
                hex (.) new-buffer-line append decimal
            endof
            TAG_DATA of
                new-buffer-data !
            endof
            TAG_SPACES of
                0 do
                    bl new-buffer-line addchar
                loop
            endof
            ncurses-uninstall cr ." invalid tag "
            5000 msec 0 sys::exit
        endcase
    repeat
    drop
    new-buffer-line count pad place-cstr
    pad r>
    Buffer.append-text 
    new-buffer-data @ swap s! Line.data
    b Buffer.touch
;

: can-save? { buf -- flg , can buffer be saved? }
    buf s@ Buffer.fullpath 0= if false exit then
    buf Buffer.read-only? if false exit then
    buf Buffer.permanent? if false exit then
\     buf Buffer.modified? not if false exit then
    true
;

: (Buffer.write) { buf filename | top lin txt fp d -- success , write buffer to file }
    0 buffer-pad c!
    buffer-pad filename string::strcat drop
    buffer-pad z" .bak" 1+ String::strcat drop
    filename sys::access not if
        buffer-pad filename Path.copy-file not if
           die{ cr ." *** could not backup file " }die
            false exit
        then
    then
    z" w" 1+ filename sys::fopen -> fp
    fp 0= if
    ncurses.refresh
        messageline{ 
            ." *** fopen: " 
            ascii " emit 
            ." .../"
            filename Path.basename zcount type 
            ascii " emit space 
            sys::strerror zcount type  
            }messageline
    ncurses.refresh
        false exit
    then

    buf Buffer.keystone -> top
    top -> lin
    begin
        lin s@ ListNode.next -> lin
        lin top <>
    while
        lin s@ Line.text -> txt
        txt string::strlen 0> if
            txt fp sys::fputs drop
        then
        $ 0a fp sys::fputc drop
    repeat
    fp sys::fclose drop
    true
;
' (Buffer.write) is Buffer.write

: (Buffer.save) { buf | fn typ -- flg , write buffer to disk }
    buf s@ Buffer.filetype -> typ
    buf s@ Buffer.fullpath -> fn
    buf can-save?  if
        buf fn Buffer.write not if
            false exit 
        then
    else
            false exit
        true exit
    then
    time::time-ms dup buf s! Buffer.ctime 
    buf s! Buffer.mtime
\     messageline{  ." saved " fn zcount type  }messageline
    true
;
' (Buffer.save) is Buffer.save

: Buffer.save-one { b -- , }
    b can-save? if
        Buffer.save drop
    then
;
: (BufferList.saveall)
    buffer-list ['] Buffer.save-one List.iterate
;
' (BufferList.saveall) is BufferList.saveall

: BufferList.find-or-create { caddr u | b  fn -- b , find or create buffer }
    caddr u BufferList.find -> b
    b not if
        caddr u Buffer.new -> b
        caddr u find-file  -> fn
        b fn Path.realpathz 
        Buffer.set-filename
        b b s@ Buffer.fullpath zcount 
        Buffer.read 
        b BufferList.add
    then
    b
;

privatize

trace-include @ [if]
cr ." End phred/buffer.fth"
[then]
