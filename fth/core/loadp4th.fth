\ @(#) loadp4th.fth 98/01/28 1.3
\ Load various files needed by PForth
\
\ Author: Phil Burk
\         and Mike Schwartz
\
\ Copyright 1994 3DO, Phil Burk, Larry Polansky, David Rosenboom
\
\ Permission to use, copy, modify, and/or distribute this
\ software for any purpose with or without fee is hereby granted.
\
\ THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
\ WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
\ WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL
\ THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
\ CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING
\ FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
\ CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
\ OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

\ : key sys::key ;
\ : key? sys::key? ;
\ : ?terminal sys::?terminal ;

include? forget  core/forget.fth
include? >number core/numberio.fth
include? task-misc1.fth   core/misc1.fth
include? case    core/case.fth
include? +field  core/structure.fth
include? $=      core/strings.fth
include? privatize   core/private.fth
include? (local) core/ansilocs.fth
include? {       core/locals.fth
include? fm/mod  core/math.fth
include? [if]    core/condcomp.fth
include? task-misc2.fth core/misc2.fth
include? save-input core/save-input.fth
include? read-line  core/file.fth
include? require    core/require.fth
include? s\"     core/slashqt.fth   \ "
include? microseconds core/time.fth

\ load floating point support if basic support is in kernel
exists? F*
   [IF]  include? task-floats.fth core/floats.fth
   [THEN]

\ useful but optional stuff follows --------------------

macos? [if]
include core/signals-macos.fth
[else]
include core/signals-linux
[then]

include? task-member.fth   core/member.fth
include? :struct core/c_struct.fth
include? smif{   core/smart_if.fth
include? file?   help/filefind.fth
include? see     help/see.fth
include? words.like help/wordslik.fth
include? trace   core/trace.fth
include? ESC[    core/termio.fth
include? HISTORY core/history.fth
include? fa-folder font-awesome.fth
\ include? cstr" core/c-strings.fth

\ 500000 CODE-SIZE !         \ request code portion of new dictionary to be 500000 bytes
\ 300000 HEADERS-SIZE !      \ request name portion of new dictionary to be 300000 bytes
map
