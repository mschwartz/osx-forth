anew task-sdl-pixels.fth

255 constant SDL_ALPHA_OPAQUE
  0 constant SDL_ALPHA_TRANSPARENT

\ Pixel type.
 0 constant SDL_PIXELTYPE_UNKNOWN
 1 constant SDL_PIXELTYPE_INDEX1
 2 constant SDL_PIXELTYPE_INDEX4
 3 constant SDL_PIXELTYPE_INDEX8
 4 constant SDL_PIXELTYPE_PACKED8
 5 constant SDL_PIXELTYPE_PACKED16
 6 constant SDL_PIXELTYPE_PACKED32
 7 constant SDL_PIXELTYPE_ARRAYU8
 8 constant SDL_PIXELTYPE_ARRAYU16
 9 constant SDL_PIXELTYPE_ARRAYU32
10 constant SDL_PIXELTYPE_ARRAYF16
11 constant SDL_PIXELTYPE_ARRAYF32

\ Bitmap pixel order, high bit -> low bit. 
0 constant SDL_BITMAPORDER_NONE
1 constant SDL_BITMAPORDER_4321
2 constant SDL_BITMAPORDER_1234

\ Packed component order, high bit -> low bit.
0 constant SDL_PACKEDORDER_NONE
1 constant SDL_PACKEDORDER_XRGB
2 constant SDL_PACKEDORDER_RGBX
3 constant SDL_PACKEDORDER_ARGB
4 constant SDL_PACKEDORDER_RGBA
5 constant SDL_PACKEDORDER_XBGR
7 constant SDL_PACKEDORDER_BGRX
8 constant SDL_PACKEDORDER_ABGR
9 constant SDL_PACKEDORDER_BGRA

\ Array component order, low byte -> high byte.
0 constant SDL_ARRAYORDER_NONE
1 constant SDL_ARRAYORDER_RGB
2 constant SDL_ARRAYORDER_RGBA
3 constant SDL_ARRAYORDER_ARGB
4 constant SDL_ARRAYORDER_BGR
5 constant SDL_ARRAYORDER_BGRA
6 constant SDL_ARRAYORDER_ABGR

\ Packed component layout.
0 constant SDL_PACKEDLAYOUT_NONE
1 constant SDL_PACKEDLAYOUT_332
2 constant SDL_PACKEDLAYOUT_4444
3 constant SDL_PACKEDLAYOUT_1555
4 constant SDL_PACKEDLAYOUT_5551
5 constant SDL_PACKEDLAYOUT_565
6 constant SDL_PACKEDLAYOUT_8888
7 constant SDL_PACKEDLAYOUT_2101010
8 constant SDL_PACKEDLAYOUT_1010102

: SDL_FOURCC ( a b c d -- code , define a four character code as a uint32 )
    0
    24 LSHIFT OR
    16 LSHIFT OR
     8 LSHIFT OR
     0 LSHIFT OR
;

: SDL_DEFINE_PIXELFOURCC ( a b c d -- code )
    SDL_FOURCC
;

: SDL_DEFINE_PIXELFORMAT { pixeltype order layout bits nbytes -- format , encode pixel format }
    1 28 LSHIFT
    pixeltype 24 LSHIFT OR
    order 20 LSHIFT OR
    layout 16 LSHIFT OR
    bits 8 LSHIFT OR
    nbytes 0 LSHIFT OR
;

: SDL_PIXELFLAG ( x -- pixelflag ) 28 RSHIFT $ 0f AND ;
: SDL_PIXELTYPE ( x -- pixeltype ) 24 RSHIFT $ 0f AND ;
: SDL_PIXELORDER ( x -- pixelorder ) 20 RSHIFT $ 0f AND ;
: SDL_PIXELLAYOUT ( x -- pixellayout ) 16 RSHIFT $ 0f AND ;
: SDL_BITSPERPIXEL ( x -- bitsperpixel ) 8 RSHIFT $ ff AND ;

: SDL_ISPIXELFORMAT_FOURCC ( format -- f , )
    dup SDL_PIXELFLAG AND 1 1 <>
;

: SDL_ISPIXELFORMAT_INDEXED { format | pixeltype -- f , }
    format SDL_ISPIXELFORMAT_FOURCC if
        false
    else
        format SDL_PIXELTYPE -> pixeltype
        pixeltype SDL_PIXELTYPE_INDEX1 = if true exit then
        pixeltype SDL_PIXELTYPE_INDEX4 = if true exit then
        pixeltype SDL_PIXELTYPE_INDEX8 = if true exit then
    then
    false
;

: SDL_ISPIXELFORMAT_PACKED { format | pixeltype -- f , }
    format SDL_ISPIXELFORMAT_FOURCC if
        false
    else
        format SDL_PIXELTYPE -> pixeltype
        pixeltype SDL_PIXELTYPE_PACKED8 = if true exit then
        pixeltype SDL_PIXELTYPE_PACKED16 = if true exit then
        pixeltype SDL_PIXELTYPE_PACKED32 = if true exit then
    then
    false
;

: SDL_ISPIXELFORMAT_ARRAY { format | pixeltype -- f , }
    format SDL_ISPIXELFORMAT_FOURCC if
        false
    else
        format SDL_PIXELTYPE -> pixeltype
        pixeltype SDL_PIXELTYPE_ARRAYU8 = if true exit then
        pixeltype SDL_PIXELTYPE_ARRAYU16 = if true exit then
        pixeltype SDL_PIXELTYPE_ARRAYU32 = if true exit then
        pixeltype SDL_PIXELTYPE_ARRAYF16 = if true exit then
        pixeltype SDL_PIXELTYPE_ARRAYF32 = if true exit then
    then
    false
;

: SDL_ISPIXELFORMAT_ALPHA { format | pixelorder -- f , }
    format SDL_PIXELORDER -> pixelorder
    format SDL_ISPIXELFORMAT_PACKED if
        pixelorder SDL_PACKEDORDER_ARGB = if true exit then
        pixelorder SDL_PACKEDORDER_RGBA = if true exit then
        pixelorder SDL_PACKEDORDER_ABGR = if true exit then
        pixelorder SDL_PACKEDORDER_BGRA = if true exit then
    then
    format SDL_ISPIXELFORMAT_ARRAY if
        pixelorder SDL_ARRAYORDER_ARGB = if true exit then
        pixelorder SDL_ARRAYORDER_RGBA = if true exit then
        pixelorder SDL_ARRAYORDER_ABGR = if true exit then
        pixelorder SDL_ARRAYORDER_BGRA = if true exit then
    then
    false
;

0 constant SDL_PIXELFORMAT_UNKNOWN 
SDL_PIXELTYPE_INDEX1 SDL_BITMAPORDER_4321 0 1 0 SDL_DEFINE_PIXELFORMAT constant SDL_PIXELFORMAT_INDEX1LSB
SDL_PIXELTYPE_INDEX1 SDL_BITMAPORDER_1234 0 1 0 SDL_DEFINE_PIXELFORMAT constant SDL_PIXELFORMAT_INDEX1MSB
SDL_PIXELTYPE_INDEX4 SDL_BITMAPORDER_4321 0 4 0 SDL_DEFINE_PIXELFORMAT constant SDL_PIXELFORMAT_INDEX4LSB
SDL_PIXELTYPE_INDEX4 SDL_BITMAPORDER_1234 0 4 0 SDL_DEFINE_PIXELFORMAT constant SDL_PIXELFORMAT_INDEX4MSB
SDL_PIXELTYPE_INDEX8  0 0 8 1 SDL_DEFINE_PIXELFORMAT constant SDL_PIXELFORMAT_INDEX8
