\
\ C / asciiz strings, growable, parsable
\     by Mike Schwartz
\
anew task-lib/c-strings.fth

[undefined] die{ [if]
    : die{ ;
    : }die 0 sys::exit ;
[then]

require sys/regex.fth

:STRUCT CString
    APTR CString.data
    LONG CString.size
    LONG CString.length
    LONG CString.index              \ for parsing
;STRUCT

: CString.dump { str -- , dump string }
    cr
    cr ." CString $" str .hex
    cr ."    .data   " str s@ CString.data .hex
    cr ."    .size   " str s@ CString.size .
    ."            " str s@ CString.data  str s@ CString.size dump
    ."    .length " str s@ CString.length .
    ."            " str s@ CString.data  str s@ CString.length 1+ dump
    ."    .index  " str s@ CString.index .
    cr
;

private{
    4096 constant cstring-padsize
    create cstring-pad cstring-padsize allot
    create cstring-pad2 cstring-padsize allot

    2 constant REGMATCH_COUNT
    create cstring-regex sizeof() regex_t allot
    variable cstring-regmatch REGMATCH_COUNT regex::alloc_regmatch_t cstring-regmatch !
}private

: CString.peek-ch { str -- ch , get character at index but do not  advance index }
    str s@ CString.data str s@ CString.index + c@
;

: CString.reset-index { str -- , begin parsing }
    0 str s! CString.index
;

: CString.advance-index { str -- , advance index }
    str s@ CString.index 1+ str s! CString.index
;
: CString.back-index { str -- , advance index back one }
    str s@ CString.index 1- dup 0 >= if
        str s! CString.index
    else
        drop
    then
;

: CString.get-index { str -- ndx , get index }
    str s@ CString.index
;
: CString.set-index { str n -- ndx , get index }
    n str s@ CString.size < if
        n str s! CString.index
    then
;

: CString::getch { str | ch -- ch , get character at index and advance index }
    str CString.peek-ch -> ch
    ch if
        str CString.advance-index
    then
    ch
;

: CString.bol? { str -- f , is index at beginning of line? }
    str s@ CString.index 0=
;
: CString.eol? { str -- f , is index at end of line? }
    str s@ CString.index str s@ CString.length  >= 
;

: CString.set { str new-data | len data -- , replace CString data }
    new-data string::strlen 1+ -> len
    len str s@ CString.size <= if
        str s@ CString.data -> data
    else
        str s@ CString.data ?dup if mem::free then
        len mem::malloc -> data
    then
    data new-data string::strcpy drop
    data str s! CString.data
;

: CString.new { data0 | len str -- str , create new CString }
    sizeof() CString mem::malloc -> str
    data0 string::strlen -> len
    len 2+ mem::malloc str s! CString.data
    len str s! CString.length
    len 2+ str s! CString.size
    str s@ CString.data data0 string::strcpy drop
    str CString.reset-index
\ debug? if cr ." CString.new " cr then
    str
;

: CString.new-empty ( -- str )
    z" " 1+ CString.new
;

: CString.destroy { str -- , free resources }
    str s@ CString.data mem::free
    str mem::free
;

: CString.truncate { str -- , truncate string to 0 length }
    0 str s! CString.length
    0 str s@ CString.data c!    \ null terminate
    str CString.reset-index
;

: CString.grow { str needed -- , grow string data }
    needed str s@ CString.length + 1+ str s@ CString.size >= if
        \ resize to 2x current size
        str s@ CString.size 2* str s! CString.size
        str s@ CString.data str s@ CString.size mem::realloc 
            str s! CString.data
    then
;

: CString.strcat { str txt | new  -- , append text to string }
    str txt string::strlen CString.grow
    str s@ CString.data -> new
    new txt string::strcat drop
    new str s! CString.data
    new string::strlen str s! CString.length
;

: CString.strncat { str txt cnt -- , append text for cnt chars to str }
    txt cnt pad place-cstr
    str CString.strcat
;

: CString.append-char { str ch -- , append character to string }
    ch cstring-pad c!
    0 cstring-pad 1+ c!
    str cstring-pad CString.strcat
;

: CString.delete-char { str ndx | data -- , delete character at index }
    str s@ CString.data -> data
        data ndx +      \ dst
        data ndx + 1+   \ src
    string::strcpy drop
    str s@ CString.length 1- str s! CString.length
;

: CString.insert-char { str ch ndx | tmp data -- , insert character at index }
    str 1 CString.grow
    str s@ CString.data -> data
    data string::strlen 10 + mem::malloc -> tmp
    tmp data ndx + string::strcpy drop
    0 data ndx + c!     \ null terninate
    str ch CString.append-char
    data tmp string::strcat drop
    tmp mem::free
;

: CString.insert-string { str caddr u ndx | ch -- , insert string at index }
    u 0 do
        caddr c@ -> ch
        caddr 1+  -> caddr
        str ch ndx CString.insert-char
        str CString.advance-index
        ndx 1+ -> ndx
    loop
;

: CString.append-string { str caddr u -- , append string }
    u if
        u 0 do
            str caddr c@ CString.append-char
            caddr 1+ -> caddr
        loop
    then
;

: CString.append-$string { str caddr | u -- , append string }
    caddr count -> u -> caddr
    u if
        u 0 do
            str caddr c@ CString.append-char
            caddr 1+ -> caddr
        loop
    then
;

: CString.newline { str -- , append newline to string }
    str $ 0a  CString.append-char
;

: CString.append-number { str n -- , append number to string in current BASE }
    str n (.) CString.append-string
;

: CString.append-spaces { str n -- , append n spaces }
    n if
        n 0 do
            str bl CString.append-char
        loop
    then
;

: CString.zrest { str -- caddr , return caddr  of CString starting at index }
    str s@ CString.data str s@ CString.index 
    +  \ caddr
;

: CString.rest { str -- caddr u , return caddr u of CString starting at index }
    str s@ CString.data str s@ CString.index 
    +  \ caddr
    zcount                                      \ caddr u
;

: CString.string { str -- caddr u , return caddr u of CString }
    str s@ CString.data zcount
;

: CString.zstring { str -- caddr , return caddr of CString }
    str s@ CString.data 
;

\ CString is likely to be more than 255 chars long
\ : CString.$string { str -- $str , return $string of CString }
\     str s@ CString.data zcount cstring-pad place
\     cstring-pad
\ ;

: CString.getch { str -- ch , get character at index and advance index }
    str CString.peek-ch
    str CString.advance-index
;

: CString.skip-blanks { str | ch -- , move index past blanks }
    begin
        str CString.peek-ch -> ch
        ch 0= if exit then
        ch bl = 
    while
        str CString.advance-index
    repeat
;

: CString.parse { str delim | ptr c ndx -- caddr u , parse from str until ch is found }
    str s@ CString.data -> ptr
    str s@ CString.index -> ndx
    ptr ndx + -> ptr
    begin
        str CString.getch -> c
        c 0<>
        c delim <> and
    while
    repeat
    ptr                             \ caddr
    str CString.get-index ndx - 1-     \ u
;

: CString.parse-pattern { str delim dst | ch -- success , parse a regex pattern from str up to delim }
    begin
        str CString.getch -> ch
        ch
    while
        ch case
            ascii \ of
                ch dst c!
                dst 1+ -> dst
                str CString.getch -> ch
                ch 0= if
                    \ syntax error, like \ then null
                    false exit
                then
                ch dst c!
                dst 1+ -> dst
            endof
            delim of
                0 dst c!    \ null terminate dst
                exit
            endof
            \ default
            ch dst c!
            dst 1+ -> dst
        endcase
    repeat
    0 dst c!    \ null terminate dst
    exit
;


: CString.parse-token { str | ndx strt len ch -- caddr u , parse next token from line starting at index }
    str CString.skip-blanks
    str s@ CString.data -> strt
    str s@ CString.index -> ndx
    ndx strt + -> strt
    0 -> len
    begin
        str CString.peek-ch -> ch
        ch whitespace? not
        ch 0<> and
    while
        str CString.advance-index
        len 1+ -> len
    repeat

    strt len
;

: CString.skip-to-number { str | ch -- flg , skip in str to first char of number }
    begin
        str CString.peek-ch -> ch
        ch
    while
        ch ascii - = if
            str CString.advance-index
            str CString.peek-ch -> ch
            ch ctype::isdigit if
                str CString.back-index
                true
                exit
            then
        then
        ch ctype::isdigit if
            true exit
        then
        str CString.advance-index
    repeat
    false
;

: CString.parse-number { str | sgn n flg ch -- n flg , parse number and leave index past the number }
    1 -> sgn
    str CString.peek-ch ascii - = if 
        -1 -> sgn 
        str CString.advance-index
    then
    0 -> n
    false -> flg
    begin 
        str CString.peek-ch -> ch
        ch ctype::isnumber 
    while
        n 10 * -> n
        ch ascii 0 - n + -> n
        str CString.advance-index
        true -> flg
    repeat
    n sgn * flg
;

: CString.compare { str zstr -- n , compare CString with asciiz string }
    str s@ CString.data zstr string::strcmp
;

: CString.compare-nc { str zstr -- n , compare CString with asciiz string, ignore case }
    str s@ CString.data zstr string::strcasecmp
;

: CString.compare-substr { str zstr n -- n , compare substring of str for n chars }
    str s@ CString.data zstr n string::strncmp
;

: CString.compare-substr-nc { str zstr n -- n , compare substring of str for n chars }
    str s@ CString.data zstr n string::strncasecmp
;

: CString.index-of { str zstr -- n , index of zstr in str or -1 }
    str s@ CString.data zstr string::strstr
;

: CString.index-of-nc { str zstr -- n , index of zstr in str or -1 }
    str s@ CString.data zstr string::strcasestr
;

: CString.substr { str strt len -- caddr u , get substring }
    str s@ CString.data strt + len
;

: CString.replace { str ndx len caddr u | src tmp -- , replace ndx through len of str with caddr u }
    ndx 0< if exit then
    str s@ CString.data -> src
    str s@ CString.size u + 2+  str s! CString.size
    str s@ CString.size mem::malloc -> tmp
    ndx 0> if
        tmp src ndx 1- string::strncpy drop
    else
        tmp src ndx string::strncpy drop
    then
    tmp caddr string::strcat drop
    tmp src ndx + len + string::strcat drop
    tmp string::strlen str s! CString.length
    0 str s! CString.index
    tmp str s! CString.data
    src mem::free
;

: CString.match-regex { str re | rm -- so eo , match compiled regex }
    2 regex::alloc_regmatch_t -> rm
        re                      \ preg
        str s@ CString.data     \ zstring
        rm                      \ regmatch
        REGMATCH_COUNT          \ nmatch
        0                       \ flags
    regexec 0<> if
        -1 -1
    else
        rm s@ regmatch_t.rm_so 
        rm s@ regmatch_t.rm_eo
    then
    rm regex::free_regmatch_t
;

: CString.match-pattern { str pattern | re -- so eo , match regex pattern/text }
    regex::alloc_regex_t -> re
    re pattern REG_MINIMAL REG_ENHANCED OR regcomp 0<> if
        -1 -1 exit
    then
    str re CString.match-regex
    re regex::free_regex_t
;

: CString.replace-pattern { str pattern caddr u | so eo -- flg , replace pattern if matched }
    str pattern CString.match-pattern -> eo -> so
    eo -1 = if
        \ no match
        false exit
    then

    pattern 1+ c@ ascii ) = if
        die{ pattern 2+ c@ .hex str s@ CString.data 20 dump pattern 20 dump caddr 20 dump u . eo . so . }die
    then
        str 
        so          \ ndx
        eo so -     \ len
        caddr u 
    CString.replace
    true
;

: CString.replace-$pattern { str $pattern caddr u -- flg }
    $pattern count cstring-pad place-cstr
    str cstring-pad caddr u CString.replace-pattern
;

: CString.$regex_replace { str $pattern caddr u | re col -- col }
    $pattern count cstring-pad place-cstr
    cstring-pad regex.new -> re
    caddr u cstring-pad2 place-cstr
        str s@ CString.data     \ haystack
        re
    regex.search_re -> col
        str s@ CString.data     \ haystack
        re
        cstring-pad2            \ replcement
    regex.replace_re
    str swap CString.set
    re regex.delete
    col
;

: String.append.char { str c | ptr -- , add character to string }
    str zcount + -> ptr
    c ptr c!
    ptr 1+ -> ptr
    0 ptr c!
;

: CString.fgets { str fp -- flg , read a line from FILE return false if EOF }
    fp sys::feof if false exit then
    cstring-pad cstring-padsize fp sys::fgets 0= if false exit then
    str cstring-pad CString.set
    str CString.reset-index
    true
;

: CString.fputs { str fp -- flg , write a line and newline to FILE return success }
    str CString.string cstring-pad place-cstr
    cstring-pad $ 0a String.append.char
    cstring-pad fp sys::fputs >= 0
;


: >z pad place pad count over swap chars + 0 swap c! ;

: asciiz> ( c-addr -- c-addr u , )
    dup begin count 0= until 1- over -
;

: XCOUNT DUP CELL+ SWAP @ ;

: _ZLITERAL-CODE R> XCOUNT OVER + 1+ >R ;

: ZLITERAL ( a u -- )
    STATE @ IF
        ['] _ZLITERAL-CODE COMPILE,
        DUP ,
        HERE SWAP DUP ALLOT MOVE 0 C, 
    ELSE
        OVER + 0 SWAP C!
    THEN
; IMMEDIATE

: CSTR" [CHAR] " PARSE [COMPILE] ZLITERAL ; IMMEDIATE

: ZPLACE ( a u buf -- )   SWAP 2DUP + 0 SWAP C! CMOVE ;    

: +ZPLACE ( a u buf -- )  ASCIIZ> + ZPLACE ;    

: zcount ( addr -- addr u ) dup string::strlen ;

: ztype ( addr -- ~ ) zcount type ;

\ Copy the string C-ADDR/U1 to C-ADDR2 and append a NUL.
: PLACE-CSTR  ( c-addr1 u1 c-addr2 -- )
    2dup 2>r          ( c-addr1 u1 c-addr2 )  ( r: u1 c-addr2 )
    swap cmove        ( ) ( r: u1 c-addr2 )
    0 2r> + c!        ( )
;

: ZPLACE  { caddr u dst -- , move string and null terminate it }
    caddr dst u cmove
    0 dst u + c!
;

privatize
