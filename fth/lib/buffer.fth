\
\ Buffer - growable string
\
\ This should probably be merged with Line class.
\
anew task-lib/buffer.fth

trace-include @ [if]
cr ." Compiling lib/buffer.fth... "
[then]

require sys/memory.fth

1024 1024 * constant BUFSIZE 

:STRUCT BUFFER
    APTR BUFFER-data        \ allocated memory.
    UINT BUFFER-size        \ size of allocated memory.
    UINT BUFFER-length      \ length of BUFFER-data (can be smaller than BUFFER-size).
;STRUCT

: BUFFER.new { | buf -- BUFFER }
    sizeof() BUFFER MEM.malloc -> buf 
    BUFSIZE MEM.malloc buf s! BUFFER-data 
    0 buf s! BUFFER-length 
    BUFSIZE buf s! BUFFER-size 
    quiet true <> if cr ." created new BUFFER " buf .hex buf s@ buffer-data .hex then
    buf 
;

: BUFFER.dump ( buf -- , human friendly dump of BUFFER )
    cr ." BUFFER $" dup .hex
    cr ."    data $" dup s@ buffer-data .hex
    cr ."    length " dup s@ buffer-length .
    cr ."    size " s@ buffer-size .
;

: BUFFER.grow { buf needed -- , grow buffer to 2x BUFFER-size, if needed bytes }
    buf s@ BUFFER-length needed + 
    buf s@ BUFFER-size > if
        buf s@ BUFFER-size 
        2* 
        buf s! BUFFER-size
        cr ." buffer resize " buf @ BUFFER-size .
        buf s@ BUFFER-data dup s@ BUFFER-size
        MEM.realloc
        buf s! BUFFER-data
    then
;

: BUFFER.type { buf -- , type buffer to stdout }
    cr buf s@ BUFFER-data buf s@ BUFFER-length type
;

: BUFFER.destroy ( buf -- , release resources )
    dup s@ BUFFER-data mem::free
    mem::free
;

: BUFFER.write-char { buf c -- , write character to buffer }
    buf 1 BUFFER.grow
    c buf s@ BUFFER-data buffer s@ BUFFER-length + c! 
    buf dup s@ BUFFER-length 1+ s! BUFFER-length
;

: BUFFER.write-byte { buf _byte | ptr -- , write byte to BUFFER }
    buf 1 BUFFER.grow
    buf s@ BUFFER-length buf s@ BUFFER-data + -> ptr
    _byte ptr !
    buf s@ BUFFER-length 1+ buf s! BUFFER-length 
;

: BUFFER.recv { buf fd | n -- actual err , receive bytes from fd into BUFFER }
    buf BUFSIZE BUFFER.grow

    0  buf s@ BUFFER-data bufsize fd cr ." net::recv" .s net::recv  -> n
    n buf s! BUFFER-length
    n
    swap
;

: BUFFER.send { buf fd -- , send buffer contents to fd }
    buf s@ buffer-data buf s@ buffer-length fd sys::write
    dup cr ." sent " . ." bytes"
;


: BUFFER.write-string { buf caddr u | ptr -- , write string to BUFFER }
    buf u BUFFER.grow
    buf s@ BUFFER-length 
    buf s@ BUFFER-data +  
    -> ptr

    \ cmove src dst count
    caddr u         \ src count
    ptr             \ src count dst
    swap            \ src dst count
    cmove 
    buf s@ BUFFER-length u + buf s! BUFFER-length 
;

: BUFFER.writeln-string { buf caddr u -- , write string with cr/lf }
    buf u 2+ BUFFER.grow 

    buf caddr u BUFFER.write-string
    buf 13 BUFFER.write-byte
    buf 10 BUFFER.write-byte
;

: BUFFER.read { buf size fd | ptr -- actual, read bytes from fd into buffer }
    buf size BUFFER.grow
    buf s@ BUFFER-length buf s@ BUFFER-data + -> ptr
    ptr size fd sys::read
;

: BUFFER.read-file { buf fd | size ptr -- actual, read bytes from fd into buffer }
    buf size BUFFER.grow
    buf s@ BUFFER-length buf s@ BUFFER-data + -> ptr
    ptr size fd sys::read
;

trace-include @ [if]
cr ." END buffer.fth" 
[then]
