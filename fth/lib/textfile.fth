\
\ Manage text file into linked list of lines
\

anew task-lib/textfile.fth

require sys/memory.fth
require sys/stdio.fth
require lib/lists.fth
require lib/line.fth

:STRUCT TextFile 
    APTR TextFile.filename
    APTR TextFile.message
    struct List TextFile.content
;STRUCT

: TextFile.list { tf -- head , get head of TextFile }
    tf TextFile.content +
;

create tf-pad 4096 allot

: TextFile.set-filename { tf $name -- , }
    tf s@ TextFile.filename ?dup if
        mem::free
    then
    $name count tf-pad place-cstr
    tf-pad string::strdup tf s! TextFile.filename
;

: TextFile.new { $name | tf -- , tf }
    sizeof() TextFile mem::malloc -> tf
    tf TextFile.list List.init 
    0 tf s! TextFile.filename
    0 tf s! TextFile.message
    tf $name TextFile.set-filename
    tf
;

: TextFile.clear { tf | head lin nxt -- , free file contents }
    tf TextFile.list -> head
    head -> lin
    begin
        lin s@ ListNode.next -> lin
        lin s@ ListNode.next -> nxt
        lin head <>
    while
        lin List.remove
        lin Line.destroy
        nxt -> lin
    repeat
;

: TextFile.destroy { tf -- , free resources used by tf }
    tf TextFile.clear
    tf s@ TextFile.filename mem::free
    tf mem::free
;

: TextFile.read { tf caddr u | fp lin lst -- f , read file contents to end of TextFile.content }
    tf TextFile.list -> lst
    c" r " caddr u stdio.fopen -> fp
    fp 0= if
        false exit
    then

    begin 
        fp stdio.feof not
    while
        pad 4096 fp stdio.fgets
        0<> if
            pad Line.new -> lin
            lin lst List.AddTail
        then
    repeat
    fp stdio.fclose drop
    true
;

: TextFile.revert { tf -- f , revert file/read TextFile.filename  }
    tf TextFile.clear
    tf tf s@ TextFile.filename zcount 
    TextFile.read
;

: TextFile.first { tf | lst lin -- lin|0 , get first line of TextFile }
    tf TextFile.list -> lst
    lst s@ ListNode.next -> lin
    lin lst <> if
        lin
    else
        0
    then
;

: TextFile.last { tf | lst lin -- lin|0 , get first line of TextFile }
    tf TextFile.list -> lst
    lst s@ ListNode.prev -> lin
    lin lst <> if
        lin
    else
        0
    then
;
