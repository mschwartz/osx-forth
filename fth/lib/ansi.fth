\
\ ansi escape sequences
\

private{
[undefined] ANSI_ESCAPE [if]
    $ 1B constant ANSI_ESCAPE
[then]

: TO-STRING ( n -- addr c )  s>d <# #s #> ;

[undefined] esc[ [if]
: ESC[ ( send ESCAPE and [ )
    ansi_escape  emit
    ascii [ emit
;
[then]
}private

: ansi.cls ( -- , clear screen using ansi escape sequence )
    ESC[ ." 2J"
;
: ansi.clear-scrollback ( -- , clear scrollback history )
    ESC[ ." 3J"
;

: ansi.move-to  { row col -- , move cursor to row,col }
    ESC[ 
        row TO-STRING type
        ascii ; emit
        col TO-STRING type
        ascii H emit
;

: ansi.newline ( -- , emit newline )
    $ 0a emit
;

privatize
