\
\ phred editor
\
\ programmed by Mike Schwartz
\
\ Command Line handling
\   :git
\

anew task-phred/commands/git.fth

: CommandLine.git { w str tok toku -- flg , return true if executed }
    tok toku s" git" compare 0= if
        w Gited.begin
        w Window.gited-mode
        w Window.paint
        w Window.refresh
        true exit
    then
    tok toku s" push" compare 0= if
        git.push
        w Gited.begin
        w Window.gited-mode
        true exit
    then

    tok toku s" commit" compare 0= if
        str CString.advance-index
        str CString.skip-blanks
        w str CString.rest  
        git.commit
        w Gited.begin
        w Window.gited-mode
        true exit
    then

    false
;
