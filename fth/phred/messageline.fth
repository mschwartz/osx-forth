\
\ phred editor
\
\ programmed by Mike Schwartz
\
\ Message Line
\

: (MessageLine.move-to)
    ncurses-window-reset
    theScreen s@ Screen.rows 1- 0 ncurses.move
;
' (MessageLine.move-to) is MessageLine.move-to

: (MessageLine.clear)
    ncurses-window-reset
    theScreen s@ Screen.rows 1- 0 ncurses.move
    ncurses.cleartoeol
    ncurses.refresh
;
' (MessageLine.clear) is MessageLine.clear

: (MessageLine.$type) ( $msg -- , print message line )
    ncurses-window-reset
    theScreen s@ Screen.rows 1- 0 ncurses.move
    count type
    ncurses.cleartoeol
    ncurses.refresh
;
' (MessageLine.$type) is Messageline.$type

: (MessageLine.start) ( -- , start printing to message line )
    MessageLine.move-to
;
' (MessageLine.start) is MessageLine.start

: (MessageLine.end) ( -- , end printint go message line )
    ncurses.cleartoeol
    ncurses.refresh
;
' (MessageLine.end) is MessageLine.end

: (MessageLine.format) ( ...tags -- , print message line w/tags )
\     MessageLine.start
    begin
        case 
            TAG_END of
\                 MessageLine.end
                exit
            endof
            TAG_NUMBER of
                .
            endof
            TAG_NUMBERX of
                .hex
            endof
            TAG_$STRING of
                $type
            endof
            TAG_STRING of
                type
            endof
            TAG_CHAR of
                emit
            endof
            TAG_CHARX of
                .hex
            endof
            TAG_KEY of
                keymap.key drop
            endof
        endcase
    again
;
' (MessageLine.format) is MessageLine.format

: (messageline{)
    theScreen s@ Screen.rows 1- 0 ncurses.move
    WindowList.active-window Theme.messageline
    ."  "
    ncurses.refresh
;
' (messageline{) is messageline{

: (}messageline)
    ."  " 
\     ncurses.cleartoeol
    WindowList.active-window Theme.default
;
' (}messageline) is }messageline

: (errorline{)
    theScreen s@ Screen.rows 1- 0 ncurses.move
    WindowList.active-window Theme.errorline
    ."  "
    ncurses.refresh
;
' (errorline{) is errorline{

: (}errorline)
    ."  " 
\     ncurses.cleartoeol
    WindowList.active-window Theme.default
;
' (}errorline) is }errorline
