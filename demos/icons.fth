#!/usr/local/bin/pforth_standalone -q

require lib/icons.fth

: demo
    $ ffff $ f000 do
        cr i .hex i emit-icon
    loop
    cr cr
;
demo