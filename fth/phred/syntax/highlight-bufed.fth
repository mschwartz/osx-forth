\
\ PHRed editor
\
\ programmed by Mike Schwartz
\
\ Bufed Syntax Highlighting
\

: Highlight.bufed { l | bnode str -- highlighted }
    l s@ Line.data -> bnode
    l s@ Line.text CString.new -> str
    bnode if
        regex.default
        bnode Buffer.hidden? not if
            bnode Buffer.modified? if
                str c" ^" s\" \x90\z" CString.$regex_replace drop
            else
                str c" ^" s\" \x8c\z" CString.$regex_replace drop
            then
        then
    then
    str CString.zstring string::strdup 
    str CString.destroy
;