anew task-net/socket.fth

require sys/memory.fth
require net/netdb.fth
require net.fth

:STRUCT TCPSocket
    INT TCPSocket.fd     \ fd of buffered socket
    APTR TCPSocket.fp    \ FILE* of buffered socket
    APTR TCPSocket.ip    \ address of remote
;STRUCT

defer TCPSocket.dump

: TCPSocket.raw { ip fd | client -- sock , create TCPSocket from fd }
    sizeof() TCPSocket MEM.malloc -> client
    fd client s! TCPSocket.fd
    c" r+b" client s@ TCPSocket.fd sys::fdopen 
    client s! TCPSocket.fp
    ip client s! TCPSocket.ip
    client
;

: TCPSocket.client { caddr u  port | ip fd sockaddr client -- client , create new TCPSocket }
    sizeof() TCPSocket MEM.malloc -> client
    0 SOCK_STREAM AF_INET NET.socket  -> fd

    fd client s! TCPSocket.fd

    cr cr cr ." lookup host " caddr u type ." ..."
    caddr 1- net::lookuphostname -> ip
    ." ip = " ip .ip-address ip ."  hex " .hex

    \ connect to socket
    AF_INET ip port             \ family address port
    net::allocsockaddr           \  -- address sizeof
    swap -> sockaddr drop

    cr ." connecting..."
    sockaddr fd net::connect 
    0 < if
        drop
        sockaddr mem::free
        cr c" *** Error in connect() "  sys::perror
        0
        exit
    then
    ." TCPSocket connected, fd = " fd .  cr cr

    fd sys::TCP_NODELAY
    0< if
        c" no_delay " sys::perror
    then

    c" r+b" fd sys::fdopen client s! TCPSocket.fp

\     cr ." client" client TCPSocket.dump

    client
;

: TCPSocket.destroy { sock -- , destroy socket }
    sock s@ TCPSocket.fd sys::close drop
    sock MEM.free
;

: TCPSocket.server { addr port | server fd sockaddr option -- server }
    sizeof() TCPSocket MEM.malloc -> server
    cr ." allocated server " server .hex

    0 SOCK_STREAM AF_INET net::socket
    dup 0 < if
        drop
        cr ." *** Error in socket()" sys::perror
        exit
    then
    -> fd
    ." ok fd = " fd .
    fd sys::TCP_NODELAY
    0< if
        c" TCP_NODELAY" sys::perror
    then
    fd server s! TCPSocket.fd

    1 -> option
    option SOL_SOCKET SO_REUSEPORT fd net.setsockopt
    0 <> if
        cr c" *** setsockopt(SO_REUSEPORT) " perror
    then

    1 -> option
    option SOL_SOCKET SO_REUSEADDR fd net.setsockopt 
    0 <> if
        cr c" *** setsockopt(SO_REUSEADDR) " perror
    then

    \ bind socket
    cr ." binding socket... ip " addr .ip-address ."  port " port .
    AF_INET addr port net::allocsockaddr -> sockaddr
    sockaddr swap fd net.bind
    0 < if
        cr c" *** Error in bind()" perror
        ." *** Cannot continue" cr
        abort
    then
    ." bound socket!"

    10 fd net.listen
    0 < if
        cr c" *** Error in listen()" perror
        exit
    then
    cr ." listening on port " PORT .

    server
;

: TCPSocket.flush { sock -- f , flush buffered output }
    sock s@ TCPSocket.fp sys::fflush
    0< if 
        cr c" fflush " sys::perror
        false
    then
    true
;

: TCPSocket.accept { sock | sockfd fd remote -- remote , accept a connection from sock }
    sock s@ TCPSocket.fd  -> sockfd

    LOCK_EX sockfd sys::flock drop
    sock s@ TCPSocket.fd net.accept -> fd -> remote
    LOCK_UN sockfd sys::flock drop

    fd 0<  if
        c" accept() " perror
        0
        exit
    then

    remote s@ sin_addr fd TCPSocket.raw -> remote
    fd sys::TCP_NODELAY
    0< if
        c" ACCEPT NODELAY" sys::perror
    then
    remote
;

: TCPSocket.eof? { sock -- f , test if socket is at end of file / closed }
    sock s@ TCPSocket.fp  sys::feof
;

: TCPSocket.read-line { caddr u sock | fp -- addr , read line from socket }
    sock s@ TCPSocket.fp -> fp
    caddr 1+ u 1- fp sys::fgets 
;

: TCPSocket.read { caddr u sock | fp -- actual , read bytes from socket }
    sock s@ TCPSocket.fp -> fp
    caddr 1 u fp sys::fread
;

: TCPSocket.write { caddr u sock | fp -- actual , write bytes to socket }
    sock s@ TCPSocket.fp -> fp
    fp sys::fflush drop
    caddr 1 u fp sys::fwrite
    fp sys::fflush drop
;

: TCPSocket.write-char { c sock -- n , actual , write character to socket }
    c sock s@ TCPSocket.fp sys::fputc
;

: TCPSocket.newline { sock | fp -- n , write newline to socket }
    sock s@ TCPSocket.fp -> fp 
    13 fp sys::fputc  0< if -1 exit then
    10 fp sys::fputc 0< if -1 exit then
    2
;

: TCPSocket.$writeln { $caddr sock | fp a -- actual , write bytes to socket }
    sock s@ TCPSocket.fp -> fp 
    $caddr count sock TCPSocket.write -> a
    sock TCPSocket.newline
    a +
;

: TCPSocket.writeln { caddr u sock | fp a -- actual , write bytes to socket }
    sock s@ TCPSocket.fp -> fp 
    caddr u sock TCPSocket.write -> a
    sock TCPSocket.newline
    a +
;

: (TCPSocket.dump) { sock } 
    cr ." DUMP of TCPSocket " sock .hex
    cr ." .fd = " sock s@ TCPSocket.fd .
    cr ." .fp = " sock s@ TCPSocket.fp .hex
    cr ." .eof = " sock TCPSocket.eof? .
;

' (TCPSocket.dump) is TCPSocket.dump