#!/usr/local/bin/pforth_standalone -q

require sys/regex.fth

: line c" bar foo bar foo foo bar bar" ;
: pattern c" foo";

: demo { | re rm -- , demonstrate regex }
    
    cr cr ." allocating  regex_t... "
    alloc_regex_t -> re
    re ." allocated: " .hex

    cr cr ." compiling regex for pattern '"  pattern $type ." '... " 
    re pattern REG_MINIMAL REG_ENHANCED OR regcomp
    ." compiled: " .hex

    cr cr ." allocating  regmatch_t... "
    2 alloc_regmatch_t -> rm
    rm ." allocated: ".hex

    cr cr ." matching pattern in line '" line $type ." '... "
    re line rm 2 0 regexec
    ." matched?  " .hex

    cr cr ." RESULT: "
    cr line $type
    cr rm s@ regmatch_t.rm_so 1- spaces ." ^" cr

    rm regmatch_t.dump

    z" abc" 1+ regex::regex_new -> re
    z" 123abc456" 1+ re z" def" 1+ regex::regex_replace
    ." replaced " 100 dump cr

    cr cr ." stack at exit " .s
\     rm 100 dump
;

demo
cr
