\
\ nix-forth date/time methods
\

anew task/lib-date.fth

require lib/strings.fth

private{
    create date-pad 64 allot
}private

:STRUCT Date
    LONG Date.timestamp
    APTR Date.dayname
    APTR Date.monthname
    LONG Date.month
    LONG Date.day
    LONG Date.year
    LONG Date.hour
    LONG Date.minute
    LONG Date.seconds
    LONG Date.milliseconds
    APTR Date.ctime
;STRUCT

: Date.dump { d -- , dump date to screen }
    cr ." Date $" d .hex
    cr ."          .ctime "  d s@ Date.ctime ztype \ 20 dump
    cr ."      .timestamp " d s@ Date.timestamp .
    cr ."        .dayname " d s@ Date.dayname ztype
    cr ."      .monthname " d s@ Date.monthname ztype
    cr ."          .month " d s@ Date.month .
    cr ."            .day " d s@ Date.day .
    cr ."           .year " d s@ Date.year .
    cr ."           .hour " d s@ Date.hour .
    cr ."         .minute " d s@ Date.minute .
    cr ."        .seconds " d s@ Date.seconds .
    cr ."   .milliseconds " d s@ Date.milliseconds .
;

: Date.getmonthnumber { caddr u -- n }
    caddr u s" Jan" compare 0= if 1  exit then
    caddr u s" Feb" compare 0= if 2 exit then
    caddr u s" Mar" compare 0= if 3 exit then
    caddr u s" Apr" compare 0= if 4 exit then
    caddr u s" May" compare 0= if 5 exit then
    caddr u s" Jun" compare 0= if 6 exit then
    caddr u s" Jul" compare 0= if 7 exit then
    caddr u s" Aug" compare 0= if 8 exit then
    caddr u s" Sep" compare 0= if 9 exit then
    caddr u s" Oct" compare 0= if 10 exit then
    caddr u s" Nov" compare 0= if 11 exit then
    caddr u s" Dec" compare 0= if 12 exit then
    0
;

: Date.getmonthname { n -- caddr u }
    n case
        1 of s" Jan" endof
        2 of s" Feb" endof
        3 of s" Mar" endof
        4 of s" Apr" endof
        5 of s" May" endof
        6 of s" Jun" endof
        7 of s" Jul" endof
        8 of s" Aug" endof
        9 of s" Sep" endof
        10 of s" Oct" endof
        11 of s" Nov" endof
        12 of s" Dec" endof
    endcase
    s" ???"
;


: Date.from-timestamp { d timestamp | str -- , fill in Date fields }
    timestamp 1000 / time::ctime string::strdup -> str
    0 str string::strlen str + c! \ remove trailing newline
    str d s! Date.ctime
    str 3 pad place-cstr pad string::strdup d s! Date.dayname
    str 4 +  3 pad place-cstr pad string::strdup d s! Date.monthname
    d s@ Date.monthname 3 Date.getmonthnumber
    d s! Date.month
    str 7 + zcount 
    skip-blanks parse-number
    d s! Date.day 
    skip-blanks parse-number
    d s! Date.hour
    ascii : skip-token
    parse-number d s! Date.minute
    ascii : skip-token
    parse-number d s! Date.seconds
    timestamp 1000 /mod drop d s! Date.milliseconds
    skip-blanks parse-number
    d s! Date.year
    2drop
;

: Date.new-from-timestamp { timestamp | d -- date , new date object }
    sizeof() Date mem::malloc -> d
    timestamp d s! Date.timestamp
    d timestamp Date.from-timestamp
    d
;

: Date.new { | d timestamp -- date , new date object }
    sizeof() Date mem::malloc -> d
    time::time-ms -> timestamp
    timestamp d s! Date.timestamp
    d timestamp Date.from-timestamp
    d
;

: Date.now { -- timestamp , get current time timestamp in milliseconds }
    time::time-ms
;

: to-number { n -- , }
    n (u.) 
    date-pad $append
    0 date-pad count +  c!
;

: add-number { n -- , }
    n 1000 > if
        n to-number
        exit
    then
    n 10 < if
        0 to-number
    then
    n to-number
;

: Date.hh:mm:ss { d -- caddr u , format time }
    s" " date-pad place
    d s@ Date.hour add-number
    date-pad ascii : $append.char 
    d s@ Date.minute add-number
    date-pad ascii : $append.char
    d s@ Date.seconds add-number
    date-pad count
;

: Date.hh:mm:ss.ms { d -- caddr u , format time w/ms }
    d Date.hh:mm:ss 2drop
    date-pad ascii . $append.char
    d s@ Date.milliseconds add-number
    date-pad count
;


: Date.mm-dd-yy { d -- caddr u , format date }
    s" " date-pad place
    d s@ Date.month add-number
    date-pad ascii - $append.char 
    d s@ Date.day add-number
    date-pad ascii - $append.char
    d s@ Date.year add-number
    date-pad count 1+
;
: Date.mm/dd/yy { d -- caddr u , format date }
    s" " date-pad place
    d s@ Date.month add-number
    date-pad ascii / $append.char 
    d s@ Date.day add-number
    date-pad ascii / $append.char
    d s@ Date.year 
    add-number

    date-pad count 1+

;

privatize