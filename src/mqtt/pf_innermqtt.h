#ifdef PF_SUPPORT_MQTT

// see https://mosquitto.org/api/files/mosquitto-h.html

#define QOS 0

case ID_MQTT_lib_version: { // ( -- major minor revision )
  int major, minor, revision;
  mosquitto_lib_version(&major, &minor, &revision);
  PUSH_TOS;
  TOS = (cell_t)major;
  PUSH_TOS;
  TOS = (cell_t)minor;
  PUSH_TOS;
  TOS = (cell_t)revision;
}
  endcase;

case ID_MQTT_lib_init: { // ( -- err , initialize MQTT)
  PUSH_TOS;
  TOS = (cell_t)mosquitto_lib_init();
}
  endcase;

case ID_MQTT_lib_cleanup: { // ( -- )
  PUSH_TOS;
  TOS = (cell_t)mosquitto_lib_cleanup();
}
  endcase;

case ID_MQTT_new: { // ( id clean-session obj -- handle )
  char *id = (char *)TOS;
  TOS = M_POP;
  bool clean_session = (bool)TOS;
  TOS = M_POP;
  TOS = (cell_t)mosquitto_new(id, clean_session, (void *)TOS);
}
  endcase;

case ID_MQTT_destroy: { // ( handle -- )
  mosquitto_destroy((struct mosquitto *)TOS);
  TOS = M_POP;
}
  endcase;

case ID_MQTT_reinitialize: { // ( handle id clean-session obj -- err )
  void *obj = (void *)TOS;
  TOS = M_POP;
  bool clean_session = (bool)TOS;
  TOS = M_POP;
  char *id = (char *)TOS;
  TOS = M_POP;
  TOS = (cell_t)mosquitto_reinitialise((struct mosquitto *)TOS, id, clean_session, (void *)obj);
}
  endcase;

case ID_MQTT_connect: { // ( handle host port keepalive -- err )
  int keepalive = (int)TOS;
  TOS = M_POP;
  int port = (int)TOS;
  TOS = M_POP;
  char *host = (char *)TOS;
  TOS = M_POP;
  TOS = (cell_t)mosquitto_connect((struct mosquitto *)TOS, host, port, keepalive);
}
  endcase;

case ID_MQTT_reconnect: { // ( handle -- err )
  TOS = (cell_t)mosquitto_reconnect((struct mosquitto *)TOS);
}
  endcase;

case ID_MQTT_disconnect: { // ( handle -- )
  TOS = (cell_t)mosquitto_disconnect((struct mosquitto *)TOS);
}
  endcase;

case ID_MQTT_loop: { // ( handle -- err )
  TOS = (cell_t)mosquitto_loop((struct mosquitto *)TOS, -1, 1);
}
  endcase;

case ID_MQTT_publish: { // ( handle topic payload retain -- err )
  bool retain = (bool)TOS;
  TOS = M_POP;
  char *payload = (char *)TOS;
  TOS = M_POP;
  char *topic = (char *)TOS;
  TOS = M_POP;
  TOS = (cell_t)mosquitto_publish(
    (struct mosquitto *)TOS,
    nullptr, // mid
    topic,
    strlen(payload),
    (void *)payload,
    QOS,
    retain);
}
  endcase;

case ID_MQTT_subscribe: { // ( handle topic -- err )
  char *topic = (char *)TOS;
  TOS = M_POP;
  TOS = (cell_t)mosquitto_subscribe((struct mosquitto *)TOS, nullptr, topic, 0);
}
  endcase;

case ID_MQTT_unsubscribe: { // ( handle topic -- err )
  char *topic = (char *)TOS;
  TOS = M_POP;
  TOS = (cell_t)mosquitto_unsubscribe((struct mosquitto *)TOS, nullptr, topic);
}
  endcase;

case ID_MQTT_strerror: { // ( err# -- text , get MQTT error as string )
  TOS = (cell_t)mosquitto_strerror((int)TOS);
}
  endcase;

case ID_MQTT_init: { // ( -- , initialize MQTT and queue)
  PUSH_TOS;
  TOS = (cell_t)mqttInit();
}
  endcase;
case ID_MQTT_cleanup: { // ( -- , cleanup MQTT and queue )
  mqttCleanup();
}
  endcase;

case ID_MQTT_get_message: { // ( -- msg , wait for message )
  PUSH_TOS;
  TOS = (cell_t)mqttRemoveMessage();
}
  endcase;
case ID_MQTT_has_message: { // ( -- flag , message ready? )
  PUSH_TOS;
  TOS = (cell_t)mqttHasMessage();
}
  endcase;

#endif
