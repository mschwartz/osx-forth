\
\ Shell-like words (ls, etc.)
\

: ls cr z" exa" 1+ sys::system drop ;
: ll cr z" exa -l" 1+ sys::system drop ;
: la cr z" exa -a" 1+ sys::system drop ;
: lla cr z" exa -la" 1+ sys::system drop ;

: commands 
cr
cr ."  AVAILABLE SHELL COMMANDS:"
cr ."   ls='exa'"
cr ."   ll='exa -l'"
cr ."   la='exa -a'"
cr ."   lla='exa -la'"
cr ."   ^D to quit"
cr
;
0 quiet ! 
0 trace-stack !
: my-prompt cr ." shell> " ;
' my-prompt is prompt




