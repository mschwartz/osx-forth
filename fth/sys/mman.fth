anew task-lib/mman.fth

\ protections are chosen from these bits ORed together

$ 0000 constant PROT_NONE             \ no permissions
$ 0001 constant PROT_READ             \ pages can be read
$ 0002 constant PROT_WRITE            \ pages can be written
$ 0003 constant PROT_EXEC             \ pages can be executed

\ sharing type and options
$ 0001 constant MAP_SHARED          \ share changes
$ 0002 constant MAP_PRIVATE         \ changes are private
MAP_PRIVATE constant MAP_COPY

\ Other flags
$ 0010 constant MAP_FIXED           \ interpret addr exactly
$ 0020 constant MAP_RENAME          \ Sun: rename private pages to file
$ 0040 constant MAP_NORESERVE       \ Sun: don't reserve needed swap area
$ 0080 constant MAP_RESERVED0080    \ previously unimplemented MAP_INHERIT
$ 0100 constant MAP_NOEXTEND        \ for MAP_FILE don't change file size
$ 0200 constant MAP_HASSEMAPHORE    \ region may contain semaphores
$ 0400 constant MAP_NOCACHE         \ don't cache pages for this mapping
$ 0800 constant MAP_JIT             \ Allocate a regioun that will be used for JIT purposes

\ Mapping type
$ 0000 MAP_FILE                     \ Map from file (default)
$ 1000 MAP_ANON                     \ allocated from memory, swap space
MAP_ANON constant MAP_ANONYMOUS

0 [if]
/*
 * The MAP_RESILIENT_* flags can be used when the caller wants to map some
 * possibly unreliable memory and be able to access it safely, possibly
 * getting the wrong contents rather than raising any exception.
 * For safety reasons, such mappings have to be read-only (PROT_READ access
 * only).
 *
 * MAP_RESILIENT_CODESIGN:
 *      accessing this mapping will not generate code-signing violations,
 *	even if the contents are tainted.
 * MAP_RESILIENT_MEDIA:
 *	accessing this mapping will not generate an exception if the contents
 *	are not available (unreachable removable or remote media, access beyond
 *	end-of-file, ...).  Missing contents will be replaced with zeroes.
 */
 [then]

$ 02000 constant MAP_RESILIENT_CODESIGN      \ no code signing failures
$ 04000 constant MAP_RESILIENT_MEDIA         \ no backing-store failures

$ 08000 constant MAP_32BIT                   \ return virtual addresses < 4G only

\ Flags used to support translated processes.
$ 20000 constant MAP_TRANSLATED_ALLOW_EXECUTE \ allow execute in translated processes 
$ 40000 constant MAP_UNIX03                 \ UNIX03 compliance
$ 80000 constant MAP_TPRO                   \ Allocate a region that will be protected by TPRO 

\ Process memory locking
$  0001 constant MCL_CURRENT                \ Lock only current memory
$  0002 constant MCL_FUTURE                 \ Lock all future memory as well

\ Error return from mmap()
-1 constant MAP_FAILED                      \ MF|SHM mmap failed

\ msync() flags
$ 0001 constant MS_ASYNC                    \ return immediately
$ 0002 constant MS_INVALIDATE               \ invalidate all cached data
$ 0010 constant MS_SYNC                     \ mysnc synchronously
$ 0004 constant MS_KILLPAGES                \ invalidate pages, leave mapped
$ 0008 constant MS_DEACTIVATE               \ deactivate pages, leave mapped

\ advice to madvise
 0 constant POSIX_MADV_NORMAL               \ no further special treatment
 1 constant POSIX_MADV_RANDOM               \ expect random page refs
 2 constant POSIX_MADV_SEQUENTIAL           \ expect sequential page refs
 3 constant POSIX_MADV_WILLNEED             \ will need these pages
 4 constant POSIX_MADV_DONTNEED             \ don't need these pages

POSIX_MADV_NORMAL constant MADV_NORMAL
POSIX_MADV_RANDOM constant MADV_RANDOM
POSIX_MADV_SEQUENTIAL constant MADV_SEQUENTIAL
POSIX_MADV_WILLNEED constant MADV_WILLNEED
POSIX_MADV_DONTNEED constant MADV_DONTNEED

 5 constant MADV_FREE                       \ pages unneeded, discard contents
 6 constant MADV_ZERO_WIRED_PAGES           \ zero the wired pages that have not been unwired before the entry is deleted 
 7 constant MADV_FREE_REUSABLE              \ pages can be reused (by anyone) 
 8 constant MADV_FREE_REUSE                 \ caller wants to reuse these pages
 9 constant MADV_CAN_REUSE                  \ 
10 constant MADV_PAGEOUT                    \ page out now (internal only)

\ return bits from mincore
$ 01 constant MINCORE_INCORE                \ page is incore
$ 02 constant MINCORE_REFERENCED            \ page has been referenced by us
$ 04 constant MINCORE_MODIFIED              \ page has been modified by us
$ 08 constant MINCORE_REFERENCED_OTHER      \ page has been referenced
$ 10 constant MINCORE_MODIFIED_OTHER        \ page has been modified
$ 20 constant MINCORE_PAGED_OUT             \ page has been paged out
$ 40 constant MINCORE_COPIED                \ page has been copied
$ 80 constant MINCORE_ANONYMOUS             \ page belongs to an anonymous object
