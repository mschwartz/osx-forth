\
\ git Buffer
\

private{
    variable gited-buf
    256 constant PADSIZE
    create gited-pad PADSIZE allot
    : filter { n -- , filter node }
        n s@ Line.text z" Binary file" 1+ zcount string::strncasecmp 0= if
            n List.remove
            n Line.destroy
        then
    ;
\     : buffer-command s" git status --porcelain" ;
    : buffer-clear { n -- , free resources for line }
        n List.remove
        n Line.destroy
    ;
    : fgets { ptr siz fp | done -- wait for fp ready for timeout of ms }
        false -> done
        begin
            done not
        while
            fp sys::fready if true -> done then
        repeat
        ptr siz fp sys::fgets 0= if false exit then
        true exit
    ;
}private

: Gited.append-command { buf caddr u | tmpnam fp -- , append output of command to buf }
    caddr u  gited-pad place-cstr
    gited-pad z"  >" 1+ string::strcat drop
    z" /tmp/temp.phred.git" 1+ -> tmpnam
    gited-pad tmpnam string::strcat drop
    gited-pad sys::system  drop
    z" r" 1+ tmpnam sys::fopen -> fp
    z" rm /tmp/temp.phred.git"  1+ sys::system drop
    fp 0= if
        sys::strerror zcount gited-pad place

        TAG_END
            gited-pad TAG_STRING
            c" *** Error: " TAG_$STRING
        buf Buffer.format
    else
        begin
            fp sys::feof not
        while
            fp sys::ferror ?dup if
                die{ ." ferror returned " . .s }die
            then
            gited-pad PADSIZE fp fgets 0= if
                fp sys::fclose drop
                exit
            then
            gited-pad c@ 0= if
                fp sys::fclose drop
                exit
            then
            gited-pad buf Buffer.append-text drop
        repeat
        fp sys::fclose drop
    then
;

: Gited.refresh { buf | fp -- , reload Gited buffer }
    buf ['] buffer-clear Buffer.iterate
    gited-help @ if
            TAG_END s"  Git Mode Keys: " TAG_STRING
        buf Buffer.format
            TAG_END s"   a = git add file    " TAG_STRING s"   d = git rm file     " TAG_STRING
        buf Buffer.format
            TAG_END s"   c = git commit file " TAG_STRING s"   p = git push        " TAG_STRING
        buf Buffer.format
        \ git restore --staged file-to-unstage.txt
            TAG_END s"   r = refresh buffer  " TAG_STRING s"   u = git unstage file" TAG_STRING 
        buf Buffer.format
            TAG_END s"  Use :commit [args] to commit changes" TAG_STRING
        buf Buffer.format
            TAG_END s"  Use :push [args] to push changes" TAG_STRING
        buf Buffer.format

    \         TAG_END c" " TAG_$STRING 
    \     buf Buffer.format
    \         TAG_END s" MODIFIED FILES" TAG_STRING
    \     buf Buffer.format
            TAG_END c" " TAG_$STRING
        buf Buffer.format
    then

    buf s" git status --porcelain" gited.append-command
\         TAG_END c" " TAG_$STRING 
\     buf Buffer.format
\         TAG_END s" STAGED FILES" TAG_STRING 
\     buf Buffer.format
\         TAG_END c" " TAG_$STRING 
\     buf Buffer.format
\     buf s" git diff --name-only --cached" gited.append-command
;

: (gited.begin) { w | buf -- , initialize and attach git output }
    s" GIT" BufferList.find -> buf
    buf 0= if
        s" GIT" Buffer.new -> buf
        buf BufferList.add
    then
    Buffer.filetype.git buf s! Buffer.filetype

    buf gited.refresh
    true buf s! Buffer.read-only
    true buf s! Buffer.permanent

    w buf Window.set-buffer
;

' (gited.begin) is gited.begin

privatize
