#!/usr/local/bin/pforth_standalone -q

anew task-demo-list.fth

0 trace-include !
cls 

require lib/lists

cr ." Construct keystone "
List keystone
keystone .hex

cr ." Initialize keystone - "
keystone List.init 
\ keystone List.dump
keystone List.empty? if ." list is empty" else ." list is not empty " then
cr cr

cr ." Constructing demo nodes"

:struct DemoNode 
    struct ListNode d_node
    long d_value
;struct

: node.print 
    cr ."     node " dup .hex
    ."   prev " dup s@ ListNode.prev .hex
    ."   next " dup s@ List.next .hex
    ."   value " dup s@ d_value .
;

\ test insertBefore
20 0 do
    sizeof() DemoNode mem::malloc 
\    dup cr ." DemoNode @" .hex
    dup i swap s! d_value
    keystone List.insertBefore
loop

\ test insertAfter
20 0 do
    sizeof() DemoNode mem::malloc 
\    dup cr ." DemoNode @" .hex
    dup i 100 + swap s! d_value
    keystone List.insertAfter
loop


cr cr ." DUMP LIST"  cr
keystone c" keystone " ListNode.dump
keystone ' node.print List.iterate
cr