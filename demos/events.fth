#!/usr/local/bin/pforth_standalone -q

anew task-demos/events.fth

require lib/eventemitter.fth

: print-event { e -- , print event }
    ." received event " e .hex
    true
;
: demo { | e 'sub -- , }
    cr ." New EventEmitter "
    EventEmitter.new -> e

    cr cr ." testing emit with no subscribers"
    5 0 do
        cr ." emitting " i . ." ... "
        e c" test" i EventEmitter.emit
    loop


    cr cr ." testing emit with subscribers"
    cr ." Subscribe... " 
    ['] print-event -> 'sub
    e c" test" 'sub EventEmitter.subscribe
    ." Subscribed "

    15 0 do
        cr ." emitting " i . ." ... "
        e c" test" i EventEmitter.emit
    loop

    cr cr ." testing emit after unsubscribing"
    cr ." Unsubscribe... " 
    e c" test" 'sub EventEmitter.unsubscribe
    ." Unubscribed "

    5 0 do
        cr ." emitting " i . ." ... "
        e c" test" i EventEmitter.emit
    loop
    cr ." done "
    0 sys::exit
;

demo
cr
cr