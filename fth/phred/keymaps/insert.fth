
: Keymap.insert { w | ch flg -- , run keymap for insert mode }
    false -> flg
    w Window.paint
    w Window.address-cursor   
    begin
        w Window.address-cursor   
        Keymap.key -> ch
        MessageLine.clear
        w Window.address-cursor   
        ch ncurses.ctrl_c <> if
            false -> flg
        then
        case ch
            ncurses.ctrl_c of
                flg if
                    die{ ." ^C " cr }die
                else
                    ncurses.bell
                    c" Hit ^C again to exit" MessageLine.$type
                    true -> flg
                then
            endof
            ncurses.ctrl_w of
                w Keymap.window
                false = if
                    false exit
                then
            endof
            ncurses.ctrl_d of
                w Window.page-down
            endof
            ncurses.ctrl_u of
                w Window.page-up
            endof
            ncurses.key_up of 
                w Window.cursor-up
            endof
            ncurses.key_down of 
                w Window.cursor-down
            endof
            ncurses.key_left of 
                w Window.cursor-left
            endof
            ncurses.key_right of 
                w Window.cursor-right
            endof
            ansi_escape of
                keymap.recording? if
                    ansi_escape Keymap.append-char
                    Keymap.end-recording
                then
                keymap.playing? if
                    keymap.end-playback
                then
                w Window.visual-mode
                exit
            endof
            13 of
                keymap.recording? if
                    13 Keymap.append-char
                then
                w Window.split-at-cursor
            endof
            10 of
                keymap.recording? if
                    10 Keymap.append-char
                then
                w Window.split-at-cursor
            endof
            ncurses.key_delete of     \ delete key
                ncurses.key_delete Keymap.append-char
                \ if at beginning of line, move left and join the lines
                w Window.current-line-index 0= if
                    w Window.cursor-left
                    w false Window.join-lines
                else
                    w Window.cursor-left
                    w Window.read-only? not if
                        w true Window.delete-at-cursor
                    then
                then
                w Window.address-cursor
            endof
            keymap.recording? if
                ch Keymap.append-char
            then
            w ch Window.insert-character
            w Window.address-cursor
            w Window.paint
\             w Window.paint-current-line
        endcase
    again
;
