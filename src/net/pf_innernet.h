#ifdef PF_SUPPORT_NET

case ID_NET_SOCKET: { // ( protocol type domain -- fd , create socket )
  int domain = (int)TOS;
  TOS = M_POP;
  int type = (int)TOS;
  TOS = M_POP;
  TOS = (cell_t)socket(domain, type, (int)TOS);
  //  int option = 1;
  //  printf("\n\nNET_SOCKET setsockopt %ld, %d, %d, %d (%lu)\n", TOS, SOL_SOCKET, SO_REUSEADDR, option, sizeof(option));
  //  setsockopt(TOS, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option));
  //  option = 1;
  //  setsockopt(TOS, SOL_SOCKET, SO_REUSEPORT, &option, sizeof(option));
}
  endcase;
case ID_NET_ACCEPT: { // ( fd -- sockaddr fd | -1 , accept connection )
  int fd = (int)TOS;
  struct sockaddr_in *address = new sockaddr_in;
  bzero(address, sizeof(struct sockaddr));
  socklen_t sockaddr_size = sizeof(sockaddr_in);
  int result = (cell_t)accept(fd, (sockaddr *)address, (socklen_t *)&sockaddr_size);
  TOS = (cell_t)address;
  PUSH_TOS
  TOS = result;
}
  endcase;

case ID_NET_CLOSE: { // ( fd -- , close socket )
  TOS = (cell_t)close((int)TOS);
}
  endcase;
case ID_NET_BIND: { // ( address_len address fd -- error , bind address to socket )
  int fd = (int)TOS;
  TOS = M_POP;
  struct sockaddr *address = (struct sockaddr *)TOS;
  TOS = M_POP;
  TOS = (cell_t)bind(fd, address, (socklen_t)TOS);
}
  endcase;
case ID_NET_CONNECT: { // ( address fd -- error , connect socket )
  int fd = (int)TOS;
  TOS = M_POP;
  TOS = (cell_t)connect(fd, (sockaddr *)TOS, sizeof(struct sockaddr_in));
}
  endcase;
case ID_NET_GETSOCKNAME: { // ( address_let address fd -- error,  get socket name )
  int fd = (int)TOS;
  TOS = M_POP;
  struct sockaddr *address = (struct sockaddr *)TOS;
  TOS = (cell_t)getsockname(fd, address, (socklen_t *)TOS);
}
  endcase;
case ID_NET_GETSOCKOPT: { // ( option_len option_value level optname fd -- error,  get options on socket )
  int fd = (int)TOS;
  TOS = M_POP;
  int optname = (int)TOS;
  TOS = M_POP;
  int level = (int)TOS;
  TOS = M_POP;
  void *option_value = (void *)TOS;
  TOS = (cell_t)getsockopt(fd, level, optname, option_value, (socklen_t *)TOS);
}
  endcase;
case ID_NET_SETSOCKOPT: { // ( option_value level optname fd -- error,  get options on socket )
  int fd = (int)TOS;
  TOS = M_POP;
  int optname = (int)TOS;
  TOS = M_POP;
  int level = (int)TOS;
  TOS = M_POP;
  int option_value = (int)TOS;
  //  printf("\nsetsockopt %d, %d, %d, %d\n", fd, level, optname, option_value);
  TOS = (cell_t)setsockopt(fd, level, optname, &option_value, sizeof(option_value));
}
  endcase;
case ID_NET_GETCONSTANT: { // ( caddr u -- SIO , get )
  extern cell_t pfConstant(char *name);

  Temp = M_POP; /* caddr */
  pfCopyMemory(gScratch, (char *)Temp, (ucell_t)TOS);
  gScratch[TOS] = '\0';
  TOS = (cell_t)pfConstant(gScratch);
}
  endcase;
case ID_NET_IOCTL: { // ( handle cmd arg -- )
  void *arg = (void *)TOS;
  TOS = M_POP;
  int cmd = (int)TOS;
  TOS = M_POP;
  TOS = (cell_t)ioctl((int)TOS, cmd, arg);
}
  endcase;
case ID_NET_LISTEN: { // ( backlog fd -- , listen for connections on a socket )
  int fd = (int)TOS;
  TOS = M_POP;
  TOS = (cell_t)listen(fd, (int)TOS);
}
  endcase;
case ID_NET_READ: { // (  buf size fd -- actual , read size bytes from fd - returns actual number of bytes read )
  int fd = (int)TOS;
  TOS = M_POP;
  size_t size = (size_t)TOS;
  TOS = M_POP;
  TOS = (cell_t)read(fd, (void *)TOS, size);
}
  endcase;
case ID_NET_RECV: { // ( flags buffer length fd -- , receive from fd )
  int fd = (int)TOS;
  TOS = M_POP;
  int length = (int)TOS;
  TOS = M_POP;
  void *buffer = (void *)TOS;
  TOS = (cell_t)recv(fd, buffer, length, (int)TOS);
}
  endcase;
case ID_NET_RECVFROM: { // ( flags length buffer address_len address fd -- , receive from fd )
  int fd = (int)TOS;
  TOS = M_POP;

  struct sockaddr *address = (struct sockaddr *)TOS;
  TOS = M_POP;

  size_t address_len = (size_t)TOS;
  TOS = M_POP;

  void *buffer = (void *)TOS;
  TOS = M_POP;

  int length = (int)TOS;
  TOS = M_POP;

  int flags = (int)TOS;
  TOS = M_POP;

  TOS = (cell_t)recvfrom(fd, buffer, length, flags, address, (socklen_t *)&address_len);
}
  endcase;
case ID_NET_ALLOC_FD_SET: { // (  -- fd_set )
  PUSH_TOS;
  TOS = (cell_t)malloc(sizeof(fd_set));
}
  endcase;
case ID_NET_FREE_FD_SET: { // (  fd_set -- )
  free((void *)TOS);
  TOS = M_POP;
}
  endcase;
case ID_NET_FD_CLR: { // ( fd_set fd  -- , clear fd from fd_set )
  int fd = (int)TOS;
  TOS = M_POP;
  fd_set *set = (fd_set *)TOS;
  TOS = M_POP;
  FD_CLR(fd, set);
}
  endcase;
#ifdef __APPLE__
case ID_NET_FD_COPY: { // ( fd_set-dest fd_set-src  -- , copy fd_set-src to fd_set-dest )
  fd_set *src = (fd_set *)TOS;
  TOS = M_POP;
  fd_set *dst = (fd_set *)TOS;
  TOS = M_POP;
  FD_COPY(src, dst);
}
  endcase;
#endif
case ID_NET_FD_ISSET: { // ( fd_set fd  -- f , test if fd is in fd_set )
  int fd = (int)TOS;
  TOS = FD_ISSET(fd, (fd_set *)TOS);
}
  endcase;
case ID_NET_FD_SET: { // ( fd_set fd  -- , set fd in fd_set )
  int fd = (int)TOS;
  TOS = M_POP;
  fd_set *set = (fd_set *)TOS;
  TOS = M_POP;
  FD_SET(fd, set);
}
  endcase;
case ID_NET_FD_ZERO: { // ( fd_set -- , zero fd_set )
  fd_set *set = (fd_set *)TOS;
  TOS = M_POP;
  FD_ZERO(set);
}
  endcase;
case ID_NET_SELECT: { // ( timeout errorfds writefds readfds nfds -- nready )
  int nfds = (int)TOS;
  TOS = M_POP;
  fd_set *readfds = (fd_set *)TOS;
  TOS = M_POP;
  fd_set *writefds = (fd_set *)TOS;
  TOS = M_POP;
  fd_set *errorfds = (fd_set *)TOS;
  TOS = M_POP;
  struct timeval *timeout = (struct timeval *)TOS;
  TOS = (cell_t)select(nfds, readfds, writefds, errorfds, timeout);
}
  endcase;
case ID_NET_SEND: { // ( flags length buffer fd -- )
  int fd = (int)TOS;
  TOS = M_POP;
  void *buffer = (void *)TOS;
  TOS = M_POP;
  int length = (int)TOS;
  TOS = (cell_t)send(fd, buffer, length, (int)TOS);
}
  endcase;
case ID_NET_SENDMSG: { // ( flags message fd -- )
  int fd = (int)TOS;
  TOS = M_POP;
  struct msghdr *message = (struct msghdr *)TOS;
  TOS = M_POP;
  TOS = (cell_t)sendmsg(fd, message, (int)TOS);
}
  endcase;
case ID_NET_SENDTO: { // ( dest_len dest flags length buffer fd -- num , send to )
  int fd = (int)TOS;
  TOS = M_POP;
  void *buffer = (void *)TOS;
  TOS = M_POP;
  int length = (int)TOS;
  TOS = M_POP;
  struct sockaddr *dest = (struct sockaddr *)TOS;
  TOS = M_POP;
  int dest_len = (int)TOS;
  TOS = (cell_t)sendto(fd, buffer, length, (int)TOS, dest, dest_len);
}
  endcase;
case ID_NET_SHUTDOWN: { // ( how fd -- , shudown part of a full-duplex connection )
  int fd = (int)TOS;
  TOS = M_POP;
  TOS = (cell_t)shutdown(fd, (int)TOS);
}
  endcase;
case ID_NET_SOCKETPAIR: { // ( domain type protocol -- fd1 fd2 rc , create socket pair )
  int protocol = (int)TOS;
  TOS = M_POP;
  int type = (int)TOS;
  TOS = M_POP;
  int domain = (int)TOS;
  TOS = M_POP;
  int socket_vector[2];
  int rc = socketpair(domain, type, protocol, socket_vector);
  TOS = (cell_t)socket_vector[0];
  PUSH_TOS;
  TOS = (cell_t)socket_vector[1];
  PUSH_TOS;
  TOS = (cell_t)rc;
}
  endcase;
case ID_NET_WRITE: { // ( buffer length fd -- n , write )
  int fd = (int)TOS;
  TOS = M_POP;
  size_t length = (size_t)TOS;
  TOS = M_POP;
  TOS = (cell_t)write(fd, (void *)TOS, length);
}
  endcase;
case ID_NET_SENDFILE: { // ( caddr fd -- err, send file named by caddr to fd )
  int fd = TOS;
  TOS = M_POP;
  ForthStringToC(gScratch, (char *)TOS, sizeof(gScratch));
  struct stat buf;
  if (stat(gScratch, &buf) < 0) {
    TOS = -1;
    break;
  }

  int s = open(gScratch, O_RDONLY);
  if (s < 0) {
    TOS = -1;
    break;
  }

  size_t len = (size_t)buf.st_size;
#ifdef __APPLE__
  int ret = (cell_t)sendfile(fd, s, (off_t)0, (off_t *)&len, (struct sf_hdtr *)NULL, (int)TOS);
#else
  int ret = (cell_t)sendfile(fd, s, (off_t)0, (size_t)TOS);
#endif
  //  int ret = (cell_t)sendfile(fd, s, (off_t)0, (off_t *)&len, (struct sf_hdtr *)NULL, (int)TOS);
  TOS = len;
  PUSH_TOS;
  TOS = (cell_t)ret;
}
  endcase;
case ID_NET_ALLOC_SOCKADDR: { //  ( family address port -- sockaddr sizeof(sockaddr) )
  struct sockaddr_in *sockaddr = (struct sockaddr_in *)malloc(sizeof(sockaddr_in));
  bzero(sockaddr, sizeof(struct sockaddr_in));
  sockaddr->sin_port = htons(TOS);
  TOS = M_POP;
  sockaddr->sin_addr.s_addr = TOS;
  TOS = M_POP;
  sockaddr->sin_family = (int)TOS;
  TOS = (cell_t)sockaddr;
  PUSH_TOS;
  TOS = sizeof(*sockaddr);
}
  endcase;
case ID_NET_FREE_SOCKADDR: { // ( sockaddr -- , free sockaddr_in struct )
  free((void *)TOS);
  TOS = M_POP;
}
  endcase;
#ifdef __APPLE__
case ID_NET_HTONLL: { // ( hostlonglong -- netlonglong )
  TOS = (cell_t)htonll((uint64_t)TOS);
}
  endcase;
#endif
case ID_NET_HTONL: { // ( hostlong -- netlong )
  TOS = (cell_t)htonl((uint32_t)TOS);
}
  endcase;
case ID_NET_HTONS: { // ( hostshort -- netshort )
  TOS = (cell_t)htons((uint16_t)TOS);
}
  endcase;
#ifdef __APPLE__
case ID_NET_NTOHLL: { // ( netlonglong -- hostlonglong )
  TOS = (cell_t)ntohll((uint64_t)TOS);
}
  endcase;
#endif
case ID_NET_NTOHL: { // ( netlong -- hostlong )
  TOS = (cell_t)ntohl((uint32_t)TOS);
}
  endcase;
case ID_NET_NTOHS: { // ( netshort -- hostshort )
  TOS = (cell_t)ntohs((uint16_t)TOS);
}
  endcase;
case ID_NET_GETHOSTBYNAME: { // ( name -- hostent )
  ForthStringToC(gScratch, (char *)TOS, sizeof(gScratch));
  TOS = (cell_t)gethostbyname(gScratch);
}
  endcase;
case ID_NET_LOOKUPHOSTNAME: { // ( name -- ip address )
  ForthStringToC(gScratch, (char *)TOS, sizeof(gScratch));
  hostent *he = gethostbyname(gScratch);
  // printf("IP address: %s\n", inet_ntoa(*(struct in_addr *)he->h_addr));
  struct in_addr *p = (struct in_addr *)he->h_addr;
  TOS = (cell_t)p->s_addr;
}
  endcase;
case ID_NET_GETHOSTBYADDR: { // ( type caddr u  -- hostent )

  // TODO: implement this
  TOS = M_POP;
  TOS = M_POP;
  TOS = 0;
}
  endcase;

#endif
