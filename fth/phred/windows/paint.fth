\
\ PHRed editor
\
\ programmed by Mike Schwartz
\
\ Editor Window Painting/rendering functions
\

private{ 
: in-selection? { n -- flg , is n within selection }
    n selection-start# @ < if false exit then
    n selection-end# @ > if false exit then
    true
;
}private

: Window.erase-eol { w | x y -- , erase to end of line }
    ncurses::stdscr ncurses.getyx -> x -> y
    w s@ Window.width w s@ Window.left + x do
        bl emit
    loop
    y x ncurses.move
;

: Window.paint-line { l w n | b txt -- f , paint line }
    l 0= if
        w Window.erase-eol
        exit
    then

    w s@ Window.buffer -> b

    w s@ Window.current-line l = if
        w WindowList.active-window = if
            ncurses::stdscr Theme.current-line
        then
    then

    w Window.erase-eol

    \ gutter
    w s@ Window.type case
        WindowType.nerdtree of endof
        n 5 .r 2 spaces
    endcase

    \ highlight selection if in one
    n in-selection? if  
        ncurses::stdscr Theme.selection
    then

    \ line
    l s@ Line.text Window.line-length
    w s@ Window.offset-left < if
        w Window.erase-eol
    else
        l w s@ Window.current-line = if
            l s@ Line.text string::strdup -> txt
        else
            b l Highlight.line -> txt
        then
        txt search-pad Highlight.search -> txt
            w s@ Window.offset-left 
            if
                txt w s@ Window.offset-left String.skip
            else 
                txt
            then

            w s@ Window.width GUTTER-WIDTH -                \ max-col

        String.print
        txt MEM.free
        ncurses::stdscr Theme.default
    then
;

: Window.paint-current-line { w | l cursor -- , paint current line only }
    \ l w n
    w Window.address-cursor
    w s@ Window.current-line -> l
    l w w s@ Window.line#
    Window.paint
    w Window.refresh
;

: Window.paint-current-line-at { w row col -- , move to row col and paint line  }
    row w s@ Window.top + col w s@ Window.left + ncurses.move
    w Window.paint-current-line
;

: (Window.paint) { w | b lin# lin txt row col -- , paint window contents }
    theProject @ Project.update
    theProject @ Project.save
    w Window.address-cursor
    w s@ Window.buffer -> b
    w s@ Window.top-line -> lin
    w s@ Window.top-line# -> lin#

    ncurses::stdscr Theme.default
\     cursormode-hidden ncurses.curs_set -> cursor

    0 -> row
    0 -> col
    w s@ Window.height 1 do
        row w s@ Window.top + col w s@ Window.left + ncurses.move
        lin w lin# Window.paint-line
        \ next line
        lin if
            b lin Buffer.last-line? if
                0 -> lin
            else
                lin s@ ListNode.next -> lin
            then
        then
        row 1+ -> row
        lin# 1+ -> lin#
        ncurses::stdscr Theme.default
    loop
    w Window.paint-statusline
    w Window.refresh
;
' (Window.paint) is Window.paint

privatize
