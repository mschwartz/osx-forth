false trace-include !

require lib/ncurses/ncurses.fth

\ Example 2: initizlization function example per https://tldp.org/HOWTO/NCURSES-Programming-HOWTO/init.html
: demo { | ch -- , Initialization Function Usage example }
    ncurses.initscr drop
    ncurses.raw
    ncurses.stdscr true ncurses.keypad
    ncurses.noecho
    s" Type any character to see it in bold " ncurses.printw
    ncurses.getch -> ch
    ch ncurses.key_f1 = if
        s" F1 key hit" ncurses.printw
    else
        s" \nThe pressed key is " ncurses.printw
        A_BOLD ncurses.attron
        ch pad c!
        0 pad 1+ c!
        pad 1 ncurses.printw
        A_BOLD ncurses.attroff ch .hex
    then
    ncurses.refresh
    ncurses.getch drop
    ncurses.endwin
;

demo