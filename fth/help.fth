\ anew task-help/help.fth

require lib/textfile.fth
require lib/vocabulary.fth

create help-pad 4096 allot

: help-print { e -- , print entry }
    e count type cr
    true
;

: (help) { caddr u | caddr2 u2 tf lst lin txt printing -- , print help from file }
    u 0= if
        cr ."   *** usage: help <word>" cr
        exit
    then
    caddr u help-pad place
    help-pad defined-in-file? ?dup if
        -> u2 -> caddr2
        caddr2 u2 help-pad place
        help-pad TextFile.new -> tf
        tf TextFile.revert not if
            s" fth/" help-pad place
            caddr2 u2 help-pad append
            tf help-pad TextFile.set-filename
            tf TextFile.revert not if
                cr ."    *** No help available because " caddr u type ."  is from " pad $type
                tf TextFile.destroy
                exit
            then
        then
        cr cr caddr u type ."  defined in " tf s@ TextFile.filename zcount type cr
        tf TextFile.list -> lst
        lst -> lin
        false -> printing
        begin
            lin s@ ListNode.next -> lin
            lin lst <>
        while
            lin s@ Line.text -> txt
            printing if 
            cr
                txt c@ bl <> if
                    ."     "
                then
                txt zcount type
            else 
                lin ascii : Line.starts-with-char if
                    cr txt zcount type
                    false -> printing
                    lin ascii ( Line.index-of -1 <> if
                        true -> printing
                    else
                        lin ascii { Line.index-of -1 <> if
                            true -> printing
                        then
                    then
                then
            then
            printing if
                lin ascii ) Line.ends-with-char if
                    false -> printing
                then 
                lin ascii } Line.ends-with-char if
                    false -> printing
                then 
            then
        repeat
    else
        drop
        false exit
    then
    tf TextFile.destroy
    cr cr cr
    true exit
;

: help ( <filename> -- , print help from file )
    bl parse (help)
;
