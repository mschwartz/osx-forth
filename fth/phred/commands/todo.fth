\
\ phred editor
\
\ programmed by Mike Schwartz
\
\ Command Line handling
\   :todo command
\

anew task-phred/commands/todo.fth

: CommandLine.todo { w str tok toku -- flg , return true if executed }
    tok toku s" todo" compare 0= if
        w s" grep -riHn todo . 2>/dev/null" Cmded.begin
        w Window.greped-mode
        true
    else
        false
    then
;
