variable current-bkgd

\ color offsets

\ generic colors
$ 10 constant THEME.BLACK
$ 11 constant THEME.RED
$ 12 constant THEME.GREEN
$ 13 constant THEME.YELLOW
$ 14 constant THEME.BLUE
$ 15 constant THEME.MAGENTA
$ 16 constant THEME.CYAN
$ 17 constant THEME.WHITE
$ 18 constant THEME.GREY
$ 19 constant THEME.DIM_RED
$ 1a constant THEME.DIM_GREEN
$ 1b constant THEME.DIM_YELLOW
$ 1c constant THEME.DIM_BLUE
$ 1d constant THEME.DIM_MAGENTA
$ 1e constant THEME.DIM_CYAN
$ 1f constant THEME.DIM_WHITE
$ 20 constant THEME.DIM_GREY
$ 21 constant THEME.MED_GREY
$ 22 constant THEME.ORANGE

: init-generic-colors
    THEME.BLACK         0000 0000 0000 ncurses.init_color
    THEME.RED           1000 0000 0000 ncurses.init_color
    THEME.GREEN         0000 1000 0000 ncurses.init_color
    THEME.YELLOW        1000 1000 0000 ncurses.init_color
    THEME.BLUE             0 1000 1000 ncurses.init_color
    THEME.MAGENTA       1000 0000 1000 ncurses.init_color
    THEME.CYAN          0000 1000 1000 ncurses.init_color
    THEME.WHITE         1000 1000 1000 ncurses.init_color
    THEME.GREY          0500 0500 0500 ncurses.init_color
    THEME.DIM_RED       0500 0000 0000 ncurses.init_color
    THEME.DIM_GREEN     0000 0500 0000 ncurses.init_color
    THEME.DIM_YELLOW    0500 0500 0000 ncurses.init_color
    THEME.DIM_BLUE      0000 0000 0500 ncurses.init_color
    THEME.DIM_MAGENTA   0500 0000 0500 ncurses.init_color
    THEME.DIM_CYAN      0000 0500 0500 ncurses.init_color
    THEME.DIM_WHITE     0750 0750 0750 ncurses.init_color
    THEME.DIM_GREY      0200 0200 0200 ncurses.init_color
    THEME.MED_GREY      0400 0400 0400 ncurses.init_color
    THEME.ORANGE        1000 0647 0000 ncurses.init_color
    \ #FFA500
;


\ these two are the default theme colors.  The BG is the window/screen/terminal
\   background color, and FG is the text color.  This is as if there is no other
\   theme coloring going on.
THEME.DIM_GREY  constant THEME.BG              \ general purpose THEME background color
THEME.WHITE     constant THEME.FG              \ GP theme foreground color

THEME.WHITE     constant THEME.CURLIN-FG
THEME.MED_GREY  constant THEME.CURLIN-BG

THEME.FG        constant THEME.STATUS-BG
THEME.BG        constant THEME.STATUS-FG

THEME.GREEN     constant THEME.KEYWORD-FG
THEME.BG        constant THEME.KEYWORD-BG

THEME.GREY      constant THEME.COMMENT-FG
THEME.BG        constant THEME.COMMENT-BG

THEME.MED_GREY  constant THEME.SIGNATURE-FG
THEME.BG        constant THEME.SIGNATURE-BG

THEME.CYAN      constant THEME.LOCALS-FG
THEME.BG        constant THEME.LOCALS-BG

THEME.RED       constant THEME.SEARCH-FG
THEME.BG        constant THEME.SEARCH-BG

THEME.ORANGE    constant THEME.VARIABLE-FG
THEME.BG        constant THEME.VARIABLE-BG

\ color pairs
$ 11 constant PAIR.DEFAULT
$ 12 constant PAIR.CURLIN
$ 13 constant PAIR.STATUS
$ 14 constant PAIR.KEYWORD
$ 15 constant PAIR.COMMENT
$ 16 constant PAIR.SIGNATURE
$ 17 constant PAIR.LOCALS
$ 18 constant PAIR.SEARCH
\ dired
$ 19 constant PAIR.DIR
$ 1a constant PAIR.REG
$ 1b constant PAIR.LINK
\ markdown
$ 20 constant PAIR.H1
$ 21 constant PAIR.H2
$ 22 constant PAIR.H3
$ 23 constant PAIR.H4
$ 24 constant PAIR.H5
$ 25 constant PAIR.CODE
$ 26 constant PAIR.VARIABLE
$ 27 constant PAIR.MESSAGELINE
$ 28 constant PAIR.ERRORLINE
$ 29 constant PAIR.SELECTION

: Theme.init
    init-generic-colors

    PAIR.DEFAULT THEME.FG THEME.BG ncurses.init_pair
    PAIR.CURLIN THEME.CURLIN-FG THEME.CURLIN-BG ncurses.init_pair 
    PAIR.STATUS ncurses.COLOR_BLACK THEME.STATUS-BG ncurses.init_pair
    PAIR.KEYWORD THEME.KEYWORD-FG THEME.KEYWORD-BG ncurses.init_pair
    PAIR.COMMENT THEME.COMMENT-FG THEME.BG ncurses.init_pair 
    PAIR.SIGNATURE THEME.SIGNATURE-FG THEME.SIGNATURE-BG ncurses.init_pair 
    PAIR.LOCALS THEME.LOCALS-FG THEME.LOCALS-BG ncurses.init_pair 
    PAIR.SEARCH THEME.SEARCH-FG THEME.SEARCH-BG ncurses.init_pair 
    PAIR.DIR THEME.BLUE THEME.BG ncurses.init_pair 
    PAIR.REG THEME.FG THEME.BG ncurses.init_pair 
    PAIR.LINK THEME.MAGENTA THEME.BG ncurses.init_pair 
    PAIR.H1 THEME.BLUE THEME.BG ncurses.init_pair 
    PAIR.H2 THEME.CYAN THEME.BG ncurses.init_pair 
    PAIR.H3 THEME.RED THEME.BG ncurses.init_pair 
    PAIR.H4 THEME.YELLOW THEME.BG ncurses.init_pair 
    PAIR.H5 THEME.DIM_YELLOW THEME.BG ncurses.init_pair 
    PAIR.CODE THEME.WHITE THEME.BG ncurses.init_pair 
    PAIR.VARIABLE THEME.VARIABLE-FG THEME.VARIABLE-BG ncurses.init_pair
    PAIR.MESSAGELINE THEME.green THEME.white ncurses.init_pair
    PAIR.ERRORLINE THEME.red THEME.white ncurses.init_pair
    PAIR.SELECTION THEME.FG THEME.MAGENTA ncurses.init_pair
;

: Theme.default { w -- , set default theme for window }
    A_BOLD ncurses.attroff
    PAIR.DEFAULT ncurses.color_pair ncurses.bkgdset
;

: Theme.current-line { w -- , set current-line theme for winow }
    A_BOLD ncurses.attroff
    PAIR.CURLIN ncurses.color_pair
    A_BOLD OR 
    ncurses.bkgdset
;

: Theme.dir { w -- , set theme for DIR entry for dired }
    A_BOLD ncurses.attroff
    PAIR.DIR ncurses.color_pair ncurses.bkgdset
;

: Theme.reg { w -- , set theme for REG entry for dired }
    A_BOLD ncurses.attroff
    PAIR.REG ncurses.color_pair ncurses.bkgdset
;

: Theme.link { w -- , set theme for LINK entry for dired }
    A_BOLD ncurses.attroff
    PAIR.LINK ncurses.color_pair ncurses.bkgdset
;

: Theme.status-line { w -- , set status line theme for window }
    A_BOLD ncurses.attroff
    PAIR.STATUS ncurses.color_pair A_BOLD OR ncurses.bkgdset
;

: Theme.match { w -- , turn on theme for matched text }
    ncurses::stdscr ncurses.getbkgd current-bkgd !
    PAIR.SEARCH A_BOLD OR ncurses.bkgdset
;


: Theme.match-end { w -- , turn off theme for matched text }
    current-bkgd @  ncurses.bkgdset
;

: Theme.comment { w -- , set theme for comment }
    A_BOLD ncurses.attroff
    PAIR.COMMENT ncurses.color_pair A_BOLD OR ncurses.bkgdset
;

: Theme.keyword { w -- , set theme for keyword }
    PAIR.KEYWORD ncurses.color_pair A_BOLD OR ncurses.bkgdset
;

: Theme.signature { w -- , set theme for signature }
    A_BOLD ncurses.attroff
    PAIR.SIGNATURE ncurses.color_pair A_BOLD OR ncurses.bkgdset
;

: Theme.locals { w -- , set theme for locals }
    A_BOLD ncurses.attroff
    PAIR.LOCALS ncurses.color_pair 
    A_BOLD OR 
    ncurses.bkgdset
;

: Theme.search { w -- , set theme for search/match }
    A_BOLD ncurses.attroff
    PAIR.SEARCH ncurses.color_pair A_BOLD OR ncurses.bkgdset
;

: Theme.h1 { w -- , set theme for search/match }
    A_BOLD ncurses.attroff
    PAIR.H1 ncurses.color_pair ncurses.bkgdset
;
: Theme.h2 { w -- , set theme for search/match }
    A_BOLD ncurses.attroff
    PAIR.H2 ncurses.color_pair ncurses.bkgdset
;
: Theme.h3 { w -- , set theme for search/match }
    A_BOLD ncurses.attroff
    PAIR.H3 ncurses.color_pair ncurses.bkgdset
;
: Theme.h4 { w -- , set theme for search/match }
    A_BOLD ncurses.attroff
    PAIR.H4 ncurses.color_pair ncurses.bkgdset
;
: Theme.h5 { w -- , set theme for search/match }
    A_BOLD ncurses.attroff
    PAIR.H5 ncurses.color_pair ncurses.bkgdset
;
: Theme.code { w -- , set theme for search/match }
    A_BOLD ncurses.attroff
    PAIR.code ncurses.color_pair A_BOLD OR ncurses.bkgdset
;

: Theme.variable { w -- , set theme for search/match }
    A_BOLD ncurses.attroff
    PAIR.variable ncurses.color_pair A_BOLD OR ncurses.bkgdset
;

: Theme.messageline { w -- , set theme for message line }
    A_BOLD ncurses.attroff
    PAIR.messageline ncurses.color_pair ncurses.bkgdset
;
: Theme.errorline { w -- , set theme for message line }
    A_BOLD ncurses.attroff
    PAIR.errorline ncurses.color_pair ncurses.bkgdset
;

: Theme.selection { w -- , set theme for selection }
    A_BOLD ncurses.attroff
    PAIR.SELECTION ncurses.color_pair ncurses.bkgdset
;
