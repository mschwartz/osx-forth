\
\ phred editor
\
\ programmed by Mike Schwartz
\
\ Command Line handling
\   GREP Buffer
\

private{
    variable greped-buf
    create greped-pad 2048 allot
    : filter { n -- , filter node }
        n s@ Line.text z" Binary file" 1+ zcount string::strncasecmp 0= if
            n List.remove
            n Line.destroy
        then
    ;
}private

: (Greped.begin) { w caddr u | buf -- , initialize and attach grep output }
    w s@ Window.buffer -> buf
    s" grep -Hn " greped-pad place-cstr
    caddr u pad place-cstr
    greped-pad pad string::strcat drop
    w greped-pad zcount Cmded.begin
    buf ['] filter Buffer.iterate
    true buf s! Buffer.read-only
;

' (Greped.begin) is Greped.begin

privatize
