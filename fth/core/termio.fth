\ Terminal I/O
\
\ Requires an ANSI compatible terminal.
\
\ To get Windows computers to use ANSI mode in their DOS windows,
\ Add this line to "C:\CONFIG.SYS" then reboot.
\
\  device=c:\windows\command\ansi.sys
\
\ Author: Phil Burk
\         and Mike Schwartz
\ Copyright 1988 Phil Burk
\ Revised 2001 for pForth

ANEW TASK-TERMIO.FTH
decimal

$ 1B constant ANSI_ESCAPE
$ 1B constant ASCII_ESCAPE
$ 07 constant ASCII_BEL
$ 08 constant ASCII_BACKSPACE
$ 09 constant ASCII_TAB
$ 0a constant ASCII_LINEFEED
$ 0a constant ASCII_NEWLINE
$ 0b constant ASCII_VTAB
$ 0c constant ASCII_FF
$ 0d constant ASCII_CR
$ 7f constant ASCII_DELETE

\ KEYS ( modified ascii )
$ 01 constant KEY.CTRL_A
$ 02 constant KEY.CTRL_B
$ 03 constant KEY.CTRL_C
$ 04 constant KEY.CTRL_D
$ 05 constant KEY.CTRL_E
$ 06 constant KEY.CTRL_F
$ 07 constant KEY.CTRL_G
$ 08 constant KEY.CTRL_H
$ 08 constant KEY.BACKSPACE
$ 08 constant KEY.CTRL_I
$ 09 constant KEY.TAB
$ 0a constant KEY.NEWLINE
$ 0a constant KEY.CTRL_J
$ 0b constant KEY.CTRL_K
$ 0c constant KEY.CTRL_L
$ 0d constant KEY.CTRL_M
$ 0d constant KEY.CR
$ 0d constant KEY.RETURN
$ 0d constant KEY.ENTER
$ 0e constant KEY.CTRL_N
$ 0f constant KEY.CTRL_O
$ 10 constant KEY.CTRL_P
$ 11 constant KEY.CTRL_Q
$ 12 constant KEY.CTRL_R
$ 13 constant KEY.CTRL_S
$ 14 constant KEY.CTRL_T
$ 15 constant KEY.CTRL_U
$ 16 constant KEY.CTRL_V
$ 17 constant KEY.CTRL_W
$ 18 constant KEY.CTRL_X
$ 19 constant KEY.CTRL_Y
$ 1a constant KEY.CTRL_Z
$ 1B constant KEY.ESC
$ 1B constant KEY.ESCAPE
$ 7f constant KEY.DEL

$ 81 constant KEY.F1
$ 82 constant KEY.F2
$ 83 constant KEY.F3
$ 84 constant KEY.F4
$ 85 constant KEY.F5
$ 86 constant KEY.F6
$ 87 constant KEY.F7
$ 89 constant KEY.F8
$ 89 constant KEY.F9
$ 8a constant KEY.F10
$ 8b constant KEY.F11
$ 8c constant KEY.F12
$ 8d constant KEY.NUM_HOME
$ 8e constant KEY.NUM_END
$ 8f constant KEY.NUM_UP
$ 90 constant KEY.NUM_PAGEUP
$ 91 constant KEY.NUM_LEFT
$ 92 constant KEY.NUM_RIGHT
$ 93 constant KEY.NUM_DOWN
$ 94 constant KEY.NUM_PAGEDOWN
$ 95 constant KEY.NUM_INSERT
$ 96 constant KEY.NUM_DELETE
$ 97 constant KEY.HOME
$ 98 constant KEY.UP
$ 99 constant KEY.PAGEUP
$ 9a constant KEY.LEFT
$ 9b constant KEY.RIGHT
$ 9c constant KEY.END
$ 9d constant KEY.DOWN
$ 9e constant KEY.PAGEDOWN
$ 9f constant KEY.INSERT
$ a0 constant KEY.DELETE
$ a1 constant KEY.PRTSCR
$ a2 constant KEY.PAUSE ( pause/break key)

\ ANSI arrow key sequences
\ ESC [ 0x41 is UP
\ ESC [ 0x42 is DOWN
\ ESC [ 0x43 is RIGHT
\ ESC [ 0x44 is LEFT

: TIO.ROWS ( -- rows, number of rows in terminal) terminal-rows @ ;
: TIO.COLS ( -- cols number of columns in terminal) terminal-cols @ ;

private{
variable putback 
: putback! putback c! ;
: putback@ putback c@ ;
: 0putback! 0 putback! ;
0putback!
}private

\ : ?terminal sys::?terminal ;
: key? sys::key? ;
\ : key 
\     begin 
\         sys::key? 0<> if
\             sys::key 
\             dup 3 = if
\                 1 SIGINT LSHIFT signals @ or signals !
\                 SIGINT sys::set_signal
\             then
\             exit
\         then
\     again
\ ;

: TIO.KEY { | k -- key, read key from keyboard, processing ansi escapes }
    true sys::raw

    putback@ 0= if
        key -> k 
        k ASCII_ESCAPE <> if
            0putback!
            k exit
        then

\         key putback!

        sys::key -> k

        k ascii [ <> if
            k putback!
            KEY.ESCAPE exit
        then

        sys::key -> k
        k CASE
            $ 41 of KEY.UP 0putback! exit endof
            $ 42 of KEY.DOWN 0putback! exit endof
            $ 43 of KEY.RIGHT 0putback! exit endof
            $ 44 of KEY.LEFT 0putback! exit endof
            ascii O of
                sys::key case
                    $ 50 of KEY.F1 0putback! exit endof
                    $ 51 of KEY.F2 0putback! exit endof
                    $ 52 of KEY.F3 0putback! exit endof
                    $ 53 of KEY.F4 0putback! exit endof
                    $ 54 of KEY.F5 0putback! exit endof
                    $ 55 of KEY.F6 0putback! exit endof
                    $ 56 of KEY.F7 0putback! exit endof
                    $ 57 of KEY.F8 0putback! exit endof
                    $ 58 of KEY.F9 0putback! exit endof
                    $ 59 of KEY.F10 0putback! exit endof
                    $ 5a of KEY.F11 0putback! exit endof
                    $ 5b of KEY.F12 0putback! exit endof
                endcase
            endof
        ENDCASE
        putback@ 
        0putback!
    else
        putback@
        0putback!
    then
;

\ ANSI terminal control
\ ESC [ 2J is clear screen
\ ESC [ {n} D is move left
\ ESC [ {n} C is move right
\ ESC [ K is erase to end of line

: TIO.EMIT { c  -- , emit c to screen }
    c 8 = if
        out @ 1- dup 0< if drop exit then out !
        c sys::stdout sys::fputc drop
        exit
    then
    c sys::stdout sys::fputc drop
    c 13 = if
        0 out !
        exit
    then
    c 10 = if
        0 out !
        exit
    then
    out @ 1+ out !
;

\ : (emit)  ( c -- , default emit )
\     TIO.EMIT
\     dup sys::stdout sys::fputc drop
\     EMIT-INTERNAL
\ ;

\ ' (emit) is emit

: TIO.FLUSH ( , -- fflush )
    sys::stdout sys::fflush drop
;

: TIO.TYPE { caddr u -- , type to stdout }
    u 0 do
        caddr @ TIO.emit
        caddr 1+ -> caddr
    loop
    TIO.flush
;

: TIO.NEWLINE ( -- , emit newline )
    13 TIO.emit
    10 TIO.emit
    TIO.flush
;

: ESC[ ( send ESCAPE and [ )
    ASCII_ESCAPE TIO.emit
    ascii [ TIO.emit
;

: TIO.cursor-color ( caddr -- , set cursor color )
    ESC[ ." 12;" count TIO.type
;

: TIO.HOME ( -- , move cursor to 0,0 )
    ESC[ ." H"
;

: tio.parse-number { | c k -- n , parse number as key input }
    0 -> c
    begin
        sys::key -> k
        k ascii R = if
            c exit
        then
        k ascii ; = if
            c exit
        then

        k ascii 0 < if 
            c exit 
        then
        k ascii 9 > if 
            c exit 
        then
        k ascii 0 - -> k
        c 10 * k + -> c
    again
;

\ ESC[6n	request cursor position - reports as ESC[#;#R
: TIO.REQUEST-ROW-COL ( -- row col, current cursor row/col )
    ESC[ ." 6n" 
    sys::key 
    sys::key
    2drop \ esc[

    \ parse numbers
    tio.parse-number
    tio.parse-number
;


: TIO.CLS ( -- , clear screen )
    ESC[ ." 2J"
;

: TIO.TO-STRING ( n -- addr c )  s>d <# #s #> ;

: TIO.MOVE-TO ( row col -- , move cursor to row, col )
    swap
    ESC[ 
        TIO.TO-STRING TIO.type
        ascii ; TIO.emit
        TIO.TO-STRING TIO.type
        ascii H TIO.emit
;

: TIO.MOVE-TO-COL ( col -- , move cursor to col )
    ESC[ 
        TIO.TO-STRING TIO.type
        ascii G TIO.emit
;

: TIO.MOVE-UP ( n -- , move cursor up n lines )
    ESC[ 
        TIO.TO-STRING TIO.type
        ascii A TIO.emit
;

: TIO.MOVE-UP0 ( n -- , move cursor up n lines and begin of line )
    ESC[ 
        TIO.TO-STRING TIO.type
        ascii E TIO.emit
;

: TIO.MOVE-UP-SCROLL ( , move cursor up one line, scroll if needed )
    ESC[ 
        ascii M TIO.emit
;

: TIO.MOVE-DOWN ( n -- , move cursor down n lines )
    ESC[ 
        TIO.TO-STRING TIO.type
        ascii B TIO.emit
;

: TIO.MOVE-DOWN0 ( n -- , move cursor down n lines and begin of line )
    ESC[ 
        TIO.TO-STRING TIO.type
        ascii F TIO.emit
;

: TIO.MOVE-RIGHT ( n -- , move cursor right n lines )
    ESC[ 
        TIO.TO-STRING TIO.type
        ascii C TIO.emit
;

: TIO.BACKWARDS ( n -- , move cursor backwards )
    ESC[
    base @ >r decimal
    0 .r
    r> base !
    ascii D TIO.emit
;

: TIO.FORWARDS ( n -- , move cursor forwards )
    ESC[
    base @ >r decimal
    0 .r
    r> base !
    ascii C TIO.emit
;

: TIO.ERASE.EOL ( -- , erase to the end of the line )
    ESC[
    ascii K TIO.emit
;

: BELL ( -- , ring the terminal bell )
    7 TIO.emit
;

: BACKSPACE ( -- , backspace action )
    8 TIO.emit  space  8 TIO.emit
;

\ ERASE FUNCTIONS
: TIO.ERASE-EOS ( , erase from cursor to end of screen )
    ESC[
        ascii 0 TIO.emit ascii J TIO.emit
; 

: TIO.ERASE-BOS ( , erase from cursor to beginning of screen )
    ESC[
        ascii 1 TIO.emit ascii J TIO.emit
; 

: TIO.ERASE-EOL ( , erase from cursor to end of line  )
    ESC[
        ascii 0 TIO.emit ascii K TIO.emit
; 
: TIO.ERASE-BOL ( , erase from cursor to start of line  )
    ESC[
        ascii 1 TIO.emit ascii K TIO.emit
; 

: TIO.ERASE-LINE ( , erase entire line  )
    ESC[
        ascii 2 TIO.emit ascii K TIO.emit
; 

\ COLORS / GRAPHICS MODE
: TIO.RESET-MODES ( reset all modes/styles and colors )
    ESC[ ." 0m"
;

: TIO.BOLD-MODE ( on -- , set bold mode )
    if
        ESC[ ." 1m"
    else
        ESC[ ." 22m"
    then
;


: TIO.DIM-MODE ( on -- , set dim/faint mode )
    if
        ESC[ ." 2m"
    else
        ESC[ ." 22m"
    then
;

: TIO.ITALICS-MODE ( on -- , set italics mode )
    if
        ESC[ ." 3m"
    else
        ESC[ ." 23m"
    then
;

: TIO.UNDERLINE-MODE ( on --  , set underline mode )
    if
        ESC[ ." 4m"
    else
        ESC[ ." 24m"
    then
;

: TIO.BLINKING-MODE ( on -- , set blinking mode )
    if
        ESC[ ." 5m"
    else
        ESC[ ." 25m"
    then
;

: TIO.INVERSE-MODE ( on -- , set inverse mode )
    if
        ESC[ ." 7m"
    else
        ESC[ ." 27m"
    then
;

: TIO.HIDDEN-MODE ( on -- , set hidden/invisible mode )
    if
        ESC[ ." 8m"
    else
        ESC[ ." 28m"
    then
;

: TIO.STRIKETHROUGH-MODE ( on -- , set strikethrough mode )
    if
        ESC[ ." 9m"
    else
        ESC[ ." 29m"
    then
;

\ COLORS

 30 constant COLOR-FG-BLACK
 40 constant COLOR-BG-BLACK
 90 constant COLOR-FG-BRIGHTBLACK
100 constant COLOR-BG-BRIGHTBLACK
 31 constant COLOR-FG-RED
 41 constant COLOR-BG-RED
 91 constant COLOR-FG-BRIGHTRED
101 constant COLOR-BG-BRIGHTRED
 32 constant COLOR-FG-GREEN
 42 constant COLOR-BG-GREEN
 92 constant COLOR-FG-BRIGHTGREEN
102 constant COLOR-BG-BRIGHTGREEN
 33 constant COLOR-FG-YELLOW
 43 constant COLOR-BG-YELLOW
 93 constant COLOR-FG-BRIGHTYELLOW
103 constant COLOR-BG-BRIGHTYELLOW
 34 constant COLOR-FG-BLUE
 44 constant COLOR-BG-BLUE
 94 constant COLOR-FG-BRIGHTBLUE
104 constant COLOR-BG-BRIGHTBLUE
 35 constant COLOR-FG-MAGENTA
 45 constant COLOR-BG-MAGENTA
 95 constant COLOR-FG-BRIGHTMAGENTA
105  constant COLOR-BG-BRIGHTMAGENTA
 36 constant COLOR-FG-CYAN
 46 constant COLOR-BG-CYAN
 96 constant COLOR-FG-BRIGHTCYAN
106 constant COLOR-BG-BRIGHTCYAN
 37 constant COLOR-FG-WHITE
 47 constant COLOR-BG-WHITE
 97 constant COLOR-FG-BRIGHTWHITE
107 constant COLOR-BG-BRIGHTWHITE
 39 constant COLOR-FG-DEFAULT
 49 constant COLOR-BG-DEFAULT
 00 constant COLOR-FG-RESET
 00 constant COLOR-BG-RESET

: TIO.COLOR ( color -- , set  color to COLOR-* above )
    ESC[
        TIO.TO-STRING TIO.type
        ascii m TIO.emit
; 

: TIO.FG-RGB ( b g r -- , set foreground color as RGB )
    ESC[
        ." 38;2;"
        TIO.TO-STRING TIO.type
        ascii ; TIO.emit
        TIO.TO-STRING TIO.type
        ascii ; TIO.emit
        TIO.TO-STRING TIO.type
        ascii m TIO.emit
;

: TIO.BG-RGB ( b g r -- , set foreground color as RGB )
    ESC[
        ." 48;2;"
        TIO.TO-STRING TIO.type
        ascii ; TIO.emit
        TIO.TO-STRING TIO.type
        ascii ; TIO.emit
        TIO.TO-STRING TIO.type
        ascii m TIO.emit
;

\ SCREEN MODES (may not work in windowed terminals)

\ PRIVATE MODES (not in specification by likely implemented)
: TIO.CURSOR-HIDE ( , make cursor invisible )
    ESC[ ." ?25l"
;
: TIO.CURSOR-SHOW ( , make cursor visible )
    ESC[ ." ?25h"
;
: TIO.SCREEN-SAVE ( , save screen )
    ESC[ ." ?47h"
;
: TIO.SCREEN-RESTORE ( , save screen )
    ESC[ ." ?47l"
;

\ : type { caddr u -- , type string } 
\     u 0 do
\         caddr c@ emit
\         caddr 1+ -> caddr
\     loop
\ ;

0 [IF] \ for testing

: SHOWKEYS  ( -- , show keys pressed in hex )
    BEGIN
        key
        dup .
        ." , $ " dup .hex cr
        ascii q =
    UNTIL
;

: AZ ascii z 1+ ascii a DO i TIO.emit LOOP ;

: TEST.BACK1
    AZ 5 tio.backwards
    1000 msec
    tio.erase.eol
;
: TEST.BACK2
    AZ 10 tio.backwards
    1000 msec
    ." 12345"
    1000 msec
;
[THEN]

privatize
