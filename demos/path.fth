#!/usr/local/bin/pforth -q

false trace-include !
true quiet !

require sys/path.fth

create working-directory 512 allot

: demo 
    cr ." about to getcwd " cr
    working-directory MAX-PATH-LENGTH path.getcwd drop
    working-directory zcount type cr
;

demo