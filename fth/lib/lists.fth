\ linked list methods

anew task-lib/lists.fth

trace-include @ [if]
cr ." Compiling lib/lists.fth"
[then]

\ doubly linked lists of nodes
\ your class/struct must start with a ListNode
\
\ example
\   :struct MyNode 
\       struct ListNode m_node
\       ... rest of members
\   ;struct

:struct ListNode
    APTR ListNode.prev
    APTR ListNode.next
;struct

: ListNode.dump { node text -- , print a node }
    cr text $type
    node .hex 
    ." prev " node s@ ListNode.prev .hex
    ." next " node s@ ListNode.next .hex
;

\ keystone is actually a node
:struct List
    APTR List.prev
    APTR List.next
;struct

: List.Init { keystone -- }
    keystone keystone s! List.prev
    keystone keystone s! List.next
;

: List.First { keystone -- first , return first node in list }
    keystone s@ List.next
;
: List.Last { keystone -- first , return first node in list }
    keystone s@ List.prev
;

: List.InsertBefore { new nod | prev -- insert new node before nod node }
    nod s@ ListNode.prev -> prev

    \ link in new
    prev new s! ListNode.prev
    nod new s! ListNode.next

    new prev s! ListNode.next
    new nod s! ListNode.prev
;

: List.InsertAfter { src dest | next  -- , insert src node after dest node }
    dest s@ ListNode.next -> next
    src dest s! ListNode.next \ dest->next = src
    src next s! ListNode.prev  \ next->prev = src
    next src s! ListNode.next  \ src->next = next
    dest src s! ListNode.prev  \ src->prev = dst
;

: List.AddTail ( node keystone -- , insert node at end of list )
    List.insertBefore
;

: List.AddHead ( node keystone -- , insert node at head of list )
    List.insertAfter
;

: List.RemHead { keystone -- n|nullptr , remove head of list }
    keystone s@ List.next keystone <> if
        keystone s@ List.next exit
    then
    nullptr
;

: ListNode.Remove { node | p n -- , remove node from list }
    node s@ ListNode.prev -> p
    node s@ ListNode.next -> n
    n p s! ListNode.next        \ prev->next = node->next
    p n s! ListNode.prev        \ next->prev = node->prev
;

: ListNode.removed? { node -- f , true if node has been removed }
    node s@ ListNode.next node =
;
: List.removed? { node -- f , true if node has been removed }
    node ListNode.removed?
;

: List.Dump { keystone | current  -- , dump list }
    keystone s@ ListNode.next -> current
    keystone c" keystone " ListNode.dump
    current keystone = if 
        cr ." list is empty" cr
        \ exit if list is empty
        exit 
    then 

    begin 
        current c"    node " ListNode.dump
        current s@ ListNode.next -> current
        current keystone = 
    until
    cr ." end of list"
    cr
;

\ iterate over list at keystone and call handler for each
: List.Iterate { keystone handler | current nxt -- , dump nodes in list }
    keystone s@ List.next -> current 
    current keystone = if 
        \ list is empty
        exit 
    then 
    begin 
        current s@ ListNode.next -> nxt \ save in case handler calls remove on itself
        current handler execute
        nxt -> current
        current keystone = 
    until
;

: List.End? { keystone node -- f , true if node is at end of list }
    node keystone =
;

: List.Empty? { keystone -- f, true if list is emptry }
    keystone s@ ListNode.next keystone =
;

: List.Remove { node | p n -- , remove node from list }
    node List.removed? not  if
        node ListNode.remove
        node List.Init 
    then
;

trace-include @ [if]
cr ." End lists.fth "
[then]

