private{
: print-tab { c -- c , expand tab using spaces }
	c GUTTER-WIDTH -  1+ -> c
    begin
		c TAB-WIDTH @ mod 0= if c exit then
        space
        c 1+ -> c
    again
;
}private

: Window.save-cursor { w -- , save cursor position }
    w s@ Window.cursor-row w s! Window.cursor-row-save
    w s@ Window.cursor-col w s! Window.cursor-col-save
    w s@ Window.offset-left w s! Window.offset-left-save
;

: Window.restore-cursor { w -- , restore cursor position }
    w s@ Window.cursor-row-save w s! Window.cursor-row
    w s@ Window.cursor-col-save w s! Window.cursor-col
    w s@ Window.offset-left-save w s! Window.offset-left
;

: Window.set-read-only { w flg -- , set/unset buffer read-only }
    flg w s@ Window.buffer 
    s! Buffer.read-only
;
: Window.get-read-only { w  -- f , get buffer read-only }
    w s@ Window.buffer s@ Buffer.read-only
;
: Window.read-only? { w -- flg , is buffer read-only? }
    w Window.get-read-only
;
: Window.permanent? { w -- f , is buffer permanent? }
    w s@ Window.buffer Buffer.permanent?
;

: Window.toggle-read-only { w -- , toggle read-only }
    w Window.read-only? if
        w false Window.set-read-only
    else
        w true Window.set-read-only
    then
;

: Window.client-width { w -- n , get client width }
    w s@ Window.width GUTTER-WIDTH -
;

: Window.last-line# { w -- n , get last line# }
    w s@ Window.buffer s@ Buffer.#lines
;

: Window.current-text ( w -- p , return pointer to text of current line )
    s@ Window.current-line s@ Line.text
;

: Window.current-line-index { w -- n , get index of cursor in current line }
    w s@ Window.cursor-col w s@ Window.offset-left +
;

: Window.beginning-of-line { w -- f , is cursor at beginning of line? }
    0 w s! Window.cursor-col
    0 w s! Window.offset-left
;
;
: Window.beginning-of-line? { w -- f , is cursor at beginning of line? }
    w Window.current-line-index 0=
;

: Window.last-line? { w | b -- t , is cursor at end of file? }
    w s@ Window.buffer -> b
    b w s@ Window.current-line  Buffer.end? 
;

: Window.last-line { w | b -- , move cursor to end of file }
    w s@ Window.buffer -> b
    b Buffer.last-line
;

: Window.first-line? { w -- f , determine if window's top-line is the first line of the buffer }
    w s@ Window.top-line w s@ Window.buffer Buffer.first-line =
;
: Window.first-line { w | b -- , move cursor to beginning of file }
    w s@ Window.buffer -> b
    b Buffer.first-line w s! Window.top-line
    b Buffer.first-line w s! Window.current-line

    0 w s! Window.cursor-row 
    0 w s! Window.cursor-col
    0 w s! Window.offset-left

    0 w s! Window.line#
    0 w s! Window.top-line#
;

: Window.current-line-length { w -- n , get length of current line }
    w Window.current-text Window.line-length 
;

\
\ Sometimes editor and API functions can leave the cursor on a line in an invalid position.
\ For example, past end of line, or left-offset wrong for length of line...
\
\ This routine assures the cursor is in a valid location
\
: Window.fix-cursor { w | delta -- , adjust cursor-col and offset-left so cursor is visible }
    editor-mode @ case
        editmode.dired of 
            0 w s! Window.cursor-col
            exit
        endof
        editmode.bufed of 
            0 w s! Window.cursor-col
            exit
        endof
    endcase
    w s@ Window.width 2/ -> delta
    w Window.current-line-index w Window.current-line-length >= if
        w Window.current-line-length 
        dup 0> if 1- then
        w s! Window.cursor-col
    then
    0 w s! Window.offset-left 
    begin
        w s@ Window.cursor-col w s@ Window.width GUTTER-WIDTH - >=
    while
        w s@ Window.cursor-col delta - w s! Window.cursor-col
        w s@ Window.offset-left delta + w s! Window.offset-left
    repeat
;

variable under-cursor
create tmp-line 512 allot
: Window.address-cursor { w | row col ch -- , move cursor to current position  }
    w s@ Window.cursor-row w s@ Window.top + -> row
    w s@ Window.cursor-col w s@ Window.left + 
    w s@ Window.type WindowType.nerdtree <> if
        GUTTER-WIDTH + 
    then
    -> col

    w WindowList.active-window = if
        row col ncurses::mvinch under-cursor !
        under-cursor @ ncurses::A_REVERSE OR -> ch
        under-cursor @  $ ff and bl >= if
            row col ch ncurses::mvaddch drop
        else
            row col bl ncurses::mvaddch drop
        then
    then
    w Window.refresh
;

: Window.line-up { w -- , move line up }
    w s@ Window.cursor-row 0> if
        w s@ Window.line# w s@ Window.min-line# <= if exit then
        w s@ Window.cursor-row 1- w s! Window.cursor-row
        w s@ Window.current-line s@ ListNode.prev w s! Window.current-line
        w s@ Window.line# 1- w s! Window.line#
    else
        w Window.first-line?  if
            exit
        else
            w s@ Window.line# w s@ Window.min-line# <= if exit then
            w s@ Window.top-line s@ ListNode.prev w s! Window.top-line
            w s@ Window.current-line s@ ListNode.prev w s! Window.current-line
            w s@ Window.line# 1- w s! Window.line#
            w s@ Window.top-line# 1- w s! Window.top-line#
        then
    then
    w Window.fix-cursor
;

: Window.line-down { w | b -- , move line down }
    w s@ Window.buffer -> b
    w Window.last-line? if
        exit
    then
    w s@ Window.cursor-row w s@ Window.height 2- < if
        w s@ Window.cursor-row 1+ w s! Window.cursor-row
        w s@ Window.current-line b buffer.end? if
        else
            w s@ Window.current-line s@ ListNode.next 
            w s! Window.current-line
        then
        w s@ Window.line# 1+ w s! Window.line#
    else
        \ scroll
        b w s@ Window.top-line buffer.end? if
            exit
        then
        w s@ Window.top-line s@ ListNode.next w s! Window.top-line
        w s@ Window.current-line s@ ListNode.next w s! Window.current-line
        w s@ Window.line# 1+ w s! Window.line#
        w s@ Window.top-line# 1+ w s! Window.top-line#
    then
    w Window.fix-cursor
;

: Window.line-left { w | p -- , adjust cursor to move left }
    w s@ Window.cursor-col 0= if
        w s@ Window.offset-left 0= if
            w Window.line-up
            w Window.current-line-length w s! Window.cursor-col
            w Window.fix-cursor
            exit
        then
        w s@ Window.offset-left 1- w s! Window.offset-left
        exit
    then

    w Window.current-text -> p
    p w s@ Window.offset-left w s@ Window.cursor-col + 
    +  \ todo: this should be first-char
    dup p = if
        drop
        exit
    then
    drop
    w s@ Window.cursor-col 1- w s! Window.cursor-col
;

: Window.move-right { w -- , move cursor right on current line }
    w Window.current-line-index w Window.current-line-length 1- >= if
        exit
    then
    w s@ Window.cursor-col w s@ Window.width GUTTER-WIDTH - < if
        w s@ Window.cursor-col 1+ w s! Window.cursor-col
    else
        w s@ Window.offset-left 1+ w s! Window.offset-left
    then
;
: Window.line-right { w -- , move cursor right on current line }
    w Window.current-line-index w Window.current-line-length 1- >= if
        w Window.line-down
        0 w s! Window.cursor-col 
        w Window.fix-cursor
        exit
    then
    w s@ Window.cursor-col w s@ Window.width GUTTER-WIDTH - < if
        w s@ Window.cursor-col 1+ w s! Window.cursor-col
    else
        w s@ Window.offset-left 1+ w s! Window.offset-left
    then
;

: (Window.refresh) ( w -- , refresh window )
    drop
    ncurses.refresh
;
' (Window.refresh) is Window.refresh

: Window.visual-mode { w -- , set visual mode }
    editmode.visual editor-mode !
\     ncurses.cursor-show drop
    WindowType.editor w s! Window.type    
    ncurses.refresh
    0 w s! Window.min-line#
;

privatize
