\
\ Line parsing utilities
\
\ These methods work against linked list of Lines (see lib/line.fth)
\
\ Handy for doing things like token parsing, syntax highlighting, etc.
\
\ The reason a parser would work against a list of lines is for multiline 
\ comments, for example.
\
anew task-lib/parser.fth

require sys/memory.fth
require lib/lists.fth
\ include lib/line.fth
require sys/regex.fth

:STRUCT LinesParser
    APTR LinesParser.lines
    APTR LinesParser.current-line
    ULONG LinesParser.index          \ offset into current-line
;STRUCT

private{
    2 constant REGMATCH_COUNT
    create lp-regex sizeof() regex_t allot
    variable lp-regmatch REGMATCH_COUNT regex::alloc_regmatch_t lp-regmatch !
}private

: LinesParser.new { lines current-line | parser -- parser , construct a parser }
    sizeof() LinesParser Mem.malloc -> parser
    lines parser s! LinesParser.lines
    current-line parser s! LinesParser.current-line
    parser
;

: LinesParser.find { parser $pattern handler | keystone lin col err start end match -- , call handler for every match in lines }
    \ compile regex so we can use it over and over while matching
    lp-regex pad REG_MINIMAL REG_ENHANCED OR regcomp if
        \ error!
        exit
    then
    parser s@ LinesParser.lines -> keystone
    keystone -> lin
    0 -> col
    begin
        lin s@ ListNode.next -> lin
        lin keystone <>
    while
        \ for each line we want to match 1 or more for pattern
        true -> match
        begin
            match
        while
                lp-regex 
                lin s@ Line.text col + 
                lp-regmatch @
                REGMATCH_COUNT
                0
            regexec -> err
            err 0<> if
                err REG_NOMATCH <> if
                    \ error!
                    false -> match
                else
                    true -> match
                then
            then
            match if
                lp-regmatch @ s@ regmatch_t.rm_so -> start
                lp-regmatch @ s@ regmatch_t.rm_eo -> end
                \ handler ( start end line -- offset )
                start end lin handler execute \ returns number of characters to advance
                col + -> col \ add offset to col
            then
        repeat
    repeat
;

: LinesParser.end-of-line? { parser -- f , is index into current-line at end of line? }
    parser s@ LinesParser.current-line s@ Line.text 
    parser s@ LinesParser.index + c@ 0=
;

: LinesParser.skip-blanks { parser | done txt ndx -- , advance index past any blanks for current-line }
    parser s@ LinesParser.current-line s@ Line.text -> txt
    parser s@ LinesParser.index -> ndx
    false -> done
    begin
        done false =
    while
        txt c@ 
        case
            bl of 
                txt 1+ -> txt
                ndx 1+ -> ndx
            endof
            true -> done
        endcase
    repeat
    ndx parser s! LinesParser.index
;

\ note that caddr u is string offset to start of token and length of token in current-line 
\ current-line is not modified!
: LinesParser.get-token { parser delim | ch caddr u -- caddr u , get next token end with delimeter or null terminator }
    parser s@ LinesParser.current-line s@ Line.text 
    parser s@ LinesParser.index -> caddr
    0 -> u
    begin
        caddr c@ -> ch
        ch 0= if \ end of string?
            caddr u exit
        then
        delim = if 
            caddr u exit
        then
        caddr 1+ -> caddr
        u 1+ -> u
    again
;

privatize
