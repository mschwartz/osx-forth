# nixforth 

Programmed by Mike Schwartz
           pForth by Phil Burk, et al

*See PFORTH.md for Phil's original README.md for pForth*

## About

nixforth is a Unix-oriented Forth largely based upon Phil Burk's pForth.

The original pForth was designed to be a tiny Forth, portable, intended mostly for 
deployment on bare hardware, typically boards being developed and tested.  It compiled
and ran on Linux, Mac, and Windows, though its functionality was limited to simple I/O.

nixforth is a radical departure from pForth's design goals.  It is intended to be used
to develop server, command line, and GUI programs.  Things like Signals, getopt, stdio,
fcntl, malloc/free, regex, HTTP client and server, and more are integrated into nixforth.

The pForth sources have been reorganized, the Windows logic removed, a new Makefile implemented, and numerous (many many!) system calls and libraries integrated.
 
 As well, the sources have been converted to C++ and now are compiled using the C++ compiler.
This allows access to the stl and stdc++ type libraries, hashmaps, strings, regex, and more.

nixforth includes a comprehensive FTH (Forth source) library, much of it Object oriented.

The demos/ directory contains a number of demo programs that exercise the new features.

The gem of this project is the Phred text editor, patterned after vim, but entirely written
in nixforth.

## Building

To build, you will need gcc or clang installed.  Simply type "make" from the top level directory, which will produce pforth and pforth_standalone binaries.  (Soon to be renamed to nixforth and nixforth_standalone).

## Running

To run, just enter "./pforth_standalone" at the command line and you will be in a typical
Forth INTERPRET interface.

You can edit Forth source code in files and "require" or "include" them from the Forth prompt to execute them.  You can run a program from a file from your shell's command line:

```
# ./pforth_standalone program-to-run
```

## fth/lib Library Features

### Containers
* Linked List classes/struct (List and ListNode, double linked)
* HashMap for string to <any> mapping
* Array class for arrays and stacks
* Growable Buffer class
* C Strings class (most of the Linux/Mac APIs require C Strings as arguments!)

### Syntax and Language Structure
* Table/Hashmap of keywords for C++ 
* Table/Hashmap of keywords for Forth

### General Purpose
* Debugging words
* Line class - many applications will find that linked lists of lines are useful
* DirList class - returns a Linked list of Lines representing the entries in a directory
* EventEmitter class - allows for pub/sub programming
* JSON class for reading and preparing JSON
* TextFile class for reading and writing text files from disk as List of Lines.
* ANSI escape sequences based TUI library

### System & Host Library interfaces
* Date class
* Directory Listing class
* Better readline with file based history and vim line editing
* A simple command shell

### Supported Integrations

Integrations include, but are not limited to:
* Net/sockets/DNS/etc.
* SDL
* ncurses
* Git class that erforms assorted git operations on a repo
* MQTT class that provides access to MQTT protocol
 
