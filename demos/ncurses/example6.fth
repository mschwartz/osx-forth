\ Example 6: chgat() usage example;

false trace-include !

require lib/ncurses.fth

: demo ( -- , demo program )
    ncurses.initscr
    ncurses.start_color
    1 ncurses.COLOR_CYAN ncurses.COLOR_BLACK ncurses.init_pair
    s" A Big string that I don't care to type fully" ncurses.printw
    0 0 -1 A_BOLD 1 null ncurses.mvchgat
    \ First two parameters specify the position at which to start 
	\ Third parameter number of characters to update. -1 means till 
	\ end of line
	\ Forth parameter is the normal attribute you wanted to give 
	\ to the charcter
	\ Fifth is the color index. It is the index given during init_pair()
	\ use 0 if you didn't want color
	\ Sixth one is always NULL 
    ncurses.refresh

    ncurses.getch
    ncurses.endwin
;

demo

