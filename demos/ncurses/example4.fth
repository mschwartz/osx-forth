false trace-include !

require lib/ncurses.fth
\ require sys/stdio.fth

create str 80 chars allot

: demo { | win msg row col -- , scanw example }
    c" Enter a string: " -> msg
    ncurses.initscr -> win

    ncurses.stdscr ncurses.getmaxyx -> col -> row
    row 2/ col msg c@ - 2/ msg count ncurses.mvprintw
    str ncurses.getstr
    row 2 - 0 s" You entered " ncurses.mvprintw str zcount ncurses.printw
    ncurses.refresh
    ncurses.getch drop
    ncurses.endwin
;

demo