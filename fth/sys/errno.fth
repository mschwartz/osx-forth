anew task-c/errno.fth

\ ERROR CODES

 1 constant EPERM           \ operation not permitted
 2 constant ENOENT          \ no such file or directory
 3 constant ESRCH           \ no such process
 4 constant EINTR           \ interrupted by system call
 5 constant EIO             \ I/O error
 6 constant ENXIO           \ device not configured
 7 constant E2BIG           \ argument list too long
 8 constant ENOEXEC         \ Exec format error
 9 constant EBADF           \ Bad file descriptor
10 constant ECHILD          \ No child processes
11 constant EDEADLK         \ resource deadlock avoided
12 constant ENOMEM          \ cannot allocate memory
13 constant EACCESS         \ permission denied
14 constant EFAULT          \ bad address
15 constant ENOTBLK         \ block device required
16 constant EBUSY           \ device/resource is busy
17 constant EEXIT           \ file exists
18 constant EXDEV           \ cross device link
19 constant ENODEV          \ operation not supported by device
20 constant ENOTDIR         \ Not a directory
21 constant EISDIR          \ Is a directory
22 constant EINVAL          \ Invalid argument
23 constant ENFILE          \ Too many open files in system
24 constant EMFILE          \ Too many open files
25 constant ENOTTY          \ inappropriate ioctl for device
26 constant ETXTBSY         \ text file busy
27 constant EFBIG           \ file too large
28 constant ENOSPC          \ no space left on device
29 constant ESPIPE          \ illegal seek
30 constant EROFS           \ read-only file system
31 constant EMLINK          \ too many links
32 constant EPIPE           \ broken pipe

\ math software
33 constant EDOM            \ numerical argument out of domain
34 constant ERANGE          \ result too large

\ non-blocking and interrupt I/O
35 constant EAGAIN          \ resource temporarily unavailable
EAGAIN constant EWOULDBLOCK \ operation would block
36 constant EINPROGRESS     \ operation no in progress
37 constant EALREADY        \ operation already in progress

\ IPC/network
38 constant ENOTSOCK        \ socket operation not allowed on non-socket
39 constant EDESTADDRREQ    \ destination address required
40 constant EMSGSIZE        \ message too long
41 constant EPROTOTYPE      \ protocol wrong type for socket
42 constant ENOPROTOOPT     \ protocol not availble
43 constant ENOPROTNOSUPPORT \ protocol not supported
44 constant ESOCKTNOSUPPORT \ socket type not supported
45 constant ENOTSUP         \ operation not supported
ENOTSUP constant EOPNOTSUPP
46 constant EPFNOSUPPORT    \ protocol family not supported
47 constant EAFNOSUPPORT    \ address family not supported
48 constant EADDRINUSE      \ address already in use
49 constant EADDRNOTAVAIL   \ can't assign requested address

\ IPC/Network software -- operational errors
50 constant ENETDOWN        \ network is down
51 constant ENETUNREACH     \ network is unreachable
52 constant ENETRESET       \ network dropped connection on reset
53 constant ECONABORTED     \ connection aborted
54 constant ECONNRESET      \ conneciton reset by peer
55 constant ENOBUFS         \ no buffer space available
56 constant EISCONN         \ socket is already connected
57 constant ENOTCONN        \ socket is not connected
58 constant ESHUTDOWN       \ can't send after socket shutdown
59 constant ETOOMANYREFS    \ too many references: can't splice
60 constant ETIMEDOUT       \ operation timed out
61 constant ECONNREFUSED    \ connection refused
62 constant ELOOP           \ too many levels of symbolic links
63 constant ENAMETOOLONG    \ filename too long
64 constant EHOSTDOWN       \ host is down
65 constant EHOSTUNREACH    \ no route to host
66 constant ENOTEMPTY       \ directory not empty

\ quotas and mush
67 constant EPROCLIM        \ too many processes
68 constant EUSERS          \ too many users
69 constant EDQUOT          \ Disk quota exceeded

\ network file system
70 constant ESTALE          \ stale NFS file handle
71 constant EREMOTE         \ too many levels of remote in path
72 constant EBADRPC         \ RPC struct is bad
73 constant ERPCMISMATCH    \ RPC version wrong
74 constant EPROGUNAVAIL    \ RPC prog. not available
75 constant EPROGMISMATCH   \ program version wrong
76 constant EPROCUNAVAIL    \ bad procedure for program
77 constant ENOLCK          \ no locks available
78 constant ENOSYS          \ function not implemented
79 constant EFTYPE          \ inappropriate file type or format
80 constant EAUTH           \ authentication error
81 constant ENEEDAUTH       \ Need authentication

\ intelligent device errors
82 constant EPWROFF         \ device power is off
83 constant EDEVERR         \ device error, e.g. paper out
84 constant EOVERFLOW       \ value too large to be stored in data type

\ program loading errors
85 constant EBADEXEC        \ bad executable
86 constant EBADARCH        \ bad cpu type in executable
87 constant ESHLIBVERS      \ shared library version mismatch
88 constant EBADMACHO       \ malformed Macho file

89 constant ECANCELED       \ operation canceled
90 constant EIDRM           \ identifier removed
91 constant ENOMSG          \ no message of desired type
92 constant EILSEQ          \ illegal byte seequence
93 constant ENOATTR         \ attribute not found

94 constant EBADMSG         \ bad message
95 constant EMULTIHOP       \ reserved?
96 constant ENODATA         \ no message available on STREAM
97 constant ENOLINK         \ reserved?
98 constant ENOSR           \ No stream resources
99 constant ENOSTR          \ not a STREAM

100 constant EPROTO         \ protocol error
101 constant ETIME          \ STREAM ioctl timeout
102 constant EOPNOTSUPP     \ operatio not supported on socket
103 constant ENOPOLICY      \ no such policy registered
104 constant ENOTRECOVERABLE \ state not recoverable
105 constant EOWNERDEAD     \ previous owner died

106 constant EQFULL         \ interface output queue is full

EQFULL constant ELAST       \ must be equal to the largest errno


