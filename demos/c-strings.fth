include lib/c-strings.fth

: demo { | cstr comment? -- , }
    z" /* hello, world */" 1+ CString.new -> cstr
    cstr CString.zstring zcount type cr
    cstr c" /\*" s\" \x8c/*\z" CString.$regex_replace if true -> comment? then
    cstr CString.zstring 20 dump
    comment? if ." comment! " then

;

demo cr
