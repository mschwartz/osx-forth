#!/usr/local/bin/pforth_standalone

\
\ phred editor
\
\ programmed by Mike Schwartz
\
\ Main program
\
\ false trace-include !

trace-include @ [if]
cr ." Compiling Phred" cr
[then]

7 constant GUTTER-WIDTH

create screen-resized false ,

defer ncurses-uninstall

require lib/debug.fth

variable theProject nullptr  theProject !

\
\ SIGNALS
\

: sigint-handler ; 
' sigint-handler is handle-SIGINT

: sighup-handler
    ncurses-uninstall 0 sys::exit
;
' sighup-handler is handle-SIGTERM
' sighup-handler is handle-SIGHUP

: sigchld-handler ; 
' sigchld-handler is handle-SIGCHLD

\ true to show help in gited window
variable gited-help true gited-help !

\
\ Deferred words/methods
\

defer Project.dump
defer Project.update
defer Project.load
defer Project.save

defer Screen.resize
defer Screen.paint
defer Screen.cursor-within
defer Screen.window-at
defer Screen.split-vertically
defer Screen.next-window
defer Screen.prev-window
defer Screen.left-window
defer Screen.right-window
defer Screen.remove-active
defer Screen.dump
defer Screen.press-any-key
defer Screen.execute
defer Screen.toggle-nerdtree

defer NERDTree.deactivate 
defer NERDTree.refresh

defer WindowList.active-window
defer WindowList.activate-window
defer WindowList.previous-window
defer WindowList.add
defer WindowList.find
defer WindowList.current
defer WindowList.current?
defer WindowList.find-or-create

defer Window.set-buffer
defer Window.refresh
defer Window.paint-statusline
defer Window.paint
defer Window.search             \ window search mode

defer CommandLine.move-to
defer CommandLine.run
defer CommandLine.erase

defer MessageLine.move-to
defer MessageLine.start
defer MessageLine.end
defer MessageLine.clear
defer MessageLine.$type
defer MessageLine.format
defer messageline{
defer }messageline
defer errorline{
defer }errorline

defer BufferList.saveall
defer Buffer.new
defer Buffer.write
defer Buffer.save
defer Buffer.clone
defer Buffer.save-undo
defer Buffer.save-redo
defer Buffer.undo
defer Buffer.redo

defer Bufed.refresh
defer Bufed.begin
defer Dired.read-dirlist
defer Dired.begin
defer Nerded.read-dirlist
defer Nerded.begin
defer Worded.begin
defer Helped.begin
defer Cmded.begin
defer Greped.begin
defer Gited.begin
defer Maned.begin

4 TAB-WIDTH !

1 constant editmode.visual
2 constant editmode.insert
3 constant editmode.selection
4 constant editmode.bufed
5 constant editmode.dired
6 constant editmode.worded
7 constant editmode.helped
8 constant editmode.cmded
9 constant editmode.greped
10 constant editmode.gited
11 constant editmode.selection
12 constant editmode.nerdtree
13 constant editmode.maned

variable editor-mode

require lib/xchar.fth
require sys/memory.fth
require lib/lists.fth
require lib/strings.fth
require lib/date.fth
require lib/hashmap.fth
require lib/arrays.fth
require lib/jsonc.fth
require lib/line.fth
require sys/regex.fth
require lib/parser.fth
require lib/c-strings.fth
require phred/recording.fth
require phred/macros.fth

require lib/icons.fth
require lib/vocabulary.fth
require lib/textfile.fth

require sys/stdio.fth
require sys/fcntl.fth
require sys/path.fth
require lib/dirlist.fth
require lib/git.fth

require lib/ansi.fth
require ncurses.fth
require ncurses/ncurses-use.fth
require ncurses/readline.fth

require phred/theme.fth             \ color scheme
require phred/fancy-strings.fth     \ fancy strings

require phred/buffer.fth
\ syntax highlighting
require lib/forth-keywords.fth
require lib/c++-keywords.fth

require phred/syntax/highlight-nerdtree.fth
require phred/syntax/highlight-forth.fth
require phred/syntax/highlight-c++.fth
require phred/syntax/highlight-markdown.fth
require phred/syntax/highlight-makefile.fth
require phred/syntax/highlight-git.fth
require phred/syntax/highlight-search.fth
require phred/syntax/highlight-man.fth
require phred/syntax/highlight-bufed.fth

require phred/syntax.fth
require phred/doc.fth

require phred/selection.fth

\ windows
include phred/windows/windowlist.fth
include phred/windows/utils.fth
include phred/windows/search.fth
include phred/windows/paint.fth
include phred/api.fth
include phred/window.fth

\ buffers
require phred/buffers/bufed.fth
require phred/buffers/dired.fth
require phred/buffers/worded.fth
require phred/buffers/helped.fth
require phred/buffers/cmded.fth
require phred/buffers/greped.fth
require phred/buffers/gited.fth
require phred/buffers/nerded.fth
require phred/buffers/maned.fth

require phred/nerdtree.fth
require phred/project.fth
require phred/screen.fth
require phred/commandline.fth
require phred/messageline.fth
include phred/windows/statusline.fth


: auto.term
    ncurses-uninstall
    ncurses.endwin
    false ncurses::focus_mode
    cr ." term " cr cr
    0 sys::raw
    auto.term
;

: phred { | b len n -- , main program }
    \ process command line arguments
    begin 
        next-arg dup 0<>
    while
        -> len -> n
        \ TODO: if this is a directory, open a dired!
        n Path.basename zcount Buffer.new -> b
        n len pad place-cstr
        b pad Path.realpathz 
        Buffer.set-filename
        b b s@ Buffer.fullpath zcount Buffer.read
        b BufferList.add
    repeat
    2drop

    Project.find  theProject !

    Screen.init
    Theme.init
    ncurses::stdscr Theme.default
    Screen.run
    Screen.destroy
;

' phred catch

ncurses-uninstall
ncurses.endwin
cr ." exiting " .s
cr .s
cr


