#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdio.h>
#include <fcntl.h>
#include <curses.h>

int main(int ac, char *av[]) {
  struct stat b;
  printf("AF_INET: %d %d\n", AF_INET, PF_INET);
  printf("SOCK_STREAM: %d\n", SOCK_STREAM);
  printf(" dev_t %lu\n", sizeof(dev_t));
  printf(" ino_t %lu\n", sizeof(ino_t));
  printf(" mode_t %lu S_IFMT %x sizeof(%lu)\n", sizeof(mode_t), S_IFMT, sizeof(&b.st_mode));
  printf(" offset %p %p %p\n", (void *)&b.st_mode, (void *)&b.st_ino, (void *)&b.st_dev);
  printf(" chtype %lu\n", sizeof(chtype));

  int fd = socket(AF_INET, SOCK_STREAM, 0);
  printf("fd: %d\n", fd);

  printf("sizeof(sockaddr_in): %lu\n", sizeof(struct sockaddr_in));
  return 0;
}
