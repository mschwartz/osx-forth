private{
    create filename 8192 allot

    : parse-filename { txt | ptr line# -- caddr u line# , parse filename:number: from string }
        filename -> ptr
        begin
            txt c@ ascii : <>
        while
            txt c@ ptr c!
            txt 1+ -> txt
            ptr 1+ -> ptr
        repeat
        txt 1+ -> txt   \ skip :
        0 ptr c!

        0 -> line# 
        begin
            txt c@ '0' >= txt c@ '9' <= and
        while
            txt c@ '0' -
            line# 10 * + -> line#
            txt 1+ -> txt
        repeat
        filename zcount line#
    ;
}private

: keymap.leader.greped  { w -- f , handle leader commands }
    keymap.key case
        ascii d of
            keymap.key case
                ascii e of
                    w Dired.begin
                    w Window.dired-mode
                endof
                ncurses.bell
            endcase
        endof
        ncurses.bell
    endcase
;

: Keymap.greped { w | n caddr u line# ch l b txt -- , run editor keymap for window }
    Keymap.input# -> n
    begin
        w Window.paint
        w Window.refresh
        Keymap.key -> ch
        Window.end-search
        begin
            ch ncurses.key_mouse = 
        while
            Keymap.mouse if exit then \ because a different window may have been clicked on (activated)
            Keymap.key -> ch
        repeat

        MessageLine.clear
        w Window.address-cursor

        w ch n keymap.common if
            exit
        then

        ch case
            ansi_escape of \ escape key
                ncurses.beep
                drop exit
            endof
            $ 0a of
                \ switch to selected buffer
                w s@ Window.current-line  -> l
                l s@ Line.text -> txt
                txt parse-filename -> line#  -> u -> caddr
                w caddr u WindowList.find-or-create -> w
                w Window.visual-mode
                w line# Window.goto-line
                w Window.paint
                exit
            endof
            ascii , of
                w Keymap.leader.greped
                exit
            endof
            \ default
            MessageLine.move-to w hex . decimal
        endcase
        w Window.refresh
    again
;

privatize
