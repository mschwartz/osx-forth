: ncurses-mouse.fth ;

cr ." compile ncurses-mouse.fth" cr
\
\ Mouse
\
ncurses::BUTTON1_PRESSED constant ncurses.BUTTON1_PRESSED
ncurses::BUTTON1_RELEASED constant ncurses.BUTTON1_RELEASED
ncurses::BUTTON1_CLICKED constant ncurses.BUTTON1_CLICKED
ncurses::BUTTON1_DOUBLE_CLICKED constant ncurses.BUTTON1_DOUBLE_CLICKED
ncurses::BUTTON1_TRIPLE_CLICKED constant ncurses.BUTTON1_TRIPLE_CLICKED

ncurses::BUTTON2_PRESSED constant ncurses.BUTTON2_PRESSED
ncurses::BUTTON2_RELEASED constant ncurses.BUTTON2_RELEASED
ncurses::BUTTON2_CLICKED constant ncurses.BUTTON2_CLICKED
ncurses::BUTTON2_DOUBLE_CLICKED constant ncurses.BUTTON2_DOUBLE_CLICKED
ncurses::BUTTON2_TRIPLE_CLICKED constant ncurses.BUTTON2_TRIPLE_CLICKED

ncurses::BUTTON3_PRESSED constant ncurses.BUTTON3_PRESSED
ncurses::BUTTON3_RELEASED constant ncurses.BUTTON3_RELEASED
ncurses::BUTTON3_CLICKED constant ncurses.BUTTON3_CLICKED
ncurses::BUTTON3_DOUBLE_CLICKED constant ncurses.BUTTON3_DOUBLE_CLICKED
ncurses::BUTTON3_TRIPLE_CLICKED constant ncurses.BUTTON3_TRIPLE_CLICKED

ncurses::BUTTON4_PRESSED constant ncurses.BUTTON4_PRESSED
ncurses::BUTTON4_RELEASED constant ncurses.BUTTON4_RELEASED
ncurses::BUTTON4_CLICKED constant ncurses.BUTTON4_CLICKED
ncurses::BUTTON4_DOUBLE_CLICKED constant ncurses.BUTTON4_DOUBLE_CLICKED
ncurses::BUTTON4_TRIPLE_CLICKED constant ncurses.BUTTON4_TRIPLE_CLICKED

ncurses::BUTTON5_PRESSED constant ncurses.BUTTON5_PRESSED
ncurses::BUTTON5_RELEASED constant ncurses.BUTTON5_RELEASED
ncurses::BUTTON5_CLICKED constant ncurses.BUTTON5_CLICKED
ncurses::BUTTON5_DOUBLE_CLICKED constant ncurses.BUTTON5_DOUBLE_CLICKED
ncurses::BUTTON5_TRIPLE_CLICKED constant ncurses.BUTTON5_TRIPLE_CLICKED
[then]
ncurses::BUTTON_SHIFT constant ncurses.BUTTON_SHIFT
ncurses::BUTTON_CTRL constant ncurses.BUTTON_CTRL
ncurses::BUTTON_ALT constant ncurses.BUTTON_ALT
ncurses::ALL_MOUSE_EVENTS constant ncurses.ALL_MOUSE_EVENTS
ncurses::REPORT_MOUSE_POSITION constant ncurses.REPORT_MOUSE_POSITION

:STRUCT MouseEvent
    long MouseEvent.id
    long MouseEvent.x
    long MouseEvent.y
    long MouseEvent.bstate
;STRUCT

: ncurses.has_mouse ( -- f , test if system has mouse )
    ncurses::has_mouse
;

MouseEvent last-mouse-event

: ncurses.getmouse { evt | err -- err , fill in MouseEvent evt and return true, or return false if no event available }
    ncurses::getmouse -> err
    \ these returned on stack from ncurses::getmouse:
    evt s! MouseEvent.bstate
    evt s! MouseEvent.x
    evt s! MouseEvent.y
    evt s! MouseEvent.id

    evt s@ MouseEvent.bstate last-mouse-event s! MouseEvent.bstate
    evt s@ MouseEvent.x last-mouse-event s! MouseEvent.x
    evt s@ MouseEvent.y last-mouse-event s! MouseEvent.y
    evt s@ MouseEvent.id last-mouse-event s! MouseEvent.id

    err
;

: ncurses.mouse-row ( -- row , current mouse row )
    last-mouse-event s@ MouseEvent.y
;
: ncurses.mouse-col ( -- col , current mouse col )
    last-mouse-event s@ MouseEvent.x
;
: ncurses.mouse-bstate ( -- bstate , current mouse bstate )
    last-mouse-event s@ MouseEvent.bstate
;
: ncurses.last-mouse-event ( -- evet , last mouse event )
    last-mouse-event
;

: ncurses.ungetmouse ( evt -- , push event onto the input queue )
    ncurses::ungetmouse
    drop
;

: ncurses.mousemask ( newmask -- oldmask accepted-mask )
    ncurses::mousemask
;

variable ncurses::initial-mouse-state 0 ncurses::initial-mouse-state !

: putchar ( ch -- ) sys::putchar drop ;
: ncurses.mouse-on ( f -- , enable all mosue events )
    if
        ncurses::mouseon
    \     ncurses.ALL_MOUSE_EVENTS ncurses.REPORT_MOUSE_POSITION OR ncurses::mousemask 
\         150 ncurses::mouseinterval drop
        0 ncurses::mouseinterval drop
        \   printf("\033[?1003h\n"); // Makes the terminal report mouse movement events

\         ansi_escape putchar 
\         ascii [ putchar
\         ascii ? putchar 
\             ascii 1 putchar 
\             ascii 0 putchar
\             ascii 0 putchar
\             ascii 3 putchar
\         ascii h putchar

    \     drop
    \     ncurses::initial-mouse-state !
    else
\     ncurses::mouseoff
\     0 ncurses.mousemask 2drop

    \  printf("\033[?1003l\n"); // Disable mouse movement events, as l = low

\     $ 1b putchar ascii [ putchar
\     ascii ? putchar 
\     ascii 1 putchar ascii 0 putchar ascii 0 putchar ascii 3 putchar 
\     ascii l putchar
    then
;

: auto.term
    false ncurses.mouse-on
    auto.term
;

: ncurses.wenclose ( win y x -- f , are screen coords within window )
    ncurses::wenclose
;

: ncurses.wmouse_trafo ( win y x to-screen -- y x err , transform screen to window coordinates )
    ncurses::wmouse_trafo
;

: ncurses.mouse_trafo ( y x to-screen -- y x err , transform screen to stdscr coordinates )
    ncurses::mouse_trafo
;


: ncurses.mouseinterval ( erval -- prev , set maxiumum time in thousands of a second that can elaps between press & release events , return previous value )
    ncurses::mouseinterval
;

cr ." end compile ncurses-mouse.fth"
