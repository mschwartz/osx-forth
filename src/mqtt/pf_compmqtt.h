#ifdef PF_SUPPORT_MQTT

CreateDicEntryC(ID_MQTT_init, "mqtt::init", 0);
CreateDicEntryC(ID_MQTT_cleanup, "mqtt::cleanup", 0);
CreateDicEntryC(ID_MQTT_lib_init, "mqtt::lib_init", 0);
CreateDicEntryC(ID_MQTT_lib_cleanup, "mqtt::lib_cleanup", 0);
CreateDicEntryC(ID_MQTT_new, "mqtt::new", 0);
CreateDicEntryC(ID_MQTT_destroy, "mqtt::destroy", 0);
CreateDicEntryC(ID_MQTT_reinitialize, "mqtt::reinitialize", 0);
CreateDicEntryC(ID_MQTT_connect, "mqtt::connect", 0);
CreateDicEntryC(ID_MQTT_reconnect, "mqtt::reconnect", 0);
CreateDicEntryC(ID_MQTT_disconnect, "mqtt::disconnect", 0);
CreateDicEntryC(ID_MQTT_publish, "mqtt::publish", 0);
CreateDicEntryC(ID_MQTT_subscribe, "mqtt::subscribe", 0);
CreateDicEntryC(ID_MQTT_unsubscribe, "mqtt::unsubscribe", 0);
CreateDicEntryC(ID_MQTT_loop, "mqtt::loop", 0);
CreateDicEntryC(ID_MQTT_strerror, "mqtt::strerror", 0);
CreateDicEntryC(ID_MQTT_lib_version, "mqtt::version", 0);
CreateDicEntryC(ID_MQTT_get_message, "mqtt::get_message", 0);
CreateDicEntryC(ID_MQTT_has_message, "mqtt::has_message", 0);

#endif
