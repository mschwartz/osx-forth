require st/utils.fth

: st.amatch { lin from pat | ii jj done stack offset  -- pos , match pattern and return position}
    0 -> stack
    from -> offset
    0 -> jj
    begin
        pat jj + c@ dup ST-EOS <>
    while
        ST-CLOSURE = if
            jj -> stack
            jj ST-CLOSIZE + -> jj
            offset -> ii
            begin
                lin ii + c@ ST-EOS <> 
                else
                    false
                then
            while
                lin ii pat jj st.omatch ST-NO = if
                    leave
                then
            repeat
            offset = k
        else
        then
    repeat
;

: st.match { lin pat | i -- f , find match anywhere on line }
    0 -> i
    begin
        lin i + c@ ST-EOS = if
            ST-NO exit
        then
        lin a pat amatch 0> if
            ST-YES exit
        then
        i 1+ -> i
    again
;