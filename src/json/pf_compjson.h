#ifdef PF_SUPPORT_JSON

CreateDicEntryC(ID_JSON_free, "json::free", 0);
CreateDicEntryC(ID_JSON_stringify, "json::stringify", 0);
CreateDicEntryC(ID_JSON_stringify_pretty, "json::stringify_pretty", 0);

CreateDicEntryC(ID_JSON_parse, "json::parse", 0);
CreateDicEntryC(ID_JSON_parse_file, "json::parse_file", 0);

CreateDicEntryC(ID_JSON_get_type, "json::get_type", 0);
CreateDicEntryC(ID_JSON_iterate, "json::iterate", 0);
CreateDicEntryC(ID_JSON_get_object, "json::get_object", 0);
CreateDicEntryC(ID_JSON_get_object_ex, "json::get_object_ex", 0);
CreateDicEntryC(ID_JSON_get_array, "json::get_array", 0);
CreateDicEntryC(ID_JSON_array_length, "json::array_length", 0);
CreateDicEntryC(ID_JSON_array_get_idx, "json::array_get_idx", 0);
CreateDicEntryC(ID_JSON_get_string, "json::get_string", 0);
CreateDicEntryC(ID_JSON_get_string_len, "json::string_len", 0);
CreateDicEntryC(ID_JSON_get_int64, "json::get_int64", 0);
CreateDicEntryC(ID_JSON_get_boolean, "json::get_boolean", 0);
CreateDicEntryC(ID_JSON_get_double, "json::get_double", 0);

CreateDicEntryC(ID_JSON_new_object, "json::new_object", 0);
CreateDicEntryC(ID_JSON_object_add, "json::object_add", 0);
CreateDicEntryC(ID_JSON_new_array, "json::new_array", 0);
CreateDicEntryC(ID_JSON_array_add, "json::array_add", 0);
CreateDicEntryC(ID_JSON_array_put_idx, "json::array_put_idx", 0);
CreateDicEntryC(ID_JSON_new_string, "json::new_string", 0);
CreateDicEntryC(ID_JSON_new_int64, "json::new_int64", 0);
CreateDicEntryC(ID_JSON_new_boolean, "json::new_boolean", 0);
CreateDicEntryC(ID_JSON_new_double, "json::new_double", 0);

CreateDicEntryC(ID_JSON_TYPE_null, "json::TYPE_null", 0);
CreateDicEntryC(ID_JSON_TYPE_boolean, "json::TYPE_boolean", 0);
CreateDicEntryC(ID_JSON_TYPE_double, "json::TYPE_double", 0);
CreateDicEntryC(ID_JSON_TYPE_int, "json::TYPE_int", 0);
CreateDicEntryC(ID_JSON_TYPE_object, "json::TYPE_object", 0);
CreateDicEntryC(ID_JSON_TYPE_array, "json::TYPE_array", 0);
CreateDicEntryC(ID_JSON_TYPE_string, "json::TYPE_string", 0);
#endif
