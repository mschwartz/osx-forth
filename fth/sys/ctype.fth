anew tag-sys/ctype.fth

: ctype::digittoint ( c -- n , convert character to its numerical value )
    ctype::digittoint
;
: ctype::isalnum ( c -- f , is alpha or is numeric )
    ctype::isalnum
;
: ctype::isalpha ( c -- f , is alpha character )
    ctype::isalpha
;
: ctype::isascii ( c -- f , is ascii character )
    ctype::isascii
;
: ctype::isblank ( c -- f , is blank character )
    ctype::isblank
;
: ctype::iscntrl ( c -- f , is cntrl character )
    ctype::iscntrl
;
: ctype::isdigit ( c -- f , is digit character )
    ctype::isdigit
;
: ctype::isgraph ( c -- f , is printing character except space and space-like characters )
    ctype::isgraph
;
: ctype::ishexnumber ( c -- f , is hex number character )
    ctype::ishexnumber
;
: ctype::isideogram ( c -- f , is ideographic character )
    ctype::isideogram
;
: ctype::islower ( c -- f , is lower case character )
    ctype::islower
;
: ctype::isnumber ( c -- f , is number character )
    ctype::isnumber
;
: ctype::isphonogram ( c -- f , is phonographic character )
    ctype::isphonogram
;
: ctype::isprint ( c -- f , is printing character )
    ctype::isprint
;
: ctype::ispunct ( c -- f , is punctuation character )
    ctype::ispunct
;
: ctype::isrune ( c -- f , is valid character in current character set )
    ctype::isrune
;
: ctype::isspace ( c -- f , is whitespace character )
    ctype::isspace
;
: ctype::isspecial ( c -- f , is special character )
    ctype::isspecial
;
: ctype::isupper ( c -- f , is upper case character )
    ctype::isupper
;
: ctype::isxdigit ( c -- f , is hex digit character )
    ctype::isxdigit
;
: ctype::toascii ( c -- a , convert character to ascii )
    ctype::toascii
;
: ctype::tolower ( c -- a , convert character to lower case )
    ctype::tolower
;
: ctype::toupper ( c -- a , convert character to upper case )
    ctype::toupper
;




