anew task-io/stdio.fth

require lib/tags.fth

0 constant _IOFBF       \ setvbuf should set fully buffered
1 constant _IOLBF       \ setvbuf should set line buffered
2 constant _IONBF       \ setvbuf should set unbuffered

1024 constant BUFSIZ    \ size of buffer used by setbuf
-1 constant EOF         \ end of file

20 constant FOPEN_MAX
1024 constant FILENAME_MAX

platform-id PLATFORM_ID_MACOS = [if]
create P_tmpdir s" /var/tmp" ",
[else]
create P_tmpdir s" /tmp" ",
[then]

private{
create mode-pad 4096 allot
create filename-pad 4096 allot
}private

: stdio.fopen { mode caddr u -- fp , open file }
    caddr u filename-pad place-cstr
    mode count mode-pad place-cstr
    mode-pad filename-pad sys::fopen
;

: stdio.fclose ( fp -- , close file )
    sys::fclose
;

: stdio.fgets ( buf max fp -- buf-or-null , read up to max bytes into buf from fp )
    sys::fgets
;

: stdio.fgetc ( fp -- ch, read character)
    sys::fgetc
;

: stdio.feof ( fp -- f )
    sys::feof
;

privatize

