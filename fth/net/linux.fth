anew task-net/linux.fth

\ ioctl_console

$ 4860 constant GIO_FONT        \ gets font in expanded form
$ 4861 constant PIO_FONT        \ use font in expanded form

$ 4b6b constant GIO_FONTX       \ get font using struct consolefontdesc
$ 4b6c constant PIO_FONTX       \ get font using struct consolefontdesc
:STRUCT consolefontdesc 
    USHORT charcount            \ characters in font
    USHORT charheight           \ scanlines per character
    APTR   chardata             \ font data in expanded form
;STRUCT

$ 4b6d constant PIO_FONTRESET   \ reset to default font

$ 4b70 constant GIO_CMAP        \ gets color palette on VGA+
$ 4b71 constant GIO_CMAP        \ sets color palette on VGA+

\ ... see linux/kd.h on a linux box for the rest of the constants that need to be implemented
