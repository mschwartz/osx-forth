: ncurses-video.fth ;

\ attributes
ncurses::A_NORMAL constant A_NORMAL
ncurses::A_ATTRIBUTES constant A_ATTRIBUTES
ncurses::A_COLOR constant A_COLOR
ncurses::A_STANDOUT constant A_STANDOUT
ncurses::A_UNDERLINE constant A_UNDERLINE
ncurses::A_REVERSE constant A_REVERSE
ncurses::A_BLINK constant A_BLINK
ncurses::A_DIM constant A_DIM
ncurses::A_BOLD constant A_BOLD
ncurses::A_ALTCHARSET constant A_ALTCHARSET
ncurses::A_INVIS constant A_INVIS
ncurses::A_PROTECT constant A_PROTECT
ncurses::A_HORIZONTAL constant A_HORIZONTAL
ncurses::A_LEFT constant A_LEFT
ncurses::A_LOW constant A_LOW
ncurses::A_RIGHT constant A_RIGHT
ncurses::A_TOP constant A_TOP
ncurses::A_VERTICAL constant A_VERTICAL
ncurses::A_ITALIC constant A_ITALIC

: ncurses.refresh ( -- ) 
    ncurses::refresh
;

: ncurses.getmaxyx ( win -- maxy maxx , get max cursor position)
    ncurses::getmaxyx
;

: ncurses.inch ( -- ch , get character/attr at cursor )
    ncurses::inch
;

: ncurses.mvinch ( y x -- ch , move and get character at cursor )
    ncurses::mvinch
;

: ncurses.addch ( ch -- , add character/attributes )
    ncurses::addch
    drop
;
: ncurses.addchstr ( ch -- , add character/attributes without wrap )
    ncurses::addchstr
    drop
;

: ncurses.mvaddch ( row col ch -- , move to row,col and output ch  )
    ncurses::mvaddch
    drop
;

: ncurses.addstr ( zaddr -- , add string )
    ncurses::addstr
    drop
;

: ncurses.mvaddstr ( y x zstr -- , move and add string to display )
    ncurses::mvaddstr
;

: ncurses.printw ( caddr u -- , print string to screen )
    pad place-cstr
    pad ncurses::printw
    drop
;

: ncurses.mvprintw { row col caddr u -- , move to row,col and print string }
    caddr u pad place-cstr
    row col pad ncurses::mvprintw
;

: ncurses.emit ( ch -- , )
    pad c!
    0 pad 1+ c!
    pad 2 ncurses.printw
;

: ncurses.# ( n -- , print n to screen )
    (.) ncurses.printw bl ncurses.addch
;

: ncurses.cr 
    ncurses::newline
;

: ncurses.move ( y x -- , move cursor to y,x )
    ncurses::move
    drop
;

: ncurses.erase ( -- , erase screen )
    ncurses::erase
    drop
;

: ncurses.clear ( -- , clear screen )
    ncurses::clear
    drop
;

: ncurses.cleartoeol ( -- , clear to end of line )
    ncurses::clrtoeol
    drop
;

: ncurses.attron ( attr -- , enable attribute )
    ncurses::attron
    drop
;

: ncurses.attroff ( attr -- , disable attribute)
    ncurses::attroff
    drop
;

\ lines and boxes

: ncurses.box ( win verch horch  -- , draw box )
    ncurses::box
    drop
;

: ncurses.mvhline ( y x ch n -- , render horizontal line)
    ncurses::mvhline
    drop
;

: ncurses.mvvline ( y x ch n -- , render vertical line )
    ncurses::mvvline
    drop
;
