\
\ Example 9: A simple color example
\

false trace-include !

require lib/ncurses.fth

: print-in-middle { win starty startx width caddr u | x y -- , print in middle of line }
    win null = if ncurses.stdscr -> win then
    win ncurses.getyx -> x -> y
    startx 0<> if startx -> x then
    starty 0<> if starty -> y then
    width 0= if 80 -> width then
    width u - 2 / startx + -> x
    win y x caddr u ncurses.mvwprintw
    ncurses.refresh
;

: demo  { | win COLOR_PAIR(1) -- , color demo }
    ncurses.initscr -> win
    ncurses.has_colors false = if
        ncurses.endwin
        cr ." Your terminal doesn't support color "
        1 sys::exit
    then

    ncurses.start_color
    1 NCURSES.COLOR_RED NCURSES.COLOR_BLACK ncurses.init_pair
    1 ncurses.color_pair -> COLOR_PAIR(1)

    COLOR_PAIR(1) ncurses.attron
    win LINES 2 / 0 0 s" Voila! in color :) " print-in-middle
    COLOR_PAIR(1) ncurses.attroff
    ncurses.getch
    ncurses.endwin
;

demo

