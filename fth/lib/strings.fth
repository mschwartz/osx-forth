anew task-lib/strings.fth

require sys/memory.fth 

: type-aligned { caddr u #spaces -- , type string } 
    #spaces 0< if
        #spaces negate u - spaces
    then
    u 0 do
        caddr c@ emit
        caddr 1+ -> caddr
    loop
    #spaces 0> if
        #spaces u - spaces
    then
;

: $duplicate { $string | len $new -- $copy , allocate a copy of $string }
    $string count 1+ -> len drop 
    len 1+ MEM.malloc -> $new
    $string $new len cmove 
    $new
;

: duplicate$ { caddr u | $new -- $copy , allocate a copy of $string }
    u MEM.malloc -> $new
    u $new !
    caddr $new 1+ u 1+ cmove 
    $new
;

: ZPLACE  { caddr u dst -- , move string and null terminate it }
    caddr dst u cmove
    0 dst u + c!
;

: c+!           \ c c-addr --
\ *G Add character C to the contents of address C-ADDR.
    tuck c@ + swap c!
;

: addchar       \ char string --
\ *G Add the character to the end of the counted string.
    tuck count + c!
    1 swap c+!
;

: append        \ c-addr u $dest --
\ *G Add the string described by C-ADDR U to the counted string at
\ ** $DEST. The strings must not overlap.
    >r
    tuck  r@ count +  swap cmove          \ add source to end
    r> c+!                                \ add length to count
;

: skip-blanks { caddr u | p c -- caddr u , skip blanks starting at caddr and return string/len }
    u -> c
    caddr -> p
    begin
        c 0 <= if
            0 0 exit \ all spaces
        then
        p c@ whitespace? not if
            p c exit
        then
        p 1+ -> p
        c 1- -> c
    again
;

: skip-token { caddr u ch | c -- caddr u , skip over ch }
    begin
        caddr c@ -> c
        c ch = 
    while
        c 0= if caddr u exit then
        caddr 1+ -> caddr
        u 1- -> u
    repeat
    caddr u
;
: skip-word { caddr u | ch -- caddr u , skip past word }
    begin
        caddr c@  -> ch
        ch ctype::isblank not
    while
        caddr 1+ c@ ctype::isspace if caddr u exit then
        caddr 1+ -> caddr
        u 1- -> u
        u 0< if caddr 0 exit then
    repeat
;

: parse-number { caddr u | caddr u n -- n , parse number from caddr }
    0 -> n
    u 0 do
        caddr c@ ctype::isdigit not if caddr zcount n exit then
        n 10 * -> n
        caddr c@ ascii 0 - n + -> n
        caddr 1+ ->  caddr
    loop
    caddr zcount n
;

create num-pad 64  allot

\ : append ( a1 n2 a2 --)
\     over over                  \ duplicate target and count           
\     >r >r                      \ save them on the return stack
\     count chars +              \ calculate offset target
\     swap chars move            \ now move the source string
\     r> r>                      \ get target and count
\     dup >r                     \ duplicate target and save one
\     c@ +                       \ calculate new count
\     r> c!                      \ get address and store
\ ;

: (.human) { d | base-save caddr u -- caddr u , human readable number format }
    base @ -> base-save decimal
    d GIGABYTES > if
        GIGABYTES /mod 
        \ integer portion
        (.) num-pad place
        \ decimal point
        pad ascii . $append.char
        \ fraction portion
        (.) num-pad append
        num-pad ascii G $append.char

        base-save base !
        num-pad count
        exit
    then
    d MEGABYTES > if
        MEGABYTES /mod 
        \ integer portion
        (.) num-pad place
        \ decimal point
        num-pad ascii . $append.char
        \ fraction portion
        (.) num-pad append
        num-pad ascii M $append.char
        base-save base !
        num-pad count
        exit
    then
    d KBYTES > if
        d KBYTES /mod  
        \ integer portion
        (.) num-pad place
        \ decimal point
        num-pad ascii . $append.char
        \ fraction portion
\         cr .s bye
        50 + 100 / (.) num-pad append
        num-pad ascii K $append.char
        base-save base !
        num-pad count
        exit
    then

    d (.) num-pad place 
    num-pad ascii B $append.char
    base-save base !
    num-pad count
;

: .human ( d -- , print human readable number format ) 
\     (.) type space
    (.human) type space
;

: .R.human { d l | caddr u -- , print human readable, right justified }
    d (.human)  -> u -> caddr 
    l u - spaces 
    caddr u type
;



: index-of { caddr ostr len | ptr ch -- n , return index of ostr in str or -1 if not found }
    caddr -> ptr
    begin
        caddr c@ -> ch
        ch
    while
        caddr ostr len string::strncmp 0= if
            ptr caddr -  exit
        then
        caddr 1+ -> caddr
    repeat
    -1
;
