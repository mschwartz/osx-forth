anew task-sys/getopt.fth

variable optind 0 optind !
variable optstr nullptr optstr !
variable opterr true opterr !
variable optopt 0 optopt c!
variable optarg 0 optarg !
variable optsize 1 optsize !

: strpos { str ch | c ndx -- pos or -1 , get position of ch in str }
    0 -> ndx
    begin
        str ndx + c@ -> c
        c ch = if ndx exit then
        c 0= if -1 exit then
        ndx 1+ -> ndx
    again
;

: getopt { ac av zoptions  | ch och str pos -- ch or -1 , implementaiton of C getopt see man 3 getopt }
    begin
        optind @ ac < 
    while
        av @ optind @ cells + @  -> str
        str c@ ascii - <> str c@ ascii + <> and if
            \ at end of options - non option arguments follow
            -1 exit
        then
        str 1+ c@ 0= if
            -1 exit
        then
        \ optstr is the -flags string
        optstr @ nullptr = if
            str 1+ optstr !                     \ skip over the initial -
        then
        optstr @ c@ -> ch
        optstr @ 1+ optstr !                    \ next optstr character
        ch 0= if                                \ end of optstr?
            optind @ optsize @ + optind !         \ to next argument
            1 optsize !
            nullptr optstr !
        else
            zoptions ch strpos -> pos
            ch optopt c!
            pos -1 = if
                opterr @ if cr ." *** getopt: unknown option " ch . ch emit then
                ascii ?  exit
            then
            ch optopt c!
            pos 1+ zoptions + c@ -> och
            och ascii : = if
                zoptions optstr @ c@ strpos -1 = if
                    optind @ 1+ optind !
                    optstr @ optarg !
                    nullptr optstr !
                    ch exit
                then
                2 optsize !
                optind @ 1+ ac >= if
                    opterr @ if cr ." *** getopt: " ch emit ."  requires an argument." then
                    ascii ?  exit
                then
                av @ optind @ 1+ cells + @  optarg !
            then
            ch exit
        then
    repeat
    -1
;
