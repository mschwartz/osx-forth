\
\ PHRed editor
\
\ programmed by Mike Schwartz
\
\ Makefile Syntax Highlighting
\
private{
create comment-replace OPCODE.comment c, ascii # c, 0 c,
create end-replace OPCODE.default c, 0 c,
create variable-replace OPCODE.h2 c, ascii $ c, ascii ( c, 0 c,
create end-variable-replace ascii ) c, OPCODE.default c, bl c, 0 c,

}private


: Highlight.makefile { text | dst new-text -- highlighted , syntax higlight one line of Makefile }
        text
        s\" ^#\z" drop
        s\" \x8c#\z" drop
    false regex.replace -> text
        text
        s\" $\z" drop
        s\" \x88\z" drop
    true regex.replace -> text
        text
        z" \$\(" 1+
        s\" \x91\$\(\x96\z" drop
    true regex.replace -> text
\         text
\         z" \) " 1+
\         s\" \x91\)\x88 \z" drop
\     true regex.replace -> text
        text
        z" \)" 1+
        s\" \x91\)\x88\z" drop
    true regex.replace -> text
    text exit
;

privatize
