\
\ PHRed editor
\
\ programmed by Mike Schwartz
\
\ Editor Window Search functions
\

\ ------------------------------------
\ for search
create search-string 512 allot

2 constant REGMATCH_COUNT

create search-regex sizeof() regex_t allot
variable search-regmatch REGMATCH_COUNT regex::alloc_regmatch_t search-regmatch !

variable rlsearch c" .search" Readline.new rlsearch ! 
true rlsearch @ s! Readline.insert-only

variable search-forwards true search-forwards !
variable search-match false search-match !  \ true if there is a match to render
variable search-row                         \ row of match
variable search-start                       \ start col of match
variable search-end                         \ end col of match

\ for saving/restoring window
Window search-save \ save entire window in this struct in case search fails

: Window.save-window { w -- , save window }
    w search-save sizeof() Window cmove
;
: Window.restore-window { w -- , save window }
    search-save w sizeof() Window cmove
;

: Window.end-search ( -- , end of search mode )
    false search-match !
;

create search-pad 1024 allot 0 search-pad c!

: Window.search-col { w l | col -- n , get column of search/match or -1 }
    regex.default regex.ignore-case
    l s@ Line.text w Window.current-line-index + search-pad regex.search  -> col
    col -1 = if col exit then
    col w Window.current-line-index + 
;

: Window.search-next { w | head cur l col -- index , search for next occurrence of pattern }
    w s@ Window.buffer Buffer.keystone -> head
    w s@ Window.current-line -> l
    l -> cur
    begin
        w l Window.search-col -> col
        col -1 <> if
            col exit 
        then

        search-forwards @ if
            w Window.last-line? if
                w Window.first-line
            else
                w Window.line-down
            then
        else
            w Window.first-line? if
                w Window.last-line
            else
                w Window.line-up
            then
        then
        w s@ Window.current-line -> l
            
    l cur = until
    -1
;

: Window.searchFn { k | w col -- , suggestFn for incrementally searching }
    rlsearch @ s@ Readline.userData -> w
    rlsearch @ Readline.text search-pad place-cstr
    w Window.search-next -> col
    col -1 = if
        w Window.restore-window
        MessageLine.start
        ." not found "
        MessageLine.end
        exit
    then

    col w s! Window.cursor-col
    w Window.fix-cursor
    w Window.address-cursor
    w Window.paint
    w Window.refresh
;

' Window.searchFn rlsearch @ s! Readline.suggestFn

: (Window.search) { w delim | col str len -- , incremental search }
    ncurses::stdscr Theme.default
    delim ascii / = if true else false then search-forwards !
    w Window.save-window
    w rlsearch @ s! Readline.userData

    CommandLine.move-to ncurses.cleartoeol delim emit
    ncurses::stdscr Theme.default
    rlsearch @ Readline.edit
    rlsearch @ Readline.text  -> len -> str

	len 0= if
        \ resume search for last pattern
        search-forwards if
            w Window.line-right     \ so we don't just match last position again
        else
            w Window.line-left      \ so we don't just match last position again
        then
        
\         w Window.findNext 
        w Window.search-next -> col
        col -1 = if
            w Window.restore-window
            MessageLine.start
            ." not found "
            MessageLine.end
            exit
        then
        col w s! Window.cursor-col
        w Window.fix-cursor
        w Window.address-cursor
        w Window.paint
        w Window.refresh
    then
;
' (Window.search) is Window.search
