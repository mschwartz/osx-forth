: Keymap.window { w -- f , handle window commands after ^W has been hit and return true if handled }
    keymap.key case
        ascii x of
            \ exchange windows
            c" exchange" MessageLine.$type
            true exit
        endof
        ascii s of
            \ split view/window vertically
            c" split vertically" MessageLine.$type
            Screen.split-vertically
            Screen.paint
            true exit
        endof
        ascii v of
            \ split view/window horizontally
            c" split horizontally" MessageLine.$type
            Screen.paint
            true exit
        endof
        ascii n of
            \ new view/window vertically, new empty buffer
            c" new" MessageLine.$type
            true exit
        endof
        ascii c of
            \ close view/window
            c" close" MessageLine.$type
            Screen.remove-active
            Screen.paint
            false exit
        endof
        ascii q of
            \ close view/window and quit if last window
            c" close/quit" MessageLine.$type
            Screen.paint
            true exit
        endof
        ascii w of
            \ new view/window
            Screen.next-window
            true exit
        endof
        ascii W of
            \ previous view/window
            c" prev" MessageLine.$type
            Screen.prev-window
            Screen.paint
            true exit
        endof
    endcase
    false
;
