: keymap.leader.visual  { w | b -- f , handle leader commands }
    w s@ Window.buffer -> b
    keymap.key case
        ascii f of
            \ format selection
        endof
        ascii s of
            \ save buffer
            b Buffer.save if
                messageline{
                    ." >>> Saved "
                    ascii " emit
                    b s@ Buffer.name zcount type
                    ascii " emit
                    ascii . emit
                }messageline
            else
                messageline{ 
                    ." Can't save Buffer " 
                    ascii " emit
                    w s@ Window.buffer s@ Buffer.name zcount type
                    ascii " emit
                    }messageline
                ncurses.bell
            then
            drop
            exit
        endof
        ascii b of
            keymap.key case
                ascii e of
                    w Bufed.begin
                    w Window.bufed-mode
                    drop true exit
                endof
                ncurses.bell
            endcase
        endof
        ascii l of
            0 search-pad c!
        endof
        ascii g of
            w Gited.begin
            w Window.gited-mode
\             w Window.paint
\             w Window.refresh
            exit
        endof
        ascii d of
            keymap.key case
                ascii e of
                    w Dired.begin
                    w Window.dired-mode
                    exit
                endof
                ascii a of              \ da introducer
                    keymap.key ascii w <> if exit then
                    \ delete (cut) word under the cursor and the space after or before it
                    begin
                        w Window.char-at-cursor whitespace? NOT
                    while
                        w false Window.delete-at-cursor
                    repeat
                    w true Window.delete-at-cursor
                endof
                ascii d of              
                    \ dd = delete line
                    w Window.delete-line
                endof
                ascii w of              
                    \ dw = delete word forward
                    w Window.del-word-forward
                    w Window.fix-cursor
                endof
                ascii i of              \ di introducer
                    keymap.key ascii w <> if true exit then
                    \ delete (cut) word under the cursor
                    w Window.delete-word-at-cursor
                    w Window.fix-cursor
                endof
                ncurses.bell
            endcase
        endof
        ncurses.bell
    endcase
;
