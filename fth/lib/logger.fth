\
\ Logging facility
\

anew task-lib/logger.fth

trace-include @  [if]
cr ." Compiling logger.fth "
[then]

private{
    variable logger-fp false logger-fp !
    variable stdout-fp sys::stdout stdout-fp !

    variable log-enabled
}private

: logging? log-enabled @ ;
: Logger.enable true log-enabled ! ;
: Logger.disable false log-enabled ! ;

false [if]
: Logger.open ( -- , initialize logger ) 
    0 exit
    logger-fp 0<> if exit then
    z" a" 1+ z" logfile.txt" 1+ sys::fopen logger-fp !
    logger-fp @ 0= if
        die{ c" can't open logfile " sys::perror }die
        0 sys::exit
    then
;
[then]

: Logger.close ( -- , close Logger )
    logger-fp @ 0= if
        logger-fp sys::fclose
        0< if
            die{ c" can't open logfile " sys::perror }die
            0 sys::exit
        then
    then
    0 logger-fp !
;

: Logger.flush ( -- , flush logger output )
    logger-fp @ 0<> if
        logger-fp @ sys::fflush drop
    then
;

: Logger.emit { c -- , emit c to logfile }
    logging? if
        c  logger-fp @ sys::fputc drop 
    then
;

: Logger.newline ( -- , output newline to log )
    logging? if
        ASCII_NEWLINE  Logger.emit
        Logger.flush 
    then
;
: Logger.cr ( -- , alias for Logger.newline )
    Logger.newline
;

: Logger.type { caddr u -- , type string to log }
    logging? if
        u 0 do
            caddr c@ Logger.emit
            caddr 1+ -> caddr
        loop
    then
    Logger.flush 
;

: Logger.num ( n -- , output number to log )
    dup abs 0 <# #s ROT SIGN #>
    Logger.type bl Logger.emit
    Logger.flush
\     Logger.close
;

: Logger.# ( n -- , alias for Logger.num ) Logger.num ;

: Logger.hex { n | sav -- output number as hex }
    base @ -> sav
    hex
    n Logger.#
    sav base !
    Logger.flush
;


: Logger.init ( -- , initialize logger )
    z" w" 1+ z" logfile.txt" 1+ sys::fopen logger-fp !
    logger-fp @ 0= if
        die{ c" can't open logfile " sys::perror }die
        0 sys::exit
    then
    cr cr cr ." ----- initialized logger " \ logger-fp @ .hex sys::stdout .hex
;
Logger.init
Logger.enable

Logger.cr 
s" Begin logging" Logger.type Logger.newline
s" -------------" Logger.type Logger.newline

privatize

trace-include @ [if]
cr ." End logger.fth "
[then]