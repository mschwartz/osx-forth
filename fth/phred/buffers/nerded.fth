private{

List file-List file-list List.init
variable nerded-parent 0 nerded-parent !
}private

: (nerded.read-dirlist)
    nerded-parent @ ?dup if mem::free  then
    file-list DirList.destroy-list
    current-directory zcount pad place 
    file-list pad count true DirList.read drop
;
' (nerded.read-dirlist) is nerded.read-dirlist

: nerded.dump ( -- , dump file-list )
    cr ." nerded file-list " cr
    file-list ['] dump-one List.iterate
;

: nerded.colorize { nam | ext -- color , colorize based upon extension }
    nam z" .gitignore" 1+ string::strcmp 0= if
        0 exit
    then
    nam z" Makefile" 1+ string::strcmp 0= if
        Opcode.file-makefile exit
    then
    nam Path.extension -> ext
    ext z" md" 1+ string::strcmp 0= if
        Opcode.file-md exit
    then
    ext z" fth" 1+ string::strcmp 0= if
        OPCODE.file-forth exit
    then
    ext z" txt" 1+ string::strcmp 0= if
        OPCODE.file-txt exit
    then
    ext z" h" 1+ string::strcmp 0= if
        OPCODE.file-c++ exit
    then
    ext z" cc" 1+ string::strcmp 0= if
        OPCODE.file-c++ exit
    then
    0 exit
    ext z" c" 1+ string::strcmp 0= if
        OPCODE.file-c++ exit
    then
    ext z" hpp" 1+ string::strcmp 0= if
        OPCODE.file-c++ exit
    then
    ext z" cpp" 1+ string::strcmp 0= if
        OPCODE.file-c++ exit
    then
    0
;
: nerded.add-dirent { buf dnode indent | nam typ -- , maybe format and add dnode entry to buf }
    dnode s@ DirNode.type -> typ
    dnode s@ DirNode.name -> nam
    typ DT_DIR = if
        nam z" ." 1+ string::strcmp 0= if exit then
        nam z" .." 1+ string::strcmp 0= if exit then
        nam z" .git" 1+ string::strcmp 0=  if exit then
    then

\     dnode s@ DirNode.statbuf s@ StatBuf.st_mode -> mode

    TAG_END
    OPCODE.default TAG_BYTE
    dnode TAG_DATA
    typ case 
        DT_DIR of
            ascii / TAG_CHAR 
        endof
    endcase
    nam zcount TAG_STRING
    s"  " TAG_STRING

    typ case
        DT_DIR of
            dnode s@ DirNode.flags 0<> if
                ICON.FOLDER-OPEN TAG_ICON
                OPCODE.ICON TAG_BYTE
                OPCODE.directory TAG_BYTE
            else
                ICON.FOLDER TAG_ICON
                OPCODE.ICON TAG_BYTE
                OPCODE.directory TAG_BYTE
            then
        endof
        DT_LNK of
            ICON.LINK TAG_ICON
            OPCODE.ICON TAG_BYTE
        endof
        DT_REG of
\             s"  " TAG_STRING
            ICON.FILE TAG_ICON
            OPCODE.ICON TAG_BYTE
            nam nerded.colorize ?dup if TAG_BYTE then
        endof
    endcase
    indent TAG_SPACES
\     s"  " TAG_STRING
    buf Buffer.format 
;

defer nerded.refresh-dir
: (nerded.refresh-dir) { buf nod indent | dnode frst -- , }
    nod -> frst
    frst -> dnode       \ directory node
    begin
        dnode s@ ListNode.next -> dnode
        dnode frst <>
    while
        dnode s@ DirNode.type DT_DIR = if
            dnode s@ DirNode.name 
            s" .bak" index-of -1 = if
                buf dnode indent nerded.add-dirent
                dnode s@ DirNode.flags 0<> if
                        buf                         \ buf
                        dnode DirNode.children +    \ nod
                        indent 2 +                  \ indent
                    nerded.refresh-dir
                then
            then
        then
    repeat

    frst -> dnode       \ directory node
    begin
        dnode s@ ListNode.next -> dnode
        dnode frst <>
    while
        dnode s@ DirNode.type DT_DIR <> if
            dnode s@ DirNode.name 
            s" .bak" index-of -1 = if
                buf dnode indent nerded.add-dirent
            then
        then
    repeat
;
' (nerded.refresh-dir) is nerded.refresh-dir

defer nerded.refresh
: (nerded.refresh) { buf indent | frst dnode -- , format NERDTree }
    buf Buffer.clear
    TAG_END
        s" Press ? for help" TAG_STRING
        0 TAG_DATA
    buf Buffer.format
    s" " pad place-cstr pad buf Buffer.append-text drop

    current-directory z" /" 1+ string::strcmp 0<> if
        sizeof() DirNode MEM::malloc nerded-parent !
        current-directory zcount pad place-cstr 
        pad z" /.." 1+ string::strcat drop
        pad Path.realpathz string::strdup nerded-parent @ s! DirNode.fullpath
        DT_DIR nerded-parent @ s! DirNode.type

        TAG_END
            s" .. (up a dir)" TAG_STRING
            nerded-parent @ TAG_DATA
        buf Buffer.format
    then

    TAG_END
        current-directory zcount TAG_STRING
    buf Buffer.format

        buf             \ buf
        file-list       \ nod
        1               \ indent
    nerded.refresh-dir
;
' (nerded.refresh) is nerded.refresh

: (nerded.begin) { w read? | buf dnode typ frst -- , initialize and attach buffer editor }
    read? if
        nerded.read-dirlist
    then
    
    s" NERDTree " BufferList.find ?dup if
        -> buf
    else
        s" NERDTree" Buffer.new -> buf
        buf BufferList.add
    then
    true buf s! Buffer.read-only
    Buffer.filetype.NERDTree buf s! Buffer.filetype
    true buf s! Buffer.permanent
    true buf s! Buffer.read-only
    true buf s! Buffer.hidden
    buf 1 nerded.refresh

    w buf Window.set-buffer
\     w Window.line-down
    w Window.line-down
    w Window.line-down
;
' (nerded.begin) is nerded.begin

: (NERDTree.refresh) { w | buf line# -- , refresh nerdtree }
        w s@ Window.buffer -> buf
        buf
        1                       \ indent
    nerded.refresh

    buf Buffer.first-line w s! Window.current-line
    buf Buffer.first-line w s! Window.top-line
    w s@ Window.line# -> line#
    1 w s! Window.line#
        0 w s! Window.cursor-row
        w line#
    Window.goto-line
;
' (NERDTree.refresh) is NERDTree.refresh

privatize
