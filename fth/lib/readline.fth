\ Readline - general purpose console line input with vim style keys for editing
\
\ Each Readline instance can have its own file to maintain history between operations.
\
anew task-lib/readline.fth

trace-include @ [if]
cr ." compiling lib/readline.fth " cr
[then]

require sys/memory.fth
require lib/strings.fth
require lib/lists.fth
require lib/line.fth
require lib/tui.fth

private{
0 constant editmode.insert
1 constant editmode.visual
2 constant editmode.selection

    64 constant DEFAULT_HISTORY_MAX
    variable editmode editmode.insert editmode !

    \ for using our readline for forth's query/interpret loop
    variable history.rlaccept 0 history.rlaccept !
}private

:STRUCT Readline 
    APTR Readline.filename
    struct List Readline.history
    APTR Readline.currentHistory        \ current line in history
    APTR Readline.lastHistory           \ last history in list
    UINT64 Readline.historyLength       \ current number of entries in history
    UINT64 Readline.historyMax          \ MAX number of entries in history
    APTR Readline.suggestFn             \ autosuggest function
    BOOL Readline.insert-only           \ only allow insert mode
    APTR Readline.userData              \ per Readline instance application defined data
;STRUCT

: Readline.historyList { rl -- keystone , get keystone of readline struct's history }
    rl Readline.history +
;

: Readline.dump { rl -- , dump readline }
    " Readline at $" rl .hex
    cr ."   . currentHistory = " rl s@ Readline.currentHistory .hex
    cr ."       .lastHistory = " rl s@ Readline.lastHistory .hex
    cr ."     .historyLength = " rl s@ Readline.historyLength .
    cr ."        .historyMax = " rl s@ Readline.historyMax .
    cr ."         .suggestFn = " rl s@ Readline.suggestFn .hex
    cr ."       .insert-only = " rl s@ Readline.insert-only .hex
    cr ."          .userData = " rl s@ Readline.userData .hex
;

\ TODO read and write history to/from file
: Readline.new { $filename | rl -- , initialize readline }
    sizeof() Readline MEM.malloc -> rl
    $filename $duplicate rl s! Readline.filename
    rl Readline.historyList  List.init 
    nullptr rl s! Readline.currentHistory 
    nullptr rl s! Readline.suggestFn
    nullptr rl s! Readline.userData
    false rl s! Readline.insert-only
    0 rl s! Readline.historyLength
    DEFAULT_HISTORY_MAX rl s! Readline.historyMax
    rl
;

: Readline.destroy { rl -- , destroy readline instance }
;

: Readline.last { rl -- lin , get last line in history }
    rl Readline.history List.last
;
: Readline.text { rl -- caddr u }
    rl s@ Readline.lastHistory
    s@ Line.text 
    dup string::strlen
;

: Readline.handle-common { k -- f , handle keystrokes common to both modes }
;

\ internal method called by Readline.edit, could be made private
: Readline.editLine { rl r c | k len r c start ecol lin txt handled -- , read characters from console into line }
    c -> ecol    \ editing column 

    s" " pad place-cstr       \ PLACE-CSTR  ( c-addr1 u1 c-addr2 -- )
    pad Line.new -> lin
    lin rl s! Readline.lastHistory
    lin rl  Readline.historyList  
    List.addTail

    lin s@ Line.text -> txt
    0 -> len

    lin rl s! Readline.currentHistory

    r c TUI.move-to TUI.erase.eol
    begin
        lin s@ Line.text -> txt
        r c TUI.move-to 
        txt len type 
        TUI.erase.eol
        r ecol TUI.move-to

        TUI.key -> k 
		false -> handled

        \ handle keys common to both edit modes
		k KEY.CTRL_D = if
			bye
		then

		k KEY.LEFT = if
			true -> handled
			ecol c > if
				ecol 1- -> ecol
			else
				7 TUI.emit
			then
		then

		k KEY.RIGHT = if
			true -> handled
			ecol c - len < if
				ecol 1+ -> ecol
			else
				7 TUI.emit
			then
		then

		k KEY.UP = if
			true -> handled
			rl Readline.historyList List.first lin <> if
				lin s@ ListNode.prev rl s! Readline.currentHistory
				rl s@ Readline.currentHistory -> lin
				lin s@ Line.text string::strlen -> len
				len c + -> ecol
			then
		then

		k KEY.DOWN = if
			true -> handled
			rl Readline.historyList List.last lin <> if
				lin s@ ListNode.next rl s! Readline.currentHistory
				rl s@ Readline.currentHistory -> lin
				lin s@ Line.text string::strlen -> len
				len c + -> ecol
			then
		then

		k KEY.NEWLINE  = if
			true -> handled
			0 lin s@ Line.text len + c!	\ null terminate it
			rl s@ Readline.lastHistory lin <> if
				\ assure selected line is lastHistory (new/added history)
				rl s@ Readline.lastHistory s@ Line.text MEM.free \ free old
				lin s@ Line.text 
				string::strdup 
				rl s@ Readline.lastHistory  s! Line.text \ copy new
			then
			exit
		then

        handled not if
			false -> handled
            editmode @ editmode.insert = if
				k KEY.ESCAPE = if
					rl s@ Readline.insert-only false = if
						editmode.visual editmode !
					then
					7 emit
					true -> handled
				then
				k KEY.DEL = if
					ecol c > if
						lin 
						ecol c - 1- 
						Line.delete-at-index drop
						ecol 1- -> ecol
					then
					true -> handled
				then
				k KEY.BACKSPACE = if
					ecol c > if
						lin ecol c - 1- Line.delete-at-index drop
						ecol 1- -> ecol
					then
					true -> handled
				then
				handled not if
					k bl >= if
						\ add character to input
						lin ecol c - k Line.insert-at-index  
						lin s@ Line.text string::strlen -> len
						ecol 1+ -> ecol
						rl s@ Readline.suggestFn ?dup if
							k swap execute 
						then
					then
				then
            else
                \ visual mode
                k case
                    KEY.DEL of
                        ecol c > if
                            ecol 1- -> ecol
                        then
                    endof
                    ascii h of
                        ecol c > if
                            ecol 1- -> ecol
                        else
                            7 TUI.emit
                        then
                    endof
                    ascii l of
                        ecol c - len < if
                            ecol 1+ -> ecol
                        else
                            7 TUI.emit
                        then
                    endof
                    ascii 0 of
                        c -> ecol
                    endof
                    ascii $ of
                        lin s@ Line.text string::strlen c + -> ecol
                    endof
                    ascii I of
                        c -> ecol
                        editmode.insert editmode !
                    endof
                    ascii A of
                        lin s@ Line.text string::strlen c + -> ecol
                        editmode.insert editmode !
                    endof
                    ascii i of
                        editmode.insert editmode !
                    endof
                    ascii a of
                        ecol c - len < if
                            ecol 1+ -> ecol
                        then
                        editmode.insert editmode !
                    endof
                    ascii x of
                        lin ecol c - Line.delete-at-index drop
                    endof
                    ascii D of
                        \ delete to end of line
                        begin
                            txt ecol c - + c@ 0 <> and
                            len 0> and
                        while
                            lin ecol c - Line.delete-at-index drop
                            len 1- -> len
                        repeat
                    endof
                    ascii d of
                        sys::key case
                            ascii d of
                                \ clear line
                                s" " txt place-cstr       \ PLACE-CSTR  ( c-addr1 u1 c-addr2 -- )
                                0 -> len 
                                c -> ecol
                            endof
                            ascii w of
                                \ delete word
                                begin
                                    txt ecol c - + c@ bl <> 
                                    txt ecol c - + c@ 0 <> and
                                    len 0> and
                                while
                                    lin ecol c - Line.delete-at-index drop
                                    len 1- -> len
                                repeat
                                begin
                                    txt ecol c - + c@ bl =
                                    txt ecol c - + c@ 0 <> and
                                    len 0> and
                                while
                                    lin ecol c - Line.delete-at-index drop
                                    len 1- -> len
                                repeat
                                len 0= if
                                    s" " txt place-cstr       \ PLACE-CSTR  ( c-addr1 u1 c-addr2 -- )
                                then
                            endof
                        endcase
                    endof
                    ascii b of    \ back word
                        ecol c <> if
                            ecol 1- -> ecol
                            begin
                                txt ecol c - + c@ bl = 
                            while
                                ecol 1- -> ecol
                                ecol c - < if
                                    c -> ecol
                                    leave
                                then
                            repeat

                            begin
                                txt ecol c - + c@ bl <> 
                                ecol c <> and
                            while
                                ecol 1- -> ecol
                            repeat
                            ecol c <> if
                                ecol 1+ -> ecol
                            then
                        then
                    endof
                    ascii w of    \ next word
                        begin
                            txt ecol c - + c@ bl <> 
                            ecol c - len < and
                        while
                            ecol 1+ -> ecol
                        repeat
                        begin
                            txt ecol c - + c@ bl =
                            ecol c - len < and
                        while
                            ecol 1+ -> ecol
                        repeat
                    endof
                endcase
            then
        then
    again
;

: Readline.edit { rl | r c -- , edit / read a line from terminal }
    TUI.REQUEST-ROW-COL 
    -> c        \ initial cursoor col
    -> r        \ initial cursoor row
	rl r c Readline.editLine 
\         false = if exit then
;

\ history! we can use our readline to replace accept so we get history and readline
\ . editing facilities in our forth query/interpret loop

: Readline.history-init ( -- , initialize readline for history )
    cr ." initializing readline history"
    history.rlaccept @ ?dup if
        Readline.destroy
    then
    cr ." initializing readline history"
    c" none" Readline.new history.rlaccept !
    cr ." initializing readline history"
;

: Readline.accept { addr amax -- numchars , accept replacement using readline for history }
    history.rlaccept @ Readline.edit
    history.rlaccept @ Readline.text 
    addr place-cstr
    addr string::strlen
;

: Readline.history-on ( -- , enable history )
    Readline.history-init
    ['] Readline.accept is accept
    cr ." Installed Readline history" cr
;

: Readline.history-off
    history.rlaccept @ ?dup if
        Readline.destroy
    then
    ['] (accept) is accept
;

if.forgotten Readline.history-off

\ : AUTO.INIT
\     AUTO.INIT
\     Readline.history-on
\ ;

\ Readline.history-on
privatize

trace-include @ [if]
cr ." End lib/readline.fth " cr
[then]
