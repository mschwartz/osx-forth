anew task-phred/syntax.fth

: Highlight.line { b l | typ txt -- zstr }
    l s@ Line.text -> txt
    txt c@ if
        b s@ Buffer.filetype -> typ 
        typ case
            Buffer.filetype.forth of txt Highlight.forth exit endof
            Buffer.filetype.c++ of txt Highlight.c++ exit endof
            Buffer.filetype.md of txt Highlight.markdown exit endof
            Buffer.filetype.makefile of txt Highlight.makefile exit endof
            Buffer.filetype.git of txt Highlight.git exit endof
            Buffer.filetype.nerdtree of txt Highlight.nerdtree exit endof
            Buffer.filetype.man of txt Highlight.man exit endof
            Buffer.filetype.bufed of l Highlight.bufed exit endof
        endcase
    then
    txt string::strdup 
;
