false trace-include !

require lib/ncurses.fth

true sys::raw
true sys::raw-io !

\ Example 1: hello, world per https://tldp.org/HOWTO/NCURSES-Programming-HOWTO/helloworld.html
: demo ( -- )
    ncurses.initscr drop
    s" Hello, world !!!" ncurses.printw
    ncurses.refresh
    ncurses.getch drop
    ncurses.endwin
;

demo