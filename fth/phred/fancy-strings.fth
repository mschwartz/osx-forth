\
\ PHRed editor
\
\ programmed by Mike Schwartz
\
\ # Fancy strings
\
\ These strings are enhanced ASCII (null terminated strings):
\   * TAB expands to TAB-WIDTH
\   * $ 20 through $ 7f are printable characters
\   * $ 80 through $ ff are op codes (see below)
\
\ Op codes are one or more bytes that determine what to render:
\   * $ 80 - (color_pair on) next byte indicates color_pair number
\           2 bytes follow: fg and bg (ncurses.COLOR_*)
\   * $ 81 - (mode on) next byte indicates display mode (ncurses A_* values)
\   * $ 82 - (mode off) next byte indicates display mode (ncurses A_* values)
\   * $ 83 - (icon) next byte indicates which icon to render:
\       * $ 00 - folder/directory
\       * $ 01 - link
\       * $ 02 - file
\ 

ASCII_TAB constant OPCODE.TAB
$ 80 constant OPCODE.bkgdset
$ 81 constant OPCODE.bkgdrestore
$ 82 constant OPCODE.attron
$ 83 constant OPCODE.attroff
$ 84 constant OPCODE.ICON
$ 85 constant OPCODE.save
$ 86 constant OPCODE.restore
$ 88 constant OPCODE.default
$ 89 constant OPCODE.current-line
$ 8a constant OPCODE.status-line
$ 8b constant OPCODE.keyword
$ 8c constant OPCODE.comment
$ 8d constant OPCODE.signature
$ 8e constant OPCODE.locals
$ 8f constant OPCODE.search
$ 90 constant OPCODE.h1
$ 91 constant OPCODE.h2
$ 92 constant OPCODE.h3
$ 93 constant OPCODE.h4
$ 94 constant OPCODE.h5
$ 95 constant OPCODE.code
$ 96 constant OPCODE.variable
$ 97 constant OPCODE.directory
$ 98 constant OPCODE.file-forth
$ 99 constant OPCODE.file-c++
$ 9a constant OPCODE.file-makefile
$ 9b constant OPCODE.file-txt
$ 9c constant OPCODE.file-md

\ ... more to come

$ 01 constant ICON.folder
$ 02 constant ICON.folder-open
$ 03 constant ICON.link
$ 04 constant ICON.file
$ 05 constant ICON.branch

\ ... more to come

private{
: print-tab { offset col | added -- col , expand tab using spaces offset by offset }
	col offset -  1+ -> col
    begin
		col TAB-WIDTH @ mod 0= if 
        col exit 
        then
        bl emit
        col 1+ -> col
    again
;

}private

: String.init ( str -- , initialize a string to empty )
    0 swap c!
;

: String.copy { dst src -- , copy zero terminated string to dst } 
    string::strcpy drop
;

: String.fputs { str fp -- flg , write a line and newline to FILE return success }
    str zcount pad place-cstr
    pad $ 0a String.append.char
    pad fp sys::fputs 0 >= if -1 else 0 then
;

: tab-to-col { col -- col , move col to next tab stop }
    col 1+ -> col
    begin
        col TAB-WIDTH @ MOD 0= if
            col exit
        then
		col 1+ -> col
    again
;

: String.length { str | ch len sav -- len , compute length of fancy string }
    0 -> len
    begin
        str c@ -> ch
    while
        ch case
            OPCODE.TAB of
                len tab-to-col -> len
            endof
            OPCODE.BKGDSET of
                str 1+ -> str       \ skip over color_pair
            endof
            OPCODE.BKGDRESTORE of
\                 str 0+ -> str       \ skip over color_pair
            endof
            str 1+ -> str             \ one byte
        endcase
        len 1+ -> len       \ as if we output a char
        str 1+ -> str
    repeat
    len
;

: String.append.char { str c | ptr -- , add character to string }
    str zcount + -> ptr
    c ptr c!
    ptr 1+ -> ptr
    0 ptr c!
;

: String.append.opcode { str op rg -- , append op+arg to string }
    str op String.append.char
    str rg String.append.char
;

: String.append { str to-append -- , append string to string }
    str to-append string::strcat drop
;

: String.$append { str $to-append -- , append string to string }
    $to-append count pad place-cstr 
    str pad string::strcat drop
;

: String.append-string { str caddr u -- , append string to string }
    caddr u pad place-cstr 
    str pad string::strcat drop
;

: String.format ( ... tags ) { str -- , append formatted values to string }
    begin
        dup TAG_END <>
    while
        case
            TAG_NUMBER of
                (.) str swap String.$append
            endof
            TAG_NUMBERX of
                base @ 
                    hex (.) str swap String.$append 
                base !
            endof
            TAG_$STRING of
                str swap String.$append
            endof
            TAG_STRING of \ caddr u
                pad place
                str pad String.$append
            endof
            TAG_CHAR of
                str swap String.append.char
            endof
            TAG_CHARX of
                base @ 
                    hex (.) str swap String.$append 
                base !
            endof
            die{ cr ." invalid tag " .hex }die
        endcase
    repeat
    drop
;

: String.move { dst src len | p ch new-ndx ndx skipped -- bytes-skipped , copy len columns from src to dst }
    0 -> ndx
    0 -> skipped
    begin
        src c@ -> ch
        ch 0= if
            0 dst c!
            skipped exit
        then
        ndx len < 
    while
        ch case
            OPCODE.TAB of
                ndx tab-to-col -> new-ndx
                new-ndx ndx - 0 do
                    skipped 1+ -> skipped
                    bl dst c! dst 1+ -> dst
                loop
                new-ndx -> ndx
                src 1+ -> src
            endof
            OPCODE.BKGDSET of
                ch dst c! dst 1+ -> dst \ store BKGDSET
                src 1+ -> src src c@ dst c! dst 1+ -> dst \ store color_pair
                skipped 2+ -> skipped
            endof
            ch dst c! dst 1+ -> dst
            src 1+ -> src
            ndx 1+ -> ndx
            skipped 1+ -> skipped
        endcase
    repeat
    skipped
;

: String.skip { p col | ch ndx -- p , skip forwards to column } 
    col not if p exit then
    p col + -> p
    p 
;

: String.print { p max-col | col ch pair bkgd -- , print max col characters to window }
    0 -> col
    begin
        col max-col <
    while
        p c@ -> ch
        p 1+ -> p
		ch 0= if
			exit
		then
        ch case
            OPCODE.TAB of
                0 col print-tab -> col
            endof
            OPCODE.BKGDSET of
                p c@ -> pair p 1+ -> p

                ncurses::stdscr ncurses.getbkgd -> bkgd
                pair A_BOLD OR ncurses.bkgdset 
                ncurses.refresh
            endof
            OPCODE.BKGDRESTORE of
                    bkgd
\                     A_NORMAL OR bl OR
                ncurses.bkgdset 
                ncurses.refresh
            endof
            OPCODE.ATTRON of
                p c@ 8 lshift p c@ or ncurses.attron drop
                p 2+ -> p
            endof
            OPCODE.ATTROFF of
                p c@ ncurses.attroff drop
                p 1+ -> p
            endof
            OPCODE.ICON of
            ncurses::stdscr ncurses.getyx ncurses.move
                p c@  
                case
                    ICON.folder of icon-folder 
                        ncurses.add_unicode
                    endof
                    ICON.folder-open of 
                        icon-folder-open
                        ncurses.add_unicode
                    endof
                    ICON.link of icon-link ncurses.add_unicode endof
                    ICON.file of icon-file ncurses.add_unicode endof
                    ICON.branch of icon-branch ncurses.add_unicode endof
                    bl emit
                endcase
                p 1+ -> p
                col 1+ -> col
            endof
            OPCODE.save of
                ncurses::stdscr ncurses.getbkgd -> bkgd
            endof
            OPCODE.restore of
                bkgd ncurses.bkgdset 
            endof
            OPCODE.default of
                ncurses::stdscr Theme.default
            endof
            OPCODE.current-line of
                ncurses::stdscr Theme.current-line
            endof
            OPCODE.status-line of
                ncurses::stdscr Theme.status-line
            endof
            OPCODE.keyword of
                ncurses::stdscr Theme.keyword
            endof
            OPCODE.comment of
                ncurses::stdscr Theme.comment
            endof
            OPCODE.signature of
                ncurses::stdscr Theme.signature
            endof
            OPCODE.locals of
                ncurses::stdscr Theme.locals
            endof
            OPCODE.search of
                ncurses::stdscr Theme.search
            endof
            OPCODE.h1 of ncurses::stdscr Theme.h1 endof
            OPCODE.h2 of ncurses::stdscr Theme.h2 endof
            OPCODE.h3 of ncurses::stdscr Theme.h3 endof
            OPCODE.h4 of ncurses::stdscr Theme.h4 endof
            OPCODE.h5 of ncurses::stdscr Theme.h5 endof
            OPCODE.code of ncurses::stdscr Theme.code endof
            OPCODE.variable of ncurses::stdscr Theme.variable endof
            OPCODE.directory of ncurses::stdscr Theme.keyword endof
            OPCODE.file-forth of ncurses::stdscr Theme.h2 endof
            OPCODE.file-c++ of ncurses::stdscr Theme.h4 endof
            OPCODE.file-makefile of ncurses::stdscr Theme.dir endof
            OPCODE.file-txt of ncurses::stdscr Theme.signature endof
            OPCODE.file-md of ncurses::stdscr Theme.variable endof
            \ default - print character
			ch emit
			col 1+ -> col
        endcase
    repeat
;

privatize
