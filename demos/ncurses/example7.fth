\
\ Example 7: Window border example
\

false trace-include !

require lib/ncurses.fth

3 constant height
10 constant width

: create_newwin  { hght wdth starty startx | win -- win , create a new window }
    hght wdth starty startx 
    ncurses.newwin -> win
    win 0 0 ncurses.box
    win ncurses.wrefresh
    win
;

: destroy_win { win -- , destroy window - erase borders, etc. }
    win bl bl bl bl bl bl bl bl ncurses.wborder
    win ncurses.wrefresh
    win ncurses.delwin
;

: demo { | win my_win ch lines cols startx starty -- , window border example }
    ncurses.initscr -> win
    ncurses.raw
    win  true ncurses.keypad
    ncurses.noecho
    ncurses.cbreak

    win ncurses.getmaxyx -> cols -> lines

    lines height  - 2/ -> starty
    cols width - 2/ -> startx

    s" Press F1 to exit" ncurses.printw
    ncurses.refresh

    height width starty startx create_newwin -> my_win

    begin
        ncurses.getch -> ch
        ch ncurses.key_f1 <>
        ch ascii q <> and
    while
        ch case
            ncurses.key_left of
                my_win destroy_win
                startx 1- -> startx
                height width starty startx create_newwin -> my_win
            endof
            ncurses.key_right of
                my_win destroy_win
                startx 1+ -> startx
                height width starty startx create_newwin -> my_win
            endof
            ncurses.key_up of
                my_win destroy_win
                starty 1- -> starty
                height width starty startx create_newwin -> my_win
            endof
            ncurses.key_down of
                my_win destroy_win
                starty 1+ -> starty
                height width starty startx create_newwin -> my_win
            endof
        endcase
    repeat
    ncurses.endwin
;

demo

