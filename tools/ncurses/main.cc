#define _XOPEN_SOURCE_EXTENDED

// #include <ncurses.h>

#include <stdlib.h>
#include <stdio.h>
#include <ncurses.h>
#include <assert.h>

int main(int ac, char *av[]);

#if 1
int main(int ac, char *av[]) {
  int c = 0;
  MEVENT e;

  if (initscr() == NULL)
    return -5;

  raw();
  noecho();
  cbreak();
  keypad(stdscr, TRUE);
  curs_set(0);

  mousemask(ALL_MOUSE_EVENTS | REPORT_MOUSE_POSITION, NULL);

  do {
    c = getch();
    printw("%x\n", c);
    fflush(stdout);
    if (c == KEY_MOUSE)
      printf("\r\nmouse was pressed\n");

  } while (c != 4);

  clear();
  endwin();
  return 0;
}
#else
int main(int argc, char *argv[]) {
  initscr();     /* Start curses mode 		*/
  start_color(); /* Start color functionality	*/
  printw("LINES = %d COLS = %d\n\n", LINES, COLS);

  init_pair(1, COLOR_CYAN, COLOR_BLACK);
  printw("A Big string which i didn't care to type fully ");
  mvchgat(2, 0, -1, A_BLINK, 1, NULL);
  /*
   * First two parameters specify the position at which to start
   * Third parameter number of characters to update. -1 means till
   * end of line
   * Forth parameter is the normal attribute you wanted to give
   * to the charcter
   * Fifth is the color index. It is the index given during init_pair()
   * use 0 if you didn't want color
   * Sixth one is always NULL
   */
  refresh();
  getch();
  move(20, 20);
  echo_wchar(WACS_DEGREE);
  getch();

  endwin(); /* End curses mode		  */
  return 0;
}
#endif