anew task-sdl-video.fth

\ types for SDL_FlashWindow
0 constant SDL_FLASH_CANCEL
1 constant SDL_FLASH_BRIEFLY
2 constant SDL_FLASH_UNTIL_FOCUSED

\ window flags for SDL_CreateWindow
$ 00000001 constant SDL_WINDOW_FULLSCREEN           \ fullscreen window 
$ 00000002 constant SDL_WINDOW_OPENGL               \ window usable with OpenGL context
$ 00000004 constant SDL_WINDOW_SHOWN                \ window is visible
$ 00000008 constant SDL_WINDOW_HIDDEN               \ window is not visible
$ 00000010 constant SDL_WINDOW_BORDERLESS           \ no window decoration
$ 00000020 constant SDL_WINDOW_RESIZABLE            \ window can be resized
$ 00000040 constant SDL_WINDOW_MINIMIZED            \ window is minimized
$ 00000080 constant SDL_WINDOW_MAXIMIZED            \ window is maximized
$ 00000100 constant SDL_WINDOW_MOUSE_GRABBED        \ window has grabbed mouse input
$ 00000200 constant SDL_WINDOW_INPUT_FOCUS          \ window has input focus
$ 00000400 constant SDL_WINDOW_MOUSE_FOCUS          \ window has mouse focus
$ 00001001 constant SDL_WINDOW_FULLSCREEN_DESKTOP   \ fullscreen desktop window
$ 00000800 constant SDL_WINDOW_FOREIGN              \ window not created by SDL
$ 00002000 constant SDL_WINDOW_ALLOW_HIGHDPI        \ window should be created in high-DPI mode if supported.
                                                    \ On macOS NSHighResolutionCapable must be set true in the
                                                    \ application's Info.plist for this to have any effect. */
$ 00004000 constant SDL_WINDOW_MOUSE_CAPTURE        \ window has mouse captured (unrelated to mouse grabbed)
$ 00008000 constant SDL_WINDOW_ALWAYS_ON_TOP        \ window should always be above others
$ 00010000 constant SDL_WINDOW_SKIP_TASKBAR         \ window should not be added to task bar
$ 00020000 constant SDL_WINDOW_UTILITY              \ window should be treated as a utility window
$ 00040000 constant SDL_WINDOW_TOOLTIP              \ window should be treated as a tooltip
$ 00080000 constant SDL_WINDOW_POPUP                \ window should be treated as a popup menu
$ 00100000 constant SDL_WINDOW_KEYBOARD_GRABBED     \ window has grabbed keyboartd input
$ 10000000 constant SDL_WINDOW_VULKAN               \ window usable for Vulcan surface
$ 20000000 constant SDL_WINDOW_METAL                \ window usable for Metal view
SDL_WINDOW_MOUSE_GRABBED constant SDL_WINDOW_INPUT_GRABBED

\ Window position special values

$ 1fff000 constant SDL_WINDOWPOS_UNDEFINED_MASK
: SDL_WINDOWPOS_UNDEFINED_DISPLAY ( x -- constant , )
    SDL_WINDOWPOS_UNDEFINED_MASK OR
; immediate
0 SDL_WINDOWPOS_UNDEFINED_DISPLAY constant SDL_WINDOWPOS_UNDEFINED
: SDL_WINDOWPOS_ISUNDEFINED ( x -- bool , test if windowpos value is "undefined" )
    $ ffff0000 and SDL_WINDOWPOS_UNDEFINED_MASK =
; immediate


$ 2fff000 constant SDL_WINDOWPOS_CENTERED_MASK
: SDL_WINDOWPOS_CENTERED_DISPLAY ( x -- constant , )
    SDL_WINDOWPOS_CENTERED_MASK OR
; immediate
0 SDL_WINDOWPOS_CENTERED_DISPLAY constant SDL_WINDOWPOS_CENTERED
: SDL_WINDOWPOS_ISCENTERED ( x -- bool , test if windowpos value is "undefined" )
    $ ffff0000 and SDL_WINDOWPOS_CENTERED_MASK =
; immediate

