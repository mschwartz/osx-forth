anew task-sdl-surface.fth

trace-include @ [if]
cr ." compiling sdl/surface.fth"
[then]

\ all SDL_Surface members are read-only
:STRUCT SDL_Surface 
    UINT32 SDL_Surface.flags
    APTR SDL_Surface.format         \ ptr to SDL_PixelFormat
    INT  SDL_Surface.w
    INT  SDL_Surface.h
    INT  SDL_Surface.pitch
    APTR SDL_Surface.pixels
    APTR SDL_Surface.userdata
    INT  SDL_Surface.locked
    APTR SDL_Surface.list_blitmap   \ private list of BltMap that hold a reference to this surface
    SDL_Rect SDL_Surface.clip_rect
    APTR SDL_Surface.map            \ private info for fast blit mapping to other surfaces
    INT  SDL_Surface.refcount
;STRUCT

: dump-surface { surface -- , dump surface struct }
    cr ." --------"
    cr surface ." SDL_Surface struct at $" .hex
    cr surface s@ SDL_Surface.flags .s ."     flags $" .hex
    cr surface s@ SDL_Surface.format .s ."     format @ $" .hex
    cr surface s@ SDL_Surface.w ."     w " .
    cr surface s@ SDL_Surface.h ."     h " .
    cr surface s@ SDL_Surface.pitch ."     pitch " .
    cr surface s@ SDL_Surface.pixels ."     pixels @ $" .hex
    cr ." --------" 
;

:STRUCT SDL_Color 
    UINT8 SDL_Color.r;
    UINT8 SDL_Color.g;
    UINT8 SDL_Color.b;
    UINT8 SDL_Color.a;
;STRUCT

:STRUCT SDL_Palette 
    INT SDL_Palette.ncolors
    APTR SDL_Palette.colors         \ array of SDL_Color as SDL_Color*
    UINT32 SDL_Palette.version
    INT SDL_Palette.refcount
;STRUCT

:STRUCT SDL_PixelFormat
    UINT32 SDL_PixelFormat.format
    APTR SDL_PixelForamt.palette    \ ptr to SDL_Palette
    UINT8  SDL_PixelFormat.BitsPerPixel
    UINT8  SDL_PixelFormat.BytesPerPixel
    UINT8  SDL_PixelFormat.padding1
    UINT8  SDL_PixelFormat.padding2
    UINT32 SDL_PixelFormat.Rmask
    UINT32 SDL_PixelFormat.Gmask
    UINT32 SDL_PixelFormat.Bmask
    UINT32 SDL_PixelFormat.Amask
    UINT8  SDL_PixelFormat.Rloss
    UINT8  SDL_PixelFormat.Gloss
    UINT8  SDL_PixelFormat.Bloss
    UINT8  SDL_PixelFormat.Aloss
    UINT8  SDL_PixelFormat.Rshift
    UINT8  SDL_PixelFormat.Gshift
    UINT8  SDL_PixelFormat.Bshift
    UINT8  SDL_PixelFormat.Ashift
    INT    SDL_PixelFormat.refcount
    APTR   SDL_PixelFormat.next
;STRUCT

: dump-pixelformat { pf -- , dump SDL_PixelFormat struct }
    cr ." dump-pixelformat " .s
    cr ." --------"
    cr pf ." SDL_PixelFormat struct at $" .hex
    cr pf s@ SDL_PixelFormat.format ."     SDL_PixelFormat.format $" dup .hex SDL_GetPixelFormatName ztype
    cr pf s@ SDL_PixelFormat.BitsPerPixel ."     BitsPerPixel " .
    cr pf s@ SDL_PixelFormat.BytesPerPixel ."     BitsPerPixel " .
    cr ." --------" 
    cr ." /dump-pixelformat " .s
;

$ 00000000 constant SDL_SWSURFACE
$ 00000001 constant SDL_PREALLOC
$ 00000002 constant SDL_RLEACCEL
$ 00000004 constant SDL_DONTFREE
$ 00000008 constant SDL_SIMD_ALIGNED

$0 constant SDL_YUV_CONVERSION_JPEG
$0 constant SDL_YUV_CONVERSION_BT601
$0 constant SDL_YUV_CONVERSION_BT709
$0 constant SDL_YUV_CONVERSION_AUTOMATIC

trace-include @ [if]
cr ." End of sdl/surface.fth "
[then]