anew task-sdl-keycods.fth

$  0 constant SDLK_UNKNOWN
$  c constant SDLK_RETURN
$  8 constant SDLK_BACKSPACE
$  9 constant SDLK_TAB
ascii space constant SDLK_SPACE
ascii ! constant SDLK_EXCLAIM
ascii " constant SDLK_QUOTEDBL \ "
ascii # constant SDLK_HASH
ascii % constant SDLK_PERCENT
ascii $ constant SDLK_DOLLAR
ascii & constant SDLK_AMPERSAND
ascii ' constant SDLK_QUOTE
ascii ( constant SDLK_LEFTPAREN
ascii ) constant SDLK_RIGHTPAREN
ascii * constant SDLK_ASTERISK
ascii + constant SDLK_PLUS
ascii , constant SDLK_COMMA
ascii - constant SDLK_MINUS
ascii . constant SDLK_PERIOD
ascii / constant SDLK_SLASH
ascii 0 constant SDLK_0
ascii 1 constant SDLK_1
ascii 2 constant SDLK_2
ascii 3 constant SDLK_3
ascii 4 constant SDLK_4
ascii 5 constant SDLK_5
ascii 6 constant SDLK_6
ascii 7 constant SDLK_7
ascii 8 constant SDLK_8
ascii 9 constant SDLK_9
ascii : constant SDLK_COLON
ascii ; constant SDLK_SEMICOLON
ascii < constant SDLK_LESS
ascii = constant SDLK_EQUALS
ascii > constant SDLK_GREATER
ascii ? constant SDLK_QUESTION
ascii @ constant SDL_AT
\ SKIP UPPERCASE LETTERS
ascii [ constant SDLK_LEFTBRACKET
ascii \ constant SDLK_BACKSLASH
ascii ] constant SDLK_RIGHTBRACKET
ascii ^ constant SDLK_CARET
ascii _ constant SDLK_UNDERSCORE
ascii ` constant SDLK_BACKQUOTE
ascii a constant SDLK_a
ascii b constant SDLK_b
ascii c constant SDLK_c
ascii d constant SDLK_d
ascii e constant SDLK_e
ascii f constant SDLK_f
ascii g constant SDLK_g
ascii h constant SDLK_h
ascii i constant SDLK_i
ascii j constant SDLK_j
ascii k constant SDLK_k
ascii l constant SDLK_l
ascii m constant SDLK_m
ascii n constant SDLK_n
ascii o constant SDLK_o
ascii p constant SDLK_p
ascii q constant SDLK_q
ascii r constant SDLK_r
ascii s constant SDLK_s
ascii t constant SDLK_t
ascii u constant SDLK_u
ascii v constant SDLK_v
ascii w constant SDLK_w
ascii x constant SDLK_x
ascii y constant SDLK_y
ascii z constant SDLK_z
$ 7f constant SDLK_DELETE
\ TODO: MISSING consants from SDK_keycode.h;

\ enumeration of valid key mods (possibly OR'd together)
\ type: SDL_KeyMod
$ 0000 constant KMOD_NONE
$ 0001 constant KMOD_LSHIFT
$ 0002 constant KMOD_RSHIFT
$ 0040 constant KMOD_LCTRL
$ 0080 constant KMOD_RCTRL
$ 0100 constant KMOD_LALT
$ 0200 constant KMOD_RALT
$ 0400 constant KMOD_LGUI
$ 0800 constant KMOD_RGUI
$ 1000 constant KMOD_NUM
$ 2000 constant KMOD_CAPS
$ 4000 constant KMOD_MODE
$ 8000 constant KMOD_SCROLL
KMOD_LCTRL KMOD_RCTRL    OR constant KMOD_CTRL
KMOD_LSHIFT KMOD_RSHIFT OR constant KMOD_SHIFT
KMOD_LALT KMOD_RALT     OR constant KMOD_ALT
KMOD_LGUI KMOD_RGUI     OR constant KMOD_GUI
KMOD_SCROLL                constant KMOD_RESERVED
