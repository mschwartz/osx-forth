anew task-lib/semaphore.fth

require sys/memory.fth
require sys/fcntl.fth

32767 constant SEM_VALUE_MAX
-1 constant SEM_FAILED
octal 666 constant SEM_DEFAULT_MODE

: Semaphore.new { caddr u flags | sem -- sem , create a semaphore }
    caddr u pad place-cstr
    pad flags SEM_DEFAULT_MODE 1 sys::sem_open_ex -> sem
    sem
;

: Semaphore.unlink { caddr u | sem -- sem , create a semaphore }
    caddr u pad place-cstr
    pad sys::sem_unlink
    sem
;

: Semaphore.close { sem -- f , destroy Semaphore }
    sem sys::sem_close
;

: Semaphore.obtained? { sem -- f , is Semaphore obtained already? }
    sem sys::sem_trywait
;

: Semaphore.obtain { sem -- f , obtain Semaphore }
    sem sys::sem_wait 
;

: Semaphore.release { sem -- f , release Semaphore }
    sem sys::sem_post
;

