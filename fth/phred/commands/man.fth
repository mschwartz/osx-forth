\
\ phred editor
\
\ programmed by Mike Schwartz
\
\ Command Line handling
\   :man command
\

anew task-phred/commands/man.fth

: CommandLine.man { w str tok toku -- flg , return true if executed }
    tok toku s" man" compare 0= if
        str CString.skip-blanks
        w str CString.rest maned.begin
        w Window.maned-mode
        Buffer.filetype.man w s@ Window.buffer s! Buffer.filetype 
        true
    else
        false
    then
;
