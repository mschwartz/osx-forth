anew task-lib/mqtt.fth

require lib/eventemitter.fth

-4 constant MOSQ_ERR_AUTH_CONTINUE
-3 constant MOSQ_ERR_NO_SUBSCRIBERS
-2 constant MOSQ_ERR_SUB_EXISTS
-1 constant MOSQ_ERR_CONN_PENDING
 0 constant MOSQ_ERR_SUCCESS
 1 constant MOSQ_ERR_NOMEM
 2 constant MOSQ_ERR_PROTOCOL
 3 constant MOSQ_ERR_INVAL
 4 constant MOSQ_ERR_NO_CONN
 5 constant MOSQ_ERR_CONN_REFUSED
 6 constant MOSQ_ERR_NOT_FOUND
 7 constant MOSQ_ERR_CONN_LOST
 8 constant MOSQ_ERR_TLS
 9 constant MOSQ_ERR_PAYLOAD_SIZE
10 constant MOSQ_ERR_NOT_SUPPORTED
11 constant MOSQ_ERR_AUTH
12 constant MOSQ_ERR_ACL_DENIED
13 constant MOSQ_ERR_UNKNOWN
14 constant MOSQ_ERR_ERRNO
15 constant MOSQ_ERR_EAI
16 constant MOSQ_ERR_PROXY
17 constant MOSQ_ERR_PLUGIN_DEFER
18 constant MOSQ_ERR_MALFORMED_UTF*
19 constant MOSQ_ERR_KEEPALIVE
20 constant MOSQ_ERR_LOOKUP
21 constant MOSQ_ERR_MALFORMED_PACKET
22 constant MOSQ_ERR_DUPLICATE_PROPERTY
23 constant MOSQ_ERR_TLS_HANDSHAKE
24 constant MOSQ_ERR_QOS_NOT_SUPPORTED
25 constant MOSQ_ERR_OVERSIZE_PACKET
26 constant MOSQ_ERR_OSCP
27 constant MOSQ_ERR_TIMEOUT
28 constant MOSQ_ERR_RETAIN_NOT_SUPPORTED
29 constant MOSQ_ERR_TOPIC_ALIAS_INVALID
30 constant MOSQ_ERR_ADMINISTRATIVE_ACTION
31 constant MOSQ_ERR_ALREADY_EXISTS

0 constant MQTTMessageTypeUnknown
1 constant MQTTMessageTypeConnect
2 constant MQTTMessageTypeDisconnect
3 constant MQTTMessageTypeMessage
4 constant MQTTMessageTypePublish

:STRUCT MQTTMessage
    APTR MQTTMessage.next;
    UINT64 MQTTMessage.type
    APTR MQTTMessage.topic
    APTR MQTTMessage.payload
;STRUCT

: MQTT.strerror ( err# -- caddr u )
    MQTT::strerror zcount
;

\ MQTT.onMessage is called for every message, no matter the topic
\  This is important to handle topics that end with wildcard #
\  since the incoming topic won't match the subscription topic.

:STRUCT MQTT 
    APTR MQTT.handle
    APTR MQTT.host
    ULONG MQTT.port
    LONG MQTT.error
    APTR MQTT.emitter
    APTR MQTT.onConnect         \ handler for onConnect event
    APTR MQTT.onMessage         \ handler for onMessage event
    APTR MQTT.onPublish         \ handler for onPublish event
    APTR MQTT.onDisconnect      \ handler for onDisconnect event
;STRUCT

: MQTT.dump { m -- , dump MQTT struct }
    cr ." MQTT $" m .hex
    cr ."   handle " m s@ MQTT.handle .hex
    cr ."   host " m s@ MQTT.host 10 dump
    cr ."   port " m s@ MQTT.port .
    cr ."   error " m s@ MQTT.error .
    cr
;

: MQTT.new { host0 port | m -- m , new MQTT }
    sizeof() MQTT mem::malloc -> m
    host0 string::strdup m s! MQTT.host
    port m s! MQTT.port
    mqtt::init m s! MQTT.handle
    EventEmitter.new m s! MQTT.emitter
    nullptr m s! MQTT.onConnect
    nullptr m s! MQTT.onMessage
    nullptr m s! MQTT.onPublish
    nullptr m s! MQTT.onDisconnect
    m
;

: MQTT.connect { m -- err , connect to server }
        m s@ MQTT.handle 
        m s@ MQTT.host 
        m s@ MQTT.port 
        5 
    mqtt::connect m s! MQTT.error
    m s@ MQTT.error  
;

: MQTT.version ( -- major minor revision )
    MQTT::version
;

: MQTT.init ( -- , )
    MQTT::init
;

: MQTT.cleanup ( -- , )
    MQTT::cleanup
;

: MQTT.get-message ( -- m )
    MQTT::get_message
;

create MQTT-topic 512 allot

: MQTT.subscribe { m $topic 'handler | ee -- err , subscribe to a topic }
    m s@ MQTT.emitter -> ee
    MOSQ_ERR_SUCCESS m s! MQTT.error

    $topic count MQTT-topic place-cstr

    ee $topic EventEmitter.subscriber-count 0= if
        m s@ MQTT.handle MQTT-topic mqtt::subscribe m s! MQTT.error
    then

    m s@ MQTT.error MOSQ_ERR_SUCCESS = if
        ee $topic 'handler EventEmitter.subscribe
    then
    m s@ MQTT.error
;

: MQTT.publish { m $topic payload0 retain -- err , publish message to topic }
    $topic count MQTT-topic place-cstr
cr ." PUBLISH " MQTT-topic ztype 
cr ."    payload " payload0 ztype
        m s@ MQTT.handle 
cr m .hex hex .s
        MQTT-topic
        payload0 
        retain 
    mqtt::publish m s! MQTT.error
cr ." published "
    m s@ MQTT.error
;

: MQTT.loop { m | msg payload ee --, process MQTT connection/message }
    begin
        MQTT::has_message 
    while
        MQTT::get_message  -> msg
        msg s@ MQTTMessage.type case
            MQTTMessageTypeConnect of
                m s@ MQTT.onConnect ?dup if
                    execute
                then
            endof
            MQTTMessageTypePublish of
                m s@ MQTT.onPublish ?dup if
                    execute
                then
            endof
            MQTTMessageTypeDisconnect of
                m s@ MQTT.onDisconnect ?dup if
                    execute
                then
            endof
            MQTTMessageTypeMessage of
                msg s@ MQTTMessage.payload -> payload
                msg s@ MQTTMessage.topic zcount MQTT-topic place
                m s@ MQTT.onMessage if
                    MQTT-topic payload m s@ MQTT.onMessage execute
                then
                m s@ MQTT.emitter -> ee
                ee MQTT-topic payload EventEmitter.emit
                true
            endof
        endcase
    repeat
;

