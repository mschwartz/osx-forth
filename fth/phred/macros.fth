\
\ Phred editor
\
\ programmed by Mike Schwartz
\
\ Phred editor macros
\

0 constant MACRO-NONE
1 constant MACRO-RECORDING
2 constant MACRO-PLAYING

:STRUCT Macro
    APTR Macro.string
;STRUCT

private{
    create macros 26 cells allot        \ for a-z
        macros 26 cells 0 fill          \ set all to null
}private

: Macro.new { | m -- m , create new macro }
    sizeof() macro mem::malloc -> m
    CString.new-empty m s! Macro.string
    m
;

: Macro.destroy { m -- , free resources }
    m s@ Macro.string ?dup if CString.destroy then
    m ?dup if mem::free then
;

: Macro.clear { ch | m -- , clear macro for ch a-z }
    ch tolower -> ch
    ch ascii a < if exit then
    ch ascii z > if exit then
    macros ch ascii a - cells + @ -> m
    m if m Macro.destroy then
    0 macros ch ascii a - cells + !
;

: Macro.get { ch -- macro , get macro for ch a-z }
    ch tolower -> ch
    ch ascii a < if 0 exit then
    ch ascii z > if 0 exit then
    macros ch ascii a - cells + @
;

: Macro.set { ch m -- , set macro for ch a-z }
    ch ascii a < if 0 exit then
    ch ascii z > if 0 exit then
    ch tolower -> ch
    ch Macro.clear
    m macros ch ascii a - cells + !
;

: Macro.rewind { m -- , rewind macro }
    m s@ Macro.string CString.reset-index
;

: Macro.addch { m ch -- , add ch to macro }
    m s@ Macro.string ch CString.append-char
;
: Macro.getch { m | str -- ch , add ch to macro }
    m s@ Macro.string -> str
\     str CString.eol? if 0 exit then
    str CString.getch
;

privatize
