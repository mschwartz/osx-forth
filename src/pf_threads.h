#ifndef PF_THREADS_HH
#define PF_THREADS_HH

#include <pthread.h>

struct ThreadDetail {
  pthread_t thread;
  int success;
  pfTaskData_t *taskData;
};

pthread_t thread_start(cell_t func, void *arg);
void thread_join(void *arg);

#endif
