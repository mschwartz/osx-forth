\
\ PHRed editor
\
\ programmed by Mike Schwartz
\
\ Git Syntax Highlighting
\

anew TASK-syntax/highlight-git.fth

: Highlight.git { text | new -- , highlighted }
    text string::strlen 3 + mem::malloc -> new
    0 new c!
    text c@ case
        ascii M of
            new OPCODE.keyword String.append.char
            new text String.append
        endof
        ascii ? of
            new OPCODE.search String.append.char
            new text String.append
        endof
        new text String.append
        new OPCODE.default String.append.char
    endcase
    new
;