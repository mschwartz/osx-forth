#!/usr/local/bin/pforth_standalone -q

anew task-demos/http-server.fth

false trace-include ! 

require net.fth
require sys/memory.fth 
require sys/semaphore.fth
require lib/buffer.fth
require net/http.fth

\ constants
8080 constant PORT
\ prefork this many children
1 constant PREFORK-CHILDREN \ set to 0 to not prefork

variable sem

cr ." create semaphore ... "
c" semfile" Semaphore.new sem !
." created."

: lock  sem @ Semaphore.obtain drop ;
: trylock  sem @ Semaphore.obtained?  ;
: unlock  sem @ Semaphore.release drop ;

variable content-length
variable file-data 

lock
cr cr
c" demos/http-server.fth" FILE.size content-length !
cr ." demos/http-server.fth is " content-length @ .  ." bytes"
c" demos/http-server.fth" FILE.contents file-data !
cr ." file data length =  " content-length @ .
unlock

create child-pids PREFORK-CHILDREN cell * allot
child-pids PREFORK-CHILDREN cell * erase

: handle-request { sock | req res -- , handle http request }
    cr .pid ." handle-request " .s
    lock
    cr cr cr .pid ." HANDLE REQUEST " sock s@ TCPSocket.ip ." FROM CLIENT " .ip-address ."   " .s
    cr
    unlock

    sock HTTPRequest.new -> req 
    cr .pid ." got new request " .s
    sock HTTPResponse.new -> res
    cr .pid ." got new response " .s

    cr .pid ." about to lock " .s
    lock
    cr .pid ." READING REQUEST HEADERS...  "  .s
    req HTTPRequest.read-headers
    true <> if
        .pid c" can't read headers" sys::perror
        unlock
        exit
    then
    cr .pid ." END OF HEADERS "

    cr cr .pid ." SENDING RESPONSE HEADERS... " 

    s" HTTP/1.0 200 OK" res HttpResponse.set-status  
    0< if
        .pid c" can't set-status " sys::perror
        unlock
        exit
    then

    s" text/html" s" Content-Type" res HTTPResponse.add-header
    0< if
        .pid c" can't add-header content-type " sys::perror
        unlock
        exit
    then
    
    s" close" s" Connection" res HTTPResponse.add-header  
    0< if
        .pid c" can't add-header connection close " sys::perror
        unlock
        exit
    then

    content-length @ number>string 
    s" Content-length" res HTTPResponse.add-header
    0< if
        .pid c" can't add-header content-length " sys::perror
        unlock
        exit
    then

    res HttpResponse.end-headers 
    0< if
        .pid c" can't end headers " sys::perror
        unlock
        exit
    then
    .pid ." SENT! " .s
    unlock

\     cr ." ------ file "
\     file-data @ content-length @  type
\     cr ." ------  "
    file-data @ content-length @  res HTTPResponse.write 
    dup content-length @ <> if
        lock
        cr .pid ." did not write content-length " content-length @ . ." <> " .
        unlock
    then

    lock
    cr .pid ." sent file " . ." bytes" .s
    unlock

    cr ." destroy socket " .s
    sock TCPSocket.destroy 
    cr ." destroy req " .s
    req HTTPRequest.destroy
    cr ." destroy res " .s
    res HTTPResponse.destroy
    cr ." ------ " .pid ." end of handle-request " .s
;

: sigint-handler ( -- ) 
    0sp
\     cr ." sigint handler"
\     abort
    0 sys::exit
;

: http-child { server | remote -- , }
    lock
    cr .pid ." http-child " 
    unlock
    ['] sigint-handler is handle-SIGINT
    begin
        lock
        cr .pid ." accept ... " .s
        unlock
        server TCPSocket.accept -> remote
        lock
        cr .pid ." accepted! " ." remote " remote .hex .s
        unlock

        remote 0<> if 
            remote handle-request            
        then
    again
;

: child-exited ( -- )
    lock
    cr ." child exited!"
    unlock
;
\ ' child-exited is handle-SIGCHLD

: main { | server server-fd }
    lock
    cr ." main " .s
    unlock
    INADDR_ANY 8080 TCPSocket.server -> server \ create server socket
    server 0< if
        lock
        c" server socket" sys::perror
        unlock
        exit
    then

    cr ." prefork children " .s
    PREFORK-CHILDREN 0 do
        sys::fork dup 0= if
            drop
            server ['] http-child catch
\             lock
\             cr ." child caught " .
\             unlock
            0 sys::exit
        else
            lock
            cr ." parent: forked child pid " dup . i .
            unlock
            child-pids i cell * + ! \ store child pid
        then
    loop

    ['] child-exited is handle-SIGCHLD \ only want to do this in parent

    server http-child 
;

: cleanup ( -- , kill children )
    cr ." cleanup: cleaning up child processes..."
    PREFORK-CHILDREN 0 do
        child-pids i cell * + @ \ get PID
        cr ." killing child " i . dup .
        dup 0<> if
            SIGINT sys::kill
        then
    LOOP
    cr cr
    0 sys::exit
;

: demo 
    sys::fork dup 0= if
        \ child
        drop 
        ['] main  catch
            lock
            cr ." main caught " .
            unlock
            cleanup
    else
        \ parent
        sys::wait \ wait for main to exit
        lock
        cr ." main exited" 
        unlock
        
        lock
        cr ." cleaning up child processes..."
        child-pids 100 dump

        PREFORK-CHILDREN 0 do
            child-pids i cell * + @ \ get PID
            cr ." killing child " i . dup .
            dup 0<> if
                SIGINT sys::kill
            then
        LOOP
        unlock
        cr cr
    then
    0 sys::exit
;

0sp
' demo catch
    lock
    cr ." top-level caught " .
    unlock
cleanup
cr 0 sys::exit

