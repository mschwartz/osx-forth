\
\ PHRed editor
\
\ programmed by Mike Schwartz
\
\ Phred editor Window functions
\

: (Window.set-buffer) { w b -- , set buffer for window }
    b s@ Buffer.fullpath ?dup if
        w s! Window.name
    else
        b s@ Buffer.name w s! Window.name
    then

    b w s! Window.buffer

    b Buffer.first-line w s! Window.top-line
    b Buffer.first-line w s! Window.current-line

    0 w s! Window.cursor-row
    0 w s! Window.cursor-col
    0 w s! Window.offset-left

    1 w s! Window.line#
    1 w s! Window.top-line#

    b s@ Buffer.filetype case
        Buffer.filetype.git of
            w Window.gited-mode
            w s@ Window.min-line# 1 do
                w Window.line-down
            loop
            exit
        endof
        Buffer.filetype.text of
            w Window.visual-mode
            w Window.paint
            w Window.refresh
            exit
        endof
        Buffer.filetype.bufed of
            w Window.bufed-mode
            w Window.paint
            w Window.refresh
            exit
        endof
    endcase
    w Window.visual-mode
\     w Window.paint
\     w Window.refresh
    ncurses.cursor-hide
;
' (Window.set-buffer) is Window.set-buffer


require phred/keymaps.fth

: Window.run { w -- , process keystrokes for window }
    w w s@ Window.keymap execute
;

: EditorWindow.new { top left nlines ncols | w -- new-window , new editor window }
    WindowType.editor top left nlines ncols Window.new -> w
    ['] Keymap.editor  w s! Window.keymap
    w
;

