anew task-lib/tags.fth

trace-include @ [if]
cr ." Compiling tags... " cr
[then]

$ 00 constant TAG_END           \ TAG_END MUST BE 0
\
$ 21 constant TAG_NUMBER        \ next on stack is a number
$ 22 constant TAG_NUMBERX       \ next on stack is a number, print as hex
$ 23 constant TAG_$STRING       \ next on stack is a $string
$ 24 constant TAG_STRING        \ next on stack is a caddr u string
$ 25 constant TAG_CHAR          \ next on stack is a character
$ 26 constant TAG_CHARX         \ next on stack is a character, print as hex
$ 27 constant TAG_KEY           \ wait for key and drop it
$ 28 constant TAG_NEWLINE       \ emit newline
$ 29 constant TAG_DATA          \ set data to Line.data
$ 2a constant TAG_BYTE          \ add raw byte to string
$ 2b constant TAG_ICON          \ add raw byte to string
$ 2c constant TAG_SPACES        \ add spaces
$ 2d constant TAG_NUMBER_JUSTIFIED \ add number justified

\

: tag-dism ( -- , print tags from stack in human readable form ) 
    begin
        dup TAG_END <>
    while
        case
            TAG_NUMBER of
                cr ." TAG_NUMBER " .
            endof
            TAG_NUMBERX of
                cr ." TAG_NUMBERX " .hex
            endof
            TAG_$STRING of
                cr ." TAG_$STRING " $type
            endof
            TAG_STRING of
                cr ." TAG_STRING " dup ." len =  " decimal . type
            endof
            TAG_CHAR of
                cr ." TAG_CHAR " .hex 
            endof
            TAG_BYTE of
                cr ." TAG_BYTE " .hex 
            endof
            TAG_ICON of
                cr ." TAG_ICON " .hex 
            endof
            TAG_CHARX of
                cr ." TAG_CHARX " .hex 
            endof
            TAG_DATA of
                cr ." TAG_DATA " .hex 
            endof
            cr ." ILLEGAL TAG " .hex
        endcase
    repeat
    cr ." TAG_END "
    cr
;

trace-include @ [if]
cr ." End Tags" cr
[then]

