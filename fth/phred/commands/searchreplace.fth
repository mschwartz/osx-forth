\
\ phred editor
\
\ programmed by Mike Schwartz
\
\ Command Line handling
\   :s
\   :s/pattern/replacement
\   :s/pattern/replacement/
\   :s/pattern/replacement/i
\   :s/pattern/replacement/g
\   :s/pattern/replacement/ig
\

anew task-commands/searchreplace.fth

private{
variable search&replace-re 0 search&replace-re ! \ search & replace regex
create search-pad 512 allot
create replace-pad 512 allot
}private

\ handle :s/pattern/replacement/g nlines  where / may be any delimeter
: CommandLine.search&replace { w str strt end | cnt line# re lin replaced delim -- , perform search and replace }
    w s@ Window.line# -> line#
    0 -> cnt

    \ repeat last search&replace?
    str CString.eol? if
        \ :s means repeat last search on current line
        search&replace-re @ 0= if
            messageline{ ." *** No replacement available " }messageline
            exit
        then
        w s@ Window.current-line -> lin
            lin s@ Line.text \ haystack
            search&replace-re @
            replace-pad
        regex.replace_re -> replaced
        lin replaced s@ Line.text string::strcmp 0<> if
            w s@ Window.line# -> line#
        then
        lin replaced Line.replace-text
        regex.default
        exit
    then

    str CString.getch -> delim
    str delim search-pad CString.parse-pattern

    str delim CString.parse 
    replace-pad place-cstr      \ pad is now replacement

    \ parse modifiers like /ig
    begin
        str CString.peek-ch
    while
        str CString.getch case
            ascii i of regex.ignore-case endof
            ascii g of regex.global endof
            \ default
            messageline{ ." *** Syntax error " }messageline
            regex.default
            exit
        endcase
    repeat

    search&replace-re @ if
        search&replace-re @ regex.delete
        0 search&replace-re !
    then
    search-pad regex.new -> re
    re 0= if die{ ." *** RE error " ztype }die then
    re search&replace-re !

    end strt <> if
        end 1+ strt do
            w i Window.goto-line
            w s@ Window.current-line -> lin
            lin s@ Line.text \ haystack
                lin s@ Line.text \ haystack
                re
                replace-pad
            regex.replace_re -> replaced
            replaced lin s@ Line.text string::strcmp 0<> if
                w s@ Window.line# -> line#
                cnt 1+ -> cnt
            then
            lin replaced Line.replace-text
        loop
        w line# Window.goto-line
        regex.default
        messageline{ ." Replaced " cnt . ." lines" }messageline
        exit
    then
    w s@ Window.current-line -> lin
        lin s@ Line.text \ haystack
        re
        replace-pad
    regex.replace_re -> replaced
    lin replaced Line.replace-text
    w line# Window.goto-line
    regex.default
    messageline{ ." Replaced " cnt . ." lines" }messageline
;

privatize
