\
\ phred editor
\
\ programmed by Mike Schwartz
\
\ Command Line handling
\   :grep command
\

anew task-phred/commands/grep.fth

: CommandLine.grep { w str tok toku -- flg , return true if executed }
    tok toku s" grep" compare 0= if
        str CString.skip-blanks
        w str CString.rest Greped.begin
        w Window.greped-mode
        true
    else
        false
    then
;

