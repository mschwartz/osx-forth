\ key codes/values
\
\ Many of these seem to be from keyboards with numerous keys beyond
\ keypad and qwerty.
\

\ #define NCURSES_ATTR_SHIFT       8
\ #define NCURSES_BITS(mask,shift) (NCURSES_CAST(chtype,(mask)) << ((shift) + NCURSES_ATTR_SHIFT))
\ #define COLOR_PAIR(n)	NCURSES_BITS((n), 0)
\ #define PAIR_NUMBER(a)	(NCURSES_CAST(int,((NCURSES_CAST(unsigned long,(a)) & A_COLOR) >> NCURSES_ATTR_SHIFT)))

[defined] ncurses-uninstall not [if]
    defer ncurses-uninstall
[then]
-1 constant ncurses.ERR

variable _LINES 0 _LINES !
variable _COLS 0 _COLS !

\ make LINES and COLS act like a constant
: LINES _LINES @ ;
: COLS _COLS @ ;

create ncurses-pad 4096 allot
variable _ncurses-window ncurses::stdscr _ncurses-window !

: ncurses-window ( -- win, get active window )
    _ncurses-window @
;
: ncurses-window-set ( win -- , set active window )
    _ncurses-window !
;
: ncurses-window-reset ( win -- , set active window )
    ncurses::stdscr _ncurses-window !
;

: ncurses.initscr ( -- win )
    ncurses::initscr dup _ncurses-window !
    ncurses::lines _LINES !
    ncurses::cols _COLS !
;

: ncurses.endwin ( -- )
    ncurses::endwin
;

: ncurses.beep ( -- f , beep and/or flash screen )
    ncurses::beep
;
: ncurses.flash ( -- f , beep and/or flash screen )
    ncurses::flash
;
: ncurses.bell ( -- , beep and/or flash screen )
    ncurses.flash drop
    ncurses.beep drop
;

require ncurses/cursor.fth
require ncurses/mouse.fth
require ncurses/ncursesw.fth
require ncurses/colors.fth
require ncurses/video.fth
require ncurses/window.fth
require ncurses/keyboard.fth
