#!/usr/local/bin/pforth_standalone -q
\
\ demonstrate the processing of command line arguments
\ 
\ Programmed by Mike Schwartz
\

anew task-demos/args.fth
: args ( -- )
    begin
        next-arg 2dup 0 0 d<> while
        type space
    repeat
    2drop 
;

args cr
