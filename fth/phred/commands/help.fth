\
\ phred editor
\
\ programmed by Mike Schwartz
\
\ Command Line handling
\   :help command
\

anew task-phred/commands/help.fth

: CommandLine.help { w str tok toku -- flg , return true if executed }
    tok toku s" help" compare 0= if
        str CString.parse-token -> toku -> tok
        toku 0= if
            c" *** Word expected" MessageLine.$type
            true exit
        then
        w tok toku Helped.begin
        true exit
    then

    tok toku s" h" compare 0= if
        str CString.parse-token -> toku -> tok
        toku 0= if
            c" *** Word expected" MessageLine.$type
            true exit
        then
        w tok toku Helped.begin
        true exit
    then
    false
;
