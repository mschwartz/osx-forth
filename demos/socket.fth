#!/usr/local/bin/pforth_standalone -q

\
\ Demonstrate the use of Unix sockets.
\   This program uses the osx-forth Socket API to fetch the
\   Google homepage at http://www.google.com
\
\ Programmed by Mike Schwartz
\

false trace-include !

require lib/hashmap.fth
require net/socket.fth 
require net/netdb.fth
require net/http.fth 

\ HTTP response headers are read into a HASHMAP
variable response-headers
' mem::free HASHMAP.new response-headers !

\ allocate a buffer into which we read lines 
1024 1024 * 4 * constant max-size       \ maximum line length
create headers-line max-size allot

cr ." headers-line " headers-line .hex ." max-size " max-size . max-size .hex

\ we use this routine to look for the : in a line with a 'key: value' header in it
\ note that the first line is the status line and does not have a 'key: value'
: find-char { c strz | ptr  -- index ,  return index in string of char }
    strz -> ptr
    begin
        ptr c@
        dup 0= if   \ end of string, not found
            drop    \ drop character
            -1 exit
        then
        c = if
            ptr strz -
            exit             
        then
        ptr 1+ -> ptr
    again
;

\ Take a pointer to string and skip past any blanks and return pointer
\ to the string past the blanks.
: skip-blanks ( ptr -- ptr2 , skip blanks  )
    begin
        dup c@
        dup 0= if
            drop
            0 exit
        then
        bl <> if
            exit
        then
        1+
    again
;

\ Parse one line of text that is a key: value.
\ two counted strings are created within the line buffer itself: one 
\ is the key, the other is the value
: parse-header { | u1 caddr2 u2 -- , parse header from headers-line and add to response  headers }
    ascii : headers-line 1+ find-char -> u1
    u1 0< if 
        exit \ not a header!
    then

    u1 headers-line c!                          \ store length of the key
    headers-line u1 + 2+ skip-blanks -> caddr2
    caddr2 0= if
        exit  \ not a header
    then
    caddr2 sys::strlen caddr2 1- c!                  \ store length of the value
    response-headers @ headers-line caddr2 sys::strdup HASHMAP.set drop
;

\ read headers from the response
: read-headers { sock | fp -- , read headers from stream }
cr
    sock s@ TCPSocket.fp  -> fp
    begin
        sock TCPSocket.eof? 0<> if 
            exit
        then
        headers-line max-size sock TCPSocket.read-line
        0<> if 
            headers-line 1+ sys::strlen headers-line c!
            headers-line c@ 0= if
                exit
            then
            ." HEADER LINE " true 0 0 250 TIO.FG-RGB TIO.bold-mode headers-line count type false TIO.bold-mode TIO.reset-modes cr
            parse-header
        then
    again
;

: demo { caddr u port | sock fp -- , main program }
    cr ." creating socket... "
    caddr u port TCPSocket.client -> sock
    sock s@ TCPSocket.fp -> fp 

    cr ." sending request headers ..."
    s" GET / HTTP/1.0" sock TCPSocket.writeln drop
    sock TCPSocket.newline drop 
    ." sent"

    cr ." getting response headers..."
    sock read-headers
    ." read headers "

    cr ." reading body... "
    response-headers @ c" Content-Length" HASHMAP.get
    dup 0= if
        drop
        headers-line max-size sock TCPSocket.read
    else
        0 swap
        count  ( -- 0 number len )
        >number
        2drop
        headers-line swap sock TCPSocket.read
    then
    ." read " dup . ." bytes"
    cr cr ." CONTENT (max 2000 characters) FOLLOWS: " 
    cr cr headers-line swap 2000 min type
    cr cr
    0 sys::exit
;

s" www.google.com" 80 demo