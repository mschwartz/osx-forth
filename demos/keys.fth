#!/usr/local/bin/pforth_standalone -q

: demo
    cr ." Hit a key to see its hex value(s) or ^C to exit:"
    begin 
        key
        dup 4 = if bye then
        dup 20 >= if
            cr dup .hex dup emit
        else
            cr .hex
        then
    again 
;

demo