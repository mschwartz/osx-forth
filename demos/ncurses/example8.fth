\
\ Example 8: More border functions
\

false trace-include !

require lib/ncurses.fth

:STRUCT WIN 
    long WIN.startx
    long WIN.starty
    long WIN.height
    long WIN.width
    ubyte WIN_BORDER.ls
    ubyte WIN_BORDER.rs
    ubyte WIN_BORDER.ts
    ubyte WIN_BORDER.bs
    ubyte WIN_BORDER.tl
    ubyte WIN_BORDER.tr
    ubyte WIN_BORDER.bl
    ubyte WIN_BORDER.br
;STRUCT

: print-win-params { p_win -- , print window params }
    ncurses.cr
    25 0 s" startx=" ncurses.mvprintw 
    p_win s@ WIN.startx ncurses.#
    p_win s@ WIN.starty ncurses.#
    p_win s@ WIN.width ncurses.#
    p_win s@ WIN.height ncurses.#
    ncurses.refresh
;

: init-win-params { p_win -- , initialize WIN params }
    3 p_win s! WIN.height
    10 p_win s! WIN.width
    LINES p_win s@ WIN.height - 2/ p_win s! WIN.starty
    COLS p_win s@ WIN.width - 2/ p_win s! WIN.startx
    ascii | p_win s!  WIN_BORDER.ls
    ascii | p_win s! WIN_BORDER.rs
    ascii - p_win s! WIN_BORDER.ts
    ascii - p_win s! WIN_BORDER.bs
    ascii + p_win s! WIN_BORDER.tl
    ascii + p_win s! WIN_BORDER.tr
    ascii + p_win s! WIN_BORDER.bl
    ascii + p_win s! WIN_BORDER.br
;

: create-box { p_win flg | x y w h ii jj -- , render or erase  borders for p_win }
    p_win s@ WIN.startx -> x
    p_win s@ WIN.starty -> y
    p_win s@ WIN.width -> w
    p_win s@ WIN.height -> h
    flg if
        y x p_win s@ WIN_BORDER.tl ncurses.mvaddch
        y x p_win s@ WIN.width +  p_win s@ WIN_BORDER.tr ncurses.mvaddch
        y h + x  p_win s@ WIN_BORDER.bl ncurses.mvaddch
        y h + x w +  p_win s@ WIN_BORDER.br ncurses.mvaddch
        y x 1+ p_win s@ WIN_BORDER.ts w 1- ncurses.mvhline
        y h + x 1+ p_win s@ WIN_BORDER.bs w 1- 
        ncurses.mvhline
        y 1+ x p_win s@ WIN_BORDER.ls h 1- ncurses.mvvline
        y 1+ x w + p_win s@ WIN_BORDER.rs h 1- ncurses.mvvline
    else
\         ncurses.endwin
\         lines
\         cr ." lines =  " .
        y -> jj
        begin
            jj y h + <=
        while
            x -> ii
            begin
                ii x w + <=
            while
                jj ii bl ncurses.mvaddch
                ii 1+ -> ii
\                 cr ." ii " ii . x .
            repeat
            jj 1+ -> jj
        repeat
    then
    ncurses.refresh
;

WIN &win

: demo { | win ch COLOR_PAIR(1) -- , demo }
    ncurses.initscr -> win
    ncurses.start_color
    ncurses.cbreak
    win  true ncurses.keypad
    ncurses.noecho
    &win init-win-params
    &win print-win-params

    1 ncurses.COLOR_CYAN ncurses.COLOR_BLACK ncurses.init_pair
    1 ncurses.color_pair -> COLOR_PAIR(1)

    COLOR_PAIR(1) ncurses.attron
    s" Press F1 to exit " ncurses.printw
    ncurses.refresh
    COLOR_PAIR(1) ncurses.attroff
    &win true create-box
    begin
        ncurses.getch -> ch
        ch ncurses.key_f1 <>
        ch ascii q <> and
    while
        ch case
            ncurses.key_left of
                &win false create-box
                &win s@ WIN.startx 1- &win s! WIN.startx
                &win true create-box
            endof
            ncurses.key_right of
                &win false create-box
                &win s@ WIN.startx 1+ &win s! WIN.startx
                &win true create-box
            endof
            ncurses.key_up of
                &win false create-box
                &win s@ WIN.starty 1-  &win s! WIN.starty
                &win true create-box
            endof
            ncurses.key_down of
                &win false create-box
                &win s@ WIN.starty 1+ &win s! WIN.starty
                &win true create-box
            endof
        endcase
    repeat
    ncurses.endwin
;

demo