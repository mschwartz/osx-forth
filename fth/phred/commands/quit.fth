\
\ phred editor
\
\ programmed by Mike Schwartz
\
\ Command Line handling
\   :q 
\   :q! 
\   :quit
\   :qa
\   :qa!
\

anew task-phred/commands/quit.fth

: CommandLine.quit { w str tok toku -- flg , return true if executed }
    tok toku  s" q" compare 0= if
        BufferList.unsaved? if exit then
        err_abort throw
    then
    tok toku  s" q!" compare 0= if
        err_abort throw
    then
    tok toku  s" qa" compare 0= if
        BufferList.unsaved? if exit then
        err_abort throw
    then
    tok toku  s" qa!" compare 0= if
        err_abort throw
    then

    tok toku s" quit" compare 0= if
        BufferList.unsaved? if exit then
        err_abort throw
    then
    false
;