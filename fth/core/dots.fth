anew task-core/dots.fth

: .nybble ( n -- , print nybble in hex )
    dup 10 >= if
        ascii A +
    else
        ascii 0 +
    then
    emit
;
: .byte ( byte -- , print byte in hex )
    dup 4 rshift $ 0f and .nybble 
    $ 0f and .nybble
;
: .word16 ( word -- , print 16-bit word in hex )
    dup 8 rshift $ ff and .byte 
    $ff and .byte
;
: .word32 ( word -- , print 32-bit word in hex )
    dup 16 rshift $ ffff and .word16 
    ascii . emit 
    $ ffff and .word16
;
: .word64 ( cell -- , print cell in hex )
    dup 32 rshift .word32 
    ascii . emit 
    $ ffffffff and .word32
;

: .s { | dpth addr -- , print stack }
    depth -> dpth
    sp@ -> addr
    dpth 0> if
        addr dpth 1- cells + -> addr
        base @ 16 = if
            ." Stack Top " cr
            dpth 0 ?do
                4 spaces addr .hex addr @ .word64  ."  = " addr @ . cr
                addr cell - -> addr
            loop
        else
            ." Stack<" base @ .# ." > "
            dpth 0 ?do
                addr @ .
                addr cell - -> addr
            loop
            cr
        then
    then
;