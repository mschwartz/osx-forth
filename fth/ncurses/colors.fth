$ 0 constant ncurses.COLOR_BLACK
$ 1 constant ncurses.COLOR_RED
$ 2 constant ncurses.COLOR_GREEN
$ 3 constant ncurses.COLOR_YELLOW
$ 4 constant ncurses.COLOR_BLUE
$ 5 constant ncurses.COLOR_MAGENTA
$ 6 constant ncurses.COLOR_CYAN
$ 7 constant ncurses.COLOR_WHITE

$ 8 constant ncurses.COLOR_BRIGHT_BLACK
$ 9 constant ncurses.COLOR_BRIGHT_RED
$ a constant ncurses.COLOR_BRIGHT_GREEN
$ b constant ncurses.COLOR_BRIGHT_YELLOW
$ c constant ncurses.COLOR_BRIGHT_BLUE
$ d constant ncurses.COLOR_BRIGHT_MAGENTA
$ e constant ncurses.COLOR_BRIGHT_CYAN
$ f constant ncurses.COLOR_BRIGHT_WHITE


: ncurses.has_colors ( -- f , colors available? )
    ncurses::has_colors
;

: ncurses.start_color ( -- , initialize use of colors )
    ncurses::start_color
    drop
;

: ncurses.color_pair ( n -- attr )
    ncurses::color_pair
;

: ncurses.init_color { n red green blue -- , initialize color }
    n red green blue ncurses::init_color
    ncurses.ERR = if
        ncurses-uninstall cr ." bad init_color " n . red . green . blue . cr
        0 sys::exit
    then
;
: ncurses.init_pair ( n fg bg -- , initialize color pair )
    ncurses::init_pair
    drop
;

: ncurses.mvchgat ( row col n attr color opts -- , move and change attributes )
    ncurses::mvchgat
    drop
;

variable ncurses_bkgd
: ncurses.bkgdset ( ch -- , set background character/attribute for screen )
    dup ncurses_bkgd !
    ncurses::bkgdset
;

: ncurses.wbkgdset ( win ch -- , set background charater/attribute for window )
    dup ncurses_bkgd !
    ncurses::wbkgdset
;

: ncurses.bkgd ( ch -- , set background property for screen, applying to every character on screen )
    dup ncurses_bkgd !
    ncurses::bkgd
    drop
;

: ncurses.wbkgd ( ch -- , set background property for window applying to every character on screen )
    dup ncurses_bkgd !
    ncurses::wbkgd
    drop
;

: ncurses.getbkgd ( win -- ch , get background property for window )
    ncurses::getbkgd
;

false [if]
: color-num { fg bg | bbb ffff -- color , unique color index based on fg and bg }
    bg 7 and 4 lshift -> bbb
    fg 7 and -> ffff
    $ 80 bbb or ffff or
;

: curs-color ( n -- clr , return COLOR_* )
    7 and
    case
        0 of ncurses.COLOR_BLACK endof
        1 of ncurses.COLOR_BLUE endof
        2 of ncurses.COLOR_GREEN endof
        3 of ncurses.COLOR_CYAN endof
        4 of ncurses.COLOR_RED endof
        5 of ncurses.COLOR_MAGENTA endof
        6 of ncurses.COLOR_YELLOW endof
        7 of ncurses.COLOR_WHITE endof
    endcase
;

: init-color-pairs { | pair -- , initialize color pairs }
    8 0 do \ bg
        8 0 do \ fg
            i j color-num -> pair
            pair 
            i curs-color
            j curs-color
            ncurses.init_pair
        loop
    loop
;

: bright? { clr -- f , test if color is bright }
    clr 1 3 lshift AND
;

: set-color { fg bg -- , turn on color attribute }
    fg bg color-num ncurses.color_pair ncurses.attron
    fg bright? if
        ncurses::A_BOLD ncurses.attron
    then
;

: wset-color { w fg bg -- , turn on color attribute }
    w fg bg color-num ncurses.color_pair ncurses.wattron
    fg bright? if
        w ncurses::A_BOLD ncurses.wattron
    then
;

: unset-color { fg bg -- , turn on color attribute }
    fg bg color-num ncurses.color_pair ncurses.attroff
    fg bright? if
        ncurses::A_BOLD ncurses.attroff
    then
;

: wunset-color { w fg bg -- , turn on color attribute }
    w fg bg color-num ncurses.color_pair ncurses.wattroff
    fg bright? if
        w ncurses::A_BOLD ncurses.wattroff
    then
;
[then]