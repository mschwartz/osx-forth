MouseEvent evt

private{
    variable scroll-timer time::time-ms scroll-timer !
    128 8 - constant scroll-timeout
}private

: Keymap.mouse { | win bstate row col -- , handle mouse event }
    evt ncurses.getmouse 
    0= if
        evt s@ MouseEvent.bstate -> bstate
        evt s@ MouseEvent.y -> row
        evt s@ MouseEvent.x -> col
        evt s@ MouseEvent.bstate ncurses.REPORT_MOUSE_POSITION AND if
            false exit
        then
        evt s@ MouseEvent.bstate ncurses.BUTTON1_PRESSED AND if
            row col Screen.window-at -> win
            win WindowList.activate-window
            win row win s@ Window.top -
            col win s@ Window.left -
            Window.move-to
            true exit
        then
        evt s@ MouseEvent.bstate ncurses.BUTTON2_PRESSED AND if
            false exit
        then
        evt s@ MouseEvent.bstate ncurses.BUTTON3_PRESSED AND if
            false exit
        then
        time::time-ms scroll-timer @ - scroll-timeout < if
            false exit
        then
        time::time-ms scroll-timer !
        evt s@ MouseEvent.bstate ncurses.BUTTON4_PRESSED AND if
            WindowList.active-window Window.line-up
        then
        evt s@ MouseEvent.bstate ncurses.BUTTON5_PRESSED AND if
            WindowList.active-window Window.line-down
        then
        true
    else
        false
    then
;

privatize
