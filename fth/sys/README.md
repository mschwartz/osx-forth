# sys/

This subdirectory contains system methods.  System is defined to be standard library 
methods, standard IO methods, regex, and whatever else is provided to C programs by
the Linux or MacOS system.

The methods defined in the C source use the sys:: namespace.
