\
\ PHRed editor
\
\ programmed by Mike Schwartz
\
\ Man page Syntax Highlighting
\

: Highlight.man  { text | str -- highlighted , highlight line of manpage }
    text CString.new -> str
    regex.default
    regex.global
    str c" NAME" s\" \x90NAME\z" CString.$regex_replace drop
    str c" SYNOPSIS" s\" \x90SYNOPSIS\z" CString.$regex_replace drop
    str c" ENVIRONMENT" s\" \x90ENVIRONMENT\z" CString.$regex_replace drop
    str c" EXIT STATUS" s\" \x90EXIT STATUS\z" CString.$regex_replace drop
    str c" EXAMPLES" s\" \x90EXAMPLES\z" CString.$regex_replace drop
    str c" COMPATIBILITY" s\" \x90COMPATIBILITY\z" CString.$regex_replace drop
    str c" LEGACY DESCRIPTION" s\" \x90LEGACY DESCRIPTION\z" CString.$regex_replace 0< if
        str c" DESCRIPTION" s\" \x90DESCRIPTION\z" CString.$regex_replace drop
    then
    str c" SEE ALSO" s\" \x90SEE ALSO\z" CString.$regex_replace drop
    str c" STANDARDS" s\" \x90STANDARDS\z" CString.$regex_replace drop
    str c" HISTORY" s\" \x90HISTORY\z" CString.$regex_replace drop
    str c" BUGS" s\" \x90BUGS\z" CString.$regex_replace drop
    str CString.zstring string::strdup 
    str CString.destroy
;
