\
\ PForth library 
\   By Mike Schwartz
\
\ Lines.  A Line is a node that has zero terminated text.  
\
\ Useful for making linked list of lines
\

anew task-lib/line.fth

trace-include @ [if]
cr ." Compiling lib/line.fth"
[then]

require lib/tags.fth
require sys/memory.fth
require lib/lists.fth
require sys/regex.fth
require lib/strings.fth

:STRUCT Line
    struct ListNode Line.node
    APTR Line.text
    APTR Line.data              \ arbitrary user data associated with this line
;STRUCT

: Line.dump { l -- , dump Line }
    cr
    cr ." Line at $" l .hex
    cr ." Line.text at $" l Line.text + @ .hex
    cr ." .   text = " l Line.text + @ zcount type
    cr ." .   data = " l s@ Line.data .hex
;
private{
    create regmatch sizeof() regmatch_t 2* allot
}private

: Line.new { ztext | l -- line , create new line }
    sizeof() Line MEM::malloc -> l
    ztext string::strdup l s! Line.text
    nullptr l s! Line.data
    l
;

: Line.clone { old | new -- new , duplicate line }
    sizeof() Line MEM::malloc -> new
    old s@ Line.data new s! Line.data
    old s@ Line.text string::strlen 1+ mem::malloc new s! Line.text
    new s@ Line.text old s@ Line.text string::strcpy drop
    new Line.node + List.init
\     die{ ." line.clone " .s }die
    new
;

: Line.destroy { l -- , destroy line }
    l s@ Line.text MEM.free
;

: Line.delete-at-index { l x | t p -- f , delete character at index }
    l s@ Line.text -> t
\     t strlen  cr . x . bye
    t string::strlen x <= if 
        false
        exit 
    then
    t x +  -> p
    begin
        p 1+ c@ 
        dup p c!
        0= if true exit then
        p 1+ -> p
    again
;

: Line.remove { l -- , remove line from list }
    l ListNode.removed? not if
        l ListNode.remove
    then
;

: Line.join { l flg | nxt new t1 t2 -- , join line with next line adding space between if flag }
    l s@ ListNode.next -> nxt
    l s@ Line.text -> t1
    nxt s@ Line.text -> t2

    t1 string::strlen t2 string::strlen + 4 + MEM::malloc -> new
    new t1 string::strcpy drop
    flg if
        t2 c@ bl <> if
            new z"  " 1+ string::strcat drop
        then
    then
    new t2 string::strcat drop
    nxt List.remove
    nxt Line.destroy
    t1 MEM.free
    new l s! Line.text
;

: Line.character-at-index { l ndx -- ch , get character at index or -1 if index past end }
    l s@ Line.text string::strlen ndx < if -1 exit then
    l s@ Line.text ndx + c@
;

: Line.split-at-index { l ndx | txt l1 l2 new -- , split line at index as if insertint a newline }
    l s@ Line.text -> txt
    ndx 1+ MEM::malloc -> l1
    txt string::strlen ndx - 2+  MEM::malloc -> l2

    l1 txt ndx string::strncpy drop

    l2 txt ndx + dup string::strlen string::strncpy drop
    txt MEM.free
    l1 l s! Line.text
    l2 Line.new -> new
    new l List.insertAfter
;

\ strncpy  ( dst src u -- dest-addr )
\ strcpy   ( dst src -- dest-addr )
: Line.insert-at-index { l ndx ch | nstr ostr -- , insert character at index }
    l s@ Line.text -> ostr \ old string
    ch 13 = if
        l ndx Line.split-at-index
        cr ." return " .s bye
        exit
    then

    \ grow line
    ostr string::strlen 2 + mem::malloc -> nstr   \ 2: 1 for '\0' and 1 for the inserted char
    nstr l s! Line.text

    \ copy first part of old string
    nstr ostr ndx  string::strncpy drop

    \ add the character
    ch nstr ndx + c!

    \ copy the end of the old string
    nstr ndx + 1+ ostr ndx +  string::strcpy drop

    \ free the old string
    ostr MEM.free
;

: Line.match { l col re | text -- start end , match pattern against line }
    l s@ Line.text col + -> text
    re text regmatch 1 0 regexec
\ regmatch 32 dump 3000 msec bye
    regmatch s@ regmatch_t.rm_so col +
    regmatch s@ regmatch_t.rm_eo
;

: Line.starts-with-char { lin ch | txt -- f , test if line starts with character }
    lin s@ Line.text -> txt
    begin
        txt c@ 0= if false exit then
        txt c@ bl =
    while
        txt 1+ -> txt
    repeat
    txt c@ ch =
;

: Line.ends-with-char { lin ch | ptr txt -- f , test if line ends with character }
    lin s@ Line.text -> txt
    txt string::strlen txt + 1- -> ptr
    begin
        txt ptr >
        ptr c@ bl = and
    while
        ptr 1- -> ptr
    repeat
    ptr c@ ch =
;

: Line.index-of { lin ch | ptr txt -- n , index of ch in line or -1 }
    lin s@ Line.text -> txt
    txt -> ptr
    begin
        ptr c@ 0<>
    while
        ptr c@ ch = if ptr txt - exit then
        ptr 1+ -> ptr
    repeat
    -1
;

: Line.replace-text { lin txt -- , replace existing text with txt }
    lin s@ Line.text if
        lin s@ Line.text mem::free
    then
    txt lin s! Line.text
;

create new-format-line 512 allot
variable new-format-data

: Line.format ( tags... ) { lin -- , print formatted into lin }
    0 new-format-line  c!
    0 new-format-data !
    begin
        dup TAG_END <>
    while
        case
            TAG_NUMBER of
                (.) new-format-line append
            endof
            TAG_NUMBERX of
                hex (.) new-format-line append decimal
            endof
            TAG_$STRING of
                count new-format-line append
            endof
            TAG_STRING of
                new-format-line append
            endof
            TAG_CHAR of
                new-format-line addchar
            endof
            TAG_CHARX of
                hex (.) new-format-line append decimal
            endof
            TAG_DATA of
                new-format-data !
            endof
            cr ." invalid tag "
            5000 msec 0 sys::exit
        endcase
    repeat
    drop
    new-format-line count pad place-cstr
    pad string::strdup lin s! Line.text
    new-format-data @ lin s! Line.data
;
privatize

trace-include @ [if]
cr ." End of line.fth "
[then]

