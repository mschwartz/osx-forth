\
\ Word Buffer
\

private{
    create worded-pad 4096 allot
    variable worded-buf
    variable worded-like

    : filter-word { n | s -- , filter word to worded-buf }
        n count worded-pad place-cstr       \ haystack
        worded-pad worded-like @ string::strcasestr if
            worded-pad  worded-buf @ Buffer.append-text drop
        then
        true
    ;
    
    : add-word { n | s -- , add word to worded-buf }
        n count worded-pad place-cstr       \ haystack
        worded-pad  worded-buf @ Buffer.append-text drop
        true
    ;
}private

: (Worded.begin) { w wlike | buf wrd  -- , initalize and attach word editor }
    \ if wlike is not null, then do "words-like wlike" instead of "words"
    s" Word Editor" BufferList.find ?dup if
        -> buf 
        buf Buffer.clear
    else
        s" Word Editor" Buffer.new -> buf
        buf BufferList.add
    then
    true buf s! Buffer.read-only
    true buf s! Buffer.permanent
    buf worded-buf !
    wlike worded-like !
    wlike if
        ['] filter-word dictionary.iterate
    else
        ['] add-word dictionary.iterate
    then
    w buf Window.set-buffer
;
' (WordEd.begin) is Worded.begin

privatize
