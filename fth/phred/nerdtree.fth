\
\ PHRed editor
\
\ programmed by Mike Schwartz
\
\ Phred editor Window status line functions
\

private{
    List nerdtree-list nerdtree-list List.init
}private

variable nerdtree-active 0 nerdtree-active !

\ : nerdtree.new { nlines ncols | w buf -- , new nerdtree window }
\     0 0 
\     nlines ncols
\     EditorWindow.new -> w
\     WindowType.nerdtree w s! Window.type
\     z" nerdtree" 1+ w s! Window.name
\     s" nerdtree"  Buffer.new -> buf
\     true buf s! Buffer.read-only
\     true buf s! Buffer.permanent
\     nerdtree-list DirList.destroy-list
\     current-directory zcount pad place 
\     nerdtree-list pad count true DirList.read drop
\     w
\ ;

: nerdtree.activate { | w -- , activate nerdtree }
    nerdtree-window @ -> w
    w WindowList.activate-window
    WindowType.nerdtree  w s! Window.type
    w Window.nerdtree-mode
;

: nerdtree.next-window { w -- , next window }
    WindowList.previous-window if
        WindowList.previous-window WindowList.activate-window
        w Window.paint
        WindowList.previous-window Window.set-editmode
\         WindowList.previous-window Window.paint
    then
;

: (nerdtree.deactivate) { w -- , deactivate nerdtree }
    WindowList.active-window w = if
        w nerdtree.next-window
    then
;
' (nerdtree.deactivate) is nerdtree.deactivate

privatize
