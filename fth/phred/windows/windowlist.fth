0 constant WindowType.editor
1 constant WindowType.bufed
2 constant WindowType.dired
3 constant WindowType.worded
4 constant WindowType.helped
5 constant WindowType.cmded
6 constant WindowType.greped
7 constant WindowType.gited
8 constant WindowType.nerdtree
9 constant WindowType.maned

:STRUCT Window
    struct ListNode Window.node
    APTR Window.name
    LONG Window.type
    APTR Window.buffer                  \ current buffer
    LONG Window.top
    LONG Window.left
    LONG Window.width
    LONG Window.height
    APTR Window.top-line
    APTR Window.current-line
    UINT64 Window.top-line#             \ line # in file of top line
    UINT64 Window.line#                 \ line # in file of current line
    UINT64 Window.min-line#
    UINT64 Window.cursor-row            \ (screen) row of cursor
    UINT64 Window.cursor-col            \ (screen) col of cursor
    UINT64 Window.offset-left           \ # characters window is scrolled left
    UINT64 Window.cursor-row-Save
    UINT64 Window.cursor-col-Save
    UINT64 Window.offset-left-Save
    \ per window type
    APTR Window.keymap                  \ word to handle keys
;STRUCT

variable nerdtree-window 

: Window.new { typ top left nlines ncols | w -- w }
    sizeof() Window Mem.malloc -> w
    typ w s! Window.type
    top w s! Window.top
    left w s! Window.left
    nlines w s! Window.height
    ncols w s! Window.width
    nullptr w s! Window.buffer
    nullptr w s! Window.top-line
    nullptr w s! Window.current-line
    0 w s! Window.top-line#
    0 w s! Window.line#
    0 w s! Window.min-line#
    0 w s! Window.cursor-row
    0 w s! Window.cursor-col
    0 w s! Window.offset-left
    w
;
: Window.tab-to-col { col -- col , move col to next tab stop }
    col 1+ -> col
    begin
        col TAB-WIDTH @ MOD 0= if
            col exit
        then
		col 1+ -> col
    again
;

: Window.line-length { p | ch c -- n , compute length of line if displayed }
    0 -> c
    begin
        p c@  -> ch p 1+ -> p
        ch 0= if 
            c exit 
        then
        ch ASCII_TAB = if
            c Window.tab-to-col -> c
        else
            c 1+ -> c
        then
    again
;

: Window.dump { w -- , dump window }
    ." Window $" w .hex  cr
    w s@ Window.name ."     name " zcount type cr
    w s@ Window.type ."     type $" .hex cr
    w s@ Window.buffer ."     buffer $" .hex cr
    ."     top left width height " 
    w s@ Window.top . w s@ Window.left . w s@ Window.width . w s@ Window.height . cr
    w s@ Window.top-line ."     top-line $" .hex cr
    w s@ Window.current-line ."     current-line $" .hex cr
    w s@ Window.top-line# ."     top-line# " . cr
    w s@ Window.line# ."     line# " . cr
    w s@ Window.min-line# ."     min-line# " . cr
    w s@ Window.cursor-row ."     cursor-row " . cr
    w s@ Window.cursor-col ."     cursor-col " . cr
    w s@ Window.offset-left ."     offset-left " . cr
    w s@ Window.current-line if
        w s@ Window.current-line s@ Line.text Window.line-length ."    len "  .
        w s@ Window.current-line s@ Line.text  ."     line " 20 dump
    then
;
: Window.destroy { w -- , destroy window }
    w s@ Window.name ?dup if mem::free then
    w ?dup if mem::free then
;
: Window.clone { w | new -- new , duplicate window }
    sizeof() Window MEM.malloc -> new
    w new sizeof() Window  move
    w s@ Window.name string::strdup new s! Window.name
    new
;

private{
    List window-list window-list List.init
    variable active-window
    variable found-window
    variable find-name
    variable find-type
    variable find-name-length
    variable previous-window nullptr previous-window !

: window-find { n -- , iterator function for window list }
    find-name @ find-name-length @
    n s@ Window.name zcount
    compare
    0= if 
        n found-window !
    then
;
: window-find-by-type { n -- , iterator function for window list find by type }
    find-type @ n s@ Window.type = if
        n found-window !
        exit
    then
;

}private

: Window.list ( -- keystone )
    window-list
;

: WindowList.list ( -- keystone )
    window-list
;

: (WindowList.activate-window) { w -- , activate window }
    WindowList.active-window nerdtree-window @ <> if 
        WindowList.active-window previous-window ! 
    then
    w active-window !
;
' (WindowList.activate-window) is WindowList.activate-window

: (WindowList.active-window) ( -- w , get active window )
    active-window @
;
' (WindowList.active-window) is WindowList.active-window

: (WindowList.previous-window) ( -- w , get previous window )
    previous-window @
;
' (WindowList.previous-window) is WindowList.previous-window
: (WindowList.add) { w -- , add window to window list }
    w Window.list List.addTail
    
    WindowList.active-window 0= if
        w active-window !
    then
;
' (WindowList.add) is WindowList.add

: WindowList.iterate { handler -- , }
    Window.list handler List.Iterate
;

: WindowList.dump { | w -- , dump window list }
    Window.list -> w
    begin
        w s@ ListNode.next -> w
        w Window.list <>
    while
        w Window.dump
    repeat
;
: WindowList.find-by-type { t -- , find first window by type }
    0 found-window !
    ['] window-find-by-type WindowList.iterate
    found-window @
;

: (WindowList.find) { caddr u -- win , find window with specified name }
    caddr find-name !
    u find-name-length !
    0 found-window !
    window-list ['] window-find List.iterate
    found-window @
;
' (WindowList.find) is WindowList.find

: (WindowList.current) ( -- w , get active window )
    WindowList.active-window
;
' (WindowList.current) is WindowList.current

: (WindowList.current?) { w -- f , is w current window? }
    w WindowList.current =
;
' (WindowList.current?) is WindowList.current?

: (WindowList.find-or-create) { w caddr u | b -- , find window or create it }
    \ create means to try to load from disk and if that fails, make an empty buffer

    \ already in a window?
    caddr u WindowList.find ?dup if
        \ TODO activate the window
        die{ ." TODO activate the window " .s }die
        exit
    then

    caddr u BufferList.find-or-create -> b
    w b Window.set-buffer
    w
;

' (WindowList.find-or-create) is WindowList.find-or-create

privatize