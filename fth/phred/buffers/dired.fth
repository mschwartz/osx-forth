\
\ PHRed editor
\
\ programmed by Mike Schwartz
\
\ Buffer editor mode for Window
\

private{

List file-List file-list List.init
variable dired-parent 0 dired-parent !

: dump-one { n -- , dump one node }
    n s@ DirNode.name zcount type cr
;
    : filter { n -- , filter node }
        n s@ Line.text s" .bak" index-of -1 <> if
            n List.remove
            n Line.destroy
        then
    ;
}private

: (Dired.read-dirlist)
    dired-parent @ ?dup if mem::free  then
    file-list DirList.destroy-list
    current-directory zcount pad place 
    file-list pad count true dirList.read drop
;
' (Dired.read-dirlist) is Dired.read-dirlist

: Dired.dump ( -- , dump file-list )
    cr ." Dired file-list " cr
    file-list ['] dump-one List.iterate
;

: Dired.add-dirent { buf dnode | typ mode -- , maybe format and add dnode entry to buf }
    dnode s@ DirNode.type -> typ
    typ DT_DIR = if
        dnode s@ DirNode.name c@ ascii . = if exit then
    then

    dnode s@ DirNode.statbuf s@ StatBuf.st_mode -> mode

    TAG_END
    dnode TAG_DATA
    dnode s@ DirNode.name zcount TAG_STRING
    s"    " TAG_STRING
    dnode s@ DirNode.statbuf s@ StatBuf.st_size 8 TAG_NUMBER_JUSTIFIED
    s"    " TAG_STRING
    mode Path.mode  TAG_STRING
    s"    " TAG_STRING

    typ case
        DT_DIR of
\                 S"   DIR " TAG_STRING
            ICON.FOLDER TAG_ICON
            OPCODE.ICON TAG_BYTE
        endof
        DT_LNK of
\                 S"   LINK " TAG_STRING
            ICON.LINK TAG_ICON
            OPCODE.ICON TAG_BYTE
        endof
        DT_REG of
\                 s"   REG " TAG_STRING
            ICON.FILE TAG_ICON
            OPCODE.ICON TAG_BYTE
        endof
    endcase
    s"  " TAG_STRING
    buf Buffer.format 
;

: (Dired.begin) { w | buf dnode typ frst dpth -- , initialize and attach buffer editor }
    Dired.read-dirlist
    
    s" Directory Editor" BufferList.find ?dup if
        -> buf
        buf Buffer.clear
    else
        s" Directory Editor" Buffer.new -> buf
        buf BufferList.add
    then
    true buf s! Buffer.read-only
    Buffer.filetype.dir buf s! Buffer.filetype

    TAG_END
        s" Press ? for help" TAG_STRING
        0 TAG_DATA
    buf Buffer.format
    s" " pad place-cstr pad buf Buffer.append-text drop

    current-directory z" /" 1+ string::strcmp 0<> if
        sizeof() DirNode MEM::malloc dired-parent !
        current-directory zcount pad place-cstr 
        pad z" /.." 1+ string::strcat drop
        pad Path.realpathz string::strdup dired-parent @ s! DirNode.fullpath
        DT_DIR dired-parent @ s! DirNode.type

        TAG_END
            s" .. (up a dir)" TAG_STRING
            dired-parent @ TAG_DATA
        buf Buffer.format
    then

    TAG_END
        current-directory zcount TAG_STRING
    buf Buffer.format

\     s" " pad place-cstr pad buf Buffer.append-text drop

    depth -> dpth
    file-list -> frst
    frst -> dnode       \ directory node
    begin
        dnode s@ ListNode.next -> dnode
        dnode frst <>
    while
        dnode s@ DirNode.name 
        s" .bak" index-of -1 = if
            buf dnode Dired.add-dirent
        then
    repeat
    w buf Window.set-buffer
    w Window.line-down
    w Window.line-down
    w Window.line-down
;
' (Dired.begin) is Dired.begin

privatize
