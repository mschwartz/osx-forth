anew sys/regex.fth

trace-include @ [if]
cr ." Compiling sys/regex.fth "
[then]

:STRUCT regex_t
    INT regex_t.re_magic        \ private
    LONG regex_t.re_nsub        \ number of parentesized sub expressions
    APTR regex_t.re_endp        \ end pointer for REG_PEND
    APTR regex_t.re_guts        \ private
;STRUCT

:STRUCT regmatch_t
    LONG regmatch_t.rm_so       \ start of match
    LONG regmatch_t.rm_eo       \ end of match
;STRUCT

: regmatch_t.dump { rx -- , dump regex_t }
    cr ." regmatch_t at $" rx .hex
    cr ."    regmatch_t.rm_so " rx s@ regmatch_t.rm_so .
    cr ."    regmatch_t.rm_eo " rx s@ regmatch_t.rm_eo .
    cr
;

\ regerror flags
-1 constant REG_ENOSYS          \ reserved
 1 constant REG_NOMATCH         \ regex() function failed to match
 2 constant REG_BADPAT          \ invalid regular expression
 3 constant REG_ECOLLATE        \ invalid collating element
 4 constant REG_ECTYPE          \ invalid character class
 5 constant REG_EESCAPE         \ trailing backslash (\)
 6 constant REG_ESUBREG         \ invalid backreference number
 7 constant REG_EBRACK          \ brackets ([ ]) not balanced
 8 constant REG_EPAREN          \ parenthesis not balanced
 9 constant REG_BRACE           \ braces not balanced
10 constant REG_BADBR           \ invalid repetition count(s)
11 constant REG_ERANGE          \ invalid character range
12 constant REG_ESPACE          \ out of memory
13 constant REG_BADRPT          \ repeitition operator operand invalid
14 constant REG_EMPTY           \ unused
15 constant REG_ASSERT          \ unused
16 constant REG_INVARG          \ invalid argument to regex routine
17 constant REG_ILLSEQ          \ illegal byte sequence
255 constant REG_ATOI           \ convert name to number (!)
$ 100 constant REG_ITOA         \ convert number to name (!)

\ regexec() flags
$ 0001 constant REG_NOTBOL      \ first character not at beginning of line
$ 0002 constant REG_NOTEOL      \ last character not at end of line
$ 0004 constant REG_STARTEND    \ string start/end in pmatch[0]
$ 0100 constant REG_TRACE       \ unused
$ 0200 constant REG_LARGE       \ unused
$ 0400 constant REG_BACKR       \ force use of backref code
REG_BACKR constant REG_BACKTRACKING_MATCHER

\ regcomp() flags
$ 0000 constant REG_BASIC       \ basic regular expression (synomym for 0)
$ 0001 constant REG_EXTENDED    \ extended regular expressions
$ 0002 constant REG_ICASE       \ compile ignoring upper/lower case
$ 0004 constant REG_NOSUB       \ compile only reporting success/failure
$ 0008 constant REG_NEWLINE     \ compile for newline sensitive matching
$ 0010 constant REG_NOSPEC      \ compile turning off all special characters
REG_NOSPEC constant REG_LITERAL
$ 0020 constant REG_PEND        \ use re_endp as end pointer
$ 0040 constant REG_MINIMAL     \ compile using minimal repetition
REG_MINIMAL constant REG_UNGREEDY
$ 0080 constant REG_DUMP        \ unused
$ 0100 constant REG_ENHANCED    \ additional (non-posix) features

: alloc_regex_t ( -- regex_t , allocate a regex_t struct )
    regex::alloc_regex_t
;
: free_regex_t ( regex_t -- , free allocated regex_t )
    regex::free_regex_t
;
: alloc_regmatch_t ( max-size -- regmatch_t , allocate a regmatch_t with max size )
    regex::alloc_regmatch_t
;
: free_regmatch_t ( regmatch_t -- , free an allocated regmatch_t )
    regex::free_regmatch_t
;
: regcomp { regex pat flgs  -- res  , compile a regular expression string }
    regex pat flgs regex::regcomp
;

: regexec { regex zstr regmatch nmatch flags -- err , execute regex against zstr }
    regex zstr regmatch nmatch flags regex::regexec
;

: regerror ( code regex_t -- caddr u , get error message for regular expression )
    regex::regerror 
    zcount
;

\ C++ regex methods
variable regex_options 
variable regex_flags 

: regex.options ( options -- , set regex_option to be used by match and replace )
    regex_options !
;
: regex.flags ( match_type -- , set regex_flags to be used by match and replace )
    regex_flags !
;
: regex.default ( -- , set regex_option and regex_flags to default values )
    regex::option_extended regex_options !
    regex::match_default regex::format_first_only or regex_flags !
;
regex.default

: regex.ignore-case regex_options @ regex::option_icase or regex_options ! ;
: regex.ignore-case? regex_options @ regex::option_icase and ;
: regex.global regex::match_default regex_flags ! ;
: regex.global? regex_flags @ regex::match_default = ;

: regex.new { pattern -- re, new regex }
    pattern regex_options @ regex::regex_new 
;

: regex.delete { re -- re, delete regex }
    re regex::regex_delete 
;

: regex.escape { zstring | s ch len src dst -- , escape pattern zstring in place }
    zstring string::strlen -> len
    len if 
        zstring string::strlen 4 * mem::malloc -> s
        zstring -> src
        s -> dst
        len 0 ?do
            src c@ -> ch src 1+ -> src
            ch case 
                ascii \ of 
                    ascii \ dst c! dst 1+ -> dst
                    ascii \ dst c! dst 1+ -> dst
                endof
                ascii ? of 
                    ascii \ dst c! dst 1+ -> dst
                    ascii ? dst c! dst 1+ -> dst
                endof
                ascii * of 
                    ascii \ dst c! dst 1+ -> dst
                    ascii * dst c! dst 1+ -> dst
                endof
                ascii . of 
                    ascii \ dst c! dst 1+ -> dst
                    ascii . dst c! dst 1+ -> dst
                endof
                ascii ( of 
                    ascii \ dst c! dst 1+ -> dst
                    ascii ( dst c! dst 1+ -> dst
                endof
                \ default
                    ch dst c! dst 1+ -> dst
            endcase
        loop
        0 dst c!
        zstring s string::strcpy drop
        s mem::free
    then
;
\
\ NOTE:
\
\ regex_search will successfully match any sub-sequence of the given sequence, 
\ whereas std::regex_match will only return true if the regular expression 
\ matches the entire sequence.
\

: regex.match_re { haystack re -- flg , match regular expression against haystack returns -1 if no match }
    haystack re regex_flags @ regex::regex_match
;

: regex.search_re  { haystack re -- ndx , search pattern against haystack returns -1 if no match }
    haystack re regex_flags @ regex::regex_search
;

: regex.match { haystack pattern | re -- ndx , match pattern against haystack }
    pattern regex.new -> re
    re if 
        haystack re regex_flags @ regex::regex_match
        dup -1 <> if die{ haystack 20 dump pattern 20 dump .s }die then
        re regex.delete
    then
;

: regex.search { haystack pattern | re -- index , search pattern against haystack }
    pattern regex.new -> re
    re if 
        haystack re regex_flags @ regex::regex_search
        re regex.delete
    then
;

: regex.replace_re { haystack re replacement -- new-string , replace pattern/re with replacement }
    haystack re replacement regex_flags @ regex::regex_replace 
;

: regex.replace { haystack pattern replacement flg | re -- new-string , replace pattern with  }
    pattern regex.new -> re
    re if
        haystack re replacement regex.replace_re
        re regex.delete
    then
    flg if haystack mem::free then
;

trace-include @ [if]
cr ." End of sys/regex.fth " cr
[then]