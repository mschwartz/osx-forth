anew task-c/path.fth

trace-include @ [if]
cr ." Compiling sys/path.fth "
[then]

4096 constant MAX-PATH-LENGTH

private{
create path-pad 512 allot
: mode-bit { ch mode bit -- , print ch if bit is set or ascii - if not }
    mode bit and if
        path-pad ch $append.char
    else
        path-pad ascii - $append.char
    then
;
}private

\ This is the current directory, which may be changed by the program
create current-directory MAX-PATH-LENGTH allot
variable current-directory-length
current-directory MAX-PATH-LENGTH sys::getcwd drop
current-directory string::strlen current-directory-length !

\ Initial directory - this should not be changed by the program
create initial-directory MAX-PATH-LENGTH allot
variable initial-directory-length
initial-directory MAX-PATH-LENGTH sys::getcwd drop
initial-directory string::strlen initial-directory-length !

: Path.startup ( -- zaddr , get startup path as c-string )
    initial-directory 
;

: Path.current ( -- zaddr , get current path as c-string )
    current-directory MAX-PATH-LENGTH sys::getcwd drop
    current-directory string::strlen current-directory-length !
    current-directory
;

: Path.relative { zpath -- zaddr , return path relative to Path.current }
    zpath current-directory current-directory-length @ 
    string::strncmp
    0<> if
        zpath
    else
        zpath current-directory-length @ + 1+
    then
;

: Path.basename ( zaddr1 -- zaddr2 , extract the base portion of a pathname at caddr1, returning base at caddr2 )
    sys::basename
;

: Path.dirname ( caddr1 -- caddr2 , extract the directory part of a pathname at caddr1, returning it at caddr2 )
    sys::dirname
;

: Path.extension { caddr1 | str -- caddr2 , get file extension }
    caddr1 ascii . string::strrchr -> str
    str 0<> if
        str 1+ -> str
        str string::strdup exit
    then
    z" " 1+  string::strdup
;

: Path.getcwd ( buf size -- buf , get current directory, limited to size )
    sys::getcwd
;

: Path.chdir ( path -- , set current directory )
    sys::chdir
;

: Path.chroot ( path -- , change root directory )
    sys::chroot
;

: Path.realpath ( $path -- resolved , resolve path to canonicalized absolute path )
    sys::realpath
    \ caller should $duplicate the string, it's a temporary buffer
;
: Path.realpathz ( zpath -- resolved , resolve path to canonicalized absolute path )
    sys::realpathz
    \ caller should $duplicate the string, it's a temporary buffer
;

: Path.file-exists? { zpath -- flg , does file exist? }
    zpath Path.realpathz sys::fileexists
;

: Path.copy-file { newname oldname | newfd caddr u -- flg , rename file }
    oldname File.contents -> u -> caddr
    newname caddr u File.write-contents
    caddr mem::free
    u <> not
;

: Path.rename-file { newname oldname -- success , rename file }
    oldname newname sys::rename
;

: Path.add-slash? { $path -- , add / to end of $path if it doesn't end with a slash }
    $path count 1- + c@ ascii / = if exit then
    $path c@ 1+ $path c!
    ascii /  $path count 1- + c!
;

: dev_t UINT32 ;
: ino_t UINT64 ;
: mode_t UINT16 ;
: nlink_t UINT16 ;
: uid_t UINT32 ;
: gid_t UINT32 ;
: off_t UINT64 ;
: time_t UINT64 ;
: blkcnt_t UINT64 ;
: blksize_t UINT32 ;

:STRUCT TIMESPEC 
    LONG tv_sec
    LONG tv_nsec
;STRUCT

:STRUCT statbuf
    dev_t statbuf.st_dev                    \ device inode resides on
    mode_t statbuf.st_mode                  \ inode permissions
    ino_t statbuf.st_ino                    \ inode's number
    nlink_t statbuf.st_nlink                \ number of hard links to the file
    uid_t statbuf.st_uid                    \ user id of owner
    gid_t statbuf.st_gid                    \ group id of owner
    dev_t statbuf.st_rdev                   \ device type for special file inode
    struct timespec statbuf.st_atimespec
    struct timespec statbuf.st_mtimespec
    struct timespec statbuf.st_ctimespec
    struct timespec statbuf.st_birthtimespec
    off_t statbuf.st_size                   \ size of file in bytes
    blkcnt_t statbuf.st_blocks                \ blocks allocated for file
    blksize_t statbuf.st_blksize                \ optimal file sys IO/ops block size
    UINT32 statbuf.st_flags                  \ user defined flags for file
    UINT32 statbuf.st_gen                    \ file generation number
    UINT32 statbuf.st_lspare
    UINT64 statbuf.st_qspare1
    UINT64 statbuf.st_qspare2
;STRUCT

: Path.stat { pth buf -- res , get file information (statbuf) }
    sys::stat
;

: Path.statz { zpth buf -- res , get file information (statbuf) }
    zpth buf sys::statz
;

octal
0170000 constant S_IFMT
0010000 constant S_IFIO
0020000 constant S_IFCHR
0040000 constant S_IFDIR
0060000 constant S_IFBLK
0100000 constant S_IFREG
0120000 constant S_IFLNK
0140000 constant S_IFSOCK
0160000 constant S_IFWHT
0004000 constant S_ISUID
0002000 constant S_ISGID
0001000 constant S_ISVTX
0000400 constant S_IRUSR
0000200 constant S_IWUSR
0000100 constant S_IXUSR
0000040 constant S_IRGRP
0000020 constant S_IWGRP
0000010 constant S_IXGRP
0000004 constant S_IROTH
0000002 constant S_IWOTH
0000001 constant S_IXOTH
decimal


: S_ISFIO ( m -- f ) S_IFMT AND S_IFIO = ;
: S_ISCHR ( m -- f ) S_IFMT AND S_IFCHR = ;
: S_ISDIR ( m -- f ) S_IFMT AND S_IFDIR = ;
: S_ISBLK ( m -- f ) S_IFMT AND S_IFBLK = ;
: S_ISREG ( m -- f ) S_IFMT AND S_IFREG = ;
: S_ISLNK ( m -- f ) S_IFMT AND S_IFLNK = ;
: S_ISSOCK ( m -- f ) S_IFMT AND S_IFSOCK = ;
: S_ISWHT ( m -- f ) S_IFMT AND S_IFWHT = ;

: ST.type { mode -- caddr u  , printable }
    mode S_ISFIO if s" FIFO" exit then
    mode S_ISCHR if s" CHR " exit then
    mode S_ISDIR if s" DIR " exit then
    mode S_ISBLK if s" BLK " exit then
    mode S_ISREG if s" REG " exit then
    mode S_ISLNK if s" LNK " exit then
    mode S_ISSOCK if s" SOCK" exit then
    mode S_ISWHT if s" WHT " exit then
;

:STRUCT DirEnt
    UINT64      d_ino
    UINT16      d_reclen
    UINT8       d_type
    UINT8       d_namelen
    UINT64      d_pad
    1024 bytes   d_namex \ use d_name constant below
;STRUCT
\ d_name is at offset 21, but pforth structs won't let us align on the byte boundary
21 constant d_name

 0 constant DT_UNKNOWN
 1 constant DT_FIFO
 2 constant DT_CHR
 4 constant DT_DIR
 6 constant DT_BLK
 8 constant DT_REG
10 constant DT_LNK
12 constant DT_SOCK
14 constant DT_WHT

: DirEnt.type ( type -- caddr u , returns  type )
    case 
        DT_UNKNOWN  of s" UNKNOWN" endof
        DT_FIFO     of s" FIFO" endof
        DT_CHR      of s" CHR" endof
        DT_DIR      of s" DIR" endof
        DT_BLK      of s" BLK" endof
        DT_REG      of s" REG" endof
        DT_LNK      of s" LNK" endof
        DT_SOCK     of s" SOCK" endof
        DT_WHT      of s" WHT" endof
        s" unknown type" 
    endcase
;

: st_mode>de_type { mode -- de_type }
    mode S_ISFIO if DT_FIFO exit then
    mode S_ISCHR if  DT_CHR exit then
    mode S_ISDIR if DT_DIR exit then
    mode S_ISBLK if DT_BLK exit then
    mode S_ISREG if DT_REG exit then
    mode S_ISLNK if DT_LNK exit then
    mode S_ISSOCK if DT_SOCK then
    mode S_ISWHT if DT_WHT exit then
    0
;

: Path.opendir { $path -- DIR , open directory }
    $path sys::opendir
;

: Path.opendirz { zpath -- DIR , open directory }
    zpath sys::opendirz
;

: Path.rewinddir { DIR -- , rewind directory }
    DIR sys::rewinddir
;

: Path.readdir { DIR -- dirent , read next directory entry }
    DIR sys::readdir
;

: Path.closedir { DIR -- f , close directory }
    DIR sys::closedir
;

: Path.join { zpath zpart -- , add zpart to zpath adding / if needed, assumes zpath is big enough to hold result }
    zpath zcount + 1- c@ 
    ascii / <> if
        zpath z" /" 1+ string::strcat drop
    then
    zpath zpart string::strcat drop
;

: Path.mode { mode -- caddr u , convert mode_t to rwxrwx... style string }
    0 path-pad c!

    mode S_IFMT AND S_IFDIR = if 
        path-pad ascii d $append.char
    else
        path-pad ascii - $append.char
    then
\     ascii d mode S_IFDIR mode-bit
    ascii r mode S_IRUSR mode-bit
    ascii w mode S_IWUSR mode-bit
    ascii x mode S_IXUSR mode-bit
    ascii r mode S_IRGRP mode-bit
    ascii w mode S_IWGRP mode-bit
    ascii x mode S_IXGRP mode-bit
    ascii r mode S_IROTH mode-bit
    ascii w mode S_IWOTH mode-bit
    ascii x mode S_IXOTH mode-bit
    path-pad count
;

privatize

trace-include @ [if]
cr ." End of sdtio/path.fth"
[then]
