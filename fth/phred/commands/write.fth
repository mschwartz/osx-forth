\
\ phred editor
\
\ programmed by Mike Schwartz
\
\ Command Line handling
\   :write command
\

anew task-phred/commands/write.fth

private{
: dowrite { b str | toku tok -- flg , write file }
    str CString.parse-token -> toku -> tok
    toku 0= if
        CommandLine-start @ CommandLine-end @ <> if
            messageline{ ."  *** Use ! to write partial buffer " }messageline
            true exit
        then
        b Buffer.save if
            messageline{
                ." >>> Saved "
                ascii " emit
                b s@ Buffer.name zcount type
                ascii " emit
                ascii . emit
            }messageline
        else
            errorline{
                ." Can't save Buffer " 
                ascii " emit
                b s@ Buffer.name zcount type
                ascii " emit
                }errorline
            ncurses.bell
        then
        true exit
    then
    tok toku pad place-cstr
    CommandLine-start @ CommandLine-end @ <> if
        pad sys::fileexists if
            errorline{ ."  *** File exists - add ! to write partial buffer " }errorline
            true exit
        then
        pad zcount b CommandLine-start @ CommandLine-end @ Buffer.from -> b
        b pad Buffer.write drop
        b Buffer.destroy
        true exit
    then
    b pad Buffer.write drop
    true exit
;
}private

: CommandLine.write { w str tok toku | b -- flg , return true if executed }
    w s@ Window.buffer -> b
    w Window.read-only? if
        errorline{ ."  *** Buffer is read only " }errorline
        true exit
    then

    tok toku s" sav" compare 0= if
        b str dowrite
        exit
    then
    tok toku s" save" compare 0= if
        b str dowrite
        exit
    then
    tok toku s" saveas" compare 0= if
        b str dowrite
        exit
    then
    tok toku s" w" compare 0= if
        b str dowrite
        exit
    then
    tok toku s" write" compare 0= if
        b str dowrite
        exit
    then

    tok toku s" w!" compare 0= if
        str CString.parse-token -> toku -> tok
        toku 0= if
            b Buffer.save if
                messageline{
                    ." >>> Saved "
                    ascii " emit
                    b s@ Buffer.name zcount type
                    ascii " emit
                    ascii . emit
                }messageline
            else
                errorline{ 
                    ." Can't save Buffer " 
                    ascii " emit
                    b s@ Buffer.name zcount type
                    ascii " emit
                    }errorline
                ncurses.bell
            then
            true exit
        then
        tok toku pad place-cstr
        CommandLine-start @ CommandLine-end @ <> if
            pad zcount b CommandLine-start @ CommandLine-end @ Buffer.from -> b
            b pad Buffer.write
            b Buffer.destroy
            true exit
        then
        b pad Buffer.write
        true exit
    then

    false
;

privatize
