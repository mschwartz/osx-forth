#!/usr/local/bin/pforth_standalone -q

\
\ demonstrate the use of semaphores
\
\ programmed by Mike Schwartz
\

require sys/semaphore.fth

variable main-pid
: print{
    true TIO.BOLD-MODE
    sys::getpid main-pid @ = if
        0 255 0 TIO.FG-RGB
        .pid time::time . ."  MAIN "
    else
        255 0 0 TIO.FG-RGB
        .pid time::time . ."  CHILD "
    then
;
: }print
    false TIO.BOLD-MODE
    tio.reset-modes
;

variable child-sem 0 child-sem !
: child-sem-name s" /tmp/child-sem" ;

: child ( -- , child process )
    cr cr 
print{
    ." BEGIN CHILD PROCESS "  cr
}print
cr
print{
    ." child obtain semaphore " child-sem @  .hex ." ... "
    child-sem @  Semaphore.obtain  0< if
        cr ." err '" sys::strerror ztype ." '"
    then
    ." obtained."  cr
}print
print{
    ." sleeping 5 seconds " cr
}print
cr 
    5 sys::sleep drop
print{
    ." child release semaphore " child-sem @  .hex
}print

    child-sem @  Semaphore.release
    print{
        cr .pid ." child exiting..." 
    }print
    0 sys::exit
;

: demo { | child-pid done -- }
    sys::getpid main-pid !
    cr cr 
    print{
    .pid ." Semaphore Demo"
    cr cr
    }print
    child-sem-name Semaphore.unlink 0< if
        c" sem_unlink " sys::perror
        bye
    then
    print{
        ." create child semaphore ... "
        child-sem-name O_CREAT O_EXCL or Semaphore.new child-sem !
        child-sem @ SEM_FAILED = if
            drop
            cr ." *** demo SEM_FAILED " sys::strerror ztype
            cr ." child-sem " child-sem .hex child-sem @ .hex
            bye
        then
        ." created " child-sem @  .hex cr 
    }print

    sys::fork -> child-pid 
    child-pid 0= if
        \ child
        ['] child catch
            cr .pid ." child caught " .
        0 sys::exit
    else
        drop
        print{
            ." forked child process pid " child-pid . ." depth= " depth .
        }print
        false -> done
        begin
            done false =
        while
            1 sys::sleep drop
            print{
                ." obtaining semaphore... "  
                child-sem @ semaphore.obtained? 
                0= if
                    cr cr cr .pid time::time . ." child released semaphore" cr
                    true -> done
                else
                    ." busy " cr
                then
            }print
            key? if 
                key case 
                    3  of 
                        true -> done
                    endof
                endcase
            then
        repeat
    then
    sys::wait
    child-sem-name Semaphore.unlink drop
    ." parent exit "
    0 sys::exit
;


0sp
demo
\ ' demo catch
    cr ." top-level caught " . 10 msec
    cr 0 sys::exit