\
\ PHRed editor
\
\ programmed by Mike Schwartz
\
\ Buffer editor mode for Window
\

: (Bufed.refresh) { buf | bnod b d frst max-len padding -- , initialize and attach buffer editor }
    0 -> max-len
    BufferList.list -> frst
    frst -> bnod
    begin
        bnod s@ ListNode.next -> bnod
        bnod frst <>
    while
        bnod s@ Buffer.name string::strlen -> padding 
        padding max-len > if
            padding -> max-len
        then
    repeat
    max-len 2+ -> max-len

    BufferList.list -> frst
    frst -> bnod
    s" Buffer List" pad place-cstr pad buf Buffer.append-text drop
    s" " pad place-cstr pad buf Buffer.append-text drop
    begin
        bnod s@ ListNode.next -> bnod
        bnod frst <>
    while
        bnod -> b
        bnod s@ Buffer.hidden not if
            TAG_END
            bnod s@ Buffer.fullpath ?dup if
                Path.dirname
                zcount 
                TAG_STRING
            then
            bnod s@ Buffer.mtime Date.new-from-timestamp -> d
            2 TAG_SPACES
            d Date.hh:mm:ss TAG_STRING

            bnod s@ Buffer.name string::strlen -> padding 
            max-len padding - TAG_SPACES
            bnod s@ Buffer.name zcount TAG_STRING
            bnod Buffer.modified? if
                s" +" TAG_STRING
            else
                s"  " TAG_STRING
            then
            b TAG_NUMBERX

            b TAG_DATA
            buf Buffer.format
        then
    repeat
;
' (Bufed.refresh) is Bufed.refresh

: (Bufed.begin) { w | buf bnod d frst max-len padding -- , initialize and attach buffer editor }
    s" Buffer Editor" BufferList.find ?dup if
        -> buf
        buf Buffer.clear
    else
        s" Buffer Editor" Buffer.new -> buf
        buf BufferList.add
    then
    true buf s! Buffer.read-only
    true buf s! Buffer.permanent
    true buf s! Buffer.hidden
    buf Bufed.refresh
    w buf Window.set-buffer
    WindowType.bufed w s! Window.type
    Buffer.filetype.bufed buf s! Buffer.filetype
    w Window.line-down
    w Window.line-down
    
;

' (Bufed.begin) is Bufed.begin